Changelog
=========

v4.4.2
------

Bug Fixes
- Navigating to `Exam View` after opening TabSINT will now correctly re-load the most recent protocol.
- Fixed display of inputs in multipleInputResponseArea.

Enhancements
- Added a `never` option for hideExamProperties.
- Google Play Store and WRNMMC version of TabSINT have different App ID's and can be both installed simultaneously.

v4.4.1
------

Bug Fixes
- Include missing Media plugins.

v4.4.0
------

Features
- Added TabSINT UUID per device, shareable among co-resident TabSINT installs.

Enhancements
- Manual audiometry moved from a TabSINT evaluation into the CHA firmware.
- Moved to custom internal Android filesystem plugin to facilitate file management.
- Cleanup and rework of app local file storage management.
- Moved protocol validation to native JAVA implementation.
- Documentation of exam skip functionality.
- Added more test coverage and tests.
- Deprecated exams: chaAudiometryList, chaCRM, ChaSoundRecognition, chaTAT, and freeformResponseArea

Bug Fixes

- Fix likert labels for even-level response areas.
- Fix protocol internal copying to include recursive directories properly.
- Fix manual audiometry not recording RETSPL.
- Fix QR code generation to save `shutdownTimer` as a number instead of a string.
- Fix submit delay and button ui appearance.
- Fix date result in audiometry-input response area.
- Fix daily calibration check empty in PDF.


v4.3.0
------

Features

- Update to Android 11.
- Implement configurable WAHTS shutdown time. Option is available in the Configuration pane of the `Admin View`.
- Add GAP Training GUI.

Enhancements

- Update built-in developer protocols for software and WAHTS testing.
- Refactor DPOAE exam.
- Add option to review multiple-input forms without the input text boxes before submitting. Option is defined in the protocol.
- Add checkbox to `multipleInputResponseArea`.
- Add translations.
- Deprecate chaThreeDigit `maxSNR` and `maxLevel` parameters.
- Alert users that `chaTAT`, `audiometry-list`, `chaCRM`, `chaSoundRecognition` and `freeformResponseArea` will be fully deprecated in 4.4.0.

Bug Fixes

- Set volume correctly.
- Fix protocol highlighting of the loaded protocol.
- Fix collapsed welcome screen on OS 6.0.1.
- Fix `hideExamProperties` so that response areas correctly display the properties based on the protocol.
- Update `chaHINT` schema to match specification.
- Fix `chaMLD` button appearance issue.
- Small UI fixes.


v4.2.2
------

Bug Fixes

- Fix likert labels.
- Fix WAHTS Calibration Check exam results page scrolling.

v4.2.1
------

Bug Fixes

- Add CSV export support for the `chaBekesyMLD` audiometry exam.

v4.2.0
------

Features

- Add `exportToCsv` to `chaCalibrationCheck`, `likert`, and `chaThreeDigit`.
- Support Android 11 file permissions.
- View and export `chaCalibrationCheck` to PDF via the results view.

Enhancements

- Improve and standardize UI styling across the entire app.
- Dynamically size UI to better support web-TabSINT and various tablet screen sizes.

Bug Fixes

- Fix an issue with `verticalSpacing`.

v4.1.1
------

Features

- Update Bekesy MLD schema to support cutoff frequencies.
- Update tone generation schema to support binaural exams.
- Support binaural results in audiometry tables.
- Dichotic Digits response area.
- Frequency Pattern response area.
- Accelerated threshold exam type.

Bug Fixes

- Fix the recording of ResponseTime in the WAHTS firmware.
- Resolve issue with PlaySound exam when user attempts to proceed before audio
  is done playing.
- Allow media to play when protocol and calibration headset fields do not match.

v4.1.0
------

Features

- Add Manual Screener response area.
- Update audiometry table displays.
- Audiometry plot (audiogram) updates to support masking and meet ASHA specification.
- Update firmware to `2021-06-03_walrus_4`:
- Support exporting results to CSV.
- Gain service support for Tab E calibration from TabSINT server.
- Add automated masking exam on the WAHTS.
- Support binaural tone generation on the WAHTS.

Improvements

- Include gif demonstration in calibration check.
- Use date input in audiometry list.
- Avoid clipping with Nth octave band noise on the WAHTS.
- During the Bekesy exam, presentations may go below the minimum level by up to 6 dB to capture thresholds right at the minimum level.
- Pediatric Latin American Spanish added to the HINT exam.
- Hughson-Westlake common logic updated, so that if the first two valid peaks match the exam will end, instead of waiting for a third peak.
- Updated GUI styling for better accessibility and code styling.
- Upgrade to the WAV playback module on the WAHTS, to properly play media with metadata located at the end of the file. Previously only media with metadata before the audio data was supported.

Bug Fixes

- Show equipment information in resultsViewResponseArea.
- Fix file access on Android 10 with legacy support flag.
- Reset tabletGain to 0 when WAHTS selected.
- Update BHAFT results to match specification from the WAHTS.
- Fix `buttonScheme` parameter
- Fix training mode in Gap exam.
- Fix `autoSubmit` in triple digit.
- Fix the `PlaySound` exam.

v4.0.1
------

Bug Fixes

- Prevent error when playing calibrated "as-recorded" media without TargetSPL specified.

v4.0.0
------

Features

- Added `resultsView` response area to display and/or save exam results in pdf format.
- Added beta support for TabSINT in a web browser.
- Added beta bekesy response area.

Improvements

- Added `tabsint-protocols` local directory for use with QR codes.
- Allowed user to toggle whether protocols must be validated when loading.
- Updated the daily calibration check plots and add a table.
- Improved release testing procedure, including new testing protocol.
- Added `volumeLevel` page property to set tablet volume.
- Updated Matlab analysis tool to work without subject id.
- Firmware:
  - Now generates noise in real-time
  - Can specify passband in Bekesy MLD
  - Updated automated masked threshold algorithm
  - Updated 3-digit exam to remove SNR limit
    - Also switched to use wideband scaling, like the playsound exam.

Bug Fixes

- Fixed `responseRequired` logic.
- Changed age to a number input in `audiometryInput` response area.

v3.9.3
------

Bug Fixes

- Handle tablet gain when using as-recorded media calibrated using Nexus 7 profile.

v3.9.2
------

Improvements

- Stored calibration check `Spectrum` data and updated the plot.

Bug Fixes

- Prevent an error when connecting to the WAHTS with streaming enabled.
- Prevent the selected default headset, language, and results mode from being overwritten on restart.

v3.9.1
------

Improvements

- Update documentation on tabsint.org:
  - Include QR code generation tutorial
  - Add documentation for the WAHTS calibration check

Bug Fixes

- Prevent brief static when switching test ears (fixed in Vicuna_4 firmware).
- Expand dynamicStartLevel to chaBekesyLike and apply bounds.
- Fix repeatIfFailedOnce logic for Bekesy tests.

v3.9.0
------

Features

- Update firmware to `2020-12-09_vicuna_3`:
  - TalkThrough exam no longer severs A2DP connection.

Improvements

- Move audio streaming to its own service and make more reliable and performant.
- Update gain service with new value for Tab A and use as default.
- Update PDF Response Area schema.

Bug Fixes

- Automated Screener now properly registers successful button presses.
- responseElapTimeMS now reports correct elapsed time between start of page and submission.

v3.8.1
------

Features

- Update firmware to `2020-10-05_vicuna_2`:
  - Revised BHAFT exam to meet the newly updated specification.

Improvements

- Hide the buttons to update TabSINT for Edare.

v3.8.0
------

Features

- Added a response area to export results to PDF.

Improvements

- MRT response area can now use `delayEnable` property.

Bug Fixes

- Handle error from dosimeter during MPANL page.

v3.7.1
------

Features

- Added new default audiometry protocol.

Improvements

- Updated styling for consistency.

Bug Fixes

- Automated Audiometry results table now populates.

v3.7.0
------

Features

- Support reading WAHTS calibration date to determine RetSPL value version.
- Allow protocol to ignore masking in manual audiometry.
- Protocols can specify sound level meter parameters to ignore in the results object.

Improvements

- Improved TabSINT unit test suite.
- Better cross-platform styling.
- Added USB-C event buffer for more robust headphone support.
- Improve Unity results analysis for CAT protocol

Bug Fixes

- No longer throw an alert to the user when the sound level meter plugin fails to stop
- Only record masking level if masking is presented in manual audiometry.

v3.6.0
------

Features

- Add button to reset TabSINT configuration

Improvements

- Address BekesyMLD feedback:
  - Remove `Ear` field from sub text
  - Added detail to software button text
  - Added detail to sub text for `repeatIfFailedOnce` use case.
- Use the StepSize property to increase/decrease level in manual audiometry exam.
- Updated CSS for consistent styling (!93)

Bug Fix

- Do not exit protocol and submit partial results when USB-C headset is disconnected if the protocol does not require the USB-C headset.

v3.5.1
------

Improvements

- Fixed Bekesy MLD level issue
- Allow for Bekesy MLD noise bandwidth to span multiple octave bands

v3.5.0
------

Features

- Add bluetooth hardware button for WAHTS responses (!89)
- Generate and export QR configuration code for current tablet configuration (!90)
- Implement the Bekesy MLD audiometry test on the WAHATS (!96)
- Do not record test location by default (location recording can be enabled in the Advanced Settings) (!97)

Improvements

- Submit partial results when USB-C headset is removed
- Attempt to upload HearTrack results every time a network connection is established
- Improve cross device compatibility of user interface (!93)
- Move developer guide into the TabSINT repository (!92)
- Add masking levels to results
- Add `feedback` to `checkboxResponseArea` (!99)

Bug Fixes

- Update log server to DNS `logs.tabsint.org`
- Fix results decryption
- Fix headset enumeration in MPANL response area

v3.4.0
------

Features

- Refactor the WHATS Manual Audiometry to support masking and improve the user interface (!83)
- Implement `freeformResponseArea` to allow user to input a drawing using the touchscreen (!79)
- Add user interface for the WAHTS daily calibration check (!84)
- Add CSV output to the TabSINT MATLAB results processor
- Implement a draft of the `chaMaskedThresold` response area which implements an automated algorithm for determining a masked audiometric threshold

Improvements

- Change default results handling to "Upload Only" and prevent export by default (#403)
- Improve streaming connection and reliability
- Disable audio streaming to the WAHTS by default
- Add `textSize` option for the likert response area (@hgalloza !82)
- Improve organization of the persistent disk storage (!73)
- Improve consistency among UI elements, colors, and text
- Remove permission warning popup if Android permissions are denied

Bug Fixes

- Query for WAHTS media version on connect instead of protocol start. This reduces the "File Streaming" error messages from the WHATS when a protocol is first started.
- Fix file reading bug in the TabSINT results processor (!81)
- Fix result handling in the checkboxResponseArea (!85)
- Fix schema issue with `VicFirthS2` headset (#425)
- Fix flag reset in debug-view (#415)

v3.3.3
------

Bug Fix

- Fix file "streaming" conflicts when beginning an exam.

v3.3.2
------

Improvements

- Make automated dev-build available through CI.

Bug Fixes

- Fix adding protocol from Gitlab repository.
- Fix exam page repeating logic.

v3.3.1
------

Improvements

- Update third party custom cordova plugin.

Bug Fixes

- Fix emoticons in likert response area.
- Update Computro response area schema.

v3.3.0
------

Features

- Select specific Svantek dosimeter from list of nearby devices for recording background noise.

Improvements

- Streaming audio files through the WAHTS is now more reliable.
- Svantek connection logic is more robust. Nearby Svantek devices must be powered on to show up in the list of available devices.
- Improved TabSINT logic when updating the WAHTS firmware.

Bug Fixes

- Three digit response area no longer fails when `changedFields` is not defined.
- Fixed 3D response area formatting.
- Exams only warn the user of a missing svantek connection once, on the first page that attempts to use it.
- Computro visual and audio rewards now match.

v3.2.2
------

Improvement

- Add `copyFilesOnLoad` field to protocol. This field defines a path on the tablet to copy protocol files when the protocol is loaded. Only protocol files in a special directory `filesToCopy` will be copied.

Bug Fixes

- Fix Computro randomization
- Revert `preProcessFunction` to merge changed fields instead of overwriting. Changed array fields will continue to overwrite.

v3.2.1
------

Bug Fixes

- Fix `preProcessFunction` handling to overwrite protocols instead of merging
- Remove *Active Tasks* window while adding a protocol from device storage to avoid blocking the directory selection box
- Rename gitlab configuration parameter `namespace` to `group`. This fix is backwards compatible to support existing configurations using `namespace`.

Known Bug

- This version is not compatible with the HearTrack protocol.

v3.2.0
------

Feature

- New TabSINT User Forum: https://forum.tabsint.org

Improvements

- Support `setVolume` command of the Sensimetrics SaveWAV TabSINT plugin
- Support LZMA compression of Configuration codes
- Remove *Save Partial Results* from HearTrack builds

Bug Fixes

v3.1.2
------

Bug Fixes

- Fix bugs in the the MPANL response area and background noise measurements using the Svantek 104A
- Parse the tablet location latitude and longitude into a string

v3.1.1
------

Bug Fixes

- Add lodash (_) and jquery ($) javascript libraries to global namespace for use in pre-process functions and custom response areas

v3.1.0
------

Features

- Integrate custom calibrated Computro radio recordings along with transcripts.

Improvements

- Remove limits on number of results that can be stored and retrieved from TabSINT.
- Improve Fetch and Computro response-area schemas to include fields increasing protocol customization.

Bug Fixes

- Populate the tabsintServer protocol configuration parameters from the configuration file.
- Fix image-map response-area.

v3.0.2
------

Bug Fixes

- Fix double-tap issue in `NatoResponseArea`
- Fix recursive application of fields in pre-processing functions

v3.0.1
------

Bug Fixes

- Fix adding protocols from `Device Storage`.

v3.0.0
------

Breaking Changes

- Remove [Crosswalk](https://github.com/crosswalk-project/cordova-plugin-crosswalk-webview) from build. This removes support Nexus 7 and simplifies compatibility with newer platforms.
- Compile for target Android SDK 28. This may break custom Cordova based plugins.

Features

- Enable auto-configuration via a QR code.
- Implement lock-down mode (no access to `Admin View`) for `HearTrack` build.
- Integrate Computro Unity game.
- Add admin option to enforce output encryption.
- Add MATLAB results analysis for Fetch and Computro games  (part of `TabsintResults.m` tool).
- Add support for calibrated output through Samsung Tab A .
- Add Gap Detection Test plot after the exam is done.
- Add date entry response field as part as the `multiple-input` response area.

Improvements

- Reduce `$scope` variables in controllers to improve latency in larger tests.
- Remove bower package manager in favor of npm.
- Refactor the codebase to use ES6 module imports and [parcel](https://parceljs.org/) application bundler.
- Enable link to custom releases URL.
- Update `jshint` code linter to `eshint`.
- Auto-format codebase using [prettier](https://github.com/prettier/prettier).

Bug Fixes

- Restore Three Digit exam auto-begin functionality.
- Cleanup Svantek operation and results display.

---

v2.9.3
------

Bug Fixes

- Fix double tap in Nato Demo Response Area

v2.9.2
------

Improvements

- Update DPOAE frequencies and add DPOAE exam properties for a Special Olympics event.

v2.9.1
------

Features

- Integrate Computro Unity game and add its schema.
  - Demo in `feature-demo` > `Advanced features`

Improvements

- Refine `Sync Headset Media` feature:
  - Always record chaMediaVersion and chaProtectedMediaVersion in results if WAHTS is connected.
  - If `MEDIAVER.TXT` or `PROTECTE.TXT` files are missing on the WAHTS, return an empty media version instead of raising a `WAHTS File System` error.
  - Notify user via a pop-up window when headset sync is done.
- Add [MATLAB tool example and instructions](https://gitlab.com/creare-com/tabsint/tree/master/tools/matlab) to format and write wave files to WAHTS.

v2.9.0
------

Features

- Implement the Gap Detection Test.
- Integrate USBC (i.e. Essential Earbuds) headsets into TabSINT.
- Add EssentialHD Calibration to TabSINT Server.

Improvements

- Add Fetch Response Area schema.

Bug Fixes

- Enable Android 7 to reconnect to the WAHTS after firmware update.
- Minor improvements to streaming functionality.
- Disable `no` button while the presentation is playing.
- Fix promise chain to download media from Gitlab and sync to headset.

v2.8.2
------

Bug Fixes

- Edit Threshold Validation exam schema and firmware to accept different thresholds for the left and right ear. Each object in the `Thresholds` array contains a `ThresholdLevel`, a `Frequency` and an `Ear`.
- Fix likeRT response area submission logic.
- Edit `ResponseTime` result length to match the number of presentations.
- Solve Svantek issues when scanning multiple Svantek dosimeters.
- Make `chaStream` an inheritable property.

v2.8.1
------

Bug Fixes

- Edit tabsint-wrnmmc release to be compatible with Nexus 7.
- Add `NPresentations` parameter to Threshold Validation schema.

v2.8.0
------

Features

- Update the WAHTS firmware `2019-03-07_sealion_3`.
  - Add Threshold Response Time exam.
  - Support for HINT Latin American Spanish.
  - Other miscellaneous improvements and bug fixes.
- Release on Google Play.
- Overhaul and improve tabsint.org.
- Add Threshold Response Time exam.
- Add machinery to process wavfiles in TabSINT and communicate calibrated levels with external applications.
- Cache volume before entering TabSINT and return to that volume upon exiting.
- Add custom plugin to identify USB devices.

Improvements

- Add Spanish translations for the HINT.
- Add Vic Firth S2 headset.
- Logs are disabled by default.

Known Bugs

- On 64-bit devices (like the Samsung Galaxy S3 and S4), the `Crosswalk Project 64bit` app must be downloaded from the Google Play app store for TabSINT to run.

v2.7.0
------

Features

- Encrypt TabSINT results using hybrid encryption
  - Implements asymmetric private/public RSA key pair authentication for high security
  - Implements the Advanced Encryption Standard (AES) for efficient symmetric encryption, which was adopted by U.S. NIST in 2001
  - If the protocol contains a `publicKey`, all results backup, saved on the tablet, and uploaded to Gitlab are encrypted
  - For more information on how to use TabSINT encryption, please visit [tabsint.org](http://tabsint.org/docs/UserGuide/analysis/results-encryption/)
  - [Matlab support](http://tabsint.org/docs/UserGuide/analysis/matlab-tool/) to generate RSA key pairs and decrypt results.
- Implement Maximum Permissable Ambient Noise Levels (MPANLs) response area
- Added schema parameters (`disable`, `textColor`, `backgroundColor`, and `fontSize`) to button-grid and multi-choice response areas.

Bug Fixes

- Fix checkbox response area submission logic.

v2.6.1
------

Bug Fixes

- Fix results not uploading to Gitlab
- Fix sound-recognition response area not completing correctly

v2.6.0
------

Features

- Implement `chaPlaySoundArray` response area to support the *FieldCHA*
- Encrypt all application storage by default in preparation for full result encryption support in v2.7.0
- Document tablet setup and quick start procedure on tabsint.org
- Implement `chaStream` page field to enable protocols to open a streaming connection to the WAHTS even when no wav files or videos are present on the page
- Include basic *Audiometry* protocol for all builds

Improvements

- Enable the option `UseMetaRMS` by default in the *SoundRecognition* task
- Remove *headset* checking from protocol loading process. In future versions of TabSINT, the *headset* selection dropdown will be removed in favor or selecting and identifying the headset via the protocol only.

Bug Fixes

- Fix Likert *AutoSubmit* (@hgalloza)

v2.5.2
------

Bug Fixes

- Make audiometry-input response area submittable.
- Update manual-audiometry results to fix results plot.
- Fix thorough protected-media check.

Improvements

- Update dpoae exam results reporting to use more up-to-date TabSINT standards.
- Update `InputChannel` schema  description for OAE Screener devices.

v2.5.1
------

Bug Fixes

- Update the WAHTS firmware `2018-09-14_rhino_4`. Fixes connection errors with boards other than the WAHTS (hot-probe, probe-screener).

v2.5.0
------

Features

- Add protocol field to override the default filename for exported results. See protocol field `resultFilename` in the root of the protocol schema.

Improvements

- Support PDF 417 format in QR response area.

Bug Fixes

- Fix page submission in image-map response area.
- Fix `RemoteChaError` that pops up after pressing test *Begin*.

Breaking Changes

- Rename the `TimePause` parameter in the WAHTS Masking Level Difference test (MLD) to `ResponseWindow`. Implement new `TimePause` parameter to prescribe the amount of time before the next presentation after a response.

v2.4.4
------

Bug Fixes

- Fix page submission in image-map response area.
- Fix `RemoteChaError` that pops up after pressing test *Begin*.

v2.4.3
------

Improvements

- Add functionality to prevent uploading or exporting results by mistake.
- Change the "Automatically output test results" checkbox so that it defaults to false.

v2.4.2
------

Features

- Add the ability to sync media from the tablet to the headset via a USB cable.
- Modify sound-detection task.
  - Style improvement: increase focus on the category buttons during the sound-recognition portion of the test.
  - Remove crowd tokens.
  - Hide seconds to press information from the subject.

v2.4.1
------

Bug Fixes

- Update volume calculation to account for the tablet gain in the 'as-measured' calibration method.
- Make the volume alert appear outside of admin mode if the volume played is not equal to the requested volume.

v2.4.0
------

Features

- Implement tablet gain parameter that allows the TabSINT Server calibration procedures for the Vic Firth and HDA200 headsets to apply to tablets other than Nexus 7.
  - This version of TabSINT natively supports calibrated output on the Samsung Tab-E tablets for media downloaded from the TabSINT Server.
- Implement CRM Response Area support for the WAHTS.
- Allow the user to hide active tasks dialog box. Hidden active tasks are indicated by a clipboard icon in the navbar. The user is able to show the active tasks by pressing on the clipboard icon. Active tasks are hidden by default in exam view, but may be shown by clicking on the clipboard icon.
- Include `loadhint()` method to MATLAB results processor.
- Add `labelFontSize` option to the Likert Response area to enable the protocol developer to specify the font size of the likert labels.

Improvements

- Consolidate audiometry processing methods in matlab results processor. Add the `svankek` and `slm` results to processed audiometry results by default.
- Rename built in protocol `cha-tone-generation` protocol to `wahts-calibration-check`. This protocol can be used to play pure tones through the WAHTS for a calibration check.
- Add feature-test protocol to test new TabSINT releases.

Bug Fixes

- Correctly mark digits in Three Digit Response area when the response area is generated by a preprocessing function
- Update Bower package manager to version 1.8.4 and point to new bower registry.
- Remove `Creare Headset` from the list of potential headsets in the protocol schema.
- Remove the `feature-demo-french` protocol.
- Handle error when attempting to push a result to gitlab that already exists by allowing to continue uploading other results and providing a useful error message.

v2.3.1
------

Improvements

- Add *Start Recording* button the `NatoResponseArea`
- Add `autoPlay` option to `NatoResponseArea`. This boolean parameter will play recorded automatically when the user presses *Stop Recording*. By default, `autoPlay` will be true.

Bug Fixes

- Update to WAHTS firmware `2018-06-04_rhino_1`. Fixes pink noise in the noise feature of the WAHTS.

v2.3.0
------

Features

- Add `ResponseTime` result field, the response time as recorded by the WAHTS for Hughson Westlake presentations. `ResponseTime` is an array of the response times for each presentation in milliseconds.
- Edit sound-recognition response area to record the sound-detection time.

Improvements

- Change audiometry result field from `responseTimes` to the more descriptive and self-explanatory `buttonPressTimes`.
- Add level bounds-checking to the manual-audiometry exam.
- Add results analysis documentation.

Bug Fixes

- Fix number of log messages display in admin view.
- Fix ``responseRequired`` behavior in ``textboxResponseArea``.
- Replace the calibration Linear Chirp wave file to get full scale.

Known Issues

- Noise feature does not work with pink noise. Cannot play sound with responseArea property: `"maskingNoise":{"Type": "pink"}`

v2.2.2
------

Bug Fixes

- Fix results parsing from sqLite table, which fixes exporting results functionality.

Improvements

- Extend Spanish translations.
- Update Linear Chirp wav file.

v2.2.1
------

Bug Fixes

- Fix submission logic in qrCode, multiple-inputs and likert response areas.

v2.2.0
------

Features

- Store results in a sqLite table instead of on the disk before they are uploaded or exported. This avoids slowing down TabSINT when queued results accumulate.
- Add USB media file transfer functionality.
- Add a tablet gain parameter to adjust the sound file volume being played on a tablet to the actual volume output for that tablet.

Improvements

- Fix tests.
- Add a linear chirp button ``CompAudioTestLinear`` below the log one ``CompAudioTestLog`` in Admin View>Setup>TabSINT>Advanced Settings>Calibration.
- In the Quick Start>WHATS section of the documentation, add an expanded description of the Headset bluetooth icon.
- Add a headset connection section in the FAQ documentation.
- Reorganize and simplify the exam logic Javascript code.

Bug Fixes

- In manual audiometry, add alerts to users when the parameters are outside of the calibration limits.
- Fix qrCodeResponseArea:
- Implement `responseRequired` in qrCodeResponseArea to resolve discrepancy with schema.
- Add `autoSubmit` field to allow user to view qrCode after scanning.
- Clean up controller and view.

v2.1.11
-------

Bug Fixes

- Update WAHTS firmware to `2018-06-28_orca_9`. Fix HINT test on WAHTS.

v2.1.10
-------

Bug Fixes

- Add Spanish translations for messages with word 'connect'.

v2.1.9
------

Bug Fixes

- Fix Spanish translations in the audiometry-table response area.

v2.1.8
------

Bug Fixes

- Fix error when playing wav files from the SD card of the WAHTS (`chaWavFiles`) introduced in v2.1.4.

v2.1.7
------

Bug Fixes

- Fix HINT exam submission logic.

v2.1.6
------

Bug Fixes

- Update WAHTS firmware to `2018-04-05_orca_8`. Fix critical bug related to the SD card reading.

v2.1.5
------

Bug Fixes

- Don't allow two HINT submissions one immediately after the other.

v2.1.4
------

Improvements

- Add preliminary Spanish language support.

Bug Fixes

- Update WAHTS firmware to `2018-01-04_orca_7`. Implement fix for SD card reading noise in second generation of WAHTS.
- Update translation to support to include *Show/Hide Advanced Settings* and support the prompts in AudiometryList.
- Change wording in audiometry response area *Pause and reset this question* to *Pause*.

v2.1.3
------

Bug Fixes

- Update WAHTS firmware to `2018-01-04_orca_6`. Implement temporary fix to improve FIR filter calculation for media played off the SD card of the WAHTS.

v2.1.2
------

Features

- Add network interface cordova plugin in WRNMMC configurations

Improvements

- Include 'loadflft' method in the 'TabsintResults' MATLAB results processor. This method is similar to 'loadresponses' except it will automatically pre-process **BekesyFrequency** and **BHAFT** responses to make analysis easier. See [TabsintResults.m](https://gitlab.com/creare-com/tabsint/blob/master/tools/matlab/TabsintResults.m#L394) for usage example.
- Improve sound-recognition comments and play sound loop
- Update MILSINT icon links

Bug Fixes

- Update WAHTS firmware to `2018-01-04_orca_5`.
- Fix noise feature and play sound not playing concurrently properly
- Fix third octave band not being resolved properly

v2.1.1
------

Improvements

- Include source code for json decoding library alongside the MATLAB results processor. This will allow the processor to work with older versions of MATLAB.
- Include `loadaudiometry` method in the `TabsintResults` MATLAB results processor. This method is similar to `loadresponses` except it will automatically pre-process certain Hughson Westlake responses to make analysis easier.  See [TabsintResults.m](https://gitlab.com/creare-com/tabsint/blob/release/v2.1.1/tools/matlab/TabsintResults.m#L325) for usage example.

Bug Fixes

- Fix communication bugs for the MLD test on the WAHTS

v2.1.0
------

Features

- Add support for Svantek Dosimeter SV104A via Bluetooth LE. TabSINT uses the Svantek dosimeter to measure the background noise levels when configured in protocol. See the [Svantek-Demo Protocol](https://gitlab.com/creare-com/tabsint/blob/master/www/res/protocol/svantek-demo/protocol.json) for an example protocol. ([#134](https://gitlab.com/creare-com/tabsint/issues/134))
- Include Sound Level Meter and Svantek Dosimeter results in audiometry table. See [Audiometry Results Table Schema](https://gitlab.com/creare-com/tabsint/blob/master/www/res/protocol/schema/cha/response-areas/chaAudiometryResultsTable.json) to include these properties in a protocol. ([#59](https://gitlab.com/creare-com/tabsint/issues/59))
- Add MATLAB results analysis processor. See [MATLAB Tools directory](https://gitlab.com/creare-com/tabsint/blob/master/tools/matlab). This library requires MATLAB 2016B or greater. ([#153](https://gitlab.com/creare-com/tabsint/issues/153))
- Update WAHTS firmware to `2018-01-04_orca_4` ([#160](https://gitlab.com/creare-com/tabsint/issues/160)).
- Include `cordova-plugin-device-motion` cordova plugin into the project.
- Develop and document framework to allow TabSINT to communicate with other applications on an Android tablet. OTher applications must be configured to listen for communication from TabSINT. Documentation for this feature is in the [User Guide / Advanced Protocols](http://tabsint.org/docs/UserGuide/advanced-protocols/communicate_with_other_apps). See the Android application [tabsint-crosstalk](https://gitlab.com/creare-com/tabsint-crosstalk) for a working demo of another application that can communicate with TabSINT.  ([#136](https://gitlab.com/creare-com/tabsint/issues/136))

Improvements

- Finish update of User Guide posted on [tabsint.org](http://tabsint.org) ([#103](https://gitlab.com/creare-com/tabsint/issues/103))
- Enable `Enter` key to close soft-keyboard ([#148](https://gitlab.com/creare-com/tabsint/issues/148))
- Improve `logs` display on the Admin page
- Add `loadAsync` method to the `json` service
- Add `Frequencies`, `Leq`, and `LeqA` properties to SLM results to be consistent with Svantek Dosimeter results.
- Remove conditional skipping of the first page in a protocol when protocol is randomized `WithoutReplacement`. All protocol pages will be randomized when `"randomization": "WithoutReplacement"` is defined in the protocol. ([#150](https://gitlab.com/creare-com/tabsint/issues/150))
- Include `Output` field in **Recent Uploads** to show the location of previously uploaded / output results. ([#132](https://gitlab.com/creare-com/tabsint/issues/132))
- Update MILSINT protocol icons
- Improve Sound-Recognition service to acquire data on all 68 sound tokens

Bug Fix

- Fix `ToneRepetitionInterval` bug which increased the speed of Bekesy FLFT presentations introduced in `2017-12-06_orca_2`
- Fix missing WAHTS build/firmware version
- Fix "failed to stop media while switching to admin view" recurring log

v2.0.3
------

Feature

- Update WAHTS firmware to `2017-12-06_orca_3`

Bug Fix

- Fix handling of aborting WAHTS tests while a TabSINT exam is running

v2.0.2
------

Bug Fix

- Update HINT schema to match the WAHTS properties

v2.0.1
------

Improvements

- Change headset selection name from  "Creare Headset" to "WAHTS". Support will continue for protocols that still use "Creare Headset" as the headset for calibration.
- Only check for new releases automatically once per day

Bug Fixes

- Fix WAHTS firmware update bug when not connected to USB
- Fix WAHTS error handler so that all messages use the same format

v2.0.0
------

> Documentation and Releases are now available through http://tabsint.org

BREAKING CHANGES:

- Default tone duration (`ToneDuration` in `ToneGeneration` class) on the WAHTS increased from 100 to 225 ms to meet ANSI S3.6 specification. This default `ToneDuration` is used by the following exams:
  - `ToneGeneration`
  - `Bekesy`
  - `Hughson-Westlake`
  - `Bekesy Fixed Level Frequency Threshold`
  - `Hughson-Westlake Fixed Level Frequency Threshold`
  - `Bekesy Highest Audible Frequency Threshold (FLFT)`
- Config file field `plugins` has been split into `tabsintPlugins` and `cordovaPlugins`
- The private release repository for TabSINT with support for the WAHTS (`https://gitlab.com/creare-com/tabsint-cha`) will no longer be updated. All releases from TabSINT 2.0 forward will come with support for the WAHTS.

Features

- Merge the WAHTS interface repository into the TabSINT repository
  - Full WAHTS support within TabSINT still requires TabSINT to be built with a private external cordova plugin `cordova-plugin-creare-cha`
- Refactor exam and admin logic to simplify and streamline code base
- Add *Advanced Settings* accordion sections to enable simpler navigation for typical TabSINT users
- Add option to disable audio streaming to the WAHTS
- Deprecate bug report
- Update to cordova 7
- Merge active task services and display them via sticky notifications
- Make "Only Track Tags" selected by default under the Gitlab server settings
- Release documentation and Android apks via `http://tabsint.org`

Improvements

- Polish style
- Upgrade documentation
  - Add *Quick Start* section for basic instructions
  - Add FAQs
  - Add information and references on the Wireless Automated Hearing-Test System (WAHTS)
  - Refine existing documentation
- Streamline WAHTS connection sequence

Bug Fixes

- Fix csv service
- Fix headset reboot after updating firmware or reconnecting headset

v1.9.2
------

Bug Fixes

- Fix cha.playSound calls via page field chaWavFiles - useMetaRms was defaulting to true for all files in the user directory.  now useMetaRms is a protocol field and defaults to false.

v1.9.1
------

Improvements

- Removes documentation of downloading gitlab protocols by commit hash

Bug Fixes

- Fix unreturned promise from `slm` service when `slm` cordova plugin is not installed
- Fix `subjectIdResponseArea` so that `"generate": false` allows user to input subject ID
- Fix bug where `nRepeats` is not reset if user reset exam in the middle of a page

v1.9.0
------

Features

- Enable subject ID generation in `subjectIdResponseArea`. See the [Subject Id Response Area Schema](https://gitlab.com/creare-com/tabsint/blob/master/www/res/protocol/schema/response-areas/subjectIdResponseArea.json) for implementation.
- Implement android based sound level meter that can be configured to run during protocol pages. See the [Page Schema]() for implementation details. SLM results will be stored on `responses` object of the result. **Note: This feature requires a seperate TabSINT plugin to operate**
- Add python script to convert test result output file to csv file. See https://gitlab.com/creare-com/tabsint/tree/master/tools/results-analysis/python/json2csv.py
- Add preliminary Japanese language support

Improvements

- Refactor `gitlab` logic to fix bugs and extend ability to leverage gitlab services across tabsint
- Disable the hardware back button on Android tablets
- Alphabetically order protocols on Admin page

Bug Fixes

- Fix `Site config task still in progress...` message when downloading protocols from the TabSINT server [#55](https://gitlab.com/creare-com/tabsint/issues/55)
- Fix `Host is not defined` popup on Admin page

v1.8.2
------

Bug Fixes

- Fixes audio streaming to CHA devices

v1.8.1
------

> Note: This release replaces the built-in Crosswalk web browser from TabSINT framework. Please avoid installing version `v1.8.0` on older devices.

Improvements

- Switched TabSINT source control to https://gitlab.com/creare-com/tabsint and release location to https://gitlab.com/creare-com/tabsint/tags
- Adds option to configure Gitlab results location other than `results`. Previously, gitlab results were only uploaded to the `results` repository within a given group.
- Shows server credentials even when `Admin Mode` is not enabled
- Improves multi-lingual support
- Adds preliminary Japanese language support
- Replaces fragmented documentation sources with single documentation source hosted at https://creare-com.gitlab.io/tabsint
- This release re-adds the crosswalk support. Since TabSINT is run primarily on older tablets, crosswalk will remain critical for the near future.

Bug Fixes

- Fixes permissions issue with csv file export
- Disables back button while TabSINT is open

v1.8.0
------

> Note: This release removes the built-in Crosswalk web browser from TabSINT framework. Crosswalk is no longer supported and causing issues in newer devices. In practice, this means that TabSINT will now be run on devices using the native device Webview.  This may have unintended consequences for TabSINT running on older mobile devices.

Features

- Begins adding support for additional languages using the [`gettext` standard](https://www.gnu.org/software/gettext/) (*Beta*)
  - Additional languages can be provided by editing the [`extract.pot` template file](translations/extract.pot) using a gettext compatible editor (like [Poedit](https://poedit.net))
  - Language support will continue to grow in future releases
- Adds service to calculate CRC32 checksums for file synchronization
- Adds a preference to show *Skip* button while in **Admin Mode**

Improvements

- Updated support for iOS
- Removes python build system used in TabSINT < v1.3
- Removes the Crosswalk web browser from the built apk
- Add convience method (`npm run plugins.debug`) to set `debug: true` for all plugins in the current workspace.
- Adds file method for recursively listing directory contents
- Updates logging for better reliability, searching, and performance

Bug Fixes

- Fixes automatic protocol validation when TabSINT first loads. On first load, the protocol will validate only if the preference is set on the Config page.

v1.7.4
------

> Note: The filenames of results exported locally have been changed to be consistent with the filenames of results uploaded to gitlab servers. See below for the result file naming convention.

Improvements

- Unifies the filenames of results that are backed up, exported, and uploaded to gitlab. The new convention for the filename of exported/uploaded result files is `[protocol-name]/[tablet-id]_[test-date-time]`. (*Previously, the filename included the time of export/upload. Now, the filename includes the time the test was taken - `testDateTime` in the results structure*)

Bug Fixes

- Changes the gitlab results handling to the `Commits` API endpoint. This avoids encoding issues encountered while using the `Respository Files` API endpoint.
- Fixes bugs related to the asynchronous results exporting/uploading
- Fixes automatic protocol validation the first time the app loads a protocol

v1.7.3
------

Improvements

- Adds `group` field to gitlab protocol handling

Bug Fixes

- Fixes url encoding of results uploaded to gitlab
- Fixes bug in gitlab pull where file urls would grow in each loop
- Fixes non-uniform spacing of help popover icon

v1.7.2
------

Features

- Adds ability to change the admin pin. Default admin pin is still `7114`
- Adds ability to configure the start time delay of a wav file. By default, wav files will start to play 1000 milliseconds after the page is loaded. This can be changed using the `wavfileStartDelayTime` property on any page.
- Adds `Audiometer` headset and calibration to the TabSINT server

Improvements

- Sets preference to turn off magnifying glass on long press in UIWebView on iOS

Bug Fixes

- Disallow clicking outside of the partial exam dialog box
- Fix bug where "Loading Protocol..." would persist after loading a protocol
- Fix bug in `omtResponseArea` and `multipleChoiceSelectionResponseArea` related to grading

v1.7.1
------

Bug Fixes

- Fix duplicate `.wav` suffix in files recorded with the `natoResponseArea`

v1.7.0
------

Features

- Incorporates welcome popup the first time the app is ever opened
- Supports Gitlab API v4, including protocol repositories stored in subgroups
- Supports sending and receiving messages to other apps on the device (`externalAppResponseArea`)
- Includes an option to override the automatic volume control on each page
- Include the `natoResponseArea` for recording audio using the internal or external microphone. See the [Nato Response Area Schema](https://github.com/creare-com/tabsint/blob/develop/www/res/protocol/schema/response-areas/natoResponseArea.json) for the allowable input properties.
  - *Note*: This response area requires a closed source tabsint plugin for full functionality

Improvements

- Gitlab based protocols will now default to being tracked by *commit* instead of *tags*. Protocols can still be managed using tags by using the `Only Track Tags` preference.
- Refactors `TabSINTNative` cordova plugin
- Improves style of protocol table
- Improves protocol documentation in the User Guide

v1.6.0
------

*Note*: This build incorporates a new system for uploading log messages
from the tablet. Please export your logs before you install this version
of TabSINT to make sure that no log messages are lost.

Features

- Includes `multipleChoiceSelectionResponseArea` to allow cutomized, table-like multiple-choice inputs. See the [Multiple Choice Selection Response Area Schema](https://github.com/creare-com/tabsint/tree/master/www/res/protocol/schema/response-areas/multipleChoiceSelectionResponseArea.json) for the full list of inputs. This UI of the response area is based on the `omtResponseArea`.
- Incorporates centralized log server for all releases of TabSINT

Improvements

- Exposes the TabSINT Server service to all builds of tabsint
- Exposes the BugReport form to all builds of tabsint
- Incorporates many improvements for use on smaller screen devices

Bug Fixes

- Fixes bug where preprocessing functions were not getting updated
  when when protocols were updated

v1.5.0
------

Features

- Shows status of each connection indicator in the top bar when
  pressed
- Creates preference to allow user to disable all logging in the app
- Adds csv export option to results page

Improvements

- Replaces partial exam notification with a modal to improve
  perforance and user interface
- `Latest Release: ...` button now downloads the release to a more
  easily accessible location
- Improves support for smaller screen sizes (Samsung Note 4S)
- Improves network handling to avoid issues while trying to connect to
  a network that requires a login
- Now exporting results locally deletes the result from memory in
  TabSINT, to avoid stockpiling results in memory. This is consistent
  with what happens when exporting (uploading) to other servers.

Bug Fixes

- Fixes radio button sizes in `likertResponseArea` on smaller screens
  (Samsung Note 4S)
- Fixes bug that allowed `Submit` button to be pressed while disabled

v1.4.2
------

Bug Fixes

- Includes plugin documentation with the user guide

v1.4.1
------

Bug Fixes

- Fixes the user guide styling within the application

v1.4.0
------

Features

- Added **Creare Headset** as an option for the selected headset. When
  used in conjunction with the Creare Wireless Audiometric Screener,
  this option will stream calibrated wav files from TabSINT to the
  Creare Headset.
- Include a calibration scale factor for streaming audio from the
  tablet to the Creare Headset

Improvements

- The semantic version of the app is now completely maintained in
  package.json
- Add notification when protocol has not been loaded
- Include docs directly into the application
- Show the server selection buttons all the time

Bug Fixes

- Fixed multiple bugs in the new npm build scripts

v1.3.0
------

*Note*: You will need to run npm install to update your development
environment with this release of TabSINT.

Features

- Refactored build system to use npm scripts instead of python. Python
  is still required to build the documentation for the user guide and
  the develop guide. See the README.md for more information about how
  to use the scripts in building the project. Background information
  on npm scripts generally is located at
  npmjs.com: https://docs.npmjs.com/misc/scripts.
- Add **csv** copy of results when exported locally to the tablet. The
  csv file will have a flattened version of the full results
  json structure.

Improvements

- Reformatted the debug view at the bottom of each page

Bug Fixes

v1.2.3
------

Features

- Include response area and page information to results structure. The
  following fields will now be available on each `response`: :

  {
  ...
  responseArea: 'response-area-type',
  page: {
  wavfiles: {...},
  chaWavFiles: {...},
  image: {...},
  video: {...},
  responseArea: {...}
  }
  }

Improvements

- Converted protocol loading to be asynchronous
- Added protocol loading and validation message

Bug Fixes

- Fix responsive font size on Admin page

v1.2.2
------

Improvements

- Release link on the config panel of the admin view will now directly
  download the latest TabSINT APK file. The parameters for this
  download location can be customized in the releases key of the
  configuration file for the build.
- Add responsive support for smaller screen sizes, specifically the
  Samsung Note 4

v1.2.1
------

Features

- Debug protocols in the browser using the protocols field in the
  config file. These protocols must be stored in a /protocols/
  directory in the root of the tabsint repository.

Improvements

- autoSubmitDelay is set to a minimum of 50 ms to avoid race condition
  with page loading
- Add **Device UUID** to config page
- Improved tablet location handling in exam results. Location
  reflected in results will either be the current location or the last
  known location, if the current location is unavailable

Bug Fixes

- Fixed **copyAsset** issue in file handling
- Fixed **multipleInputResponseArea** require field
- Fixed parse error in protocol parsing of custom response areas

v1.2.0
------

**Android Note**: The inclusion of the Crosswalk WebView plugin may
require the previous TabSINT android package to be uninstalled before
you can install version v1.2.0.

Features

- Includes the Crosswalk WebView plugin. This builds a webview
  directly into the tabsint package that includes the latest HTML5
  features and signifigant performance improvements.

v1.1.0
------

**Note**: Version 1.1.0 includes much more rigorous protocol validation
on the tablet. Some protocols may need to be updated for successful
validation through the protocol schema. This will allow for easier
compatibility checking with future versions of TabSINT.

Features

- Protocol schema overhauled to make validation more precise
- Protocol schema broken into multiple files for easier use

Improvements

- SD Card renamed *Device Storage*
- Gitlab token always visible
- Confirms controllers are available when loading custom response
  areas
- Protocol schema validation more rigorously enforced
  - **Note**: Some protocols may need to be updated for successful
    validation through the schema
- Github Pages site included in master branch within the /docs
  directory

Bug Fixes

- Fixed **Export All** button on the results page
- Fixed broken links to developer documentation

v1.0.4
------

*Note*: Releases will now include an android package (.apk) to easily
install the current release

Features

- Checkbox to turn on/off protocol validation on the *Protocols* page

Improvements

- `autoSubmit` included to OMT reponse area
- Protocol schema source included in user guide
- **Feature Demo** protocol available in admin mode

Bug Fixes

- `autoSubmitDelay` moved to the correct section of the protocol
  schema
- `responseRequired` fixed response areas
- `autoSubmit` removed from common response areas

v1.0.3
------

Bug Fixes

- Catching gitlab authorization token errors on protocol add
- Fixing issue with "correct" specification on three digit test
- Removing redundent alert

v1.0.2
------

Bug Fixes

- Fixed bug related to move to lodash
- `_.all` deprecated, replaced by `_.every`

v1.0.1
------

Features

- Added the ability to change page properties from custom response
  areas

v1.0.0
------

Version 1.0.0 includes many new features and improvements to increase
the reliability and access of the TabSINT system:

- **New Protocol System**
  - Included Gitlab based protocol system
    - Supports hosting, downloading, editing protocols as git
      repositories in the cloud
    - Supports downloading changes to a protocol
  - Support for common media repositories
    - Protocols can reference a common repository, then use wav
      files from that repository
    - Share common media files across multiple protocols
    - Update a protocol without re-uploading and re-downloading
      all media files
- **Admin View**
  - Admin view split into three tabs:
    - Configuration
    - Protocols
    - Results
  - **Protocols**
    - Multiple protocols can be stored on the tablet and loaded
      into TabSINT at one time
    - Protocols can be sourced from different locations: TabSINT
      Server, Gitlab, or the local SD Card
  - **Results**
    - Result objects can be viewed in the **Results** tab of the
      *Admin View*
    - Any result can now be exported to a local file on the tablet
    - Results may be output to different locations: TabSINT
      Server, or the SD card
- **Features**
  - Scrollbar added in exam views that are larger than the vertical
    screen
  - Adding scrolling when the keyboard is opened
- **Plugins**
  - Added support for defining custom headset values on
    `disk.headset`
- **Refactoring**
  - **Custom Javascript**
    - The syntax for including custom javascript (custom response
      areas, pre-processing functions) has changed. See the
      **Custom Response Areas** and **Dynamically Calculated Page
      Properties** sections of the user-guide for instructions and
      examples using the new syntax.
  - Code organized into *Services*, *Components*, and *Routes*
  - All unit tests now included alongside with files being tested
  - Split services into single files
  - Organized angular module dependency hierarchically
- **Dependencies**
  - Replaced Underscore with lodash
  - Included es6-shim to use some es6 utilities

---

Older Releases
==============

05/14/2016: v0.8.2
------------------

- Added `autoSubmitDelay` to page properties.
- Added the ability to start playing a wav file from a specific time
  and end playing a wav file at a specific time
  - This functionality may be added to any wav file on any page
    using the syntax described in the protocol schema

```
    "wavfiles":{
      ...
      "items":{
        "properties":{
          ...
          "startTime":{
            "type":"number",
            "description":"Time in audio file (in milliseconds) to start playback from.  Default 0."
          },
          "endTime":{
            "type":"number",
            "description":"Time in audio file (in milliseconds) to stop playback"
          }
        }
      }
    }
```

04/27/2016: v0.8.1
------------------

- **Response Area Improvements**
  - New response area `audiometryInputResponseArea` allows for an
    audiogram input to TabSINT.

04/14/2016: v0.8.0
------------------

- **Local Server**
  - TabSINT now ships with a 'local' server mode. This allows
    protocols to be loaded from local .zip files on the tablet.
    - Local mode may be defined in the build config file
      (`config.json`), or you may select it from the admin page while
      debug mode is enabled.
  - Results are output to a directory on the tablet. The user may
    choose the results output directory on the admin page.
  - Logs are saved to a json file on the tablet. The user may choose
    the log output directory on the admin page.
- **Protocol System Improvements**
  - Protocols are now validated when they are loaded (both from the
    server, and from local .zip files)
- **Response Area Improvements**
  - Dropdown option added to multiple input response areas
- **Build System**
  - Cordova and Bower are now included as npm
    development dependencies. You no longer need these libraries
    installed globally.
  - All TabSINT plugins are now required to have a "version" key.
    This version will be used as a git reference when cloning
    the plugin.

03/15/2016: v0.7.7
------------------

- **Network Connection**
  - Allows 'unknown' network connection types

03/03/2016: v0.7.6
------------------

- **Automatic Text Backups**
  - The automatic test-backup directory has been switched to:
    `sdcard/tabsint-results` for android builds

03/01/2016: v0.7.5
------------------

- **Bug Fix**
  - Fixed bug related to siteId mismatch when using the sandbox
    server

03/01/2016: v0.7.4
------------------

- **Continued Network Improvements**
  - Adds latency to offline/online listeners to reduce noise
  - 2G Signal strengths are now considered 'offline' because of
    intermittency
  - Reduced duplicate test result upload
- **Automatic Text Backups of Result**
  - All test results will be exported a local text file before being
    queued for upload
  - This directory can only be cleared manually, or when the
    application is uninstalled

02/01/2016: v0.7.3
------------------

- **Network Information**

  - Improved handling of network connectivity
  - Fixed bug related to 'offline' status after app installation
  - Added connectivity status icon to the app header
  - Added connectivity details to the *Software and Hardware Status*
    pane of the admin page
- **Improvements**

  > - Title bar reflects bluetooth and network connectivity status
  > - Titles now automatically get truncated at 30 characters
  >

  - Improves error logging for file downloads
  - Improves CHA Bluetooth Plugin connection handling
  - Adds app-developer mode
- **Bug Fixes**

  - Site ID properly set in the logging system

12/28/2015: v0.7.2
------------------

- **Multiple Choice Response Area**
  - `delayEnable`: new field in multiple choice response area to
    allow the choices to have a delay before being active
  - `feedback`: you can now provide feedback to a user's response in
    multiple choice response area
- **Triple Digit Response Area**
  - `delayEnable`: new field in triple digit response area to allow
    the choices to have a delay before being active

10/15/2015: v0.7.1
------------------

- **Protocol Improvements**
  - *minTabsintVersion* can be used to specify the minimum required
    TabSINT version to run a given protocol
  - *responseRequired* now a possible field for for
    seeSawResponseArea
  - **NOTE**: Custom Response Area syntax has now slightly changed
    (see custom\_response\_areas and dynamic\_properties). This
    version of TabSINT will not run custom response areas in
    protocols developed for older versions of TabSINT. The new
    custom response areas do not require the user to restart TabSINT
    each time a protocol is downloaded.
- **User Guide**: TabSINT userguide is now available offline on the
  tablet
- **Sandbox Server**: While in debug mode, an admin can select to use
  the 'Server Sandbox Mode'. Checking this box will make the tablet
  talk to the sandbox server specified in the config file. This can
  help keep the production server free of clutter while testing and
  debugging protocols.
- **Build System Improvements**
  - Dependencies are checked during the build process
  - All dependency versions are included in version.json along with
    the build

9/08/2015: v0.7.0 - Beta
------------------------

This is the first official release of open source TabSINT

- Plugin System
  - Added a full feature plugin framework to allows custom
    functionality to be built alongside the core open
    source TabSINT.
  - Plugins can add response areas, stylesheets, pre-processing
    functions, and any custom modules required for a specific build
  - See the **plugins** section of the developer documentation for
    more information
- Open Source Migration
  - Implemeneted a *requirejs* framework around angular to create
    the tabsint plugin framework and improve custom response area
    functionality
  - Added customizable server options that will allow protocols to
    be served locally
  - Began semantic versioning of TabSINT releases (v0.7.0)
  - Simplified footer to show Tablet UUID - Build - Version
  - Added developer documentation to guide open source collaboration
    with the TabSINT project
  - Continuous integration testing runs on every commit to the
    Master branch of the project

9/03/2015
---------

- Bug Fix
  - Native Volume Checking: The tablet was inappropriately checking
    the volume set on the tablet before media files were
    being played. Tablet now appropriately confirms that media files
    are always played at 100%.

7/17/2015: Rev approx. 4890
---------------------------

- Memory Management and Logging
  - The logging functionality of the tablet has been switched from a
    text format to a local sqLite database, which is faster and more
    robust at longer log lengths
  - Log uploading has been switched to a batch process, which
    uploads 50 logs at a time
  - Tablets are no longer limited to administering 15 exams before
    upload
  - **Note:** It is still not recommended to build up exam results
    on a tablet for long periods of time before upload. Data is most
    secure once uploaded to the server.
- Open Source Migration
  - Migrated all code from subversion version control to git version
    control
  - Implemented server API to allow for other server types (i.e.
    local files, dropbox, etc)
- Other
  - Multi-line input response area for inputing multiple pieces of
    data with one response area
  - Server can now provide generated versions of a protocol for
    download on a computer
  - Server can now generate calibrated protocols
  - Bugfix: Bug report submission button has been fixed
  - The size and spacing of the integer response area buttons have
    been increased
  - New custom designed App Icon and Splash Screen

5/11/2015: Rev approx. 4633
---------------------------

- Calibration
  - Bugfix: VicFirth Calibration. A 3 dB offset error was found in
    the Vic Firth calibrations dating from mid-December. This error
    is now fixed, and the test results now include additional
    version information about the calibration settings used.
    Protocols on the HFFD server have been updated/fixed.
  - A **flat** calibration type is now supported (see calibration).
- Navigation and Protocol Development
  - A custom **Navigation Menu** (see navigation\_menu) can now be
    included, which is accessible from the Navicon in the upper
    right corner of the TabSINT window.
  - An optional **Back Button** (see back\_button) can be easily
    added to any protocols.
  - **Dynamically Calculated Page Properties**: Pages can now
    include flexible custom javascript functions that can change any
    page property in response to user input, past test results, or
    even randomly. (see dynamic\_properties for more information
    and examples).
  - **Page Repeat**: Pages can be repeated. Repetition is controlled
    by a conditional, and a maximum number of repeats can be set.
    See repeats for more details.
  - **Feedback**: Various types of feedback regarding right or wrong
    answers can be displayed. This functionality is currently
    enabled for MRT and OMT response areas.
  - The **progress bar** can be set as a page property (or
    dynamically via custom functions) using the
    "progressBarVal" property. See the json\_schema for details.
- Other
  - Several example protocols (see examples) are now given in
    the documentation.
  - If a sound saturates (requested volume &gt; 100%) and the tablet
    is in debug mode, an error pops up.
  - Bugfix: TabSINT should automatically scroll to the top of every
    displayed page.
  - TabSINT now has built-in maximums for the amount of test data it
    will store, in order to help prevent data loss.
  - Files can now be saved to the tablet's embedded storage from
    custom response areas.
  - Alert and Confirm screens now need to be closed explicitly by
    clicking on an appropriate button. In previous TabSINT versions,
    alerts could be closed accidentally by clicking on the screen
    background around the alert.
    \* The documentation now includes a faster JSON parser
    (see loadjson)
  - The screen responds to the presence of a test-input keyboard by
    resizing, in an effort to retain the submit button on-screen.
  - Bug reports now automatically include the TabSINT SVN
    Revision Number.
  - Bugfix: The screen once again stays on during downloads
    and testing.

2/10/2015: Rev 4040
-------------------

- Bugfix: Some sounds weren't played due to a roundoff error in
  volume calculation. Problem appears to have been limited to clipped
  audio, and most prominent for audio that was originally recorded
  using a small part of the dynamic range.

1/19/2015: Rev 3889
-------------------

- Button Spacing: added configurable verticalSpacing and/or
  horizontalSpacing options to configure spacing between buttons in
  the CRM, MRT, multiple choice, yes/no, checkbox, and buttonGrid
  response areas.
- Video Autoplay: Video autoplay now works correctly.
- Volume Checking: The software now checks that the volume is set
  correctly (Android can sometimes prevent volume from being increased
  over 50% as a safety measure.) We now display and log prominent
  error messages if the volume is not set correctly.
- Removed many non-critical logging messages, in order to reduce
  chance of overloading the logging upload system.
- Added a SubjectID Response Area that will be used in future subject
  tracking applications.
- New CHA-related Response Area: HughsonsWeslake.
- Admin page displays which server it is connected to when in
  Debug Mode.
- BugFix: Adjustment to the 1 kHz calibration tone accessed on the
  Admin screen.
- BugFix: Fixed bug where the number of rows in a Textbox response
  area may not be updated properly with back-to-back response areas.
- BugFix: Screen did not always update properly when partial results
  were submitted.

12/8/2014: Rev 3649
-------------------

### Workflow Changes

- Creare now uses a separate server --
  http://creare.crearecomputing.com for most testing and development
  activities, so as to not interfere with client data collection.

### App Changes

- When the exam is terminated prematurely, TabSINT checks for a
  special subprotocol with a protocolId equal to @PARTIAL. If found,
  this protocol is run before the exam is submitted. This behavior is
  documented in the user manual.
- A new Bug Report Form is accessible throughout the app. The form
  makes it easy to report bugs, and it automatically includes a great
  deal of relevant debugging information.
- An in\_browser\_dev\_environment.zip file with the code needed to do
  in-browser development is now included in the documentation for each
  new release, and can be found in the "Advanced Information" -&gt;
  "Development Process" -&gt; "In-Browser Protocol Development"
  section of the userguide.
- The page-to-page transition has been improved so that (a) the user
  gets feedback that a button has been pressed, (b) the page is
  slightly 'grayed out' momentarily to make it clear that the page is
  completed, and then (c) the new page is loaded.
- The tablet screen is prevented from automatically turning off
  either (a) during an exam, or (b) during a protocol download.
- The integer response area can now handle negative values and
  floating point numbers (optionally).
- Protocol downloads can be cancelled while in-progress.
- The 1 kHz test tone now changes to match the chosen headset.
- Underlying libraries have all been updated to the latest versions.
- TabSINT now support both the new on-demand calibration API, as well
  as the legacy calibration API, and can auto-detect which one to use.
- The logfile size is now capped.
- Only the last several kB of logs are displayed in the admin view, to
  prevent crashes.
- Bugfix: Fixed a bug where a 'log upload already in progress' message
  was displayed repeatedly.

11/17/2014: Rev 3473
--------------------

### App Changes

- Logging: TabSINT now records detailed usage logs for
  debugging purposes.

  - All errors, warnings, and internal debug messages are recorded.
  - Logs are uploaded whenever the tablet connects to the internet,
    as well as whenever an exam is uploaded.
  - Logs are viewable via the Admin screen until they are uploaded.
  - Log are accessible via a server-side API call once they
    are uploaded.

  \* These logs will allow Creare to help debug problems remotely,
  even if the problem is not reproducible.
- Videos

  - Videos stop automatically when the page changes.
  - Videos correctly autoplay when requested.
- The OMT Response Area no longer auto-submits.
- A CRM Response Area (Coordinate Response Measure) is now included.
- Checkbox response areas can now be styled so that selected choices
  are clearly marked as 'correct' or 'incorrect'.
- The Custom Response Area has been reworked to be more reliable and
  have a more well-defined API.
- Bugfix: The app automatically reloads as needed to load new Custom
  Response Areas.

### Documentation Updates

- Details on how to enable or disable TabSINT as a
  single-app 'launcher'.
- Details on the Custom Response Area API, as well as best practices
  for custom response area implementation.

10/23/2014: Rev 3350
--------------------

### App Changes

- The Admin screen now includes a log of uploaded exam results.
- Bugfix: The app no longer displays an alert if the tablet type
  is 'undefined'.
- Bugfix: The black bar with the 'Home' and 'Back' buttons should once
  again be properly hidden, to maximize screen real estate and prevent
  subjects from inadvertently exiting the application.
- Bugfix: The OMT response area now ignores punctuation when deciding
  if a response is correct.

### Server Changes

- The server now supports improved .CSV and .XLSX outputs that should
  be easier to import and easier to use in data analysis.

10/17/2014: Rev 3306
--------------------

### App Changes

- It is possible to upload partial results at any time via the
  drop-down menu in the upper-right. Partial results are tagged as
  such when they are uploaded ot the server. An admin password is
  required to do this, unless debug mode is enabled.
- Many more HTML constructs are now supported in fields like
  questionMainText, including 'style' attributes. These were
  previously restricted for security reasons, but with the inclusion
  of custom response areas, this restriction now seems
  unnecessarily conservative.
- The app now lets users select which headset is connected, and it
  alerts the user if the active protocol's calibration does not match
  the current headset.
- The results grid is now hidden (except in debug mode).
- Tablets are now identified by a unique ID. A shortened 5-character
  version of this ID is displayed on the admin screen.
- Submitted results now include additional location, configuration,
  and tablet identification information. This information is returned
  with the results download.
- The app displays the last date and time that results were uploaded.
- Admin page now includes more information about the device, and the
  'Demonstration Release' note on the status bar has been replaced
  with actual device status.
- Bugfix: The OMT response grid has been reordered and updated, and
  the OMT response scoring has been fixed.

### Server Changes

- The server no longer supports the original .CSV output format as it
  was error-prone and hard to interpret. It will be replaced by the
  option to download the data in one of several other formats, which
  will be phased in.
- The server now supports raw JSON as an output. This is the
  definitive representation of the data, and where possible, we
  recommend using it in automated scripts.
- The server now supports calibration for Vic Firth headphones.
  Currently, headset type should be indicated in uploaded protocols;
  in the future, the server will perform custom
  calibrations on-demand.
- Bugfix: An issue with the server using the old HDA-200 filter has
  been fixed.

10/13/2014: Rev 3254
--------------------

### App Changes

- Exam results can now be automatically uploaded to the server, if
  this option is chosen in the admin view. Regardless, the app now
  alerts the administrator if an unexpected number of un-uploaded
  exams accumulate on the tablet.
- The app now displays the name of the active protocol as well as
  which user uploaded it, and when.
- The question mark icon in the upper-left of the exam view is
  now enabled. It displays the protocol-wide help text when clicked.
- Bugfix: The protocol configuration is no longer deleted when the
  app restarts.
- Bugfix: The QR code scanner works properly again.
- Bugfix: The documentation no longer lists omtUsResponseArea as a
  valid response area type.

### Server Changes

- The 'Update Protocol' button removed. A new protocol update system
  is being deployed which will improve database integrity, and which
  will ensure that past exam results are unambiguously linked to a
  specific exam protocol .zip file.
- The server now records and displays which user uploaded a protocol,
  and when.
- The documentation is now properly linked from the website, and
  is password-protected.
- A matlab file to read in the .CSV files downloaded from the website
  is now available via a link from the documentation.
- The server documentation has been reorganized into three
  user-focused sub-manuals, and the getting-started guide has
  been improved.
- Protocols can now be 'hidden' in order to de-clutter the UI.
  However, the underlying protocol information remains in the database
  so that no data are lost.
- Bugfix: The 'download exam results' tab was briefly nonfunctional;
  it is now fixed.
- Bugfix: Slow rendering of some parts of the website has been fixed.
- Bugfix: An issue that briefly prevented protocols from being
  uploaded was fixed.
- Bugfix: An issue that prevented exam results from being uploaded
  after a site protocol was changed has been fixed.

9/18/2014: Rev 3115
-------------------

This major release contains many under-the-hood changes to improve
reliability and maintainability, as well as several features and
bugfixes.

### App Changes

- The software now supports a 'language' parameter on the OMT response
  area, which determines which OMT grid is shown. Currently supported
  languages are 'british' and 'american'.
- A custom response area lets protocol designers insert custom
  response areas (i.e., AngularJS directives) and bundle them into
  specific protocols. These directives are just as flexible and
  powerful as the built-in response areas. Currently, each protocol
  can contain *one* custom response area type. For instance, these
  directives can be used to produce intermediate 'results' pages which
  include extensive javascript logic.
- The results page more clearly states that exam results have been
  saved, and the 'Reset' button more clearly states its purpose and is
  more appropriately styled.
- Added a textbox input area, and improved it with a 'rows' parameter,
  and optimized UI control widths.
- Support for pairing with and connecting to the CHA (Creare
  Hearing Assessor) via Bluetooth.
- TabSINT now has a built-in Launcher capability, so that it can
  (if enabled) prevent subjects from easily accessing the base Android
  Operating System.
- TabSINT now refers to and interoperates with the new server.
- When large protocols are downloaded, the % progress of the download
  is displayed.
- The error and status messages associated with protocol downloading
  have been improved.
- Substantial progress was made towards an iOS version of TabSINT.
  Some work still remains, especially regarding file paths for bundled
  images and videos.

### Server Changes

- The server has been rebuilt from the ground up with
  production-quality tooling using a [Bitnami server
  stack](https://bitnami.com/stack/lamp).
- The server can now run multiple virtual servers at different domain
  names, using different independent databases, from the
  same codebase. This allows different studies and user groups to be
  completely segregated from each other, without the maintenance cost
  of supporting multiple codebases.
- The server produces a warning when files are clipped
  during calibration.
- Support was improved for large (&gt;200 MB) protocol files.

### New Build System

- A new build system automatically builds and uploads new installers
  and documentation to the server.
- The new build system allows builds to be customized with different
  branding and plugins, and to be associated with different
  virtualized servers.
- The new system also sets up separate 'developer' and 'release'
  channels, so that new features can be tested without accidentally
  updating deployed tablets.

### Android Custom ROM

- For best results, this release is intended to be installed on
  tablets running Creare's custom Android ROM, which is a bare-bones
  AOSP variant without any Google apps, without the Google Play Store,
  and without any software that will auto-update in the background.
  However, the software should still function properly on the
  stock OS.
- When using the Creare ROM, TabSINT is automatically installed and
  configured to point to the appropriate server, app update location,
  documentation directory, and siteName.

7/21/2014: Rev 657
------------------

- The server now clips files which are uploaded to be calibrated in
  'As-Recorded' mode. Specifically, it (1) calculates the volume
  necessary to play at the desired SPL level, (2) scales the .wav file
  appropriately, and then (3) 'clips' all values greater than +/- 1.0
  to 1.0 (or the equivalent integer value). This prevents files with
  large dynamic ranges from playing too softly.

7/16/2014: Rev 651
------------------

- The server now includes a final step which re-scales wav files to
  use the entire dynamic range (e.g., +/- 1.0 or +/- 32,767).
  Previously, a conservative algorithm was used based on the highest
  amplitude in the FIR correction filter. This enables to us to add,
  on average, roughly 5 dB of volume to files. This *only affects*
  files which were previously 'saturating' in volume; properly
  calibrated files remain properly calibrated.

7/2/2014: Rev 630
-----------------

- Basic *Video Playback* is now enabled, as described in the Protocol
  JSON Schema. Future improvements will make this functionality
  more configurable.
- The test tones on the Admin page work again. E.g., the 94 dB test
  tone provides an easy way to test whether audio output levels are
  proprly calibrated.
- *Debug Mode* is now configurable from the Admin page. It controls
  both (1) whether the debug collapsible menus are visible on each
  page (including the admin page), and also (2) whether passwords must
  be entered for sensitive actions.
- Upload limits and session timeout limits have been substantially
  increased, to prevent aborted uploads.
- Disk space on the server has been dramatically increase to deal with
  larger than expected protocol sizes.
- Error reporting for file uploads and protocol downloads is improved.
- Various bugfixes and stability improvements.

6/27/2014: Rev 621
------------------

- Improved JSON Schema documentation.
- The server now performs JSON Schema validation before accepting an
  uploaded protocol.
- Improved error reporting for server-side schema validation.
- Re-designed Admin menu, with better control flow and
  error reporting.
- Enabled an 'other' option on the clickable image response areas.
- Further improved styling.
- Bugfix: Subprotocols weren't counted as 'pages' for purposes of
  executing page-count-based timeouts. This is now fixed, enabling
  cleaner implementation of several important logical flows.
- Bugfix: In newest Android OS, protocols were cached by the
  underlying OS and required an app restart. Caching is now
  disabled again.

6/23/2014: Rev 608
------------------

- New input response area: 'Button-grid' response area, which lets
  users define a custom m x n grid of buttons.
- New input response area: Clickable image response area, which lets
  users define 'hotspots' on a custom image.
- Updated styling and images for the See-Saw response area.
- Updated user manual.
- Added a response time (in milliseconds) to each presentation result.
- If protocols are uploaded pre-calibrated (with a calibration.json
  file), then the calibration algorithms are not run. (This is an
  *unsupported* feature which may be removed.)
- The 'Help' button now displays the exam- or presentation-specific
  'helpText' when pressed.
- The submit button is now styled so that it is much more obvious when
  it is disabled.
- The OMT input response area now auto-submits.
- Documentation (user guide) is now linked to from the HFFD website.
- Bugfix: Spaces in protocol names caused errors when uploaded.
- Bugfix: SubmitText does not display.
- Bugfix: Some protocol uploads were resulting in an empty .zip.
- Bugfix: Some protocols could not be uploaded (disk space issue)
- Bugfix: Consecutive Integer response areas are now
  properly initialized.

5/28/2014: Rev 540
------------------

**Initial WRNMMC Delivery**
