#!/usr/bin/env node

'use strict';

var config = require('./util/config.js');
var log = require('./util/log.js');
var plugins = require('./util/plugins.js');
var fs = require('fs-extra');
var process = require('process');
var _ = require('lodash');

if (require.main === module) {
  var c = config.load();

  // remove old plugins directory
  // fs.removeSync('src/tabsint_plugins');
  fs.ensureDirSync('src/tabsint_plugins');

  // iterate through each plugin
  _.forEach(c.plugins, function(p, name) {
    p.name = name;
    
    if (p.src) {
      log.info('Installing tabsint plugin: ' + p.name);

      var version = plugins.getVersion(p);

      // configm version in config file
      if (version === c.plugins[name].version || c.plugins[name].debug) {
        plugins.installApp(p);
      } else {
        log.error('Plugin: ' + p.name + ' requires version ' + c.plugins[name].version +'. Version ' + version + ' found');
        process.exit(1);
      }
    }

  });

  plugins.writeImportJs();
}
