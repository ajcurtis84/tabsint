#!/usr/bin/env node

'use strict';

var config = require('./util/config.js');
var log = require('./util/log.js');
var plugins = require('./util/plugins.js');
var dircompare = require('dir-compare');
var fs = require('fs-extra');
var _ = require('lodash');


if (require.main === module) {
  var c = config.load();

  if (c.plugins && _.keys(c.plugins).length > 0) {

    // iterate through each plugin
    _.forEach(c.plugins, function(p, name) {
      p.name = name;

      try {
        var res = dircompare.compareSync(p.src, 'src/tabsint_plugins/' + p.name, {compareSize: true});
        res.diffSet.forEach(function (entry) {
          if (entry.state !== 'equal') {
            // if file exists in src/plugins, delete it
            if (entry.state === 'right' && entry.type2 === 'file' &&  entry.path2 !== '/') {
              log.info('Removing ' + entry.path2 + '/' + entry.name2);
              fs.unlinkSync(entry.path2 + '/' + entry.name2);
            }
            // else copy it from cmr_plugins to src/plugins
            else if (entry.type1 === 'file') {
              var destDir = entry.path1.split(p.src)[1] || '';
              var dest = 'src/tabsint_plugins/' + p.name + '/' + destDir;
              log.info('Copying file ' + entry.path1 + '/' + entry.name1 + ' to ' + dest + '/' + entry.name1);
              fs.copySync(entry.path1 + '/' + entry.name1, dest + '/' + entry.name1);
            }
          }
        });
      } catch(e) {
        log.error('Error while comparing plugin directories for plugin:' + p.name + ' - attempting to reinstall', e);
        plugins.installApp(p);
        plugins.writeImportJs();
        plugins.writeImportStyl();
      }

    })
  }
}
