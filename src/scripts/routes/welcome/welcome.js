/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as LZString from "lz-string";

angular
  .module("tabsint.routes.welcome", [])

  .directive("welcomeView", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/routes/welcome/welcome.html",
      controller: "WelcomeCtrl",
      scope: {}
    };
  })

  .controller("WelcomeCtrl", function(
    $q,
    $scope,
    $uibModal,
    adminLogic,
    app,
    autoConfig,
    config,
    devices,
    disk,
    examLogic,
    gettextCatalog,
    logger,
    notifications,
    router
  ) {
    $scope.app = app;
    $scope.devices = devices;
    $scope.disk = disk;
    $scope.router = router;
    $scope.config = config;

    //HG, So the protocol name can be specified on the web-link
    if (app.browser) {
      var protocol = document.URL;
      var field_val = {};
      var indexBegin = protocol.indexOf("?");
      var indexEnd = protocol.length;
      protocol = protocol.substring(indexBegin + 1, indexEnd);
      var splitName = protocol.split(",");

      for (var i = 0; i < splitName.length; i++) {
        var tmp_field_val = splitName[i].split("=");
        field_val[tmp_field_val[0]] = tmp_field_val[1];
      }

      if (field_val.name && field_val.name.indexOf("@") >= 0) {
        var tmp_protocol_name_key = field_val.name.split("@");
        field_val.name = tmp_protocol_name_key[0];
        field_val.key = tmp_protocol_name_key[1];
      }

      if (indexBegin >= 0 && indexEnd >= 0) {
        $scope.disk.protocol = {
          name: field_val.name,
          path: "res/protocol/" + field_val.name + "/",
          creator: "creare",
          server: "developer",
          demo: true,
          field_val: field_val,
          key: field_val.key
        };
        $scope.disk.protocol.siteId = 1;
        $scope.disk.autoUpload = true;
        examLogic.switchToExamView();
      }
    }

    $scope.switchToExamView = function() {
      examLogic.switchToExamView();
    };

    $scope.switchToAdminView = function() {
      adminLogic.switchToAdminView();
    };

    /**
     * Scan Config Qr code and load it into the config object
     */
    $scope.scanQrCodeandAutoConfig = function() {
      function scanQrCode() {
        var deferred = $q.defer();
        var scanner;
        // check that QR code scanner is enabled
        try {
          scanner = cordova.plugins.barcodeScanner;
        } catch (e) {
          logger.error("Could not load barcode scanner: " + angular.toJson(e, true));
          deferred.reject("Could not load barcode scanner with error: " + e);
        }
        scanner.scan(
          function(result) {
            let decompressed;

            // try parsing with JSON first for backwards compatibility
            // if this fails, parse with LZString
            try {
              let c = JSON.parse(result.text);
              if (typeof c !== "object") {
                throw "Failed to parse text to object";
              }
              decompressed = result.text;
            } catch (e) {
              decompressed = LZString.decompressFromBase64(result.text);
            }

            logger.debug("QR Scan: " + decompressed);

            $scope.$apply(function() {
              if (result.cancelled) {
                deferred.reject("Scanning cancelled");
              }

              // confirm we can read object
              try {
                let c = JSON.parse(decompressed);
                if (typeof c !== "object") {
                  deferred.reject("Failed to read configuration");
                }
              } catch (e) {
                deferred.reject("Failed to parse configuration text");
              }

              deferred.resolve(decompressed);
            });
          },
          function(result) {
            $scope.$apply(function() {
              deferred.reject("Scanner failed with error: " + angular.toJson(result, true));
            });
          },
          {
            orientation: "landscape",
            formats: "QR_CODE, PDF_417, CODE_128", // default: all but PDF_417 and RSS_EXPANDED
            disableSuccessBeep: true
          }
        );
        return deferred.promise;
      }

      return scanQrCode()
        .catch(function(e) {
          if (e !== "Scanning cancelled") {
            notifications.alert(
              gettextCatalog.getString("TabSINT failed to read configuration code error: ") + angular.toJson(e, true)
            );
          }

          return $q.reject();
        })
        .then(function(configObject) {
          return config.load(configObject, true);
        }) // Load the configuration Qr Code into the config object
        .then(autoConfig.load); // Act on the config object: apply admin settings, load protocol and exam view
    };

    var DisclaimerInstanceCtrl = function($scope, $uibModalInstance, disk) {
      $scope.ok = function() {
        $uibModalInstance.close();
        disk.init = false;
      };
    };

    app
      .ready()
      .then(function() {
        //HG;so browser don't show disclaimer
        if (disk.init & !app.browser) {
          // only run when the app loads for the first time (when disk.init is true)
          let disclaimerModal = $uibModal.open({
            templateUrl: "scripts/routes/welcome/disclaimer.html",
            controller: DisclaimerInstanceCtrl //is there a reason not to use this simplistic logic?
          });
          return disclaimerModal.result;
        }
      })
      .then(function() {
        if (
          disk.versionCheck == false &&
          devices.platform.toLowerCase() === "android" &&
          parseInt(devices.version) >= 11
        ) {
          notifications.alert(
            `Default file locations in TabSINT 4.4.0 have changed.
Files created by TabSINT are now located in the Documents folder.
Custom folders configured in the Admin panel can also be found in the Documents folder.
Please add your local protocols via the Documents/tabsint-protocols folder.
`
          );
          disk.versionCheck = true;
        }
      });
  });
