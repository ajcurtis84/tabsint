/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

import "./cha-response-areas";
import "./basicResponseAreas";
import "./textbox/textbox";
import "./omt/omt";
import "./multiple-choice/multiple-choice";
import "./button-grid/button-grid";
import "./custom/custom";
import "./checkbox/checkbox";
import "./integer/integer";
import "./mrt/mrt";
import "./likert/likert";
import "./seesaw/seesaw";
import "./qr/qr";
import "./image-map/image-map";
import "./frequency-pattern/frequency-pattern";
import "./tdt/tdt";
import "./multiple-input/multiple-input";
import "./subject-id/subject-id";
import "./audiometry-input/audiometry-input";
import "./nato/nato";
import "./external-app/external-app";
import "./audiometry/audiometry";
import "./dpoae/dpoae";
import "./hint/hint";
import "./third-octave-bands/third-octave-bands";
import "./threshold-check/threshold-check";
import "./three-digit/three-digit";
import "./dichotic-digits/dichotic-digits";
import "./mld/mld";
import "./mpanls/mpanls";
import "./tone-generation/tone-generation";
import "./cha-complex-response-areas/complex-response-areas";
import "./masked-threshold/masked-threshold";
import "./bekesy/bekesy";
import "./results-view/results-view";

angular
  .module("tabsint.components.response-areas", [
    "tabsint.components.response-areas.cha-response-areas",
    "tabsint.components.response-areas.basic-response-areas",
    "tabsint.components.response-areas.textbox",
    "tabsint.components.response-areas.omt",
    "tabsint.components.response-areas.multiple-choice",
    "tabsint.components.response-areas.button-grid",
    "tabsint.components.response-areas.custom",
    "tabsint.components.response-areas.checkbox",
    "tabsint.components.response-areas.integer",
    "tabsint.components.response-areas.mrt",
    "tabsint.components.response-areas.likert",
    "tabsint.components.response-areas.seesaw",
    "tabsint.components.response-areas.qr",
    "tabsint.components.response-areas.results-view",
    "tabsint.components.response-areas.image-map",
    "tabsint.components.response-areas.tdt",
    "tabsint.components.response-areas.multiple-input",
    "tabsint.components.response-areas.subject-id",
    "tabsint.components.response-areas.audiometry-input",
    "tabsint.components.response-areas.nato",
    "tabsint.components.response-areas.mpanls",
    "tabsint.components.response-areas.external-app",
    "tabsint.components.response-areas.cha.frequency-pattern",
    "tabsint.components.response-areas.cha.audiometry",
    "tabsint.components.response-areas.cha.dpoae",
    "tabsint.components.response-areas.cha.hint",
    "tabsint.components.response-areas.cha.third-octave-bands",
    "tabsint.components.response-areas.cha.three-digit",
    "tabsint.components.response-areas.cha.dichotic-digits",
    "tabsint.components.response-areas.cha.threshold-check",
    "tabsint.components.response-areas.cha.mld",
    "tabsint.components.response-areas.cha.tone-generation",
    "tabsint.components.response-areas.cha.complex-response-areas",
    "tabsint.components.response-areas.cha.masked-threshold",
    "tabsint.components.response-areas.bekesy"
  ])

  .factory("responseAreas", function() {
    var api = {
      list: undefined,
      native: {},
      plugins: {}
    };

    api.native = {
      textboxResponseArea: {
        templateUrl: "scripts/components/response-areas/textbox/textbox.html"
      },
      omtResponseArea: {
        templateUrl: "scripts/components/response-areas/omt/omt.html"
      },
      multipleChoiceSelectionResponseArea: {
        templateUrl: "scripts/components/response-areas/omt/omt.html"
      },
      multipleChoiceResponseArea: {
        templateUrl: "scripts/components/response-areas/multiple-choice/multiple-choice.html"
      },
      buttonGridResponseArea: {
        templateUrl: "scripts/components/response-areas/button-grid/button-grid.html"
      },
      checkboxResponseArea: {
        templateUrl: "scripts/components/response-areas/checkbox/checkbox.html"
      },

      // NOTE: this is here for backwards compatibility
      mpanls: {
        templateUrl: "scripts/components/response-areas/mpanls/mpanls.html"
      },
      mpanlResponseArea: {
        templateUrl: "scripts/components/response-areas/mpanls/mpanls.html"
      },
      integerResponseArea: {
        templateUrl: "scripts/components/response-areas/integer/integer.html"
      },
      mrtResponseArea: {
        templateUrl: "scripts/components/response-areas/mrt/mrt.html"
      },
      likertResponseArea: {
        templateUrl: "scripts/components/response-areas/likert/likert.html"
      },
      seeSawResponseArea: {
        templateUrl: "scripts/components/response-areas/seesaw/seesaw.html"
      },
      qrResponseArea: {
        // may want to test qr code on a tablet
        templateUrl: "scripts/components/response-areas/qr/qr.html"
      },
      imageMapResponseArea: {
        templateUrl: "scripts/components/response-areas/image-map/image-map.html"
      },
      threeDigitTestResponseArea: {
        // need to find protocol to test this
        templateUrl: "scripts/components/response-areas/tdt/tdt.html"
      },
      multipleInputResponseArea: {
        templateUrl: "scripts/components/response-areas/multiple-input/multiple-input.html"
      },
      customResponseArea: {
        // where's the controller? (make a folder with only view)
        templateUrl: "scripts/components/response-areas/custom/custom.html"
      },
      subjectIdResponseArea: {
        // What to do with directive? (delete directive, copy controller)
        templateUrl: "scripts/components/response-areas/subject-id/subject-id.html"
      },
      yesNoResponseArea: {
        // Just use multiple-choice for this? (just change template to be same as multiple choice)
        templateUrl: "scripts/components/response-areas/multiple-choice/multiple-choice.html"
      },
      audiometryInputResponseArea: {
        templateUrl: "scripts/components/response-areas/audiometry-input/audiometry-input.html"
      },
      externalAppResponseArea: {
        templateUrl: "scripts/components/response-areas/external-app/external-app.html"
      },
      natoResponseArea: {
        templateUrl: "scripts/components/response-areas/nato/nato.html"
      },
      bekesyResponseArea: {
        templateUrl: "scripts/components/response-areas/bekesy/bekesy.html"
      },

      // see cha-response-areas.html for where some of these templates are further customized
      chaBekesyLike: {
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      chaBHAFT: {
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      chaBekesyMLD: {
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      chaFrequencyPattern: {
        // need to find protocol to test this
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      chaThreeDigit: {
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      chaDichoticDigits: {
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      chaMLD: {
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      chaHughsonWestlake: {
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      chaAcceleratedThreshold: {
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      chaThirdOctaveBands: {
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      chaToneGeneration: {
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      chaDPOAE: {
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      chaHINT: {
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      chaGAP: {
        templateUrl: "scripts/components/response-areas/cha-complex-response-areas/complex-response-areas.html"
      },
      chaTRT: {
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      chaManualAudiometry: {
        templateUrl: "scripts/components/response-areas/cha-complex-response-areas/complex-response-areas.html"
      },
      chaManualAudiometryExam: {
        templateUrl: "scripts/components/response-areas/cha-complex-response-areas/complex-response-areas.html"
      },
      //TODO chaRecordFile: {
      //   templateUrl: "scripts/components/response-areas/cha-complex-response-areas/complex-response-areas.html"
      // },
      chaManualScreener: {
        templateUrl: "scripts/components/response-areas/cha-complex-response-areas/complex-response-areas.html"
      },
      chaPlaysoundArray: {
        templateUrl: "scripts/components/response-areas/cha-complex-response-areas/complex-response-areas.html"
      },
      chaManualToneGeneration: {
        templateUrl: "scripts/components/response-areas/cha-complex-response-areas/complex-response-areas.html"
      },
      chaAudiometryResultsPlot: {
        templateUrl: "scripts/components/response-areas/audiometry/audiogram/audiogram.html"
      },
      chaAudiometryResultsTable: {
        templateUrl: "scripts/components/response-areas/audiometry/audiometry-table/audiometry-table.html"
      },
      chaCalibrationCheck: {
        templateUrl: "scripts/components/response-areas/cha-complex-response-areas/complex-response-areas.html"
      },
      chaMaskedThreshold: {
        templateUrl: "scripts/components/response-areas/cha-response-areas.html"
      },
      resultsViewResponseArea: {
        templateUrl: "scripts/components/response-areas/results-view/results-view.html"
      }
    };

    api.all = function() {
      return _.extend(api.plugins, api.native);
    };

    return api;
  })

  .directive("responseArea", function() {
    return {
      restrict: "E",
      scope: {}
    };
  })

  .controller("ResponseAreaCtrl", function($scope, responseAreas) {
    this.responseAreas = responseAreas.all();
    //this.exists = function(type) {
    //  return _.includes(_.keys(responseAreas.all()), type);
    //};
  });
