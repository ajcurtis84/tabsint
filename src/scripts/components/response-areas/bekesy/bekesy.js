/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.components.response-areas.bekesy", [])

  .controller("BekesyResponseAreaCtrl", function($scope, page, media, examLogic, devices, disk) {
    if ("activeInterval" in page.dm.responseArea) {
      clearInterval(page.dm.responseArea.activeInterval);
      console.log("CLEARING OLD INTERVAL");
    }

    if (typeof bekesy_Interval !== "undefined") {
      clearInterval(bekesy_Interval);
      console.log("CLEARING OLD INTERVAL");
    }

    $scope.bClass = "buttonReleased";

    if (page.dm.responseArea.enableSubmit === undefined) {
      page.dm.responseArea.enableSubmit = false;
    }

    if (page.dm.responseArea.buttonBehavior === undefined) {
      page.dm.responseArea.buttonBehavior = "lowerOnClick";
    }

    if (page.dm.responseArea.saturatedRollOver === undefined) {
      page.dm.responseArea.saturatedRollOver = true;
    }

    var bekesy_channel = 0;
    var bekesey_splLevel;
    var bekesey_splLevel_requested;
    var bekesey_fixed_volLevel;
    var bekesey_fixed_splLevel;
    var bekesey_fixed_splLevel_saturated;
    var bekesy_wavfile = page.dm.wavfiles[0];

    //for stereo updating only
    var saturated_flag = false;

    //HG;9/24/20 Adding lookup inter-speaker correction
    var lookUpCorrection = 0;

    var minCorrection = 0;
    var maxCorrection = 0;
    var correction = {
      0: 0
    };
    if (page.dm.responseArea.lookUpCorrection !== undefined) {
      var ILD_values = page.dm.responseArea.lookUpCorrection.map(function(elt) {
        return elt[0];
      });
      minCorrection = Math.min.apply(null, ILD_values);
      maxCorrection = Math.max.apply(null, ILD_values);
      correction = {};
      for (var i = 0; i < page.dm.responseArea.lookUpCorrection.length; i++) {
        correction[i] = page.dm.responseArea.lookUpCorrection[i][0] - page.dm.responseArea.lookUpCorrection[i][1];
      }
    }

    if (page.dm.responseArea.channel !== undefined) {
      if (page.dm.responseArea.channel.toLowerCase() == "left") {
        bekesy_channel = 1;
        page.dm.responseArea.channel = "left";
        bekesey_splLevel = page.dm.responseArea.startSPL[0];

        bekesy_wavfile.targetSPL = page.dm.responseArea.startSPL[1];
        bekesey_fixed_volLevel = media.calculateVolume(bekesy_wavfile);
        bekesey_fixed_splLevel = bekesy_wavfile.targetSPL;
      }
      if (page.dm.responseArea.channel.toLowerCase() == "right") {
        bekesy_channel = 2;
        page.dm.responseArea.channel = "right";
        bekesey_splLevel = page.dm.responseArea.startSPL[1];

        bekesy_wavfile.targetSPL = page.dm.responseArea.startSPL[0];
        bekesey_fixed_volLevel = media.calculateVolume(bekesy_wavfile);
        bekesey_fixed_splLevel = bekesy_wavfile.targetSPL;
      }
      var bekesey_fixed_volLevel_unsaturated = bekesey_fixed_volLevel;
    } else {
      page.dm.responseArea.channel = "mono";
      bekesey_splLevel = page.dm.responseArea.startSPL || bekesy_wavfile.targetSPL;
      bekesey_fixed_splLevel = NaN;
    }
    bekesey_splLevel_requested = bekesey_splLevel;

    $scope.bekesey_splLevel = bekesey_splLevel;

    if (page.dm.responseArea.buttonText === undefined) {
      page.dm.responseArea.buttonText = "Press and Hold";
    }
    $scope.bText = page.dm.responseArea.buttonText;

    if (page.dm.responseArea.buttonPressedText === undefined) {
      page.dm.responseArea.buttonPressedText = "Press and Hold";
    }

    if (page.dm.responseArea.buttonReleasedText === undefined) {
      page.dm.responseArea.buttonReleasedText = "Press and Hold";
    }

    if (page.dm.responseArea.buttonAlign === undefined) {
      page.dm.responseArea.buttonAlign = "center";
    }
    $scope.div_style = {
      "text-align": page.dm.responseArea.buttonAlign
    };

    var bekesy_direction = 1;
    if (page.dm.responseArea.buttonBehavior == "higherOnClick") {
      bekesy_direction = -1;
    }

    if (page.dm.responseArea.enableSubmit) {
      page.dm.isSubmittable = true;
    }

    if (page.dm.responseArea.minTargetLevel === undefined) {
      page.dm.responseArea.minTargetLevel = 40;
    }

    if (page.dm.responseArea.maxTargetLevel === undefined) {
      page.dm.responseArea.maxTargetLevel = 85;
    }

    //HG; code to calculate max level without clipping
    bekesy_wavfile.volume = 1;
    var maxTargetLevel = calculateSPL(bekesy_wavfile, disk);
    if (maxTargetLevel < page.dm.responseArea.maxTargetLevel) {
      page.dm.responseArea.maxTargetLevel = maxTargetLevel;
    }

    if (page.dm.responseArea.timeout === undefined) {
      page.dm.responseArea.timeout = 180;
    }

    if (page.dm.responseArea.levelRate === undefined) {
      page.dm.responseArea.levelRate = 2;
    }

    var bekesy_reponse = [];

    var bekesy_Interval;
    var bekesy_refreshInterval = 25;

    //HG;8/14/20 per CD and DB request level will start to change only after first click
    var bekesy_stepSize = 0;

    if (page.dm.responseArea.numberReversals === undefined) {
      page.dm.responseArea.numberReversals = 6;
    }

    var bekesy_timeout_timer = setTimeout(
      function() {
        bekesy_reponse.push({
          splLevel: bekesey_splLevel,
          time: getTimeStr(),
          button: -1
        });
        $scope.clickSubmit(0);
      },
      1000 * page.dm.responseArea.timeout,
      $scope
    ); // if ($scope.flags.order==undefined){

    var bekesy_startDelayTime = angular.isDefined(page.dm.wavfileStartDelayTime) ? page.dm.wavfileStartDelayTime : 1000; // use default delay of 1000ms if no time specified

    var bekesy_delay_play_wav = setTimeout(function() {
      bekesy_Interval = setInterval(updateLevelInterval, bekesy_refreshInterval, media);
      page.dm.responseArea.activeInterval = bekesy_Interval;
    }, bekesy_startDelayTime); // if ($scope.flags.order==undefined){

    var reversals = -1; //first two does not count

    document.addEventListener(
      "deviceready",
      function() {
        onDeviceReady($scope);
      },
      false
    );

    function onDeviceReady($scope) {
      var box1 = document.getElementById("bekesy_button");

      if (devices.platform.toLowerCase() === "android") {
        box1.addEventListener(
          "touchstart",
          function(e) {
            touchstartFun($scope);
          },
          false
        );
        box1.addEventListener(
          "touchend",
          function(e) {
            touchendFun($scope);
          },
          false
        );
        box1.addEventListener(
          "touchmove",
          function(e) {
            touchmoveFun($scope);
          },
          false
        );
      } else {
        box1.addEventListener(
          "mousedown",
          function(e) {
            touchstartFun($scope);
          },
          false
        );
        box1.addEventListener(
          "mouseup",
          function(e) {
            touchendFun($scope);
          },
          false
        );
      }
    }

    function touchstartFun($scope) {
      //HG;8/14/20 per CD and DB request level will start to change only after first click
      if (bekesy_stepSize == 0) {
        bekesy_stepSize = page.dm.responseArea.levelRate * (bekesy_refreshInterval / 1000);
      }

      $scope.bClass = "buttonPressed";
      $scope.bText = page.dm.responseArea.buttonPressedText;

      bekesy_direction *= -1;
      reversals++;

      if (saturated_flag) {
        bekesy_reponse.push({
          splLevel: bekesey_splLevel,
          splLevelRequested: bekesey_splLevel_requested,
          splLevelFixed: bekesey_fixed_splLevel_saturated,
          time: getTimeStr(),
          button: 1,
          lookUpCorrection: lookUpCorrection //HG;9/24/20 Adding lookup inter-speaker correction
        });
      } else {
        bekesy_reponse.push({
          splLevel: bekesey_splLevel,
          splLevelFixed: bekesey_fixed_splLevel,
          time: getTimeStr(),
          button: 1,
          lookUpCorrection: lookUpCorrection //HG;9/24/20 Adding lookup inter-speaker correction
        });
      }
      $scope.bekesey_splLevel = bekesey_splLevel;
      $scope.$digest();
    }

    function touchmoveFun($scope) {
      $scope.bClass = "buttonPressed";
      $scope.bText = page.dm.responseArea.buttonPressedText;

      $scope.$digest();
    }

    function touchendFun($scope) {
      $scope.bClass = "buttonReleased";
      $scope.bText = page.dm.responseArea.buttonReleasedText;

      bekesy_direction *= -1;
      reversals++;

      if (reversals >= page.dm.responseArea.numberReversals) {
        $scope.clickSubmit(0);
      }

      // if (trueFalseVal[$scope.flags.expStepSize]) {
      // stepSize /= $scope.flags.stepSizeAdjustmentFactor;
      // }

      if (saturated_flag) {
        bekesy_reponse.push({
          splLevel: bekesey_splLevel,
          splLevelRequested: bekesey_splLevel_requested,
          splLevelFixed: bekesey_fixed_splLevel_saturated,
          time: getTimeStr(),
          button: 0,
          lookUpCorrection: lookUpCorrection //HG;9/24/20 Adding lookup inter-speaker correction
        });
      } else {
        bekesy_reponse.push({
          splLevel: bekesey_splLevel,
          splLevelFixed: bekesey_fixed_splLevel,
          time: getTimeStr(),
          button: 0,
          lookUpCorrection: lookUpCorrection //HG;9/24/20 Adding lookup inter-speaker correction
        });
      }
      $scope.bekesey_splLevel = bekesey_splLevel;
      $scope.$digest();
    }

    function getTimeStr() {
      var time = new Date();
      var hh = time.getHours();
      var mm = time.getMinutes();
      var ss = time.getSeconds();
      var ff = time.getMilliseconds();
      return hh + ":" + mm + ":" + ss + ":" + ff;
    }

    $scope.clickSubmit = function(val) {
      clearTimeout(bekesy_timeout_timer);
      clearTimeout(bekesy_delay_play_wav);
      clearInterval(page.dm.responseArea.activeInterval);
      delete page.dm.responseArea.activeInterval;
      page.result.response = bekesy_reponse;
      examLogic.submit = examLogic.submitDefault;
      examLogic.submit();
    };

    examLogic.submit = submitButton;

    function submitButton() {
      //HG;9/24/20 Adding lookup inter-speaker correction
      if (saturated_flag) {
        bekesy_reponse.push({
          splLevel: bekesey_splLevel,
          splLevelRequested: bekesey_splLevel_requested,
          splLevelFixed: bekesey_fixed_splLevel_saturated,
          time: getTimeStr(),
          button: -2,
          lookUpCorrection: lookUpCorrection
        });
      } else {
        bekesy_reponse.push({
          splLevel: bekesey_splLevel,
          splLevelFixed: bekesey_fixed_splLevel,
          time: getTimeStr(),
          button: -2,
          lookUpCorrection: lookUpCorrection
        });
      }
      $scope.clickSubmit(0);
    }

    function updateLevelInterval(media) {
      if (media.instances.length > 0) {
        var current_spl = bekesey_splLevel;
        bekesey_splLevel = bekesey_splLevel + bekesy_stepSize * bekesy_direction;

        if (saturated_flag) {
          bekesey_splLevel = bekesey_splLevel_requested + bekesy_stepSize * bekesy_direction;
          bekesey_splLevel_requested = bekesey_splLevel;
        } else {
          bekesey_splLevel_requested = bekesey_splLevel;
          bekesey_fixed_volLevel = bekesey_fixed_volLevel_unsaturated;
        }

        if (bekesey_splLevel_requested < page.dm.responseArea.maxTargetLevel) {
          saturated_flag = false;
        }

        //HG;9/24/20 Adding lookup inter-speaker correction, THIS LINE WILL ALWAYS EXECUTE THOUGH IT MAY BE OVERWRITTING BELLOW

        lookUpCorrection = correctionTable(bekesey_splLevel_requested - bekesey_fixed_splLevel);

        if (bekesy_channel == 1) {
          bekesy_wavfile.targetSPL = page.dm.responseArea.startSPL[1] - lookUpCorrection;
          bekesey_fixed_volLevel = media.calculateVolume(bekesy_wavfile);
        }
        if (bekesy_channel == 2) {
          bekesy_wavfile.targetSPL = page.dm.responseArea.startSPL[0] - lookUpCorrection;
          bekesey_fixed_volLevel = media.calculateVolume(bekesy_wavfile);
        }

        if (bekesey_splLevel < page.dm.responseArea.minTargetLevel) {
          bekesey_splLevel = page.dm.responseArea.minTargetLevel;
        } else if (bekesey_splLevel > page.dm.responseArea.maxTargetLevel) {
          bekesey_splLevel = page.dm.responseArea.maxTargetLevel;

          if ((bekesy_channel > 0) & page.dm.responseArea.saturatedRollOver) {
            saturated_flag = true;
            var delta_max = bekesey_splLevel_requested - page.dm.responseArea.maxTargetLevel;
            if (bekesy_channel == 1) {
              bekesy_wavfile.targetSPL = page.dm.responseArea.startSPL[1] - delta_max - lookUpCorrection;
              bekesey_fixed_splLevel_saturated = page.dm.responseArea.startSPL[1] - delta_max;
              bekesey_fixed_volLevel = media.calculateVolume(bekesy_wavfile);
            }
            if (bekesy_channel == 2) {
              //HG;9/24/20 Adding lookup inter-speaker correction
              lookUpCorrection = correctionTable(delta_max);
              bekesy_wavfile.targetSPL = page.dm.responseArea.startSPL[0] - delta_max - lookUpCorrection;
              bekesey_fixed_splLevel_saturated = page.dm.responseArea.startSPL[0] - delta_max;
              bekesey_fixed_volLevel = media.calculateVolume(bekesy_wavfile);
            }
          }
        }

        bekesy_wavfile.targetSPL = bekesey_splLevel;
        var tmp_vol = media.calculateVolume(bekesy_wavfile);
        //PREVENT CLIPPING AND INCORRECT STORING OF LEVEL
        if (tmp_vol > 1) {
          bekesey_splLevel = current_spl;
          tmp_vol = 1;
        }

        if (bekesy_channel == 0) {
          media.instances[0].instance.setVolume(tmp_vol);
        } else if (bekesy_channel == 1) {
          media.instances[0].instance.setVolume([tmp_vol, bekesey_fixed_volLevel]);
        } else if (bekesy_channel == 2) {
          media.instances[0].instance.setVolume([bekesey_fixed_volLevel, tmp_vol]);
        }
        $scope.bekesey_splLevel = bekesey_splLevel;
      } else {
        // clearInterval(page.dm.responseArea.activeInterval);
        clearInterval(bekesy_Interval);
        clearTimeout(bekesy_timeout_timer);
        clearTimeout(bekesy_delay_play_wav);

        delete page.dm.responseArea.activeInterval;
      }

      $scope.$digest();
    }

    //HG;based on calculateVolume from media.js
    function calculateSPL(wavfile, disk) {
      var inputFlag = false,
        specifiedPaRMS,
        waveformRMS,
        level,
        method,
        weighting,
        targetSPL;

      method = wavfile.playbackMethod || "arbitrary"; // default to arbitrary.
      weighting = wavfile.weighting || "Z"; // default to Z weighting

      if (method === "arbitrary") {
        waveformRMS = wavfile.volume * wavfile.cal["wavRMS" + weighting];
        specifiedPaRMS = waveformRMS / wavfile.cal.scaleFactor;
        level = 20 * Math.log10(specifiedPaRMS / 20e-6);
      }
      //TODO: Dynamically calculate gain based on wavfile
      targetSPL = level - disk.tabletGain; // tablet specific gain: measured jack output sound level difference from the Nexus 7

      return targetSPL;
    }

    function correctionTable(delta) {
      delta = Math.round(delta);

      if (delta < minCorrection) {
        delta = minCorrection;
      }
      if (delta > maxCorrection) {
        delta = maxCorrection;
      }

      return correction[delta];
    }
  });
