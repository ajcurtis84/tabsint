/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.response-areas.custom", [])

  .controller("CustomResponseAreaCtrl", function($scope, examLogic, disk, page, results) {
    // set the submittable logic to always be true
    page.dm.isSubmittable = true;

    // Expose useful bits, deep copy to protect data
    $scope.responses = angular.copy(results.current.testResults.responses); // actual results[]: response, correct, eachCorrect, numberCorrect, numberIncorrect
    $scope.tabletLocation = angular.copy(disk.tabletLocation); //gps coords: latitude, longitude
    $scope.site = angular.copy(disk.site); //  tablet info: siteName, siteId, protocolId, protocolHash, protocolName, protocolCreationDate, protocolOwner

    // Expose dm, without copy, so it can be modified (Careful!)
    $scope.flags = examLogic.dm.state.flags;

    $scope.loadSuccess = true; // these should be set in some background factory
    $scope.loadError = false;

    // get the javascript and html file names from the protocol
    $scope.js = page.dm.responseArea.js;
    $scope.html = page.dm.responseArea.html;

    // Convience data for using in custom response areas
    $scope.tabResults = function() {
      $scope.nCorrect = 0;
      $scope.nIncorrect = 0;
      $scope.nResponses = 0;
      $scope.responses = [];

      for (var i = 0; i < results.current.testResults.responses.length; i++) {
        $scope.responses[i] = results.current.testResults.responses[i];
        $scope.nResponses += 1;

        if ($scope.responses[i].correct === true) {
          $scope.nCorrect += 1;
        } else if ($scope.responses[i].correct === false) {
          $scope.nIncorrect += 1;
        }
      }
    };
  });
