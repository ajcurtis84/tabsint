/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "./complex-response-areas.ctrl";
import "./manual-audiometry/manual-audiometry";
//TODO import "./record-file/record-file";
import "./manual-audiometry/manual-audiometry-exam";
import "./manual-screener/manual-screener";
import "./manual-tone-generation/manual-tone-generation";
import "./playsound-array/playsound-array";
import "./gap/gap";
import "./calibration-check/calibration-check";

angular.module("tabsint.components.response-areas.cha.complex-response-areas", [
  "cha.complex-response-areas.ctrl",
  "cha.manual-audiometry",
  //TODO "cha.record-file",
  "cha.manual-audiometry-exam",
  "cha.manual-screener",
  "cha.manual-tone-generation",
  "cha.playsound-array",
  "cha.gap",
  "cha.calibration-check"
]);
