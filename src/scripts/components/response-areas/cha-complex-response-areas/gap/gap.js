/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "../../audiometry/software-button/software-button";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("cha.gap", ["cha.software-button"])

  .directive("gapExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/cha-complex-response-areas/gap/gap.html",
      controller: "gapExamCtrl"
    };
  })
  .controller("gapExamCtrl", function(
    $interval,
    $q,
    $scope,
    $timeout,
    cha,
    chaAudiometryService,
    chaExams,
    chaResults,
    examLogic,
    logger,
    notifications,
    page
  ) {
    chaExams.setup(chaExams.complexExamType, page.dm.responseArea.examProperties);
    $scope.useSoftwareButton = chaExams.examProperties.UseSoftwareButton;
    $scope.gapTraining = page.dm.responseArea.training;
    $scope.gapLengths = page.dm.responseArea.trainingAllowableGapLengths;
    if (
      page.dm.responseArea.training &&
      (!$scope.gapLengths || ($scope.gapLengths && $scope.gapLengths.length !== 5))
    ) {
      $scope.gapLengths = [40, 20, 12, 6, 0];
    }
    $scope.gapState = "start";
    $scope.noiseLevel = 65;
    if (!$scope.gapTraining) {
      $scope.startGapExam(undefined, undefined, true);
    }
    // Canvas Styling for training GUI
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    resetCanvas();
    canvas.style.width = "90%";
    canvas.style.height = "20%";
    canvas.width = canvas.offsetWidth;

    // Initialize training GUI variables
    const x0 = 0, // Training GUI start x position
      y0 = 0, // TrainingGUI start y position
      w = 2, // Width of the moving sound tick mark
      speed = 30; // Refresh rate of the GAP Training animation, in msec
    var hitOrMiss, windowPos, windowPosEnd, windowWidth, gapPos, gapWidth, playPos, x, draw, timePres;

    $scope.startExam = function(examType, examProperties) {
      return cha
        .requestStatusBeforeExam()
        .then(switchToExamView)
        .then(startNoiseIfCalledFor)
        .then(function() {
          return cha.queueExam(examType, examProperties).then(function() {
            if ($scope.gapTraining) {
              return cha.requestResults().then(
                function(results) {
                  initializeGapTraining(results, examProperties);
                  $timeout(function() {
                    return cha.requestResults().then(function(resFromCha) {
                      var deferred = $q.defer();
                      if ($scope.gapTraining) {
                        hitOrMiss = resFromCha.HitOrMiss;
                      }
                      deferred.resolve(resFromCha);
                      return deferred.promise;
                    });
                  }, (windowPosEnd * timePres) / canvas.width + 50);
                },
                function(err) {
                  $q.reject(err);
                }
              );
            }
          });
        })
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    };

    $scope.startGapExam = function(gapLength, noiseLevel, fullExam) {
      var examProperties = {};
      if (!$scope.gapTraining || fullExam) {
        // actually queue the exam.
        $scope.gapTraining = false; // disable the training section
        examProperties = chaExams.examProperties;
      } else {
        // queue training exam
        examProperties.Channel = angular.isDefined(page.dm.responseArea.examProperties.Channel)
          ? page.dm.responseArea.examProperties.Channel
          : 0;
        examProperties.TimePres = angular.isDefined(page.dm.responseArea.examProperties.TimePres)
          ? page.dm.responseArea.examProperties.TimePres
          : 4000;
        examProperties.TimeLead = angular.isDefined(page.dm.responseArea.examProperties.TimeLead)
          ? page.dm.responseArea.examProperties.TimeLead
          : 1000;
        examProperties.TimeTrail = angular.isDefined(page.dm.responseArea.examProperties.TimeTrail)
          ? page.dm.responseArea.examProperties.TimeTrail
          : 1000;
        examProperties.TimeWindow = angular.isDefined(page.dm.responseArea.examProperties.TimeWindow)
          ? page.dm.responseArea.examProperties.TimeWindow
          : 900;
        examProperties.TimeNoResp = angular.isDefined(page.dm.responseArea.examProperties.TimeNoResp)
          ? page.dm.responseArea.examProperties.TimeNoResp
          : 50;
        examProperties.UseSoftwareButton = 1;
        examProperties.AllowableGapLengths = [gapLength];
        examProperties.LNoise = noiseLevel;

        examProperties.GapLengthStartIndex = 0;
        examProperties.NReversals = 1;
        examProperties.NReversalsCalc = 1;
        examProperties.NLowestReversals = 0;
        examProperties.NPresMax = 1;
        examProperties.NHits = 1;
        examProperties.NMiss = 1;
        examProperties.SendFullResults = 2;
        timePres = examProperties.TimePres;
      }
      resetCanvas();
      $scope.gapState = "exam";
      chaExams.resetPage();
      chaAudiometryService.reset();
      chaAudiometryService.examDone = false;

      $scope
        .startExam(chaExams.examType, examProperties)
        .then(chaExams.wait.forReadyState)
        .then(function() {
          chaAudiometryService.examDone = true;
          // check to make sure we have not paused.  If state is still exam, request results
          if (chaExams.state === "exam") {
            return cha
              .requestResults()
              .then(chaAudiometryService.processResults)
              .then(function() {
                var deferred = $q.defer();
                delete page.result.ResultType;
                delete page.result.response;
                $scope.result = page.result;

                $scope.gapState = "results";
                chaExams.state = "results";
                if (!$scope.gapTraining) {
                  $scope.gapResultsData = chaResults.createGapData($scope.result);
                  page.dm.isSubmittable = true;
                  if (page.dm.responseArea.autoSubmit) {
                    examLogic.submit();
                  }
                }
                return deferred.promise;
              })
              .catch(function(err) {
                if (err.msg !== "repeating exam") {
                  cha.errorHandler.main(err);
                }
              })
              .finally(function() {
                $scope.gapState = "results";
              });
          }
        });
    };

    function startGapTraining() {}

    function switchToExamView() {
      chaExams.state = "exam";
      // reset the MainText and SubText to the original page. This will happen when a test is failed and the person repeats the test
      $scope.page.dm.questionMainText = page.dm.questionMainText;
      $scope.page.dm.questionSubText = page.dm.questionSubText;
      page.dm.instructionText = page.dm.examInstructions || page.dm.responseArea.examInstructions; // custom instructions for each response area - blank if no instructions provided
    }

    function startNoiseIfCalledFor() {
      if (angular.isDefined(page.dm.responseArea.maskingNoise)) {
        cha.startNoiseFeature(page.dm.responseArea.maskingNoise);
      }
    }

    function initializeGapTraining(res, examProperties) {
      // Initialize variables
      canvas.height = 80;
      playPos = res.PlayPosition; // Time elapsed since beginning of noise presentation
      x = (playPos * canvas.width) / examProperties.TimePres; // GUI current x position
      timePres = examProperties.TimePres; // Total time of the GAP presentation
      gapPos = (res.CurrentGapStartTime * canvas.width) / examProperties.TimePres; // Position of the gap within the noise, in px
      gapWidth = (examProperties.AllowableGapLengths[0] * canvas.width) / examProperties.TimePres; // Width of the gap, in px
      windowPos =
        gapPos +
        ((examProperties.AllowableGapLengths[0] + examProperties.TimeNoResp) * canvas.width) / examProperties.TimePres; // Position of the beginning of the time window, in px
      windowPosEnd = windowPos + (examProperties.TimeWindow * canvas.width) / examProperties.TimePres; // Position of the end of the time window, in px
      windowWidth = (examProperties.TimeWindow * canvas.width) / examProperties.TimePres; // width of the response window, in px

      draw = setInterval(gapAnimation, speed);
    }

    function gapAnimation() {
      // Draw sound GUI up to current position
      ctx.fillStyle = "#ddd"; // background noise
      ctx.fillRect(x0, y0, canvas.width, canvas.height);

      ctx.fillStyle = "#fff"; // GAP
      ctx.fillRect(gapPos, y0, gapWidth, canvas.height);

      if (hitOrMiss === 1) {
        // Response window
        ctx.fillStyle = "#4cae4c";
      } else if (hitOrMiss === 0) {
        ctx.fillStyle = "#d43f3a";
      } else {
        ctx.fillStyle = "#666";
      }
      ctx.fillRect(windowPos, y0, windowWidth, canvas.height);

      ctx.fillStyle = "#000"; // Current position tick mark
      ctx.fillRect(x, y0, w, canvas.height);
      x = x + (speed * canvas.width) / timePres;

      if (x > canvas.width) {
        clearInterval(draw);
      }
    }

    function resetCanvas() {
      canvas.style.height = "0%";
      canvas.height = 0;
      hitOrMiss = undefined;
    }
  })

  .directive("gapResultsPlot", function() {
    return {
      restrict: "E",
      template: '<div id="gapResultsPlotWindow"></div>',
      controller: "GapResultsPlotCtrl",
      scope: {
        data: "="
      }
    };
  })

  .controller("GapResultsPlotCtrl", function($scope, d3Services) {
    d3Services.gapResultsPlot("#gapResultsPlotWindow", $scope.data);
  });
