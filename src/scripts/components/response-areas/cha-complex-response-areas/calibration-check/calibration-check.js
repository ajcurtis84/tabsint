/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import { Number } from "es6-shim";

angular
  .module("cha.calibration-check", [])
  .directive("calibrationCheckExam", function() {
    return {
      restrict: "E",
      templateUrl:
        "scripts/components/response-areas/cha-complex-response-areas/calibration-check/calibration-check.html",
      controller: "calibrationCheckCtrl"
    };
  })
  .controller("calibrationCheckCtrl", function($scope, $q, cha, page, chaExams, logger) {
    $scope.Math = Math;
    $scope.Number = Number;
    // this is defined for Results View
    let resultsView = $scope.state === "results" && angular.isDefined($scope.calibrationData);
    if (!resultsView) {
      $scope.state = "start";
    }
    let frequencies = [
      125,
      250,
      500,
      750,
      1000,
      1500,
      2000,
      3000,
      4000,
      6000,
      8000,
      9000,
      10000,
      11200,
      12500,
      14000,
      16000
    ]; // later map to indices
    if (angular.isUndefined(chaExams.complexExamType)) {
      // this is undefined for Results View
      chaExams.complexExamType = "CalibrationCheck";
    }
    chaExams.setup(chaExams.complexExamType, page.dm.responseArea.examProperties);
    let calData = {
      left: {
        xLabel: "Frequency (Hz)",
        yLabel: "Deviation from baseline (dB)",
        title: "Left Ear Calibration Results"
      },
      right: {
        xLabel: "Frequency (Hz)",
        yLabel: "Deviation from baseline (dB)",
        title: "Right Ear Calibration Results"
      }
    };
    // this is defined for Results View
    if (resultsView) {
      $scope.data = $scope.calibrationData;
    } else {
      $scope.calibrationData = calData;
    }

    $scope.startCalibrationCheckExam = function() {
      $scope.state = "running";
      return cha
        .requestCalibrationEntry(0)
        .then(function(calResult) {
          calData.left.calibration = calResult;
        })
        .then(function() {
          return cha.requestCalibrationEntry(2); // Right speaker
        })
        .then(function(calResult) {
          calData.right.calibration = calResult;
        })
        .then(cha.requestStatusBeforeExam)
        .then(function() {
          // Queue chirp for left ear
          chaExams.examProperties.Speaker = "HPL0"; // Left speaker
          chaExams.examProperties.Input = "SMICR0";
          return cha.queueExam("Chirp", chaExams.examProperties);
        })
        .then(function() {
          let deferred = $q.defer();
          // set a failSafe timeout
          let failSafe = setTimeout(function() {
            if (deferred) {
              deferred.reject("the failSafe timeout");
            }
          }, 15000);
          // Do recursive request/process.
          process(calData.left, deferred, failSafe);
          return deferred.promise;
        })
        .then(cha.requestStatusBeforeExam)
        .then(function() {
          // Queue chirp for right ear
          chaExams.examProperties.Speaker = "HPR0"; // Right speaker
          chaExams.examProperties.Input = "SMICR1";
          return cha.queueExam("Chirp", chaExams.examProperties);
        })
        .then(function() {
          let deferred = $q.defer();
          // set a failSafe timeout
          let failSafe = setTimeout(function() {
            if (deferred) {
              deferred.reject("the failSafe timeout");
            }
          }, 15000);
          // Do recursive request/process.
          process(calData.right, deferred, failSafe);
          return deferred.promise;
        })
        .then(function() {
          $scope.state = "results";
          page.result.calibrationData = calData;
          page.result.chaInfo = chaExams.getChaInfo();
          page.dm.isSubmittable = true;
        })
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    };

    function process(side, deferred, failSafe) {
      let repeatTimeout = undefined;
      cha.errorHandler.responseArea = function(err) {
        if (err.msg) {
          if (err.code === 1) {
            // Want to keep requesting results
            logger.debug("In responseArea error handler with code 1, will repeat...");
            repeatTimeout = setTimeout(process(side, deferred), 1000);
          }
        }
      };
      cha
        .requestResults()
        .catch(function(err) {
          // Caught error requesting results from cha, repeat after a second.
          logger.error("Caught error requesting results from cha. Will repeat after a second.");
          setTimeout(process(side, deferred), 1000);
        })
        .then(function(result) {
          if (
            result.Spectrum.length !== null &&
            result.Spectrum.length !== result.Freq.length &&
            result.Spectrum.length !== side.calibration.speakerBaseline.Baseline.length
          ) {
            var message = "Calibration lengths don't match.";
            logger.error(message);
            deferred.reject(message);
          }
          page.result.calSpectrum = result.Spectrum;
          // Got results, so process them.
          let indices = [];
          frequencies.forEach((item, index) => {
            let mapping = result.Freq.map(val => Math.abs(val - item));
            indices.push(mapping.indexOf(Math.min(...mapping)));
          });
          side.measured = indices.map(i => result.Spectrum[i] - side.calibration.speakerBaseline.Baseline[i]);
          side.frequencies = frequencies;
          if (failSafe) {
            clearTimeout(failSafe);
          }
          if (repeatTimeout) {
            clearTimeout(repeatTimeout);
          }
          deferred.resolve();
        });
    }
  })

  // Set up the results plot directive/controller, which just passes data to the D3 plot.
  .directive("calibrationPlot", function() {
    return {
      restrict: "E",
      template: '<div id="calibrationPlotLeftWindow"></div><div id="calibrationPlotRightWindow"></div>',
      controller: "calibrationPlotCtrl",
      scope: {
        data: "="
      }
    };
  })

  .controller("calibrationPlotCtrl", function($scope, d3Services) {
    d3Services.calibrationPlot($scope.data);
  });
