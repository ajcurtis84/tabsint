/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import { Set } from "es6-shim";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("cha.audiometry-table", [])
  .filter("round", function() {
    return function(input) {
      if (typeof input === "number") {
        return Math.round(input);
      }
      return input;
    };
  })
  .filter("getFrequency", function() {
    return function(input) {
      if (angular.isDefined(input.examProperties)) {
        return input.examProperties.F / 1000;
      } else {
        return input.page.responseArea.examProperties.F / 1000;
      }
    };
  })
  .controller("AudiometryTableCtrl", function($scope, page, chaResults, gettextCatalog, disk, protocol) {
    page.dm.hideProgressbar = true;
    page.dm.isSubmittable = true;

    // Update page view, get rid of instructions
    $scope.showSLMNoise = page.dm.responseArea.showSLMNoise;
    $scope.showSvantek = page.dm.responseArea.showSvantek;
    $scope.page.dm.title = gettextCatalog.getString("Audiometry Results");
    $scope.page.dm.questionMainText = "";
    $scope.page.dm.instructionText = "";

    // this function caclulates audiometry results based on the display id's passed. It will return an array of [resultsList, audiogramData]
    // If we are generating a PDF, resultsList will already be defined as a subset of the results corresponding to a page of the protocol.
    // Otherwise, resultsList will be undefined (ex. showing results after test is complete)
    if (angular.isUndefined($scope.resultsList)) {
      $scope.resultsList = chaResults.createAudiometryResults(page.dm.responseArea.displayIds)[0];
    }
    console.log("$scope.resultsList for audiometry-table", $scope.resultsList);
    // Manual audiometry resultsList has entry even for skipped items
    // Automated audiometry resultsList only has entries for non-skipped items.
    // Need to add empty entries for missing items
    let outputChannels = new Set();

    $scope.resultsList.forEach(element => {
      if (angular.isDefined(element.examProperties)) {
        outputChannels.add(element.examProperties.OutputChannel);
      }
      // If no examProperties, this is an Automated Audiometry result that was skipped.
      else if (angular.isDefined(element.page)) {
        element.examProperties = {
          OutputChannel: element.page.responseArea.examProperties.OutputChannel,
          F: element.page.responseArea.examProperties.F,
          LevelUnits: element.page.responseArea.examProperties.LevelUnits
        };
        outputChannels.add(element.page.responseArea.examProperties.OutputChannel);
      }
    });
    // TODO: make this a filter
    $scope.getThresholdText = chaResults.getDisplayThreshold;
    $scope.outputChannels = Array.from(outputChannels);

    let sortOrder = ["HPL0", "HPL1", "LINEL0 NONE", "HPR0", "HPR1", "LINEL0", "NONE LINEL0"]; // Sort the output channels left first so the table can iterate this array to create columns in correct order.
    $scope.outputChannels.sort(function(a, b) {
      return sortOrder.indexOf(a) - sortOrder.indexOf(b);
    });

    if (angular.isUndefined($scope.resultsList)) {
      $scope.page.dm.questionSubText = gettextCatalog.getString("No Results to Show");
    }
    $scope.formatScreenerResult = function(result) {
      if (result == "Fail") {
        return gettextCatalog.getString("REFER");
      }
      return result;
    };
  });
