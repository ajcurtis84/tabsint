/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha.hint", [])
  .directive("hintExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/hint/hint.html",
      controller: "HintExamCtrl"
    };
  })
  .controller("HintExamCtrl", function(
    $scope,
    $timeout,
    cha,
    chaExams,
    examLogic,
    gettextCatalog,
    logger,
    page,
    results
  ) {
    var ListNumberMax = 12; // from cha spec
    page.dm.questionMainText = page.resultMainText || "HINT Exam";
    page.dm.questionSubText =
      page.resultSubText || gettextCatalog.getString("Select all the words that the subject heard correctly.");
    var correctPresentations;
    var tempSpacer = "$SPACER";

    function resetSubmitButton() {
      page.dm.isSubmittable = false;
      $timeout(function() {
        page.dm.isSubmittable = true;
      }, 3000);
    }

    // Function to run at exam initiation - happens when the responseArea changes
    function newHintExam() {
      // override submit logic, so submit can be used for multiple questions within the page
      examLogic.submit = function() {
        processSelectedWords();
      };
      // Make submit button actice
      resetSubmitButton();

      $scope.presentationCount = 0; // counted number of presentation displayed
      correctPresentations = 0; // counted number of presentations that were answered 100% correctly
      if (_.isUndefined(chaExams.examProperties.NumberOfPresentations)) {
        chaExams.examProperties.NumberOfPresentations = 20;
      }
      if (angular.isUndefined(chaExams.examProperties.ListNumber)) {
        chaExams.examProperties.ListNumber = Math.ceil(Math.random() * ListNumberMax);
        logger.info("CHA HINT exam called without a ListNumber defined, using " + chaExams.examProperties.ListNumber);
      }
      $scope.clearSelection();
      resetResults();

      return getPresentationInfo();
    }

    // we've already queued the Hint exam, these steps are really checking status and getting next sentence or finishing and submitting
    function getPresentationInfo() {
      return cha
        .requestResults()
        .then(function(result) {
          presentationHandler(result);
        })
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    }

    //Adds temporary spacers outside of
    //parentheses to faciliate button split
    function btnSplitSpacer(listOfWords) {
      var newListOfWords = "";
      var current = "";
      var parenthesis = 0;
      for (var i = 0, l = listOfWords.length; i < l; i++) {
        if (listOfWords[i] == "(") {
          parenthesis++;
          current = current + "(";
        } else if (listOfWords[i] == ")" && parenthesis > 0) {
          parenthesis--;
          current = current + ")";
        } else if (listOfWords[i] === " " && parenthesis == 0) {
          newListOfWords = newListOfWords + current + tempSpacer;
          current = "";
        } else {
          current = current + listOfWords[i];
        }
      }
      if (current !== "") {
        newListOfWords = newListOfWords + current;
      }
      return newListOfWords;
    }

    // This is less a results handler in this case, more an exam handler
    function presentationHandler(resultFromCha) {
      if (resultFromCha.State === 0) {
        // State 0 = exam still running
        $scope.clearSelection();
        resetSubmitButton();

        // Put presentation properties in the result
        page.result = $.extend({}, page.result, resultFromCha);
        $scope.props = {
          listOfWords: btnSplitSpacer(page.result.CurrentSentence.trim())
            .replace(/\s+/g, " ")
            .split(tempSpacer) // Split sentence text into buttons
        };
        $scope.wordsDisabled = false;

        // Adjust display
        if (angular.isDefined(resultFromCha.CurrentSentenceIndex)) {
          $scope.presentationCount = resultFromCha.CurrentSentenceIndex;
        } else {
          $scope.presentationCount = page.result.presentationCount + 1 || $scope.presentationCount + 1;
        }
      } else if (resultFromCha.State === 2) {
        // End of exam
        page.dm.isSubmittable = true;
        page.result.presentationId += "_Results";
        page.result = $.extend({}, page.result, {
          response: "Exam Results",
          presentationCount: $scope.presentationCount + 1,
          correctPresentationCount: correctPresentations,
          resultsFromCha: resultFromCha,
          chaInfo: chaExams.getChaInfo()
        });
        examLogic.submit = examLogic.submitDefault;
        examLogic.submit();
      }
    }

    // grade the user inputs and show correct answers
    function processSelectedWords() {
      $scope.wordsDisabled = true;
      $scope.page.dm.isSubmittable = false;
      // first word is least significant bit, generate a bit list of selected words
      var correctWords = 0;
      var selectedWords = [];
      _.forEach($scope.response, function(index) {
        correctWords += Math.pow(2, index);
        selectedWords.push($scope.props.listOfWords[index]);
      });

      var numberCorrect = $scope.response.length;
      var wordCount = $scope.props.listOfWords.length;
      if (numberCorrect === wordCount) {
        correctPresentations += 1;
      }

      page.result = $.extend({}, page.result, results.default(page.dm), {
        response: $scope.response,
        selectedWords: selectedWords,
        numberCorrect: numberCorrect,
        wordCount: wordCount,
        responseToCha: correctWords
      });

      setTimeout(submitWords, 200); //  Allow the correct answers to show for ~1 second before submitting results.
    }

    // Send digits back to the CHA and push presentation score onto the exam results stack
    function submitWords() {
      // push results
      //console.log('DEBUG: CHA pushing HINT presentation result onto stack: ' +  angular.toJson(page.result));
      examLogic.pushResults();

      logger.debug("Submitting HINT presentation results to Cha...via examSubmission");
      // send results to cha and start new presentation
      return (
        cha
          .examSubmission("HINT$Submission", {
            CorrectWords: page.result.responseToCha,
            WordCount: page.result.wordCount
          })
          //          .then(chaExams.runTOBifCalledFor)
          .then(resetResults)
          .then(getPresentationInfo)
          .catch(function(err) {
            cha.errorHandler.main(err);
          })
      );
    }

    function resetResults() {
      var presentationId = page.result.presentationId;
      page.result = {};
      page.result = {
        examType: chaExams.examType,
        examProperties: chaExams.examProperties,
        presentationId: presentationId,
        responseStartTime: new Date()
      };
    }

    $scope.hintChosen = function(wordIndex) {
      return $scope.response.indexOf(wordIndex) > -1;
    };

    // Clear keypad at the start and after each response - also fires on 'Clear' keypress
    $scope.clearSelection = function() {
      $scope.response = [];
    };

    $scope.selectAllWords = function() {
      $scope.response = [];
      for (var wordIndex = 0; wordIndex < $scope.props.listOfWords.length; wordIndex++) {
        $scope.response.push(wordIndex);
      }
    };

    // User input from HTML
    $scope.toggleWord = function(wordIndex) {
      var ind = $scope.response.indexOf(wordIndex);
      if (ind < 0) {
        $scope.response.push(wordIndex);
      } else if (ind > -1) {
        $scope.response.splice(ind, 1);
      }
    };

    return newHintExam();
  });
