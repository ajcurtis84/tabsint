/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as $ from "jquery";
import "../audiometry/audiometry-properties/audiometry-properties";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha.mld", ["cha.audiometry-properties"])
  .directive("mldExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/mld/mld.html",
      controller: "MLDExamCtrl"
    };
  })
  .controller("MLDExamCtrl", function($scope, $timeout, cha, chaExams, examLogic, logger, page, results) {
    var pollingInterval,
      pollingTimeout,
      userResponses = [],
      currentState;
    var examProps = page.dm.responseArea.examProperties || {};

    // If requireResponse, play each presentation in full with no disabled,
    // then enabled buttons asking listener if they heard tones.
    // If !requireResponse, only show red button, move smoothly from pres to pres
    $scope.requireResponse = angular.isDefined(examProps.RequireResponse) ? examProps.RequireResponse : true;
    $scope.yesDisabled = true;
    $scope.noDisabled = true;

    // onExit function, to make sure intervals do not remain
    $scope.$on("$destroy", function() {
      $timeout.cancel(pollingTimeout);
    });

    // exam initiation, happens when response area is loaded
    resetResults();
    getState();

    // start polling the cha for state
    function getState() {
      cha
        .requestResults()
        .then(handleState)
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    }

    // handle state response from cha
    function handleState(resultFromCha) {
      console.log("handleState begin");
      console.log(chaExams);
      console.log("handleState end");
      currentState = resultFromCha.State;
      page.result = $.extend({}, page.result, results.default(page.dm), resultFromCha); // don't need to do this so many times, but extend should prevent duplicates

      if (currentState === "BETWEEN") {
        // disable the button
        pollingTimeout = $timeout(function() {
          getState();
        }, 200);
      } else if (currentState === "PLAYING") {
        // enable response
        $scope.yesDisabled = false;
        pollingTimeout = $timeout(function() {
          getState();
        }, 200);
      } else if (currentState === "WAITING_FOR_RESULT") {
        $scope.yesDisabled = false;
        $scope.noDisabled = false;

        if (!$scope.requireResponse) {
          pollingTimeout = $timeout(function() {
            getState();
          }, 200);
        }
      } else if (currentState === "DONE") {
        // End of exam

        page.result = $.extend({}, page.result, {
          response: "Exam Results",
          chaInfo: chaExams.getChaInfo(),
          userResponses: userResponses
        });

        // process results a little
        page.result.ReferenceSNRArray = page.result.ReferenceSNRArray.map(function(val) {
          return parseFloat(val);
        });
        page.result.TargetSNRArray = page.result.TargetSNRArray.map(function(val) {
          return parseFloat(val);
        });
        page.result.ReferenceHitOrMiss = page.result.ReferenceHitOrMiss.map(function(val) {
          return !!parseInt(val);
        });
        page.result.TargetHitOrMiss = page.result.TargetHitOrMiss.map(function(val) {
          return !!parseInt(val);
        });

        // Ready to submit
        page.dm.isSubmittable = true;

        examLogic.submit();
      }
    }

    $scope.submitUserInput = function(heardSound) {
      logger.debug("CHA processing MLD response: " + heardSound);

      // disable response using timeout to allow button to visibly display as pressed
      setTimeout(function() {
        $scope.yesDisabled = true;
        $scope.noDisabled = true;
      }, 250);

      // save responses
      userResponses.push({
        Condition: page.result.Condition,
        Response: heardSound
      });

      // submit result
      cha
        .examSubmission("MLD$Submission", { UserResponse: heardSound ? 1 : 0 })
        .then(function() {
          if (currentState === "WAITING_FOR_RESULT" && $scope.requireResponse) {
            getState();
          }
        })
        .catch(function(err) {
          $timeout.cancel(pollingTimeout);
          cha.errorHandler.main(err);
        });

      // start polling again in 1 second
    };

    function resetResults() {
      // TODO: this should be replaced by a "page.newResult()" or "results.new()"
      // this will require a change to examlogic, so skipping for now
      var presentationId = page.result.presentationId;
      page.result = {};
      page.result = {
        examType: chaExams.examType,
        presentationId: presentationId,
        responseStartTime: new Date()
      };
    }
  });
