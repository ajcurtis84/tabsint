/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.components.response-areas.integer", [])

  .controller("IntegerResponseAreaCtrl", function($scope, page) {
    $scope.negative = page.dm.responseArea.negative;
    $scope.float = page.dm.responseArea.float;

    if (page.dm.responseArea.responseRequired === false) {
      page.dm.isSubmittable = true;
    }

    $scope.$watch("response", function() {
      if (_.isNumber(parseFloat($scope.response))) {
        // checks to make sure it is a number
        page.result.response = parseFloat($scope.response).toString(); // gets rid of decimal at the end of an integer
      } else {
        page.result.response = undefined;
      }
      if (page.dm.responseArea.float) {
        if (
          page.result.response !== undefined &&
          (page.dm.responseArea.minAllowedValue === undefined ||
            parseFloat(page.result.response) >= page.dm.responseArea.minAllowedValue) &&
          (page.dm.responseArea.maxAllowedValue === undefined ||
            parseFloat(page.result.response) <= page.dm.responseArea.maxAllowedValue)
        ) {
          page.dm.isSubmittable = true;
        } else {
          if (page.dm.responseArea.responseRequired !== false) {
            page.dm.isSubmittable = false;
          }
        }
      } else {
        if (
          page.result.response !== undefined &&
          (page.dm.responseArea.minAllowedValue === undefined ||
            parseFloat(page.result.response) >= page.dm.responseArea.minAllowedValue) &&
          (page.dm.responseArea.maxAllowedValue === undefined ||
            parseFloat(page.result.response) <= page.dm.responseArea.maxAllowedValue) &&
          parseInt(page.result.response) === parseFloat(page.result.response)
        ) {
          page.dm.isSubmittable = true;
        } else {
          if (page.dm.responseArea.responseRequired !== false) {
            page.dm.isSubmittable = false;
          }
        }
      }
    });

    if (page.dm.responseArea.value) {
      $scope.response = page.dm.responseArea.value + "";
    }

    $scope.appendDigit = function(digit) {
      //        var out = '';
      if ($scope.response === undefined) {
        $scope.response = "";
      } else if ($scope.response === "0") {
        $scope.response = "";
      }
      $scope.response = $scope.response + digit.toString();
    };

    $scope.backspace = function() {
      var out = "";
      if ($scope.response === undefined) {
        out = undefined;
      } else {
        out = $scope.response.slice(0, $scope.response.length - 1);

        // deal with negatives
        if ((out.length === 1 && out.slice(0) === "-") || out.length === 0) {
          out = undefined;
        }
      }
      $scope.response = out;
    };

    $scope.changesign = function() {
      if ($scope.response && $scope.response !== "0") {
        $scope.response = (-1 * parseFloat($scope.response)).toString();
      }
    };

    $scope.getSign = function() {
      if (_.isUndefined($scope.response) || parseFloat($scope.response) >= 0) {
        return "glyphicon glyphicon-minus";
      } else {
        return "glyphicon glyphicon-plus";
      }
    };

    $scope.addDecimal = function() {
      if (_.isUndefined($scope.response)) {
        $scope.response = "0.";
      } else if ($scope.response.indexOf(".") === -1) {
        $scope.response = $scope.response + ".";
      }
    };

    // Clear keypad when the question ID is changed
    function update() {
      $scope.response = undefined;
    }

    update();
  });
