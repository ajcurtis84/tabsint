/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import html2canvas from "html2canvas";
/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.results-view", ["ui.bootstrap"])
  .controller("resultsViewCtrl", function($scope, config, disk, results, examLogic, page, cha, chaResults, pdfService) {
    $scope.pdfExportEnabled = false;
    $scope.oneAtATime = true;
    page.dm.isSubmittable = true;

    examLogic.closeAll = function() {
      $scope.responseAreas.forEach(response => {
        response.isOpen = false;
      });
    };

    examLogic.submit = function() {
      if ($scope.pdfExportEnabled) {
        exportPdf();
      }
      examLogic.submitDefault();
    };

    const adminPin = disk.pin || config.adminSettings.adminPin;
    var docDefinition = {
      userPassword: adminPin.toString(),
      pageSize: "LETTER",
      content: [
        {
          text: page.dm.title,
          style: "header"
        }
      ],
      pageBreakBefore: function(currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage) {
        return currentNode.headlineLevel === 1 && followingNodesOnPage.length === 0;
      },
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          alignment: "center"
        }
      }
    };

    function exportPdf() {
      var tasks = [];
      let collapsed = Array.from(document.getElementsByClassName("collapse"));
      collapsed.forEach(element => {
        element.style.display = "inline";
      });

      var divs = document.getElementsByClassName("pdf-results");
      var elements = Array.from(divs);
      elements.forEach(element => {
        // Convert plots to canvas to reproduce accurately in PDF.
        tasks.push(html2canvas(element));
      });
      // Each call to html2canvas or htmlToPdfmake returns a promise.
      // Wait for all promises to resolve before building PDF.
      Promise.all(tasks)
        .then(canvases => {
          for (const canvas of canvases) {
            // Output from htmlToPdfmake is in an array, unlike output from html2canvas.
            if (canvas instanceof Array) {
              docDefinition.content.push(canvas);
            } else {
              const data = canvas.toDataURL();

              docDefinition.content.push({
                image: data,
                width: 325,
                margin: [10, 10]
              });
            }
          }
        })
        .then(function() {
          pdfService.createPdf(docDefinition);
        });
    }

    var responseAreas = [];
    // Loop through displayResults defined in protocol
    // We are assuming that each presentation was only completed once, as we grab the first
    // result in the array
    page.dm.responseArea.displayResults.forEach(element => {
      // displayIds is list of page IDs to display in PDF.
      element.displayIds.forEach(item => {
        // Loop through current results and find responses where presentation ID matches display ID
        // result is an array
        var result = results.current.testResults.responses.filter(
          response =>
            response.presentationId == item ||
            (item.includes("MPANL") && !angular.isUndefined(response.mpanlsData)) ||
            response.presentationId.includes("ManualScreener")
        );
        // three digit results is array of N elements.
        // results of interest in object containing digitScore
        let threedigitResults;
        if (result.some(item => item.responseArea == "chaThreeDigit")) {
          threedigitResults = result.filter(item => item.response === "Exam Results");
          result = threedigitResults;
        }
        // Assume uniqueness so grab the first element
        if (result.length > 0) {
          result = result[0];
          if (result.responseArea == "multipleInputResponseArea") {
            var fields = [];
            // Field names are stored in an object.
            // Field values stored in array. Assume order of values in array
            // matches order of field in inputList and merge.
            result.page.responseArea.inputList.forEach(element => {
              fields.push({
                key: element.text
              });
            });
            result.response.forEach((element, index) => {
              fields[index]["value"] = element;
            });
            responseAreas.push({
              title: element.text,
              responseArea: result.responseArea,
              results: fields
            });
          } else if (
            result.responseArea == "chaAudiometryResultsPlot" ||
            result.responseArea == "chaAudiometryResultsTable"
          ) {
            responseAreas.push({
              title: element.text,
              results: chaResults.createAudiometryResults(result.page.responseArea.displayIds),
              responseArea: result.responseArea
            });
          } else if (result.responseArea == "chaManualScreener") {
            let screenerResults = [];
            let scrnResults = chaResults.createAudiometryResults(result.page.responseArea.displayIds)[0];
            let levelRegex = /(\d+dB)/;
            // if manual screener, massage results to make table display easier since format is v. different
            let presentationResults = scrnResults.map(result => [result.presentationId, result.response]);
            let uniqueFreqs = [...new Set(scrnResults.map(element => element.examProperties.F))];

            uniqueFreqs.forEach(freq => {
              let result = {
                frequency: freq,
                Left: {
                  levels: [],
                  results: []
                },
                Right: {
                  levels: [],
                  results: []
                }
              };
              let freqRegex = new RegExp(`(${freq}HZ)`);
              let filteredPresentations = presentationResults.filter(pres => freqRegex.test(pres[0]));
              result.Left.levels = filteredPresentations
                .filter(pres => pres[0].indexOf("Left") > -1)
                .map(pres => parseInt(levelRegex.exec(pres)[0]));
              result.Left.results = filteredPresentations
                .filter(pres => pres[0].indexOf("Left") > -1)
                .map(pres => {
                  if (pres[1] == "P") {
                    return "Pass";
                  } else if (pres[1] == "R") {
                    return "REFER";
                  }
                });
              result.Right.levels = filteredPresentations
                .filter(pres => pres[0].indexOf("Right") > -1)
                .map(pres => parseInt(levelRegex.exec(pres)[0]));
              result.Right.results = filteredPresentations
                .filter(pres => pres[0].indexOf("Right") > -1)
                .map(pres => {
                  if (pres[1] == "P") {
                    return "Pass";
                  } else if (pres[1] == "R") {
                    return "REFER";
                  }
                });
              screenerResults.push(result);
            });
            $scope.uniqueFreqs = uniqueFreqs;
            $scope.levels = result.page.responseArea.levels.sort();
            responseAreas.push({
              title: element.text,
              results: screenerResults,
              responseArea: result.responseArea
            });
          } else if (result.responseArea == "textboxResponseArea") {
            responseAreas.push({
              title: element.text,
              results: result.response,
              responseArea: result.responseArea,
              rows: result.page.responseArea.rows
            });
          } else if (result.responseArea == "chaThreeDigit") {
            responseAreas.push({
              title: element.text,
              responseArea: result.responseArea,
              results: {
                digitScore: result.digitScore,
                fixedLevel: result.page.responseArea.examProperties.fixedLevel
              }
            });
          } else if (result.responseArea == "likertResponseArea") {
            responseAreas.push({
              title: element.text,
              responseArea: result.responseArea,
              results: result
            });
          } else if (result.responseArea == "chaCalibrationCheck") {
            responseAreas.push({
              title: element.text,
              results: result.calibrationData,
              responseArea: result.responseArea
            });
          } else if (result.responseArea == "chaDPOAE") {
            // TODO: DPOAE results.
          } else if (!angular.isUndefined(result.mpanlsData)) {
            responseAreas.push({
              title: element.text,
              results: result.mpanlsData,
              responseArea: "mpanlResponseArea"
            });
          }
        }
      });
    });
    $scope.responseAreas = responseAreas;
    $scope.equipment = cha.myCha;
    $scope.headset = results.current.testResults.headset;
    $scope.version = results.current.testResults.softwareVersion.version;
    $scope.pdfDisplay = true;
  });
