/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.components.response-areas.likert", [])
  .controller("LikertResponseAreaCtrl", function($scope, examLogic, page, $sce, $timeout, devices) {
    $scope.responseArea = page.dm.responseArea; // defaults

    var labelFontSize = 20; // px

    var questionFontSize = ""; //so it goes to default value

    $scope.selectedResponse = undefined;

    $scope.trustedHtml = $sce.trustAsHtml;
    $scope.$watch("page.result.response", function() {
      page.dm.isSubmittable = examLogic.getSubmittableLogic(page.dm.responseArea);
    }); // update view when page first loads

    function update() {
      // Note - we treat ALL likert response areas as "multiple likert response areas".
      // If the user specifies a standard single likert, we create an array of length one
      // $scope.questions holds the array of all likert presentations
      if ($scope.questions) {
        page.dm.responseArea.questions = $scope.questions;
        page.result.response = $scope.selected;
        $scope.selectedResponse = page.result.response;
      } else {
        $scope.questions = [];
        page.result.response = []; // create array of responses
      }

      if (page.dm.responseArea.questions === undefined) {
        page.dm.responseArea.questions = [];
        page.dm.responseArea.questions.push(angular.copy(page.dm.responseArea));
      }

      _.forEach(page.dm.responseArea.questions, function(question, index) {
        page.result.response.push(undefined); // Keep for autoSubmit to work
        $scope.questions.push({});
        var nLevels;
        if (!(Object.keys(question).length === 0)) {
          if (Number.isInteger(question.levels)) {
            nLevels = question.levels;
          } else if (question.specifiers.length > 0) {
            nLevels = question.specifiers.length;
          }
        }
        $scope.questions[index].levels = _.map(_.range(nLevels), function(n) {
          return n.toString();
        });
        $scope.questions[index].topLabels = _.map(_.range(nLevels), function() {
          return "";
        });
        $scope.questions[index].bottomLabels = _.map(_.range(nLevels), function() {
          return "";
        });
        $scope.questions[index].text = question.questionMainText;

        //HG;CONTINIOUS PROPERTIES
        //HG;To  align begging and end of continous likeRT first and last label
        var total_col = nLevels * 2 + 2;
        var ratio_per_col = 1 / total_col;
        $scope.questions[index].margin_left = (100 * (2 * ratio_per_col)).toString() + "%";
        $scope.questions[index].width = (100 * ((total_col - 4) * ratio_per_col)).toString() + "%";

        $scope.questions[index].continious = question.continious;
        $scope.questions[index].static = question.static;

        $scope.questions[index].sliderMin = 0;
        if (question.sliderMin !== undefined) {
          $scope.questions[index].sliderMin = question.sliderMin;
        }

        $scope.questions[index].sliderMax = 100;
        if (question.sliderMax !== undefined) {
          $scope.questions[index].sliderMax = question.sliderMax;
        }

        $scope.questions[index].showFeedback = true;
        if ((question.correct == undefined) & (question.showFeedback == undefined)) {
          $scope.questions[index].showFeedback = false;
        } else if ((question.correct == undefined) & question.showFeedback) {
          $scope.showFeedBackActive = true;
          $scope.questions[index].showFeedback = false;
        } else {
          $scope.questions[index].correct =
            (100 * (question.correct - $scope.questions[index].sliderMin)) /
            ($scope.questions[index].sliderMax - $scope.questions[index].sliderMin);
          $scope.showFeedBackActive = true;
        }

        // $scope.static=false;

        $scope.sliderFeedbackTimeOut = 2000;
        if (page.dm.responseArea.sliderFeedbackTimeOut !== undefined) {
          $scope.sliderFeedbackTimeOut = page.dm.responseArea.sliderFeedbackTimeOut;
        }

        $scope.questions[index].hideDefault = false;
        if (question.default == undefined) {
          $scope.questions[index].hideDefault = true;
        } else {
          //HG; because the likeRT on the respArea is from 0 to 100
          $scope.questions[index].default =
            (100 * (question.default - $scope.questions[index].sliderMin)) /
            ($scope.questions[index].sliderMax - $scope.questions[index].sliderMin);
          page.result.response[index] = question.default; //HG; sliders with default value don't need answer
        }

        // if(Math.random()>.5){
        // $scope.value=page.dm.responseArea.questions[0].specifiers[Math.floor(page.dm.responseArea.questions[0].specifiers.length*Math.random())].level;
        // $scope.default=$scope.value;
        // }
        // else{
        // $scope.default = page.dm.responseArea.questions[0].specifiers[Math.floor(page.dm.responseArea.questions[0].specifiers.length*Math.random())].level;
        // $scope.value=((100*($scope.default-$scope.sliderMin))/($scope.sliderMax-$scope.sliderMin));
        // }

        //HG;CONTINIOUS PROPERTIES

        $scope.questions[index].questionFontSize = question.questionFontSize || questionFontSize;
        $scope.questions[index].labelFontSize = question.labelFontSize || labelFontSize;

        if (question.specifiers) {
          _.forEach(question.specifiers, function(specifier) {
            if (specifier.position === "below") {
              $scope.questions[index].bottomLabels[specifier.level] = specifier.label;
            } else if (specifier.position === "above" || specifier.position === undefined) {
              $scope.questions[index].topLabels[specifier.level] = specifier.label;
            }
          });
        } // Can we remove the 'odd' labels to make the layout more spacious?

        function isOdd(num) {
          return num % 2;
        }

        if (isOdd(nLevels)) {
          var allIdxs = _.range(nLevels);

          var oddIdxs = _.filter(allIdxs, function(num) {
            return isOdd(num);
          });

          var evenIdxs = _.filter(allIdxs, function(num) {
            return !isOdd(num);
          });

          if (
            _.every(oddIdxs, function(idx) {
              return $scope.questions[index].topLabels[idx] === "";
            })
          ) {
            $scope.questions[index].topLabelsAreSpacious = true;
            $scope.questions[index].topLabels = _.map(evenIdxs, function(idx) {
              return $scope.questions[index].topLabels[idx];
            });
          }

          if (
            _.every(oddIdxs, function(idx) {
              return $scope.questions[index].bottomLabels[idx] === "";
            })
          ) {
            $scope.questions[index].bottomLabelsAreSpacious = true;
            $scope.questions[index].bottomLabels = _.map(evenIdxs, function(idx) {
              return $scope.questions[index].bottomLabels[idx];
            });
          }
        } else {
          var nonEmptyTopLabels = _.filter($scope.questions[index].topLabels, function(label) {
            return label !== "";
          });

          var nonEmptyBottomLabels = _.filter($scope.questions[index].bottomLabels, function(label) {
            return label !== "";
          });

          if (nonEmptyTopLabels.length < Math.floor(nLevels / 2)) {
            $scope.questions[index].topLabelsAreSpacious = true;
            $scope.questions[index].topLabels = nonEmptyTopLabels;
          }

          if (nonEmptyBottomLabels.length < Math.floor(nLevels / 2)) {
            $scope.questions[index].topLabelsAreSpacious = true;
            $scope.questions[index].topLabels = nonEmptyBottomLabels;
          }
        }
      });
    }

    update();

    //////////////////////////////////CONTINIOUS/////////////////////////////
    $scope.parentIndexCont = [];
    $scope.model = [];
    $scope.isContinous = false;
    $timeout(function() {
      for (var i = 0; i < $scope.questions.length; i++) {
        if ($scope.questions[i].continious) {
          $scope.model.push("value" + (i + 1));
          $scope["value" + (i + 1)] = $scope.questions[i].default;
          $scope.isContinous = true;
          $scope.parentIndexCont.push();
          $scope.parentIndexCont["myRange" + (i + 1)] = i;

          var box = document.getElementById("myRange" + (i + 1));
          if (devices.platform.toLowerCase() === "android") {
            box.addEventListener(
              "touchend",
              function(e) {
                touchFun($scope, e);
              },
              false
            );
            box.addEventListener(
              "touchstart",
              function(e) {
                touchStartFun($scope, e);
              },
              false
            );
          } else {
            box.addEventListener(
              "mouseup",
              function(e) {
                touchFun($scope, e);
              },
              false
            );
            box.addEventListener(
              "mousedown",
              function(e) {
                touchStartFun($scope, e);
              },
              false
            );
          }
        }
      }
    }, 200);

    // if ($scope.default==undefined){}
    // if ($scope.default==undefined){}

    function touchStartFun($scope, e) {
      $scope.questions[$scope.parentIndexCont[e.target.id]].hideDefault = false;
    }
    function touchFun($scope, e) {
      console.log("sliding!!!");

      if (!$scope.questions[$scope.parentIndexCont[e.target.id]].static) {
        $scope[e.target.id] = parseInt(e.target.value);

        if (
          page.result.response[$scope.parentIndexCont[e.target.id]] ===
          $scope.questions[$scope.parentIndexCont[e.target.id]].sliderMin +
            (parseInt(e.target.value) / 100) *
              ($scope.questions[$scope.parentIndexCont[e.target.id]].sliderMax -
                $scope.questions[$scope.parentIndexCont[e.target.id]].sliderMin)
        ) {
          page.result.response[$scope.parentIndexCont[e.target.id]] = undefined; //toggle off.
          $scope.questions[$scope.parentIndexCont[e.target.id]].hideDefault = true;
        } else {
          $scope.questions[$scope.parentIndexCont[e.target.id]].hideDefault = false;
          page.result.response[$scope.parentIndexCont[e.target.id]] =
            $scope.questions[$scope.parentIndexCont[e.target.id]].sliderMin +
            (parseInt(e.target.value) / 100) *
              ($scope.questions[$scope.parentIndexCont[e.target.id]].sliderMax -
                $scope.questions[$scope.parentIndexCont[e.target.id]].sliderMin);
        }

        /////////CHOOSE/////////
        if (page.result.response.indexOf(undefined) < 0) {
          page.dm.isSubmittable = true; // submittable when both the responseText and responseNumber are defined
        } else {
          if (page.dm.responseArea.responseRequired !== false) {
            page.dm.isSubmittable = false;
          }
        } // AUTO-SUBMIT:

        if (page.dm.responseArea.autoSubmit) {
          //to allow auto-submit, HG 07/12/16
          var checkAllResponse = true; //so autosubmit works with multi-likeRT

          for (var i = 0; i < page.result.response.length; i++) {
            if (page.result.response[i] === undefined) {
              checkAllResponse = false;
              break;
            }
          }

          if (checkAllResponse) {
            $scope.LikeRTsubmit();
          }
        }
        $scope.$digest();
        /////////CHOOSE/////////
      }
    }

    $scope.LikeRTsubmit = function() {
      if ($scope.isContinous) {
        if ($scope.showFeedBackActive) {
          $scope.showFeedback = true;
          $timeout(function() {
            console.log("inside timeout");
            examLogic.submit();
          }, $scope.sliderFeedbackTimeOut);
        } else {
          examLogic.submit();
        }
      } else {
        examLogic.submit();
      }
    };

    //////////////////////////////////CONTINIOUS/////////////////////////////
  })
  .controller("LikertChoiceController", function($scope, page) {
    update();
    function update() {
      $scope.chosen = function() {
        if ($scope.chosenResponse !== undefined) {
          return $scope.level === $scope.chosenResponse[$scope.parentIndex];
        } else {
          return $scope.level === page.result.response[$scope.parentIndex];
        }
      };

      //font support
      //https://www.fileformat.info/info/unicode/char/1f620/fontsupport.htm
      var emoticons = [
        {
          src: "😠" // U+1F620 - Strongly Disagree
        },
        {
          src: "🙁" // U+1F641 - Disagree
        },
        {
          src: "😐" // U+1F610	- No Opinion
        },
        {
          src: "🙂" // U+1F642 - Agree
        },
        {
          src: "😃" // U+1F603 - Strongly Agree
        }
      ];
      $scope.useEmoticons = page.dm.responseArea.questions[$scope.parentIndex].useEmoticons;

      $scope.btnSrc = function(ind) {
        var btnSrc;

        if ($scope.useEmoticons) {
          btnSrc = emoticons[ind].src;
        } else {
          if ($scope.chosen()) {
            btnSrc = "img/radio_selected_64_64.png";
          } else {
            btnSrc = "img/radio_unselected_64_64.png";
          }
        }

        return btnSrc;
      }; // function to run when selection is made

      $scope.choose = function(index) {
        // toggle chosen/unchosen.
        $scope.currentIndex = index;

        if (page.result.response[index] === $scope.level) {
          page.result.response[index] = undefined; //toggle off.
        } else {
          page.result.response[index] = $scope.level; // choose.
        }

        if (page.result.response.indexOf(undefined) < 0) {
          page.dm.isSubmittable = true; // submittable when both the responseText and responseNumber are defined
        } else {
          if (page.dm.responseArea.responseRequired !== false) {
            page.dm.isSubmittable = false;
          }
        } // AUTO-SUBMIT:

        if (page.dm.responseArea.autoSubmit) {
          //to allow auto-submit, HG 07/12/16
          var checkAllResponse = true; //so autosubmit works with multi-likeRT

          for (var i = 0; i < page.result.response.length; i++) {
            if (page.result.response[i] === undefined) {
              checkAllResponse = false;
              break;
            }
          }

          if (checkAllResponse) {
            $scope.LikeRTsubmit();
          }
        }
      };
    }
  });
