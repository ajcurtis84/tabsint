/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha.third-octave-bands", [])
  .directive("thirdOctaveBandsExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/third-octave-bands/third-octave-bands.html",
      controller: "ThirdOctaveBandExamCtrl"
    };
  })
  .controller("ThirdOctaveBandExamCtrl", function(
    $scope,
    $q,
    $timeout,
    cha,
    chaExams,
    chaResults,
    examLogic,
    page,
    logger
  ) {
    // Results structure:
    var resultList = [];
    var startTime = new Date();
    // temporary hijack
    examLogic.submit = function() {
      submitResults();
    };

    $scope.page.dm.questionMainText = "Background Noise Measurement";
    $scope.page.dm.questionSubText =
      page.dm.examInstructions || "Please sit quietly while we measure the background noise levels.";

    // Results Presentation
    function presentResults() {
      chaExams.state = "results";
      page.dm.hideProgressbar = true;

      // Update page view, get rid of instructions
      $scope.page.dm.title = "Results";
      $scope.page.dm.questionMainText = "Background Noise Results";
      $scope.page.dm.questionSubText = "";
      $scope.page.dm.instructionText = "";

      // set default 'response' field
      page.result.response = "continue";

      if (page.dm.responseArea.autoSubmit) {
        examLogic.submit();
        return;
      } else {
        page.dm.isSubmittable = true;
      }

      var figures = chaResults.createTOBFigures(resultList, page.dm.responseArea.standard);

      $scope.thirdOctaveBandData = figures[1];
    }

    function submitResults() {
      function submitSingleResult(pres) {
        page.result = pres;
        logger.debug("CHA pushing maunal audiometry presentation result onto stack: " + angular.toJson(page.result));
        examLogic.pushResults();
      }

      //console.log('resultList: '+angular.toJson(resultList));

      if (resultList.length > 1) {
        _.forEach(resultList, submitSingleResult);

        page.result = {
          presentationId: "ThirdOctaveBands",
          responseStartTime: startTime,
          response: "complete"
        };
      } else {
        page.result = $.extend({}, page.result, resultList[0]);
      }

      examLogic.submit = examLogic.submitDefault;
      examLogic.submit();
    }

    function addResults(results) {
      // build and push result to tmp result list, only push to actual results upon submit
      if (results.Frequencies === angular.undefined || results.Leq === angular.undefined) {
        results.dataError = "No results returned";
        $scope.dataError = results.dataError;
      }

      results.examProperties = chaExams.examProperties;
      results.examType = chaExams.examType;
      results.responseStartTime = startTime;
      results.chaInfo = chaExams.getChaInfo();
      // push all results onto the dm
      var tmpResult = $.extend({}, page.result, results);

      resultList.push(angular.copy(tmpResult));
    }

    $scope.repeatTOB = function() {
      // reset state and results
      logger.debug("repeating TOB");

      if (page.dm.responseArea.measureBothEars) {
        // go back to left ear
        chaExams.examProperties.InputChannel = "SMICR0";
      }

      cha
        .requestStatusBeforeExam()
        .then(function() {
          return cha.queueExam(chaExams.examType, chaExams.examProperties);
        })
        .then(handleResults)
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    };

    function runRightEar() {
      console.log("DEBUG: Running right ear tob after left ear");
      // switch input channel to right ear
      chaExams.examProperties.InputChannel = "SMICR1";
      return cha
        .requestStatusBeforeExam()
        .then(function() {
          return cha.queueExam(chaExams.examType, chaExams.examProperties);
        })
        .then(chaExams.wait.forReadyState)
        .then(function() {
          return cha.requestResults();
        })
        .then(function(results) {
          addResults(results);
        })
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    }

    function handleResults() {
      chaExams.wait
        .forReadyState()
        .then(function() {
          return cha.requestResults();
        })
        .then(function(results) {
          addResults(results);
        })
        .then(function() {
          if (page.dm.responseArea.measureBothEars) {
            return runRightEar();
          }
        })
        .then(presentResults)
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    }

    // Current polling/wait for response method resolves and clears after 1
    // second if no exam queued.  This timeout allows for a delay in queueing
    // the exam, in chaResponseAreaCtrl during begin or autoBegin.
    if (angular.isDefined(page.dm.responseArea.delay)) {
      //console.log('Delaying runList ny '+page.dm.responseArea.delay+'ms');
      $timeout(handleResults, page.dm.responseArea.delay);
    } else {
      //console.log('Running runList immediately');
      handleResults();
    }
  })

  .directive("thirdOctaveBandPlot", function() {
    return {
      restrict: "E",
      template: '<div id="thirdOctaveBandPlotWindow"></div>',
      controller: "ThirdOctaveBandPlotCtrl",
      scope: {
        data: "="
      }
    };
  })

  .controller("ThirdOctaveBandPlotCtrl", function($scope, d3Services) {
    d3Services.thirdOctaveBandPlot("#thirdOctaveBandPlotWindow", $scope.data);
  });
