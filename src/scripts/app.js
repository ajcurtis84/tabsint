/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

// all project dependencies
import * as angular from "angular";
import "angular-animate";
import "angular-filter";
import "angular-gettext";
import "angular-sanitize";
import "angular-ui-bootstrap";
import "crypto-js";
import "d3";
import "es6-shim";
import "jsencrypt";
import "jsonformatter";
import * as _ from "lodash";
import * as $ from "jquery";
import "ng-cordova";
import "ngstorage";
import "tv4";
import "qrcode";
import "babel-polyfill";
import "lz-string";
import "angularjs-dropdown-multiselect";

// load the dependent
import "./services/services";
import "./components/components";
import "./routes/routes";

// plugins
import "../tabsint_plugins/import";

angular
  .module("tabsint", [
    "ui.bootstrap",
    "ngAnimate",
    "ngStorage",
    "ngSanitize",
    "ngCordova",
    "jsonFormatter",
    "gettext",
    "angular.filter",
    "angularjs-dropdown-multiselect",

    // services
    "tabsint.services",
    "tabsint.components",
    "tabsint.routes",

    // tabsint - plugins
    "tabsint.plugin-modules"
  ])

  .config([
    "$httpProvider",
    "$sceDelegateProvider",
    "$compileProvider",
    "$controllerProvider",
    function($httpProvider, $sceDelegateProvider, $compileProvider, $controllerProvider) {
      // customize $httpProvider
      $httpProvider.defaults.useXDomain = true;
      delete $httpProvider.defaults.headers.common["X-Requested-With"];

      // whitelist urls
      $sceDelegateProvider.resourceUrlWhitelist([
        "self",
        "cdvfile://localhost/persistent/**",
        "file:///storage/emulated/0/**",
        "file:///data/data/com.creare.skhr.tabsint/files/**",
        "file:///data/user/0/com.creare.skhr.tabsint/files/**"
      ]);

      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file):/);
      $compileProvider.imgSrcSanitizationWhitelist(/^\s*((https?|ftp|file|cdvfile|app):|data:image\/)/);

      // add global variable for lodash and jquery for use in custom functions
      window._ = _;
      window.$ = $;

      // tabsint window variable
      window.tabsint = {};
      window.tabsint.controller = $controllerProvider.register;
      window.tabsint.hasController = $controllerProvider.has;
    }
  ])

  // directives "my-touchstart" and "my-touchend" to handle touch start and end precisely.
  // Evaluate bound scope function on "touchstart" and "touchstart" events
  .directive("myTouchstart", [
    function() {
      return function(scope, element, attr) {
        element.on("touchstart", function() {
          scope.$apply(function() {
            scope.$eval(attr.myTouchstart);
          });
        });
      };
    }
  ])
  .directive("myTouchend", [
    function() {
      return function(scope, element, attr) {
        element.on("touchend", function() {
          scope.$apply(function() {
            scope.$eval(attr.myTouchend);
          });
        });
      };
    }
  ])

  // directive to close soft-keyboard on "enter" key.
  // Must be put on element i.e. <input type="text" enter-close></input>
  .directive("enterClose", [
    "app",
    "cordova",
    function(app, cordova) {
      return function(scope, element, attr) {
        element.bind("keydown keypress", function(event) {
          if (app.tablet && event.which === 13) {
            cordova.plugins.Keyboard.close();
          }
        });
      };
    }
  ])

  // functions to run on app first start - happens after dependency injection, so all modules should be available
  // once the device is ready, initialize all services
  .run(function(
    $q,
    app,
    protocol,
    version,
    splashscreen,
    file,
    config,
    cordova,
    logger,
    results,
    devices,
    androidFullScreen,
    partialResult,
    notifications,
    tabsintNative,
    tabsintFS,
    plugins,
    network,
    gitlab,
    tabsintServer,
    disk,
    remote,
    sqLite,
    gettextCatalog,
    adminLogic,
    bluetoothStatus,
    gain,
    permissions
  ) {
    cordova
      .ready()
      .then(permissions.requestAll)
      .then(sqLite.ready)
      .then(devices.load) // this must happen before file, because file now checks device type for local directory setup
      .then(file.loadFileSystem)
      .then(file.ready)
      .then(tabsintFS.initialize)
      .then(file.initExtStorage)
      .then(network.checkStatus)
      // added code to allow local asset access in initialize
      .then(tabsintNative.initialize)
      .then(function() {
        // necessary synchronous loading functions (these must happen in order)
        config.load();
        return version.load();
      })
      .then(function() {
        tabsintServer.$init();
        gitlab.$init();
        protocol.$init();
        //          devices.getDiskSpace();
      })
      .then(permissions.requestConfig)
      .then(logger.count) // count up logs for admin page
      .then(results.count) // count up results for exam page
      .catch(function(e) {
        logger.error("Error caught during load: " + angular.toJson(e));
      })
      .finally(function() {
        splashscreen.hide();

        // add event listeners
        adminLogic.addEventListeners();

        // init bluetooth status cordova plugin
        bluetoothStatus.$init();

        // initialize the volume control through tabsintNative
        tabsintNative.resetAudio(function(msg) {
          logger.debug("tabsintNative.resetAudio: " + JSON.stringify(msg));
        }, tabsintNative.onVolumeError);
        function onPause() {
          console.log("-- Cordova pause");

          tabsintNative.setAudio(null, tabsintNative.onVolumeErr);
        }

        function onResume() {
          console.log("-- Cordova resume");
          tabsintNative.getAudioVolume(function(msg) {
            tabsintNative.cachedVolumePercent = msg.volume;
            logger.debug("cached volume from resetaudio = " + tabsintNative.cachedVolumePercent);
            tabsintNative.resetAudio(null, tabsintNative.onVolumeErr);
          }, tabsintNative.onVolumeErr);
        }
        document.addEventListener("pause", onPause, false);
        document.addEventListener("resume", onResume, false);

        // load the default gain parameter onto disk
        gain.load();

        // set immersive mode: navigation bar and title bar stay hidden unless swiped in
        androidFullScreen.immersiveMode();

        // load translations
        gettextCatalog.loadRemote("res/translations/translations.json");
        gettextCatalog.setCurrentLanguage(disk.language);

        plugins.runEvent("appLoad");
        logger.info("App Loaded");

        // check to see if partial result is still hanging on disk
        partialResult.check();

        app.deferred.resolve();
      });
  })

  .factory("app", function($q, $window) {
    var api = {
      deferred: undefined,
      ready: undefined,
      tablet: undefined,
      test: undefined,
      browser: undefined,
      debug: undefined
    };

    // for tablet/browser distinction
    if (window.cordova) {
      api.tablet = true;
    } else if (window.__karma__) {
      api.test = true;
    } else {
      api.browser = true;
    }

    api.deferred = $q.defer();
    api.ready = function() {
      return api.deferred.promise;
    };

    return api;
  });

// bootstrap app onto the document
angular.bootstrap(document, ["tabsint"]);
