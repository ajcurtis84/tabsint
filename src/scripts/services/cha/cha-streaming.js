/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

/* globals A2DP_CHA: false */

angular
  .module("tabsint.services.cha.streaming", [])
  .factory("chaStreaming", function($q, $interval, $timeout, disk, logger) {
    var api = {
      myA2DP: {},
      flags: {},
      chaTasks: {},
      streaming: false
    };

    var deferred = {},
      timers = {},
      intervals = {};

    function happy(info) {
      //console.log('DEBUG: CHA generic success callback: ' + angular.toJson(info));
      //logger.debug('CHA generic success callback: ' + angular.toJson(info));
    }

    api.getA2DP_CHA = function(obj) {
      return new A2DP_CHA(obj);
    };

    /**
     * Event handler for associatedA2dpDiscovered event from CHA.
     * @param {string} Name of event. Always has value "associatedA2dpDiscovered".
     * @return {promise} [Resolves if streaming is disabled or if A2DP_CHA is instantiated.]
     * @throws {Object} [ Rejects in all other cases, returns error code and message.]
     */
    api.a2dpEvent = function(result) {
      if (disk.disableAudioStreaming) {
        logger.debug("CHA - A2DP Event while streaming is disabled, ignoring");
        return;
      }

      if (result.A2DP_CHA && result.A2DP_CHA !== "null") {
        logger.debug("CHA - A2DP event: " + JSON.stringify(result));
        try {
          // The usage below follows the example at http://summit/svn/7114-Skilled-Hearing-III/branches/CHA_JS_API_Example/www/index.html
          api.myA2DP = api.getA2DP_CHA(result.A2DP_CHA);
        } catch (err) {
          logger.error("CHA - Could not initialize A2DP_CHA");
          // Propagate error to hit "catch" in cha.connect promise chain.
          throw {
            code: 551,
            msg: "Could not initialize A2DP_CHA"
          };
        }
      } else {
        logger.error("CHA - A2DP returned null/undefined A2DP object: " + JSON.stringify(result));
        // Propagate error to hit "catch" in cha.connect promise chain.
        throw {
          code: 552,
          msg: "A2DP object returned null/undefined"
        };
      }
    };
    /**
     * Request A2DP object from CHA
     * @return {promise} [description]
     */
    api.requestA2DP = function(myCha) {
      if (disk.disableAudioStreaming) {
        logger.debug("CHA - streaming disabled");
        return;
      }

      logger.debug("CHA - requesting A2DP");
      if (angular.isDefined(myCha)) {
        // If successful, CHA sends event to TabSINT that creates A2DP obj and stores on api.myA2DP.
        // A2DP should persist.
        myCha.requestAssociatedA2DP(happy, function(err) {
          logger.error("ERROR in cha.requestA2DP: " + angular.toJson(err));
          throw { code: 553, msg: err };
        });
      } else {
        var msg =
          "CHA requestA2DP cannot be called.  cha.state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(myCha);
        logger.warn(msg);
        throw { code: 553, msg: msg };
      }
    };
    /**
     * Connect to A2DP on cha
     * @return {promise} [description]
     */
    api.connectA2DP = async function() {
      if (disk.disableAudioStreaming) {
        return;
      }

      if (angular.isDefined(api.myA2DP)) {
        api.myA2DP.beginConnection(
          function() {
            logger.debug("CHA - connected to A2DP");
          },
          function(err) {
            logger.error("CHA - in myA2DP.beginConnection: " + angular.toJson(err));
            throw { code: 555, msg: err };
          }
        );
      } else {
        logger.error("No A2DP object to connect");
        throw {
          code: 554,
          msg: "No A2DP object to connect"
        };
      }
      try {
        await api.pairA2DP();
        api.streaming = true;
      } catch (e) {
        api.streaming = false;
        throw e;
      }
    };
    /**
     * Pair A2DP on cha
     * @return {promise} [description]
     */
    api.pairA2DP = function() {
      if (disk.disableAudioStreaming) {
        return;
      }

      // function to handle final pair check
      function checkPairStatus() {
        try {
          api.myA2DP.isPaired(
            function(response) {
              if (response === "true") {
                logger.debug("CHA - paired to A2DP");
              } else {
                logger.debug("CHA - A2DP not paired after final pair check");
                throw { code: 556, msg: "A2DP not paired" };
              }
            },
            function(err) {
              logger.error("CHA - error in myA2DP.isPaired: " + angular.toJson(err));
              throw { code: 556, msg: err };
            }
          );
        } catch (e) {
          throw { code: 556, msg: e };
        }
      }

      // function to pair with A2DP
      function pairA2DP() {
        try {
          api.myA2DP.beginPairing(
            function(response) {
              logger.debug("CHA - paired to A2DP");

              // wait a moment then check pair status
              $timeout(() => {
                checkPairStatus();
              }, 500);
            },
            function(err) {
              logger.error("CHA - error in myA2DP.beginPairing: " + angular.toJson(err));
              throw { code: 556, msg: err };
            }
          );
        } catch (e) {
          throw { code: 556, msg: e };
        }
      }

      if (angular.isDefined(api.myA2DP)) {
        try {
          api.myA2DP.isPaired(
            function(response) {
              if (response === "true") {
                logger.debug("CHA - paired to A2DP");
              } else {
                logger.debug("CHA - A2DP not paired");
                pairA2DP();
              }
            },
            function(err) {
              logger.error("CHA - error in myA2DP.isPaired: " + angular.toJson(err));
              throw { code: 556, msg: err };
            }
          );
        } catch (e) {
          throw { code: 556, msg: e };
        }
      } else {
        logger.error("No A2DP object to connect");
        throw {
          code: 554,
          msg: "No A2DP object to connect"
        };
      }
    };
    api.checkA2DPStatus = async function() {
      function handleResponse(response) {
        if (response === "true") {
          return;
        } else {
          throw "A2DP not connected";
        }
      }
      function handleError(err) {
        throw err;
      }
      api.myA2DP.isConnected(handleResponse, handleError);
    };
    /**
     * Check A2DP streaming connection
     * @return {promise} [description]
     */
    api.checkA2DPConnection = async function() {
      if (disk.disableAudioStreaming) {
        return;
      }

      function handleError(err) {
        $interval.cancel(intervals.isConnectedInterval);
        logger.error("CHA - failed to check A2DP connection with error: " + angular.toJson(err));
        throw { code: 560, msg: err };
      }

      function handleResponse(response) {
        var wasStreaming = false;
        if (api.streaming) wasStreaming = true;
        try {
          if (response === "true") {
            logger.debug("CHA - A2DP connection check successful");
            api.streaming = true;
            $interval.cancel(intervals.isConnectedInterval);
            $timeout.cancel(timers.stopAndFailTimeout);
          } else if (wasStreaming) {
            logger.error("Lost streaming in handleResponse.");
            $interval.cancel(intervals.isConnectedInterval);
            api.disconnectA2DP();
            logger.debug({
              code: 558,
              msg: "Lost streaming in handleResponse."
            });
          }
        } catch (e) {
          logger.debug({ code: 558, msg: e });
        }
      }

      function checkConnection() {
        api.myA2DP.isConnected(handleResponse, handleError);
      }

      async function handleInitialResponse(response) {
        var wasStreaming = false;
        if (api.streaming) wasStreaming = true;
        try {
          if (response === "true") {
            logger.debug("CHA - A2DP connection check successful");
            api.streaming = true;
          } else {
            await api.connectA2DP();
          }
        } catch (e) {
          logger.debug({ code: 558, msg: e });
        }
      }
      try {
        if (api.myA2DP) {
          await api.myA2DP.isConnected(handleInitialResponse, handleError);
        } else {
          throw {
            code: 557,
            msg: "No A2DP object available for connection check."
          };
        }
      } catch (e) {
        throw { code: 557, msg: e };
      }
    };

    /**
     * Disconnect from A2DP
     * @return {promise} [description]
     */
    api.disconnectA2DP = function() {
      deferred.disconnectA2DP = $q.defer();
      if (disk.disableAudioStreaming) {
        return $q.resolve();
      }

      $interval.cancel(intervals.isConnectedInterval);
      $timeout.cancel(timers.stopAndFailTimeout);
      if (api.myA2DP && Object.keys(api.myA2DP).length > 0) {
        api.myA2DP.disconnect(
          function() {
            deferred.disconnectA2DP.resolve();
            api.streaming = false;
          },
          function(e) {
            logger.error("CHA - failed trying to disconnect A2DP: " + angular.toJson(e));
            deferred.disconnectA2DP.reject({ code: 561, msg: e });
          }
        );
      } else {
        deferred.disconnectA2DP.resolve();
      }
      return deferred.disconnectA2DP.promise;
    };

    return api;
  });
