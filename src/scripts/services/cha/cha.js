/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

/* globals ChaWrap: false, A2DP_CHA: false */
/* jshint bitwise: false*/

angular
  .module("tabsint.services.cha", [])
  .factory("cha", function(
    $cordovaFile,
    $interval,
    $q,
    $timeout,
    app,
    bluetoothStatus,
    chaConstants,
    chaStreaming,
    chooseCha,
    devices,
    disk,
    file,
    gettextCatalog,
    json,
    logger,
    mockChaWrap,
    notifications,
    paths,
    tasks
  ) {
    var api = {
      myCha: undefined,
      myA2DP: undefined,
      state: "disconnected",
      fileOpState: undefined,
      fileOpStates: undefined,
      flags: [],
      connect: undefined,
      tryReconnect: undefined,
      cancelConnect: undefined,
      disconnect: undefined,
      queueExam: undefined,
      setSoftwareButtonState: undefined,
      abortExams: undefined,
      requestResults: undefined,
      requestCalibrationList: undefined,
      requestCalibrationEntry: undefined,
      requestId: undefined,
      requestSetting: undefined,
      requestProbeId: undefined,
      requestStatus: undefined,
      handleStatus: undefined,
      updateBluetoothCallback: undefined,
      isBluetoothBuild: undefined,
      getBluetoothType: undefined,
      changeBluetoothType: undefined,
      errorHandler: undefined,
      connectionDropCB: undefined,
      updateFirmware: undefined,
      cancelFirmwareUpdate: undefined,
      simulateResponses: false,
      disableAudioStreaming: false,
      streaming: false,
      formatSDCard: undefined,
      getMediaReposFromCha: undefined,
      getProtectedMediaVersionFromCha: undefined
    };

    // Globals
    var bluetoothType,
      deferred = {}, // Empty object for all deferred promises
      timers = {}, // Empty object for all $timeout timers
      embeddedFirmwarePath = paths.www("firmware") + "/CHA_firmware.dat", // We have to treat this one special - the CHA can't access android_assets
      embeddedManifestPath = paths.www("firmware") + "/manifest.json",
      firmwarePath = "chaFirmware.dat", // the location of the firmware file after it is copied from the assets directory to a more readable directory
      firmwareSize, // gets set on transfer start, used to compare against the finished file on the cha for confirmation...
      getDirectoryResultList, // container for returned items during async requestDirectory call
      tempManifestBuildDateTime; // object to old firmware info until update is complete

    //Constants defined in chaConstants module in cha-constants.js
    //giving them local names to avoid changing more code
    const FILE_OP_STATES = chaConstants.fileOpStates;
    api.fileOpStates = FILE_OP_STATES;
    const chaFlags = chaConstants.flags;
    const chaTasks = chaConstants.chaTasks;
    Object.freeze(FILE_OP_STATES); //constant

    api.fileOpState = FILE_OP_STATES.NONE; // a file operation state machine handler so the event handler knows how to parse new file/directory events

    // initialization block
    app.ready().then(function() {
      // default to BTLE
      bluetoothType = "BLUETOOTH_LE";

      // override config by disk object
      if (disk.cha.bluetoothType) {
        bluetoothType = disk.cha.bluetoothType;
      }

      // copy firmware to
      prepareFirmware();
    });

    // Disconnect from bluetooth if the program is exited / reloaded
    window.addEventListener("beforeunload", onUnload, false);
    function onUnload() {
      api.disconnect();
    }

    // add api to window variable
    // if (window.tabsint) {
    //   window.tabsint.cha = api;
    // }

    /**
     * Toggle between bluetooth types for CHA
     * @param  {string} type [description]
     * @return {undefined}
     */
    api.changeBluetoothType = function(type) {
      if (type === "BLUETOOTH_LE" || type === "BLUETOOTH" || type === "USB") {
        if (bluetoothType !== type) {
          logger.debug("set cha bluetooth type to " + type);
          bluetoothType = type;
          api.disconnect();
          disk.cha.bluetoothType = type;
          disk.cha.myCha = "";
        }
      } else {
        logger.debug("attempting to set cha bluetooth type to " + type);
      }
    };

    /**
     * Wrapper for internal bluetoothType
     * @return {string}
     */
    api.getBluetoothType = function() {
      return bluetoothType;
    };

    /**
     * Return if this build of tabsint has the CHA Cordova plugin
     * dictated by the presence of window variable ChaWrap
     * @return {Boolean} [description]
     */
    api.isBluetoothBuild = function() {
      return !!window.ChaWrap;
    };

    /*****************
     * Event Handling
     *****************/

    /**
     * CHA Event Listener
     * Set to listen for events from CHA via ChaWrap
     * The eventArray has two elements.
     * 1. string representing the type of event: ["Id", "Status", "Result", or "Error"]
     * 2. JSON object containing key/value pairs for the fields.
     *
     * @param  {array} eventArray [description]
     * @return {undefined}
     */
    function eventListener(eventArray) {
      try {
        // console.log(eventArray);
        if (eventArray[0] === "Id") {
          idEvent(eventArray[1]);
        } else if (eventArray[0] === "ProbeId") {
          probeIdEvent(eventArray[1]);
        } else if (eventArray[0] === "Setting") {
          requestSettingEvent(eventArray[1]);
        } else if (eventArray[0] === "Status") {
          statusEvent(eventArray[1]);
        } else if (eventArray[0] === "Result") {
          resultEvent(eventArray[1]);
        } else if (eventArray[0] === "FileProgress") {
          fileProgressEvent(eventArray[1]);
        } else if (eventArray[0] === "FileOperationComplete") {
          fileOperationCompleteEvent(eventArray[1]);
        } else if (eventArray[0] === "DirEntry") {
          dirEntryEvent(eventArray[1]);
        } else if (eventArray[0] === "Error") {
          // check to see if getMediaRepo promise is in progress and the error contains "The CHA found an error with its file system"
          if (
            deferred.getMediaRepo &&
            deferred.getMediaRepo.promise &&
            deferred.getMediaRepo.promise.$$state.status === 0 &&
            eventArray[1] &&
            eventArray[1].Message &&
            eventArray[1].Message.indexOf("The CHA found an error with its file system") > -1
          ) {
            logger.warn(
              "CHA - File: " + deferred.getMediaRepo.fileName + " is not on the CHA. Media version not recorded."
            );
          } else {
            logger.debug("CHA - Error event while in state: " + api.state);
            api.errorHandler.main(eventArray[1]);
          }
        } else if (eventArray[0] === "AssociatedA2dpDiscovered") {
          chaStreaming.a2dpEvent(eventArray[1]);
        } else if (eventArray[0] === "Disconnected") {
          logger.debug("CHA - Disconnect event while in state: " + api.state);
          if (api.state === "connected") {
            api.disconnect(true); // try to reconnect
          } else {
            api.disconnect(); // dont try to reconnect
          }
        } else if (eventArray[0] === "FormatComplete") {
          logger.debug("CHA - Format of SD card is complete");
          deferred.formatSDCard.resolve();
        } else if (eventArray[0] === "CalibrationList") {
          logger.debug("CHA - CalibrationList event:");
          logger.debug(eventArray);
          calibrationListEvent(eventArray[1]);
        } else if (eventArray[0].indexOf("CalibrationEntry #") > -1) {
          logger.debug("CHA - CalibrationEntry event:");
          logger.debug(eventArray);
          calibrationEntryEvent(eventArray[1]);
        } else {
          logger.debug("CHA unknown event: " + JSON.stringify(eventArray));
        }
      } catch (err) {
        if (err.code) {
          api.errorHandler.main(err);
        } else {
          api.errorHandler.main({ code: 40, msg: angular.toJson(err) });
        }
      }
    }

    function idEvent(id) {
      id = filterId(id);
      logger.debug("CHA - ID event: " + angular.toJson(id));
      api.myCha.id = id; // Object contains: .board, .buildDateTime, .cpuId, .description, .serialNumber
      if (api.myCha.id.description) {
        let dateRegex = /.*(([0-9]{4})-?(1[0-2]|0[1-9])-?(3[01]|0[1-9]|[12][0-9])).*$/;
        try {
          api.myCha.id.chaCalibrationDate = dateRegex.exec(api.myCha.id.description)[1];
        } catch (e) {
          logger.info("Could not extract calibration date from description");
        }
      }
      // handle serial number
      if (api.myCha.id.serialNumber && api.myCha.id.serialNumber < 0) {
        try {
          api.myCha.id.serialNumber = (0xffffffff + api.myCha.id.serialNumber + 1).toString(16);
        } catch (e) {
          logger.error("CHA - serial number could not converted to hex");

          // try alternate route
          if (api.myCha.id.description) {
            try {
              api.myCha.id.serialNumber = api.myCha.id.description.split(" ")[1].replace("SN#", "");
            } catch (e) {
              logger.error("CHA - serial number could not be extracted from description");
            }
          }
        }
      }

      if (deferred.requestId) {
        deferred.requestId.resolve();
      }
    }

    function requestSettingEvent(setting) {
      // console.log(setting);
      try {
        api.myCha.setting = {
          auto_shutdown_time: setting["Value"]
        };

        if (setting["Value"] !== disk.shutdownTimer) {
          api.myCha.writeSetting("auto_shutdown_time", disk.shutdownTimer);
          // below line GAURANTEES the shutdown times match but is likely overkill
          // could just use the simpler comented version below
          api.myCha.requestSetting("auto_shutdown_time");
          // api.myCha.setting = {
          //   auto_shutdown_time: setting["Value"]
          // };
        }
      } catch (e) {
        logger.info("WAHTS Shutdown Timer not Supported");
      }

      if (deferred.requestSetting) {
        deferred.requestSetting.resolve();
      }
    }

    function probeIdEvent(id) {
      id = filterId(id);
      logger.debug("CHA - Probe ID event: " + angular.toJson(id));
      try {
        api.myCha.probeId = id;
      } catch (e) {
        logger.warn("CHA - Failed while assigning probe id: " + angular.toJson(e));
      }

      if (deferred.requestProbeId) {
        deferred.requestProbeId.resolve();
      }
    }

    function checkStatus(status) {
      //checks if the Status has been defined, then if any error flags have been set,
      //then notifies the user of errors (along with the full flag code).
      var activeFlags = [];

      if (angular.isDefined(status.Flags)) {
        for (var key in chaFlags) {
          if (status.Flags & key) {
            activeFlags.push(key);
          }
        }
      }

      // check to see if the current active flags are different than the previous flags
      if (_.difference(activeFlags, api.flags).length !== 0) {
        api.flags = activeFlags;

        // create an array of only the error flags, then grab the subset of chaFlags
        var errorFlags = _.filter(api.flags, function(key) {
          return chaFlags[key].level === "error";
        });

        // display error flags
        if (errorFlags.length > 0) {
          var chaFlagsSubset = _.filter(chaFlags, function(obj, key) {
            return _.includes(errorFlags, key);
          });
          logger.error("CHA checkStatus returned error flags: " + JSON.stringify(chaFlagsSubset));
          notifications.alert(
            "The WAHTS returned the following error status messages:\n" +
              _.map(chaFlagsSubset, function(obj, key) {
                return '\n"' + obj.message + " (code: " + obj.name + ')"';
              })
          );
        }
      }
    }

    function statusEvent(status) {
      checkStatus(status); // make sure status is ok. Notify user if a new error flag set

      // handle battery voltage
      if (typeof status.vBattery === "number") {
        status.vBattery = Math.round(status.vBattery * 100) / 100;
      }

      api.myCha.batteryIndicatorWidth = 100;
      api.myCha.vBattery = status.vBattery;
      api.myCha.battery = {
        status: undefined,
        level: undefined
      };

      api.myCha.battery.status = function() {
        if (api.myCha.vBattery > 3.6) {
          return "Good";
        } else if (api.myCha.vBattery > 3.0) {
          return "Satisfactory";
        } else {
          return "Low";
        }
      };
      api.myCha.battery.level = function() {
        var full = api.myCha.batteryIndicatorWidth - 8;
        if (api.myCha.vBattery > 3.6) {
          return full;
        } else if (api.myCha.vBattery > 3.0) {
          return 50;
        } else {
          return 10;
        }
      };

      // resolve status promise
      if (deferred.requestStatus) {
        deferred.requestStatus.resolve(status);
      }
    }

    function resultEvent(result) {
      if (result.MLD && result.State && result.State !== "Done") {
        // don't log it.  these cha result objects from the MLD are huge, and they happen several times a second! TODO - remove this log altogether?
      } else {
        logger.debug("CHA - Result event: " + angular.toJson(result));
      }

      if (deferred.requestResults) {
        deferred.requestResults.resolve(result);
      }
    }

    function calibrationListEvent(result) {
      if (deferred.requestCalibrationList) {
        deferred.requestCalibrationList.resolve(result);
      }
    }

    function calibrationEntryEvent(result) {
      if (deferred.requestCalibrationEntry) {
        deferred.requestCalibrationEntry.resolve(result);
      }
    }

    function fileProgressEvent(result) {
      if (api.fileOpState === FILE_OP_STATES.FIRMWARE_TRANSFER && deferred.firmwareTransfer) {
        deferred.firmwareTransfer.notify(
          "Transferring firmware: " + result.BytesTransferred + "/" + result.TotalBytes + " bytes"
        );
      } else if (api.fileOpState === FILE_OP_STATES.MEDIA_TRANSFER && deferred.mediaTransfer) {
        deferred.mediaTransfer.notify(result.BytesTransferred + "/" + result.TotalBytes + " bytes");
      }
    }

    function fileOperationCompleteEvent(result) {
      logger.debug("CHA - File Operation Complete event: " + angular.toJson(result));
      if (api.fileOpState === FILE_OP_STATES.FIRMWARE_TRANSFER && deferred.firmwareTransfer) {
        api.fileOpState = FILE_OP_STATES.NONE;
        if (result.Outcome && result.Outcome.indexOf("cancelled") >= 0) {
          logger.debug("CHA - File Transfer Cancelled");
          deferred.firmwareTransfer.reject(result);
        } else if (result.Outcome && result.Outcome.indexOf("success") >= 0) {
          logger.debug("CHA - File Transfer Complete");
          deferred.firmwareTransfer.resolve(result);
        } else {
          deferred.firmwareTransfer.reject("ERROR: unknown return value from operation complete event");
        }
      } else if (api.fileOpState === FILE_OP_STATES.CONFIRMING_FIRMWARE && deferred.confirmingFirmware) {
        api.fileOpState = FILE_OP_STATES.NONE;
        deferred.confirmingFirmware.reject("Error checking chaFirmware - no CHA_PROG.DAT file found.");
      } else if (api.fileOpState === FILE_OP_STATES.GET_DIRECTORY && deferred.getDirectory) {
        api.fileOpState = FILE_OP_STATES.NONE;
        deferred.getDirectory.resolve({
          success: true,
          result: getDirectoryResultList
        });
      } else if (api.fileOpState === FILE_OP_STATES.GET_DIRECTORY_CRC && deferred.getDirectoryCrc) {
        api.fileOpState = FILE_OP_STATES.NONE;
        deferred.getDirectoryCrc.resolve({
          success: true,
          result: getDirectoryResultList
        });
      } else if (api.fileOpState === FILE_OP_STATES.MEDIA_TRANSFER && deferred.mediaTransfer) {
        api.fileOpState = FILE_OP_STATES.NONE;
        if (result.Outcome && result.Outcome.indexOf("cancelled") >= 0) {
          logger.debug("CHA Media Transfer Cancelled");
          deferred.mediaTransfer.reject(result);
        } else if (result.Outcome && result.Outcome.indexOf("success") >= 0) {
          logger.debug("CHA File Transfer Complete");
          deferred.mediaTransfer.resolve(result);
        } else {
          deferred.mediaTransfer.reject("ERROR: unknown return value from operation complete event");
        }
      } else if (api.fileOpState === FILE_OP_STATES.NONE) {
      }
    }

    function dirEntryEvent(result) {
      logger.debug("CHA - DirEntry event: " + angular.toJson(result));

      // if confirming firmware, we want to confirm the firmware once it is found, then cancel the file
      if (api.fileOpState === FILE_OP_STATES.CONFIRMING_FIRMWARE && deferred.confirmingFirmware) {
        if (
          result &&
          result.Path.indexOf("CHA_PROG.DAT") >= 0 &&
          result.SizeBytes &&
          result.SizeBytes === firmwareSize
        ) {
          api.fileOpState = FILE_OP_STATES.NONE; // need to set this otherwise fileOperationCompleteEvent will reject firmware
          api.cancelFileOperation().then(function() {
            deferred.confirmingFirmware.resolve();
          });
        }
      } else if (
        api.fileOpState === FILE_OP_STATES.GET_DIRECTORY ||
        api.fileOpState === FILE_OP_STATES.GET_DIRECTORY_CRC
      ) {
        getDirectoryResultList.push(result);
      } else if (
        api.fileOpState === FILE_OP_STATES.NONE &&
        deferred.getMediaRepo &&
        (result.Path === "MEDIAVER.TXT" || result.Path === "PROTECTE.TXT")
      ) {
        deferred.getMediaRepo.resolve();
      }
    }

    function filterId(id) {
      if (id.description) {
        try {
          id.description = id.description.replace(/\0/g, "");
        } catch (err) {
          logger.warn("Error while trying to fix null characters in cha ID");
        }
      }
      return id;
    }

    function happy(info) {
      console.log("DEBUG: CHA generic success callback: " + angular.toJson(info));
      //logger.debug('CHA generic success callback: ' + angular.toJson(info));
    }

    /*****************
     * File Operations
     *****************/

    api.cancelMediaTransfer = function() {
      if (api.fileOpState === FILE_OP_STATES.MEDIA_TRANSFER) {
        logger.info("CHA - Aborting CHA Media Transfer");
        if (deferred.mediaTransfer) {
          deferred.mediaTransfer.reject({
            code: 900,
            msg: "Media Transfer Aborted by User"
          });
        }
        api.fileOpState = FILE_OP_STATES.NONE;
        if (angular.isDefined(api.myCha) && api.state === "connected") {
          api.myCha.cancelFileOperation(
            function() {
              logger.debug("CHA - Media transfer operation cancelled.");
            },
            function(e) {
              logger.error("CHA - Aborting CHA file operation with error: " + angular.toJson(e));
            }
          );
        } else {
          logger.debug(
            "CHA - cancelMediaTransfer called with cha.state = " +
              api.state +
              " and api.myCha defined: " +
              angular.isDefined(api.myCha)
          );
        }
      }
    };

    api.transferFiles = function(nativeURLArray) {
      var mainMsg = "Transferring Media Files to CHA.";
      var subMessage = "";
      tasks.register(chaTasks.FILE_TRANSFER, mainMsg);
      var numberOfFiles = nativeURLArray.length;
      var fileCounter = 0;
      var bytesToTransfer = 0;
      var bytesTransferred = 0;
      var previousFileBytes = 0;
      nativeURLArray.forEach(function(singleFile) {
        bytesToTransfer += singleFile.fileSize;
      });
      var kBytesToTransfer = Math.round(bytesToTransfer / 1024);
      var FILE_TRANSFER_SPEEDS = {
        BLUETOOTH_LE: 800,
        BLUETOOTH: 6000,
        USB: 75000
      };
      var speed = FILE_TRANSFER_SPEEDS[bluetoothType];
      if (bytesToTransfer !== 0) {
        var estimateM = Math.round(bytesToTransfer / speed / 60);
        console.log(
          "total bytes to transfer: " +
            bytesToTransfer +
            ".  At a speed of " +
            speed +
            " Bytes/s, this will take roughly " +
            estimateM +
            " minutes."
        );
        notifications.alert(
          "Beginning to transfer media files to the headset.  Time estimate: " +
            estimateM +
            " minutes.  Please make sure tablet and headset remain plugged in to avoid battery issues and ensure complete transfer!"
        );
      } else {
        // something went wrong with fileSize calculation - don't show an estimate.
      }

      function progressCB(progressMsg) {
        tasks.register(chaTasks.FILE_TRANSFER, mainMsg + subMessage + "  Progress: " + progressMsg);
      }

      function transferSingleFile() {
        deferred.mediaTransfer = $q.defer();
        api.fileOpState = FILE_OP_STATES.MEDIA_TRANSFER;
        var fileObj = nativeURLArray.pop();
        fileCounter++;
        var fileURL = fileObj.nativeURL;
        var targetURL = fileObj.targetURL;
        bytesTransferred += previousFileBytes;
        var kBytesTransferred = Math.round(bytesTransferred / 1024);
        previousFileBytes = fileObj.fileSize;
        var fileName = targetURL.slice(targetURL.lastIndexOf("/") + 1);
        subMessage =
          "  Total progress: " +
          fileCounter +
          "/" +
          numberOfFiles +
          " files, " +
          kBytesTransferred +
          "/" +
          kBytesToTransfer +
          "kB, " +
          ".  Filename: " +
          fileName +
          ".";
        tasks.register(chaTasks.FILE_TRANSFER, mainMsg + subMessage);
        // have to remove the 'file://' part of the url!  TODO - how does this work on MAC?  BPF
        if (fileURL.indexOf("file://") > -1) {
          fileURL = fileURL.slice(7);
        }
        logger.debug("CHA - Transferring media file: " + fileURL + " to " + targetURL);

        if (angular.isDefined(api.myCha) && api.state === "connected") {
          api.myCha.startFileWriteRoot(
            fileURL,
            targetURL,
            function() {
              logger.debug("CHA - Starting file transfer");
            },
            function(e) {
              logger.error("CHA - Error starting file transfer: " + angular.toJson(e));
              api.fileOpState = FILE_OP_STATES.NONE;
              deferred.mediaTransfer.reject({ code: 1201, reason: e });
            }
          );
        } else {
          var msg =
            "CHA startFileWriteRoot cannot be called.  cha.state: " +
            api.state +
            ". api.myCha isDefined: " +
            angular.isDefined(api.myCha);
          logger.warn(msg);
          deferred.mediaTransfer.reject({ code: 1201, reason: msg });
        }
        return deferred.mediaTransfer.promise;
      }

      function transferIfRemainingFiles() {
        if (nativeURLArray.length > 0) {
          return transferSingleFile().then(transferIfRemainingFiles);
        } else {
          logger.info("Transfer Complete");
        }
      }

      return api
        .abortExams()
        .then(api.checkIsCHAConnectedUSB)
        .then(transferIfRemainingFiles)
        .then(function() {}, $q.reject({ code: 1202, reason: "CHA file transfer failed." }), progressCB)
        .catch(function(e) {
          logger.log("error in cha.transferFiles: " + JSON.stringify(e));
          if (e.code && e.code === 910) {
            //{code: 910, msg: 'CHA Not Connected Via USB'}
            notifications.alert("Headset must be plugged in to begin a file transfer");
          }
        })
        .finally(function() {
          logger.info("CHA Media Transfer Complete");
          tasks.deregister(chaTasks.FILE_TRANSFER);
        });
    };

    api.deleteFile = function(fileToDelete) {
      deferred.deleteFile = $q.defer();
      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.deleteFile(
          fileToDelete,
          function() {
            logger.debug("Success calling cha.deleteFile for file " + fileToDelete);
            api.requestStatus().then(function(status) {
              logger.debug("CHA deleted file.  Status now: " + JSON.stringify(status));
              deferred.deleteFile.resolve();
            });
          },
          function(e) {
            logger.warn("Failure calling cha.deleteFile for file " + fileToDelete + ".  Error : " + JSON.stringify(e));
            deferred.deleteFile.reject({ code: 126, msg: e });
          }
        );
      } else {
        var msg =
          "CHA deleteFile cannot be called.  cha.state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.deleteFile.reject({ code: 127, msg: msg });
      }
      return deferred.deleteFile.promise;
    };

    api.deleteFileCrc = function(fileToDelete) {
      deferred.deleteFileCrc = $q.defer();
      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.deleteFileCrc(
          fileToDelete,
          function() {
            logger.debug("Success calling cha.deleteFileCrc for file " + fileToDelete);
            api.requestStatus().then(function(status) {
              logger.debug("CHA deleted file by Crc.  Status now: " + JSON.stringify(status));
              deferred.deleteFileCrc.resolve();
            });
          },
          function(e) {
            logger.warn(
              "Failure calling cha.deleteFileCrc for file " + fileToDelete + ".  Error : " + JSON.stringify(e)
            );
            deferred.deleteFileCrc.reject({ code: 128, msg: e });
          }
        );
      } else {
        var msg =
          "CHA deleteFileCrc cannot be called.  cha.state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.deleteFileCrc.reject({ code: 128, msg: msg });
      }
      return deferred.deleteFileCrc.promise;
    };

    api.deleteDirectory = function(mainDirToDelete) {
      deferred.deleteDirectory = $q.defer();

      function deleteRecursively(currentDirToDelete) {
        console.log("deleteRecursively called with " + currentDirToDelete);
        return api
          .getDirectory(currentDirToDelete) // get and save cha files in this directory
          .then(function(returnedObj) {
            console.log("-- returnedFiles: " + JSON.stringify(returnedObj));
            if (returnedObj.success && returnedObj.result.length > 0) {
              var chain = $q.when();

              // separate all files and delete them
              var chaFiles = returnedObj.result.filter(function(entry) {
                return angular.isDefined(entry.Attributes) && !(entry.Attributes & 16);
              });
              console.log("cha deleteRecursively preparing to delete these files: " + JSON.stringify(chaFiles));
              chaFiles.forEach(function(fileToDelete) {
                chain = chain.then(function() {
                  var wholeFileToDelete = currentDirToDelete + "/" + fileToDelete.Path;
                  console.log("deleting file " + wholeFileToDelete);
                  return api.deleteFile(wholeFileToDelete);
                });
              });

              // Separate all dirs and recurse into them
              var chaDirs = returnedObj.result.filter(function(entry) {
                return angular.isDefined(entry.Attributes) && entry.Attributes & 16;
              });
              console.log("cha deleteRecursively preparing to delete these dirs: " + JSON.stringify(chaDirs));
              chaDirs.forEach(function(newSubDirToDelete) {
                var wholeDir = currentDirToDelete + "/" + newSubDirToDelete.Path;
                chain = chain.then(function() {
                  console.log("recursing into " + wholeDir);
                  return deleteRecursively(wholeDir);
                });
              });

              // now that every child file and directory has been deleted/removed, delete this dir
              chain = chain.then(function() {
                console.log(
                  "deleteRecursively done deleting sub folders and files for dir: " +
                    currentDirToDelete +
                    ".  Time to remove this dir."
                );
                return api.deleteFile(currentDirToDelete);
              });
              return chain;
            } else {
              // directory is empty - just delete it
              console.log(
                "deleteRecursively found empty dir for: " + currentDirToDelete + ".  Time to remove this dir."
              );
              return api.deleteFile(currentDirToDelete);
            }
          });
      }

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        // Can only delete empty directories - check status first.
        deleteRecursively(mainDirToDelete).then(function() {
          console.log("deleteRecursively is done!");
          deferred.deleteDirectory.resolve();
        });
      } else {
        var msg =
          "CHA deleteDirectory cannot be called.  cha.state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.deleteDirectory.reject({ code: 1001, reason: msg });
      }

      return deferred.deleteDirectory.promise;
    };

    api.makeDirectory = function(newDir) {
      deferred.makeDirectory = $q.defer();
      logger.info("calling makeDirectory with " + newDir);
      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.makeDirectory(
          newDir,
          function() {
            logger.debug("Successfully called cha.makeDirectory for path " + newDir);
            api.requestStatus().then(function(status) {
              if (status.LastCtrlError === 0) {
                logger.debug("cha.makeDirectory resolved for path " + newDir);
                deferred.makeDirectory.resolve();
              } else {
                deferred.makeDirectory.reject({
                  code: 1001,
                  reason: "CHA.makeDirectory resulted in status LastCtrlError: " + status.LastCtrlError
                });
              }
            });
          },
          function(e) {
            logger.warn("Failure calling cha.makeDirectory for path " + newDir + ".  Error : " + JSON.stringify(e));
            deferred.makeDirectory.reject({
              code: 1001,
              reason: "CHA.makeDirectory returned error: " + JSON.stringify(e)
            });
          }
        );
      } else {
        var msg =
          "CHA makeDirectory cannot be called.  cha.state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.makeDirectory.reject({ code: 1001, reason: msg });
      }
      return deferred.makeDirectory.promise;
    };

    api.makeDirectoryRoot = function(newDir) {
      deferred.makeDirectoryRoot = $q.defer();
      logger.info("calling makeDirectoryRoot with " + newDir);
      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.makeDirectoryRoot(
          newDir,
          function() {
            logger.debug("Successfully called cha.makeDirectoryRoot for path " + newDir);
            api.requestStatus().then(function(status) {
              if (status.LastCtrlError === 0) {
                logger.debug("cha.makeDirectoryRoot resolved for path " + newDir);
                deferred.makeDirectoryRoot.resolve();
                deferred.makeDirectoryRoot.resolve();
              } else {
                deferred.makeDirectoryRoot.reject({
                  code: 1001,
                  reason: "CHA.makeDirectoryRoot resulted in status LastCtrlError: " + status.LastCtrlError
                });
              }
            });
          },
          function(e) {
            logger.warn("Failure calling cha.makeDirectoryRoot for path " + newDir + ".  Error : " + JSON.stringify(e));
            deferred.makeDirectoryRoot.reject({
              code: 1001,
              reason: "CHA.makeDirectoryRoot returned error: " + JSON.stringify(e)
            });
          }
        );
      } else {
        var msg =
          "CHA makeDirectoryRoot cannot be called.  cha.state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.makeDirectoryRoot.reject({ code: 1001, reason: msg });
      }
      return deferred.makeDirectoryRoot.promise;
    };

    api.cancelFileOperation = function() {
      deferred.cancelFileOperation = $q.defer();
      api.myCha.cancelFileOperation(
        function() {
          logger.debug("CHA - File operation cancelled.");
          deferred.cancelFileOperation.resolve();
        },
        function(e) {
          var msg = "CHA - Aborting CHA file operation with error: " + angular.toJson(e);
          logger.error(msg);
          deferred.cancelFileOperation.reject({ code: 1500, msg: msg });
        }
      );
      return deferred.cancelFileOperation.promise;
    };

    function format() {
      deferred.formatSDCard = $q.defer();
      if (angular.isDefined(api.myCha.format) && api.state === "connected") {
        notifications.confirm("Are you sure you want to format the SD card in the WAHTS?", function(buttonIndex) {
          if (buttonIndex !== 1) {
            deferred.formatSDCard.reject();
          } else {
            api.myCha.format(
              function(success) {
                logger.debug("CHA - successfully started format of SD card.");
              },
              function(error) {
                deferred.formatSDCard.reject();
              }
            );
          }
        });
      } else {
        deferred.formatSDCard.reject({
          Error: "There is no WAHTS, or the API does not include the format method."
        });
      }
      return deferred.formatSDCard.promise;
    }

    api.formatSDCard = function() {
      tasks.register("Format WAHTS", "Formatting WAHTS SD card");
      format()
        .then(
          function(success) {
            notifications.alert("Format of WAHTS was successful.");
          },
          function(error) {
            notifications.alert("Error when formatting WAHTS." + (error ? " " + JSON.stringify(error) : ""));
          }
        )
        .finally(function() {
          tasks.deregister("Format WAHTS");
        });
    };

    api.getDirectory = function(directoryName) {
      deferred.getDirectory = $q.defer();
      api.fileOpState = FILE_OP_STATES.GET_DIRECTORY;
      getDirectoryResultList = []; // reset this container - container gets filled on DirEntry events, File Operation Complete Event resolves getDirectory promise with this container

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.requestDirectory(
          directoryName,
          function() {},
          function(e) {
            var msg = "CHA requestDirectory failed with error: " + JSON.stringify(e);
            logger.error(msg);
            deferred.getDirectory.reject({ code: 901, msg: msg });
            api.fileOpState = FILE_OP_STATES.NONE;
          }
        );
      } else {
        var msg =
          "CHA requestDirectory cannot be called.  cha.state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.getDirectory.reject({ code: 901, msg: msg });
      }
      return deferred.getDirectory.promise;
    };

    api.getDirectoryCrc = function(directoryName) {
      deferred.getDirectoryCrc = $q.defer();
      api.fileOpState = FILE_OP_STATES.GET_DIRECTORY_CRC;

      getDirectoryResultList = []; // reset this container - container gets filled on DirEntry events, File Operation Complete Event resolves getDirectory promise with this container
      if (directoryName.charAt(directoryName.length - 1) === "/") {
        // weird cha behavior - returns all '00000000's for crcs if trailing '/'
        directoryName = directoryName.slice(0, directoryName.length - 1);
      }

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.requestDirectoryCrc(
          directoryName,
          function() {},
          function(e) {
            var msg = "CHA requestDirectoryCrc failed with error: " + JSON.stringify(e);
            logger.error(msg);
            deferred.getDirectoryCrc.reject({ code: 901, msg: msg });
            api.fileOpState = FILE_OP_STATES.NONE;
          }
        );
      } else {
        var msg =
          "CHA requestDirectoryCrc cannot be called.  cha.state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.getDirectoryCrc.reject({ code: 901, msg: msg });
      }
      return deferred.getDirectoryCrc.promise;
    };

    /**
     * Check if the CHA is connected via USB
     * @return {promise} [description]
     */
    api.checkIsCHAConnectedUSB = function() {
      //TODO: could this be rewritten based on new status parsing?
      deferred.checkIsCHAConnectedUSB = $q.defer();

      api.requestStatus().then(function(status) {
        logger.debug(
          "CHA - Status before firmware update: " +
            JSON.stringify(status) +
            ". Connected to USB = " +
            (status.Flags & chaFlags.CHA_FLAGS_USB_CONNECTED)
        );
        if (status.Flags & chaFlags.CHA_FLAGS_USB_CONNECTED) {
          // from chacodes.h in HearingProducts/Shared/include SVN
          deferred.checkIsCHAConnectedUSB.resolve(status);
        } else {
          deferred.checkIsCHAConnectedUSB.reject({
            code: 910,
            msg: "CHA Not Connected Via USB"
          });
        }
      });

      return deferred.checkIsCHAConnectedUSB.promise;
    };

    /*****************
     * Firmware
     *****************/

    /**
     * Transfer firmware
     * @return {[type]} [description]
     */
    function transferFirmware() {
      logger.debug("CHA - Transferring firmware");
      deferred.firmwareTransfer = $q.defer();
      api.fileOpState = FILE_OP_STATES.FIRMWARE_TRANSFER;
      tasks.register(chaTasks.UPDATE_FIRMWARE, "Transferring firmware");

      var localPath = firmwarePath;

      getNativeURL(localPath).then(function(fileURL) {
        // have to remove the 'file://' part of the url!  TODO - how does this work on MAC?  BPF
        if (fileURL.indexOf("file://") > -1) {
          fileURL = fileURL.slice(7);
        }

        if (angular.isDefined(api.myCha) && api.state === "connected") {
          api.myCha.startFileWrite(
            fileURL,
            "CHA_PROG.DAT",
            function() {
              logger.debug("CHA - Starting file transfer");
            },
            function(e) {
              logger.error("CHA - Error starting file transfer: " + angular.toJson(e));
              deferred.firmwareTransfer.reject({
                code: 1800,
                msg: "firmware transfer failed"
              });
            }
          );
        } else {
          logger.warn("CHA - startFileWrite cannot be called in state: " + api.state);
          deferred.firmwareTransfer.reject({
            code: 1801,
            msg: "firmware transfer failed"
          });
        }
      });

      // get nativeURL
      function getNativeURL(path) {
        return file.getFile(path).then(function(fileEntry) {
          fileEntry.file(function(theFile) {
            firmwareSize = theFile.size;
          });
          return fileEntry.nativeURL;
        });
      }

      return deferred.firmwareTransfer.promise;
    }

    /**
     * Confirm Firmware on the CHA
     * If successful, this will resolve during a DirEntryEvent
     * @return {[type]} [description]
     */
    function confirmFirmware() {
      deferred.confirmingFirmware = $q.defer();
      api.fileOpState = FILE_OP_STATES.CONFIRMING_FIRMWARE;

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        //temporary workaround for OAE Screeners
        if (devices.platform === "ios") {
          logger.debug("CHA - confirmFirmware() workaround for OAES on ios.");
          deferred.confirmingFirmware.resolve(); //return right away
        } else {
          api.myCha.requestDirectory(
            "",
            function() {},
            function(e) {
              logger.error("CHA - checking transferred firmware failed with error: " + JSON.stringify(e));
              deferred.confirmingFirmware.reject({
                code: 901,
                msg: "confirm firmware failed"
              });
              api.fileOpState = FILE_OP_STATES.NONE;
            }
          );
        }
      } else {
        logger.warn("CHA - requestDirectory to confirmFirmware cannot be from state " + api.state);
        deferred.confirmingFirmware.reject({
          code: 901,
          msg: "confirm firmware failed"
        });
      }

      return deferred.confirmingFirmware.promise;
    }

    /**
     * Reprogram firmware
     * @return {[type]} [description]
     */
    function reprogramFirmware() {
      deferred.firmwareReprogram = $q.defer();
      tasks.register(chaTasks.UPDATE_FIRMWARE, "Reprogramming WAHTS");

      confirmFirmware().then(function() {
        logger.debug("CHA - firmware confirmed, initiating reprogram");
        if (angular.isDefined(api.myCha) && api.state === "connected") {
          api.state = "reprogram"; // manually override state that the Disconnect listener doesn't try to reconnect
          api.myCha.reprogram(
            function() {
              logger.debug("CHA - succesfully started reprogramming firmware");
              deferred.firmwareReprogram.resolve();
            },
            function(e) {
              logger.error("CHA - Error reprogramming CHA: " + angular.toJson(e));
              deferred.firmwareReprogram.reject({
                code: 1300,
                msg: "reprogram failed"
              });
            }
          );
        } else {
          logger.warn("CHA - reprogram cannot be called from state " + api.state);
          deferred.firmwareReprogram.reject({
            code: 1300,
            msg: "reprogram failed"
          });
        }
      });

      return deferred.firmwareReprogram.promise;
    }

    /**
     * Prepare firmware for transfer
     *
     * all a work-around to get the files from assets (which is not currently readable by the CHA java layer)
     * to the data/data/...../files/files directory used by everything downloaded
     * @return {promise} [description]
     */
    function prepareFirmware() {
      if (app.test) {
        return $q.resolve();
      }

      return file
        .copyAsset(embeddedFirmwarePath, firmwarePath)
        .then(function() {
          return file.readFile(embeddedManifestPath);
        })
        .then(function(manifest) {
          logger.debug("CHA - firmware successfully copied: " + JSON.stringify(manifest));
          tempManifestBuildDateTime = manifest.buildDateTime;
          return $q.resolve();
        })
        .catch(function(err) {
          logger.error("CHA - manifest file did not copy correctly with error: " + err);
        });
    }

    /**
     * Update firmware
     * @return {promise}                      [description]
     */
    api.updateFirmware = function() {
      logger.debug("CHA - Updating CHA firmware");
      tasks.register(chaTasks.UPDATE_FIRMWARE, "Updating WAHTS firmware");

      // update progress on the active tasks
      function firmwareUpdateProgress(msg) {
        tasks.register(chaTasks.UPDATE_FIRMWARE, msg);
      }

      return api
        .abortExams()
        .then(api.checkIsCHAConnectedUSB)
        .then(prepareFirmware)
        .then(transferFirmware)
        .then(reprogramFirmware)
        .then(null, null, firmwareUpdateProgress)
        .then(function() {
          logger.debug("CHA - Updating CHA firmware finished");

          // try a reboot.
          // In urial, the firmware automatically reboots so this will just pass
          api.reboot();
          notifications.alert(
            `The firmware on the WAHTS has been successfully updated to ${tempManifestBuildDateTime}. Please reconnect to the WAHTS to begin using the new firmware.`
          );
        })
        .catch(function(e) {
          var msg = "CHA - Firmware failed to update.  ";
          if (angular.isDefined(e.msg)) {
            msg += e.msg;
            if (e.msg.indexOf("Aborted by User") >= 0) {
              notifications.alert("CHA Firmware Update Aborted");
            } else if (e.msg.indexOf("CHA Not Connected Via USB") >= 0) {
              notifications.alert("Please connect the WAHTS to a USB power source before updating firmware.");
            } else {
              notifications.alert(
                "The device encounted an error while trying to update the WAHTS firmware.  \n\n Please try updating the firmware again."
              );
            }
          } else {
            notifications.alert(
              "The device encounted an error while trying to update the WAHTS firmware.  \n\n Please try updating the firmware again."
            );
          }
          logger.error(msg);
        })
        .finally(function() {
          tasks.deregister(chaTasks.UPDATE_FIRMWARE);
        });
    };

    /**
     * Cancel firmware upate
     * @return {[type]} [description]
     */
    api.cancelFirmwareUpdate = function() {
      tasks.register(chaTasks.CANCEL_FIRMWARE, "Cancelling firmware update");

      if (tasks.isOngoing(chaTasks.UPDATE_FIRMWARE)) {
        logger.info("CHA - Aborting CHA Firmware Transfer");
        deferred.firmwareTransfer.reject({
          code: 900,
          msg: "Firmware Transfer Aborted by User"
        });

        if (angular.isDefined(api.myCha) && api.state === "connected") {
          api.myCha.cancelFileOperation(
            function() {
              logger.debug("CHA - firmware update cancelled");
              api.deleteFirmware();
            },
            function(e) {
              logger.error("CHA - failed to cancel firmware update with error: " + angular.toJson(e));
            }
          );
        } else {
          logger.debug("CHA - cancelFileOperation cannot be called from state " + api.state);
        }
      }

      tasks.deregister(chaTasks.CANCEL_FIRMWARE);
    };

    /**
     * Software reboot of the cha
     * @return {promise} [description]
     */
    api.reboot = function() {
      deferred.reboot = $q.defer();

      if (angular.isDefined(api.myCha) && (api.state === "connected" || api.state === "reprogram")) {
        api.myCha.reboot(
          function() {
            logger.debug("CHA - Rebooting CHA");
            api.disconnect();
            deferred.reboot.resolve();
          },
          function(e) {
            logger.error("CHA - Error starting reboot: " + angular.toJson(e));
            deferred.reboot.reject({ code: 904, msg: "reboot failed." });
          }
        );
      } else {
        logger.warn("CHA reboot cannot be called from state " + api.state);
        deferred.reboot.reject({ code: 904, msg: "reboot failed" });
      }

      return deferred.reboot.promise;
    };

    /**
     * Delete Firmware on the CHA
     * @return {[type]} [description]
     */
    api.deleteFirmware = function() {
      deferred.deleteFirmware = $q.defer();

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.deleteFile(
          "CHA_PROG.DAT",
          function() {
            logger.debug("CHA - deleted CHA_PROG.DAT");
            deferred.deleteFirmware.resolve();
          },
          function(e) {
            logger.warn("CHA - Failed to delete CHA_PROG.DAT: ", e);
            deferred.deleteFirmware.reject({ code: 125, msg: e });
          }
        );
      } else {
        logger.warn("CHA - deleteFirmware cannot be called from state: " + api.state);
        deferred.deleteFirmware.reject({
          code: 124,
          msg: "delete firmware failed"
        });
      }

      return deferred.deleteFirmware.promise;
    };

    /*****************
     * Connect / Disconnect
     *****************/

    /**
     * Method to wrap "Try Reconnect" button during exam
     * @return {promise} [description]
     */
    api.tryReconnect = function() {
      logger.debug("CHA - User pressed try to reconnect while in exam");
      return api.disconnect(true);
    };

    /**
     * Connect to the CHA
     * @param  {boolean} reconnect - boolean depicting if this is a reconnect
     * @return {promise}           [description]
     */
    api.connect = function(reconnect) {
      if (api.state !== "disconnected") {
        logger.debug("CHA - alreay connected or connecting");
        return $q.reject();
      }

      function checkBluetoothRadio() {
        deferred.checkBluetooth = $q.defer();

        api.state = "checkingBluetooth";
        tasks.register(chaTasks.CONNECT, "Checking bluetooth status");

        function qs(msg) {
          if (msg === "Bluetooth Off") {
            deferred.checkBluetooth.reject({ code: 100, msg: msg });
          } else if (msg === "Bluetooth Unknown") {
            deferred.checkBluetooth.reject({ code: 101, msg: msg });
          } else {
            deferred.checkBluetooth.resolve();
          }
        }

        function qe(msg) {
          logger.warn("CHA - Could not check Bluetooth status: " + msg);
          deferred.checkBluetooth.resolve();
        }

        ChaWrap.getBluetoothAdapterState(qs, qe);
        return deferred.checkBluetooth.promise.then(() => {
          let q = $q.defer();

          // check that permission for location is enabled
          if (app.tablet && navigator.geolocation.getCurrentPosition) {
            navigator.geolocation.getCurrentPosition(
              () => {
                q.resolve();
              },
              e => {
                if (e.code == e.PERMISSION_DENIED) {
                  notifications.alert(
                    'Location services must be allowed and enabled to connect to the WAHTS. Device location will not be recorded unless the setting "Record Test Location" is enabled.'
                  );
                }
                q.resolve();
              },
              {
                timeout: 500
              }
            );
          }

          // if browser or no getCurrentPosition is available
          else {
            q.resolve();
          }

          return q.promise;
        });
      }

      function discoverCha() {
        deferred.discovery = $q.defer();
        var chaList = [];

        if (reconnect) {
          api.state = "autoDiscovery";
          logger.debug("CHA - Starting autoDiscovery in reconnect");
          tasks.register(chaTasks.CONNECT, "Searching for preferred WAHTS");

          ChaWrap.startChaSearch(
            bluetoothType,
            function(cha) {
              gotCha(cha);
            },
            function() {
              noGotCha();
            }
          );
          // wait 5 seconds to see if the saved cha is still available.  If not, error out.  Do not pop the modal in an exam.
          timers.reconnectTimer = $timeout(stopReconnect, 5000); // if this is too long, could cause problems
        } else if (disk.cha.myCha) {
          api.state = "autoDiscovery";
          logger.debug("CHA - Starting autoDiscovery");
          tasks.register(chaTasks.CONNECT, "Searching for preferred WAHTS");

          ChaWrap.startChaSearch(
            bluetoothType,
            function(cha) {
              gotCha(cha);
            },
            function() {
              noGotCha();
            }
          );
          // wait 5 seconds to see if the saved cha is still available.  If not, proceed to the modal.  This timer gets cancelled if we find a matching cha
          timers.checkSavedChaTimer = $timeout(isSavedChaFound, 5000);
        } else {
          api.state = "discovery";
          logger.debug("CHA - Starting Discovery");
          tasks.register(chaTasks.CONNECT, "Searching for WAHTS");

          chooseCha.discover(bluetoothType, chaList, deferred.discovery);
        }

        function noGotCha(info) {
          if (info === "done") {
            if (!reconnect) {
              // found nothing, pop up the modal
              api.state = "discovery";
              logger.debug("CHA - No CHA found in autoDiscovery. Starting Discovery");
              chooseCha.discover(bluetoothType, chaList, deferred.discovery);
            } else {
              deferred.discovery.reject({
                code: 50,
                msg: "No CHA found in autoDiscovery during reconnect"
              });
            }
          } else {
            logger.error("CHA - noGotCha call during autoDiscovery with info != done. Info: " + angular.toJson(info));
            deferred.discovery.reject({
              code: 51,
              msg: "ChaWrap.startChaSearch failed"
            });
          }
        }

        function stopReconnect() {
          // if the saved Cha has not been found, reject and the user/admin has to connect manually
          if (api.state === "autoDiscovery") {
            try {
              ChaWrap.cancelChaSearch();
            } catch (e) {
              logger.error("CHA - cancelChaSearch() failed with " + angular.toJson(e));
            }
            deferred.discovery.reject({
              code: 69,
              msg: "Saved CHA not found during autoDiscovery in reconnect"
            });
          }
        }

        function isSavedChaFound() {
          // if the saved Cha is not found, head to the chooseCha factory
          // make sure we have not cancelled.
          if (api.state === "autoDiscovery") {
            if (!_.includes(chaNameList(), disk.cha.myCha)) {
              api.state = "discovery";
              logger.debug("CHA - Saved CHA not found in autoDiscovery, starting Discovery");
              chooseCha.discover(bluetoothType, chaList, deferred.discovery);
            }
          }
        }

        function gotCha(cha) {
          if (disk.cha.myCha === cha.getName()) {
            logger.debug("CHA - Autodiscovery found a saved CHA: " + cha.getName());
            try {
              ChaWrap.cancelChaSearch();
            } catch (e) {
              logger.error("CHA - cancelChaSearch() failed in gotCha() with " + angular.toJson(e));
            }
            $timeout.cancel(timers.checkSavedChaTimer);
            $timeout.cancel(timers.reconnectTimer);
            deferred.discovery.resolve(cha);
          } else if (!_.includes(chaNameList(), cha.getName())) {
            logger.debug("CHA - Autodiscovery found a foreign CHA: " + cha.getName());
            chaList.push(cha); // add this one to the list that will be shown in the modal if we don't find a match
          }
        }

        function chaNameList() {
          var tempChaNameList = [];
          _.forEach(chaList, function(cha) {
            tempChaNameList.push(cha.getName());
          });
          return tempChaNameList;
        }

        return deferred.discovery.promise;
      }

      function connect(cha) {
        deferred.connect = $q.defer();
        api.state = "connecting";
        logger.debug("CHA - Attempting to connect");
        tasks.register(chaTasks.CONNECT, "Establishing connection");

        // cancel the cha search
        try {
          ChaWrap.cancelChaSearch();
        } catch (e) {
          logger.error("CHA - cancelChaSearch() failed in connect() with " + angular.toJson(e));
        }

        api.myCha = cha;
        window.myCha = api.myCha;
        api.myCha.name = api.myCha.getName();
        api.myCha.connect(
          function() {
            deferred.connect.resolve();
          },
          function(err) {
            logger.debug("CHA - myCha.connect failed, retrying connect process once.");
            $timeout(function() {
              api.myCha.connect(
                function() {
                  deferred.connect.resolve();
                },
                function(err) {
                  deferred.connect.reject({ code: 54, msg: err });
                }
              );
            }, 300);
          }
        );
        timers.connect = $timeout(checkConnectionTimeout, 20000);

        return deferred.connect.promise;
      }

      function checkConnectionTimeout() {
        // connection attempt timed out - likely a problem with the CHA
        if (deferred.connect) {
          try {
            deferred.connect.reject({
              code: 60,
              msg: "CHA connect timed out."
            });
          } catch (e) {
            logger.debug("CHA - failure while rejecting deferred.connect in checkConnectionTimeout");
          }
        }
      }

      function setListener() {
        deferred.setListener = $q.defer();
        logger.debug("CHA - Setting listener");
        $timeout.cancel(timers.connect);

        if (angular.isDefined(api.myCha)) {
          api.myCha.setListener(eventListener, deferred.setListener.resolve, function(err) {
            deferred.setListener.reject({ code: 55, msg: err });
          });
        } else {
          var msg =
            "CHA  - setListener cannot be called.  cha.state: " +
            api.state +
            ". myCha isDefined: " +
            angular.isDefined(api.myCha);
          logger.warn(msg);
          deferred.setListener.reject({ code: 55, msg: msg });
        }

        return deferred.setListener.promise;
      }

      function runConnectionDropCBIfCalledFor() {
        if (reconnect && api.connectionDropCB) {
          logger.debug("CHA - Calling connectionDropCB after reconnect");
          try {
            api.connectionDropCB();
          } catch (err) {
            logger.error("CHA - Trying to call connectionDropCB, error: " + angular.toJson(err));
          }
        }
      }

      function finalizeConnection() {
        api.state = "connected";
        logger.debug("CHA -!-!- Cha Connected -!-!-");
        tasks.register(chaTasks.CONNECT, "Finalizing connection");

        // cancel all connection timers, such as the connection attempt timeout, and the didn't find matching cha timeout
        for (var timer in timers) {
          try {
            $timeout.cancel(timer);
          } catch (e) {
            logger.debug("CHA - failed while cancelling timer: " + angular.toJson(e));
          }
        }

        //get rid of tasks that couldn't survive disconnect/reconnect
        tasks.deregister(chaTasks.UPDATE_FIRMWARE);
        tasks.deregister(chaTasks.CANCEL_FIRMWARE);
        tasks.deregister(chaTasks.FILE_TRANSFER);

        try {
          api
            .abortExams()
            .then(api.stopNoiseFeature)
            .then(runConnectionDropCBIfCalledFor);
        } catch (e) {
          logger.error("CHA - Failed to abort Exams in finalize connection with error: " + angular.toJson(e));
          runConnectionDropCBIfCalledFor();
          tasks.deregister(chaTasks.CONNECT);
        }
      }

      function checkBuild() {
        let q = $q.defer();
        if (api.myCha.id.buildDateTime !== disk.cha.embeddedFirmwareBuildDate && !disk.cha.ignoreFirmwareUpdates) {
          notifications.confirm(
            gettextCatalog.getString(
              `The firmware on the WAHTS (${
                api.myCha.id.buildDateTime
              }) is not supported by this TabSINT version. This TabSINT version supports WAHTS firmware: ${
                disk.cha.embeddedFirmwareBuildDate
              }.\n\nSelect below to update the firmware on the WAHTS. The WAHTS firmware can also be updated on the Setup page of the Admin View.`
            ),
            function(buttonIndex) {
              if (buttonIndex === 1) {
                api.updateFirmware();
                q.reject(); // reject promise here to we skip the rest of the connection logic in the Connect chain
              } else if (buttonIndex === 2) {
                logger.debug("CHA - canceled firmware update prompt");
                q.resolve();
              }
            },
            gettextCatalog.getString("WAHTS Firmware"),
            [gettextCatalog.getString("Update"), gettextCatalog.getString("Cancel")]
          );
        } else {
          q.resolve();
        }

        return q.promise;
      }

      function getCalibrations() {
        if (!api.myCha) {
          return;
        }

        // reset calibration object
        api.myCha.calibration = {};

        // get calibrations
        return (
          api
            .requestCalibrationEntry(0) // Left Speaker
            .then(function(calResult) {
              api.myCha.calibration.Left = calResult;
            })
            .then(function() {
              return api.requestCalibrationEntry(2); // Right speaker
            })
            .then(function(calResult) {
              api.myCha.calibration.Right = calResult;
            })

            // ---- TODO: Add "Bone"
            // .then(function() {
            //   return api.requestCalibrationEntry(???); // Bone oscillator
            // })
            // .then(function(calResult) {
            //   api.myCha.calibration.Bone = calResult[1];
            // })

            .catch(function(e) {
              logger.warn("CHA - Failed to get calibrations: " + angular.toJson(e));
            })
        );
      }

      function rejectionHandler(err) {
        // generic messages
        var androidBluetoothMsg =
          "This tablet is trying to connect to a CHA, but the bluetooth radio is currently turned off. \n\n " +
          "Please pull down the tablet settings menu from the top-right of the screen and press the bluetooth icon to enable this tablet's bluetooth radio. " +
          "Once enabled, go the admin page and try reconnecting to the CHA.";
        var didNotConnectMsg =
          "The tablet did not successfully connect with the CHA.  \n\n" +
          "Please check to make sure bluetooth is active on the tablet and the cha is turned on, then try attempting again from the Admin menu. \n\n" +
          "If you continue to have difficulty, please try restarting the CHA and attempting to reconnect";

        // disconnect
        api.disconnect();

        // in case not rejection message was provided
        if (!err) {
          err = {};
        }

        // compatibility with CHA Message -> msg, Code -> code
        if (err.Code) {
          err.code = err.Code;
        }
        if (err.Message) {
          err.msg = err.Message;
        }

        // add messages
        if (err.code === 69) {
          logger.debug("CHA - Saved CHA not found during autoDiscovery in reconnect");
          notifications.alert(
            "The tablet is having difficulty communicating with the CHA. Please try reconnecting to the cha on the Admin page."
          );
        } else if (err.code === 50) {
          logger.debug("CHA - No CHA found in autoDiscovery during reconnect");
          notifications.alert(
            "The tablet is having difficulty communicating with the CHA. Please try reconnecting to the cha on the Admin page."
          );
        } else if (err.code === 52) {
          logger.debug("CHA - CHA search cancelled");
        } else if (err.code === 54) {
          logger.debug("CHA - CHA connection process failed with error: " + angular.toJson(err.msg));
          notifications.alert(didNotConnectMsg);
        } else if (err.code === 68) {
          logger.debug("CHA - CHA connect cancelled");
        } else if (err.code === 60) {
          logger.debug("CHA - CHA connection attempt timed out");
          notifications.alert("Connection attempt timed out.  " + didNotConnectMsg);
        } else if (err.code === 100) {
          // Bluetooth radio is off
          logger.warn("CHA - Bluetooth Query Result: " + err.msg);
          if (devices.platform === "android") {
            notifications.alert(androidBluetoothMsg);
          } else if (devices.platform === "ios") {
            notifications.alert(
              "This tablet is trying to connect to a CHA, but the bluetooth radio is currently turned off. \n\n " +
                "Please pull up the tablet settings menu from the bottom of the screen and press the bluetooth icon to enable this tablet's bluetooth radio. " +
                "Once enabled, go the admin page and try reconnecting to the CHA."
            );
          }
        } else if (err.code === 101) {
          // Bluetooth radio state is unknown - specific to iOS.
          logger.debug("CHA - Bluetooth Query Result: " + err.msg);
          if (devices.platform === "android") {
            notifications.alert(androidBluetoothMsg);
          } else if (devices.platform === "ios") {
            logger.debug("CHA - received unknown state for bluetooth adapter in iOS, trying again in 1 second.");
            $timeout(api.connect, 1000);
          }
        } else if (_.includes([-1], err.code)) {
          logger.error("CHA - java.runtime error during connection process. Alerting user");
          notifications.alert(
            "The tablet is having difficulty communicating with the CHA. Please try reconnecting to the cha on the Admin page."
          );
        } else {
          // api.errorHandler.main(err);
          logger.error("CHA - CHA failed to connect for reason: " + angular.toJson(err));
          notifications.alert(didNotConnectMsg);
        }
      }
      async function setupA2DP() {
        // use await syntax to implicitly convert request A2DP to promise.
        try {
          // Resolve promises in serial
          await chaStreaming.requestA2DP(api.myCha);
        } catch (e) {
          logger.error("CHA - Failed to connect to A2DP with reason: " + angular.toJson(e));
          notifications.alert(
            "The tablet could not set up a streaming connection with the Headset. Try manually restarting the headset and attempt to reconnect\n\n" +
              "If you will not be streaming audio to the headset, select the option in Advanced Settings to disable audio streaming."
          );
        }
      }

      // Connection Promise Chain
      // tasks.register(chaTasks.CONNECT,'Connecting to WAHTS');
      return (
        checkBluetoothRadio()
          .then(discoverCha)
          .then(function(cha) {
            return connect(cha);
          })
          .then(setListener)
          .then(api.requestId)
          .then(api.requestSetting)
          .then(api.requestProbeId)
          // Wrap this function call in a promise.
          .then(function() {
            if (devices.platform !== "ios" && disk.cha.bluetoothType !== "USB" && !disk.disableAudioStreaming) {
              // check the platform name and preferences
              api.state = "request_A2DP";
              tasks.register(chaTasks.CONNECT, "Setting up streaming connection");
              setupA2DP();
            }
          })
          .then(finalizeConnection)
          .then(api.requestStatus)
          .catch(function(reason) {
            rejectionHandler(reason);
            return $q.reject();
          })

          // post-connect processes
          .then(checkBuild)
          .then(getCalibrations)
          .then(
            function() {
              return api.getMediaVersions();
            },

            // checkBuild will reject if the user elects to update firmware
            function() {}
          )
          .finally(function() {
            tasks.deregister(chaTasks.CONNECT);
          })
      );
    };

    /**
     * Cancel connect
     *
     * Method goes through all promises and rejects.
     * The 68 error code will then disconnect
     * This must be wrapped in a try catch, because async promise may change between checking and rejecting
     * @return {undefined}
     */
    api.cancelConnect = function() {
      logger.debug("CHA - User cancelled cha connect");

      for (var prom in deferred) {
        try {
          deferred[prom].reject({
            code: 68,
            msg: "CHA connect cancelled by user."
          });
        } catch (e) {
          logger.debug("CHA - failed to reject promise while cancelling connection with error:  " + angular.toJson(e));
        }
      }
    };

    /**
     * Disconnect from the CHA
     * @param  {boolean} tryToReconnect [description]
     * @return {promise}                [description]
     */
    api.disconnect = function(tryToReconnect) {
      logger.debug("CHA - Attempting to disconnect from state: " + api.state);
      tasks.register(chaTasks.DISCONNECT, "Disconnecting from WAHTS");
      var disconnectDeferred = {};

      function removeListener() {
        disconnectDeferred.removeListener = $q.defer();

        // remove event listener - to avoid repeat calls
        if (angular.isDefined(api.myCha)) {
          api.myCha.removeListener(
            function() {
              disconnectDeferred.removeListener.resolve();
            },
            function(e) {
              logger.error("CHA - Disconnect removeListener failed. error: " + angular.toJson(e));
              disconnectDeferred.removeListener.resolve(); // resolve anyways
            }
          );
        } else {
          disconnectDeferred.removeListener.resolve();
        }

        return disconnectDeferred.removeListener.promise;
      }

      function cancelChaSearch() {
        disconnectDeferred.cancelChaSearch = $q.defer();

        ChaWrap.cancelChaSearch(disconnectDeferred.cancelChaSearch.resolve, function(err) {
          logger.error("CHA - Disconnect cancelChaSearch failed. Error: " + angular.toJson(err));
          disconnectDeferred.cancelChaSearch.resolve();
        });

        return disconnectDeferred.cancelChaSearch.promise;
      }

      function disconnect() {
        disconnectDeferred.disconnect = $q.defer();

        // remove event listener - to avoid repeat calls
        chaStreaming.disconnectA2DP().then(function() {
          if (angular.isDefined(api.myCha)) {
            api.myCha.disconnect(
              function() {
                disconnectDeferred.disconnect.resolve();
              },
              function(e) {
                logger.error("CHA - Disconnect failed to disconnect.  Error: " + angular.toJson(e));
                disconnectDeferred.disconnect.resolve();
              }
            );
          } else {
            disconnectDeferred.disconnect.resolve();
          }
        });

        return disconnectDeferred.disconnect.promise;
      }

      function finalizeDisconnect() {
        api.state = "disconnected";
        api.flags = [];
        api.myCha = undefined;
        api.myA2DP = undefined;

        logger.debug("CHA - x-x-x-x CHA Disconnected x-x-x-x");
        // do we need default for this? mostly called w/o args
        if (tryToReconnect) {
          logger.debug("CHA - Attempting to reconnect");
          tasks.register(chaTasks.CONNECT, "Reconnecting to WAHTS"); // this task will be deregistered via the connect process
          $timeout(function() {
            api.connect(true);
          }, 3000);
        }
      }

      function rejectionHandler(reason) {
        api.errorHandler.main(reason);
        notifications.alert("Error while trying to disconnect. Please hand the device to an administrator.");
      }

      // cancel all connection timers, such as the connection attempt timeout, and the didn't find matching cha timeout
      for (var timer in timers) {
        try {
          $timeout.cancel(timer);
        } catch (e) {
          logger.debug("CHA - failed while canceling timer: " + angular.toJson(e));
        }
      }

      // reject all promises left in deffered
      for (var prom in deferred) {
        try {
          if (deferred[prom]) {
            deferred[prom].reject({ code: 68, msg: "CHA disconnect." });
          }
        } catch (e) {
          logger.debug("in cha disconnect rejecting promises, " + prom + " . " + e);
        }
      }

      // Disconnect promise chain
      return removeListener()
        .then(cancelChaSearch)
        .then(disconnect)
        .then(finalizeDisconnect)
        .catch(function(reason) {
          rejectionHandler(reason);
        })
        .finally(function() {
          tasks.deregister(chaTasks.DISCONNECT);
        });
    };

    var readMediaCallCount = 0;

    function readFileFromCha(directory, fileName) {
      let defer = $q.defer();
      readMediaCallCount++;
      $cordovaFile.readAsText(directory, fileName).then(
        function(fileText) {
          if (!fileText && readMediaCallCount < 10) {
            setTimeout(function() {
              readFileFromCha(directory, fileName).then(function(fileText) {
                defer.resolve(fileText);
              });
            }, 200);
          } else {
            defer.resolve(fileText.trim());
          }
        },
        function(error) {
          logger.error("Reading file as text = " + JSON.stringify(error));
          defer.reject();
        }
      );
      return defer.promise;
    }

    /**
     * Media versioning
     */
    function getMediaReposFromCha(fileName) {
      // only check if headset media management is turned on
      if (disk.cha.enableHeadsetMedia) {
        if (!fileName) {
          fileName = "MEDIAVER.txt";
        }

        deferred.getMediaRepo = $q.defer();
        deferred.getMediaRepo.fileName = fileName;
        readMediaCallCount = 0;

        if (api.myCha.startFileRead) {
          var filepath = paths.data(fileName);
          api.myCha.startFileRead(
            filepath.split("file://")[1],
            fileName,
            function(success) {},
            function(error) {
              logger.error("When reading media version. " + JSON.stringify(error));
              return undefined;
            }
          );
          readFileFromCha(paths.data(), fileName)
            .then(function(fileText) {
              deferred.getMediaRepo.resolve(fileText);
            })
            .catch(function() {
              deferred.getMediaRepo.reject();
            });
        } else {
          deferred.getMediaRepo.reject();
        }

        return deferred.getMediaRepo.promise;
      } else {
        return $q.reject({ msg: "media versioning disabled" });
      }
    }

    api.getMediaVersions = function() {
      return api
        .getMediaReposFromCha()
        .then(api.getProtectedMediaVersionFromCha)
        .catch(function(e) {
          if (e && e.msg !== "media versioning disabled")
            logger.error("CHA - Failed to get media version from the CHA: " + angular.toJson(e));
        });
    };

    api.getMediaReposFromCha = function() {
      return getMediaReposFromCha("MEDIAVER.txt").then(function(fileText) {
        api.myCha.chaMediaVersion = fileText;
        console.log("MEDIAVER fileText: ", fileText);
      });
    };

    api.getProtectedMediaVersionFromCha = function() {
      return getMediaReposFromCha("PROTECTEDMEDIAVERSION.txt").then(function(fileText) {
        api.myCha.chaProtectedMediaVersion = fileText;
        console.log("PROTECTEDMEDIAVERSION fileText: ", fileText);
      });
    };

    /**
     * Request CHA status
     * This does not have a task associated because it gets called often during certain tests
     * @return {promise} [description]
     */
    api.requestStatus = function() {
      deferred.requestStatus = $q.defer();

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.requestStatus(happy, function(err) {
          deferred.requestStatus.reject({ code: 64, msg: err });
        });
      } else {
        var msg =
          "CHA requestStatus cannot be called in state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.requestStatus.reject({ code: 64, msg: msg });
      }

      return deferred.requestStatus.promise;
    };

    /**
     * Request CHA status and handle if there is still an exam running
     * Simple wrapper of requestStatus. Deprecates api.handleStatus
     * @return {promise} [description]
     */
    api.requestStatusBeforeExam = function() {
      return api.requestStatus().then(function(status) {
        logger.debug("CHA - Status before queuing exam: " + angular.toJson(status));
        if (status.State === 2) {
          logger.warn("CHA exam is still running while user queues an exam. Aborting exams...");
          return api.abortExams().then(function() {
            return $q.resolve(status);
          });
        } else if (status.State !== 1) {
          return $q.reject({ code: 66, msg: "Cha is in an unknown state" });
        } else {
          return $q.resolve(status);
        }
      });
    };

    /**
     * Request CHA Id
     * @return {promise} [description]
     */
    api.requestId = function() {
      tasks.register(chaTasks.REQUESTID, "Requesting WAHTS ID");
      deferred.requestId = $q.defer();

      if (angular.isDefined(api.myCha)) {
        api.myCha.requestId(happy, function(err) {
          deferred.requestId.reject({ code: 56, msg: err });
        });
      } else {
        var msg =
          "CHA - requestID cannot be called from state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.requestId.reject({ code: 56, msg: msg });
      }
      return deferred.requestId.promise.finally(function() {
        tasks.deregister(chaTasks.REQUESTID);
      });
    };

    /**
     * Request CHA Shutdown Time
     * @return {promise} [description]
     */
    api.requestSetting = function() {
      tasks.register(chaTasks.REQUESTSETTING, "Requesting WAHTS Settings");
      deferred.requestSetting = $q.defer();

      if (angular.isDefined(api.myCha)) {
        try {
          api.myCha.requestSetting("auto_shutdown_time", happy, function(err) {
            deferred.requestSetting.reject({ code: 56, msg: err });
          });
        } catch (e) {
          logger.warn("WAHTS Shutdown Timer not Supported");
        }
      } else {
        var msg =
          "CHA - requestSetting cannot be called from state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.requestSetting.reject({ code: 56, msg: msg });
      }
      return deferred.requestSetting.promise.finally(function() {
        tasks.deregister(chaTasks.REQUESTSETTING);
      });
    };

    /**
     * Request CHA Probe ID
     * This will only work with CHA 2.1.5 and CHA 3.0 firmware > Manatee
     * @return {[type]} [description]
     */
    api.requestProbeId = function() {
      tasks.register(chaTasks.REQUESTPROBEID, "Requesting WAHTS probe ID");
      deferred.requestProbeId = $q.defer();

      try {
        if (angular.isDefined(api.myCha)) {
          api.myCha.requestProbeId(happy, function() {
            deferred.requestProbeId.resolve();
          });
        } else {
          var msg =
            "CHA - requestProbeID cannot be called.  cha.state: " +
            api.state +
            ". api.myCha isDefined: " +
            angular.isDefined(api.myCha);
          logger.warn(msg);
          deferred.requestProbeId.reject({ code: 123, msg: msg });
        }
      } catch (err) {
        logger.warn(
          "CHA - Failed while trying to request probe Id. requestProbeId() may not have yet been implemented"
        );
        deferred.requestProbeId.resolve();
      }

      return deferred.requestProbeId.promise.finally(function() {
        tasks.deregister(chaTasks.REQUESTPROBEID);
      });
    };

    /*****************
     * Exam Handling
     *****************/

    /**
     * Queue CHA Exam
     * @param  {string} examType       [description]
     * @param  {object} examProperties [description]
     * @return {promise}                [description]
     */
    api.queueExam = function(examType, examProperties) {
      deferred.queueExam = $q.defer();

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        logger.debug(
          "CHA - Queuing a " + examType + " Exam with the following properties: " + angular.toJson(examProperties)
        );
        api.myCha.queueExam(
          examType,
          examProperties,
          function() {
            deferred.queueExam.resolve();
          },
          function(err) {
            deferred.queueExam.reject({ code: 61, msg: err });
          }
        );
      } else {
        var msg =
          "CHA queueExam cannot be called.  cha.state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.queueExam.reject({ code: 61, msg: msg });
      }

      return deferred.queueExam.promise;
    };

    /**
     * Tell the CHA the software button state
     * @param {number} state [description]
     */
    api.setSoftwareButtonState = function(state) {
      deferred.setSoftwareButtonState = $q.defer();
      var msg;

      if (state === 0 || state === 1) {
        if (angular.isDefined(api.myCha) && api.state === "connected") {
          api.myCha.setSoftwareButtonState(state, function(response) {
            deferred.setSoftwareButtonState.resolve(response);
          });
        } else {
          msg =
            "CHA setSoftwareButton cannot be called.  cha.state: " +
            api.state +
            ". api.myCha isDefined: " +
            angular.isDefined(api.myCha);
          logger.warn(msg);
          deferred.setSoftwareButtonState.reject({ code: 65, msg: msg });
        }
        //$timeout(function() {api.myCha.setSoftwareButtonState(state)}, 10);
      } else if (typeof state === "number") {
        // CRAZY hack to get the 3 digit test to work.  Will hopefully remove in the future   TODO - time to remove?
        if (angular.isDefined(api.myCha) && api.state === "connected") {
          api.myCha.setSoftwareButtonState(state);
          deferred.setSoftwareButtonState.resolve();
        } else {
          msg =
            "CHA setSoftwareButton cannot be called.  cha.state: " +
            api.state +
            ". api.myCha isDefined: " +
            angular.isDefined(api.myCha);
          logger.warn(msg);
          deferred.setSoftwareButtonState.reject({ code: 65, msg: msg });
        }
        //$timeout(function() {api.myCha.setSoftwareButtonState(state)}, 10);
      } else {
        deferred.setSoftwareButtonState.reject({
          code: 65,
          msg: "api.myCha.setSoftwareButtonState(state) received an improper state as an input"
        });
      }

      return deferred.setSoftwareButtonState.promise;
    };

    /**
     * Send exam submission to CHA
     * @param  {string} type [description]
     * @param  {?} args [description]
     * @return {[type]}      [description]
     */
    api.examSubmission = function(type, args) {
      deferred.examSubmission = $q.defer();

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.examSubmission(
          type,
          args,
          function() {
            deferred.examSubmission.resolve();
          },
          function(err) {
            deferred.examSubmission.reject({ code: 67, msg: err });
          }
        );
      } else {
        var msg =
          "CHA examSubmission cannot be called.  cha.state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.examSubmission.reject({ code: 67, msg: msg });
      }

      return deferred.examSubmission.promise;
    };

    /**
     * Abort CHA Exam and close A2DP connection
     * @return {promise} [description]
     */
    api.abortExams = function() {
      deferred.abortExams = $q.defer();
      // why disconnect if audio streaming is disabled/off?
      // release the A2DP connection whenever we abort exams
      return chaStreaming.disconnectA2DP().then(function() {
        if (angular.isDefined(api.myCha) && api.state === "connected") {
          api.myCha.abortExams(
            function() {
              deferred.abortExams.resolve();
            },
            function(err) {
              deferred.abortExams.reject({ code: 62, msg: err });
            }
          );
        } else {
          deferred.abortExams.resolve();
        }

        return deferred.abortExams.promise;
      });
    };

    /**
     * Request CHA Results.
     * We do not attach a task to this because it happens very frequently
     * @return {promise} [description]
     */
    api.requestResults = function() {
      deferred.requestResults = $q.defer();

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.requestResults(happy, function(err) {
          deferred.requestResults.reject({ code: 63, msg: err });
        });
      } else {
        var msg =
          "CHA - requestResults cannot be called in state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.debug(msg);
        deferred.requestResults.reject({ code: 63, msg: msg });
      }

      return deferred.requestResults.promise;
    };

    api.requestCalibrationList = function() {
      deferred.requestCalibrationList = $q.defer();

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.requestCalibrationList(happy, function(err) {
          deferred.requestCalibrationList.reject({ code: 63, msg: err });
        });
      } else {
        var msg =
          "CHA - requestCalibrationList cannot be called in state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.debug(msg);
        deferred.requestCalibrationList.reject({ code: 63, msg: msg });
      }

      return deferred.requestCalibrationList.promise;
    };

    api.requestCalibrationEntry = function(index) {
      deferred.requestCalibrationEntry = $q.defer();

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.requestCalibrationEntry(index, happy, function(err) {
          deferred.requestCalibrationEntry.reject({ code: 63, msg: err });
        });
      } else {
        var msg =
          "CHA - requestCalibrationEntry cannot be called in state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.debug(msg);
        deferred.requestCalibrationEntry.reject({ code: 63, msg: msg });
      }

      return deferred.requestCalibrationEntry.promise;
    };

    /*****************
     * NOISE FEATURE
     *****************/

    /**
     * Start Noise Feature
     * @param  {object} params={} [description]
     * @return {promise}        [description]
     */
    api.startNoiseFeature = function(params) {
      params = params || {};

      deferred.startNoiseFeature = $q.defer();

      // add lower default level value, if not specified
      var noiseLevelDefault = 30;
      if (!params.Level || params.Level.length !== 2) {
        params.Level = [noiseLevelDefault, noiseLevelDefault];
      } else {
        if (typeof params.Level[0] !== "number") {
          params.Level[0] = noiseLevelDefault;
        }

        if (typeof params.Level[1] !== "number") {
          params.Level[1] = noiseLevelDefault;
        }
      }

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.noiseFeatureStart(
          params,
          function() {
            deferred.startNoiseFeature.resolve();
          },
          function(err) {
            deferred.startNoiseFeature.reject({ code: 801, msg: err });
          }
        );
      } else {
        var msg =
          "CHA - noiseFeatureStart cannot be called.  cha.state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.startNoiseFeature.reject({ code: 800, msg: msg });
      }

      return deferred.startNoiseFeature.promise;
    };

    /**
     * Stop Noise Feature
     * @return {promise} [description]
     */
    api.stopNoiseFeature = function() {
      deferred.stopNoiseFeature = $q.defer();

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.noiseFeatureStop(
          function() {
            deferred.stopNoiseFeature.resolve();
          },
          function(err) {
            deferred.stopNoiseFeature.reject({ code: 802, msg: err });
          }
        );
      } else {
        deferred.stopNoiseFeature.resolve();
      }

      return deferred.stopNoiseFeature.promise;
    };

    /**
     * Pause noise feature
     * @return {promise} [description]
     */
    api.pauseNoiseFeature = function() {
      deferred.pauseNoiseFeature = $q.defer();

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.noiseFeaturePause(deferred.pauseNoiseFeature.resolve, function(err) {
          deferred.pauseNoiseFeature.reject({ code: 803, msg: err });
        });
      } else {
        var msg =
          "CHA - noiseFeaturePause cannot be called.  cha.state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.pauseNoiseFeature.reject({ code: 803, msg: msg });
      }

      return deferred.pauseNoiseFeature.promise;
    };

    /**
     * Resume noise feature
     * @return {promise} [description]
     */
    api.resumeNoiseFeature = function() {
      deferred.resumeNoiseFeature = $q.defer();

      if (angular.isDefined(api.myCha) && api.state === "connected") {
        api.myCha.noiseFeatureResume(
          function() {
            deferred.resumeNoiseFeature.resolve();
          },
          function(err) {
            deferred.resumeNoiseFeature.reject({ code: 804, msg: err });
          }
        );
      } else {
        var msg =
          "CHA - noiseFeatureResume cannot be called.  cha.state: " +
          api.state +
          ". api.myCha isDefined: " +
          angular.isDefined(api.myCha);
        logger.warn(msg);
        deferred.resumeNoiseFeature.reject({ code: 804, msg: msg });
      }

      return deferred.resumeNoiseFeature.promise;
    };

    /**
     * Change noise feature level
     * @param  {array} newLevel - must be of length 2
     * @return {promise}          [description]
     */
    api.changeNoiseFeatureLevel = function(newLevel) {
      deferred.changeNoiseFeatureLevel = $q.defer();

      if (newLevel && newLevel.length === 2) {
        if (angular.isDefined(api.myCha) && api.state === "connected") {
          api.myCha.noiseFeatureChangeLevel(
            newLevel,
            function() {
              deferred.changeNoiseFeatureLevel.resolve();
            },
            function(err) {
              deferred.changeNoiseFeatureLevel.reject({ code: 805, msg: err });
            }
          );
        } else {
          var msg =
            "CHA - noiseFeatureChangeLevel cannot be called.  cha.state: " +
            api.state +
            ". api.myCha isDefined: " +
            angular.isDefined(api.myCha);
          logger.warn(msg);
          deferred.changeNoiseFeatureLevel.reject({ code: 805, msg: msg });
        }
      } else {
        deferred.changeNoiseFeatureLevel.reject({
          code: 806,
          msg:
            "Could not change CHA Noise Feautre Level to " +
            JSON.stringify(newLevel) +
            ".  Inappropriate format - expected 2-element array."
        });
      }

      return deferred.startNoiseFeature.promise;
    };

    /*****************
     * Error Handling
     *****************/

    /**
     * Error Handler
     * @type {Object}
     */
    api.errorHandler = {
      main: function(err) {
        // compatibility with CHA Message -> msg, Code -> code
        if (err.Code) {
          err.code = err.Code;
        }
        if (err.Message) {
          err.msg = err.Message;
        }

        if (err.code) {
          if (err.code === 404) {
            return; // related to repeating a cha exam
          }

          // special handling for probe, it does not have requestSetting!
          if (err.code === 7 && bluetoothType === "BLUETOOTH_LE") {
            if (deferred.requestSetting) {
              logger.error("CHA - Cannot request setting - continuing with connection.");
              deferred.requestSetting.resolve();
            }
            return;
          }

          // special handling for lack of probeId - newer wireless headsets don't have a probe!
          if (err.code === 6 && bluetoothType === "BLUETOOTH_LE") {
            if (deferred.requestProbeId) {
              logger.error("CHA - Error related to probeId - continuing with connection.");
              deferred.requestProbeId.resolve();
            }
            if (deferred.requestSetting) {
              logger.error("CHA - Error related to requestSetting - continuing with connection.");
              deferred.requestSetting.resolve();
            }
            return;
          }

          if (err.code === -1 && api.fileOpState === FILE_OP_STATES.GET_DIRECTORY_CRC && deferred.getDirectoryCrc) {
            // requested CRCs from a non-existent directory
            //TODO: near identical code w/ bottom - consolidate?
            logger.debug("Called cha.getDirectoryCrc for a non-existent or empty directory.");
            deferred.getDirectoryCrc.resolve({ success: false, result: [] });
            return;
          }

          if (err.code === -1 && api.fileOpState === FILE_OP_STATES.GET_DIRECTORY && deferred.getDirectory) {
            // requested directory listing from a non-existent directory
            logger.debug("Called cha.getDirectory for a non-existent or empty directory");
            deferred.getDirectory.resolve({ success: false, result: [] });
            return;
          }

          // otherwise, print the error and notify the user
          logger.error("CHA - Error, code:  " + err.code + ", msg: " + angular.toJson(err.msg));

          // errors while not connected (i.e during connecting)
          if (api.state !== "connected") {
            notifications.alert(
              "The tablet is having difficulty connecting to the WAHTS. Please check that the WAHTS is on and charged. For further troubleshooting help, please see the documentation at https://tabsint.org"
            );
            api.disconnect();
          }

          // handle error codes
          if (err.code === 5) {
            notifications.alert(
              "The Headset was asked to access a file it cannot find.  Please hand the tablet to a test administrator."
            );
          } else if (err.code === -1) {
            notifications.alert(
              "TabSINT encountered an error communicating with the WAHTS. Please try reconnecting to the WAHTS on the Admin page.\n\nError msg: " +
                angular.toJson(err.msg)
            );
          }
        } else {
          logger.error("CHA - Error with unknown code: " + angular.toJson(err));
        }

        // send errors along to response area error handler, if defined and code !== -1
        // the responseArea function can be defined by a response-area's controller.
        // See cha-response-areas.js for an example
        if (this.responseArea) {
          this.responseArea(err);
        }
      },
      responseArea: undefined
    };

    /*****************
     * Utilities
     *****************/

    /**
     * Get RetSPL for a frequency and channel
     * @param  {number} frequency - must be one of the RetSPL frequencies
     *                              [125,250,500,750,1000,1500,2000,3000,4000,6000,8000,9000,10000,11200,12500,14000,16000,18000,20000]
     * @param  {string} channel   - channel to get RetSPL for. One of "Left", "Right", or "Bone"
     * @return {number}           RetSPL, or undefined if not found
     */
    api.getRetSPL = function(frequency, channel) {
      // get "left" channel by default, this should be the same as "right"
      if (!channel) {
        channel = "Left";
      }

      if (api.myCha && api.myCha.calibration) {
        let RetSPLs = api.myCha.calibration[channel]["RetSPL"];
        let RetSPL_idx = RetSPLs.findIndex(val => {
          return val === frequency;
        });

        if (RetSPL_idx > -1) {
          return RetSPLs[RetSPL_idx + 1];
        }
      }

      // default to return undefined
      logger.warn("CHA - Failed to find RetSPL for " + frequency + " on channel " + channel);
      return undefined;
    };

    return api;
  });
