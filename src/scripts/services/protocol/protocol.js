/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as tv4 from "tv4";
import * as _ from "lodash";
import * as $ from "jquery";

import "./protocol.model";

angular
  .module("tabsint.services.protocol", ["tabsint.services.protocol.model"])
  .factory("protocol", function(
    $cordovaFile,
    $q,
    $rootScope,
    $timeout,
    advancedProtocol,
    app,
    cha,
    config,
    disk,
    file,
    gettextCatalog,
    json,
    logger,
    notifications,
    paths,
    plugins,
    pm,
    responseAreas,
    tabsintNative,
    tasks,
    version
  ) {
    var api = {
      ready: undefined,
      load: undefined,
      reset: undefined,
      local: undefined,
      root: undefined,
      validate: undefined,
      protocolSchema: undefined,
      usbEventCallback: undefined
    };

    var loading = {
      p: undefined,
      c: undefined,
      validate: undefined,
      meta: undefined
    };

    var callbackQueue = {
      callbacks: {},
      add: function(name, callback) {
        if (!_.includes(_.keys(callbackQueue.callbacks), name)) {
          // only want to add functions once
          this.callbacks[name] = callback;
        }
      },
      run: function() {
        _.forEach(this.callbacks, function(callback, name) {
          callback();
        });
      },
      clear: function() {
        this.callbacks = {};
      }
    };

    /**
     * Internal method for loading built-in protocols when the app first loads
     * @instance
     */
    api.$init = function() {
      // remove all the app and devleoper protocols from the disk.res.protocols object
      _.remove(disk.protocols, {
        server: "developer"
      });

      // Array to hold all local protocols when the app first loads
      pm.local = [
        api.define({
          name: "Audiometry",
          path: paths.www("res/protocol/edare-audiometry"),
          creator: "Edare",
          server: "developer"
        }),
        api.define({
          name: "Creare Audiometry",
          path: paths.www("res/protocol/creare-audiometry"),
          creator: "creare",
          server: "developer",
          admin: true
        }),
        api.define({
          name: "tabsint-test",
          path: paths.www("res/protocol/TabSINT-Test"),
          creator: "creare",
          server: "developer",
          admin: true
        }),
        api.define({
          name: "wahts-device-test",
          path: paths.www("res/protocol/wahts-device-test"),
          creator: "creare",
          server: "developer",
          admin: true
        }),
        api.define({
          name: "wahts-software-test",
          path: paths.www("res/protocol/wahts-software-test"),
          creator: "creare",
          server: "developer",
          admin: true
        })
      ];

      // load each protocol in config
      if (app.browser && config.protocols && config.protocols.length > 0) {
        _.forEach(config.protocols, function(p) {
          pm.local.push(
            api.define({
              name: p,
              path: paths.www("protocols/" + paths.dir(p)),
              creator: "creare",
              server: "developer",
              admin: true
            })
          );
        });
      }

      // put the protocols on the disk.protocol object
      _.forEach(pm.local, function(p) {
        api.store(p);
      });

      // define plugin protocols, and store on disk
      _.forEach(plugins.elems.localProtocols, function(p) {
        api.store(api.define(p));
      });

      // select the source to start
      if (!app.tablet) {
        config.load();
      }
      if (!disk.server) {
        if (config.server.type === "tabsintServer") {
          api.changeSource("tabsintServer");
        } else {
          api.changeSource("localServer");
        }
      }

      // add root, recursively will add all dependent schemas
      api.protocolSchema = addSchema("protocol_schema");

      //add 'tabsint-protocols' directory next to 'tabsint-results' to store local server TabSINT protocols
      if (app.tablet) {
        file.createDirRecursively(paths.public(""), "tabsint-protocols");
      }
    };

    /**
        Recursively adds .json files for the protocol schema, starting with protocol_schema.json
      */
    function addSchema(s) {
      // derive schema path, remove precedig 'definitions' if tv4 adds it
      var sPath = s.endsWith(".json") ? s : s + ".json";
      sPath = sPath.startsWith("definitions/definitions/") ? sPath.slice(13) : sPath; // remove preceding definitions
      sPath = sPath.startsWith("/") ? sPath : "/" + sPath; // add preceding /

      // load schema
      return file
        .readFile(paths.www("res/protocol/schema") + sPath)
        .then(function(schema) {
          var deferred = $q.defer();
          // add schema
          tv4.addSchema(s, schema);

          // find out dependent schemas
          var missing = tv4.getMissingUris();

          // handle missing schemas one by one
          if (missing.length > 0) {
            addSchema(missing[0]);
          }
          deferred.resolve(schema);
          return deferred.promise;
        })
        .catch(function(err) {
          logger.error("Protocol schema did not load properly. Error: " + err);
        });
    }

    /**
     * Defines a protocol meta-data object.
     * All keys that are not in the default object will get assigned from the object passed in
     *  @param {string} obj.id - protocol id
     *  @param {string} obj.name - name of the protocol directory and root json file
     *  @param {string} obj.path - full native path to the protocol
     *  @param {date} obj.date - protocol creation date. Default: null
     *  @param {string} obj.creator - id of the protocol creator. Default: null
     *  @param {string} obj.server - extra identifier to server protocols. Default: null
     *  @param {string} obj.admin - Only display the protocol in admin mode. Default: false
     */
    api.define = function(obj) {
      if (!obj || typeof obj !== "object" || !obj.name || !obj.path) {
        logger.error("Protocol object, name and path are required to define");
      }

      // add version to developer protocols
      if (obj.server === "developer") {
        obj.version = version.dm.tabsint;
      }

      return _.defaults(
        {
          group: obj.group || null,
          name: obj.name,
          path: obj.path,
          id: obj.id || null,
          date: obj.date || null,
          version: obj.version || null,
          creator: obj.creator || null,
          server: obj.server || null,
          admin: obj.admin || false,
          contentURI: obj.contentURI || null
        },
        obj
      );
    };

    /**
     * Stores media meta-object to disk.mediaRepos
     * @param {object} m - media meta-data object to store
     * @instance
     */
    api.storeMedia = function(m) {
      var dums = _.filter(disk.mediaRepos, {
        name: m.name,
        path: m.path,
        date: m.date
      });

      // if the media does not already exist, then push it onto the stack
      if (dums.length === 0) {
        disk.mediaRepos.push(m);
      } else {
        logger.error("Media meta data already in disk.mediaRepos");
      }
    };

    /**
     * Stores protocol meta-object to disk.protocols
     * @param {object} p - protocol meta-data object to store
     * @instance
     */
    api.store = function(p) {
      var dups = _.filter(disk.protocols, {
        name: p.name,
        path: p.path,
        date: p.date,
        contentURI: p.contentURI
      });

      // if the protocol does not already exist, then push it onto the stack
      if (dups.length === 0) {
        disk.protocols.push(p);
      } else {
        logger.error("Protocol meta data already in disk.protocols");
      }
    };

    /**
     * Deletes media meta-data object from disk.mediaRepos
     * @param {object} m - media meta-data object to delete
     */
    api.deleteMedia = function(m) {
      var idx = _.findIndex(disk.mediaRepos, m);

      if (idx === -1) {
        logger.error("Trying to delete media " + m.name + ", but it does not exist");
        return;
      }

      // remove files if its from gitlab
      if (app.tablet && m.server === "gitlab") {
        try {
          logger.info("Removing media files for protocol " + m.name);
          $cordovaFile.removeRecursively(m.path, m.name);
        } catch (e) {
          logger.error("Failed to remove media directory " + m.name + " from path " + m.path);
        }
      }

      // remove metadata from disk
      disk.mediaRepos.splice(idx, 1);
    };

    /**
     * Deletes protocol meta-data object from disk.protocols
     * @param {object} p - protocol meta-data object to delete
     */
    api.delete = function(p) {
      var idx = _.findIndex(disk.protocols, p);

      if (idx === -1) {
        logger.error("Trying to delete protocol " + p.name + ", but it does not exist");
        return;
      }

      if (_.includes(["app", "developer"], p.group)) {
        logger.error("Trying to delete app or developer protocol " + p.name + ", but this is not allowed");
        return;
      }

      if (app.tablet && _.includes(["tabsintServer", "gitlab"], p.server)) {
        try {
          logger.debug("Removing protocol files for protocol: " + p.name + " at path: " + p.path);
          var root = p.path
            .split("/")
            .slice(0, -2)
            .join("/");
          var dir = p.path.split("/").slice(-2, -1)[0];
          $cordovaFile.removeRecursively(root, dir).catch(function(e) {
            logger.error(
              "Failed to remove protocol files in directory " +
                dir +
                " within root " +
                root +
                " for protocol " +
                p.name +
                " with error: " +
                angular.toJson(e)
            );
          });
        } catch (e) {
          logger.debug("Failed to remove protocol directory " + p.name + " from path " + p.path);
        }
      }

      // remove metadata from disk
      disk.protocols.splice(idx, 1);

      // if protocol is active, remove it
      if (api.isRoot(p)) {
        pm.root = undefined;
        disk.protocol = {};
      }

      //try to erase the files copied to internal storage
      try {
        console.log("attempting to delete files");
        file.deleteCopiedInternalDir(p.path, p.name);
      } catch (error) {
        console.log("Error trying to delete files");
        console.log(error);
      }
    };

    /**
     * Function to check if protocol meta-data object is currently active
     * @param {object} p - protocol object to load. Defaults to active protocol on disk
     */
    api.isRoot = function(p) {
      return _.isEqual(disk.protocol, p);
    };

    /**
     * Protocol validation method.  Remainds on the protocol api for unit testing access
     * @param  {object} p - protcol object loaded from json file
     * @return {array}  returns array of errors, or nothing if successfully validated
     */
    api.validate = function(p) {
      if (!api.protocolSchema) {
        logger.error("Failed to load protocol schema");
        var msg = "Failed to load the protocol schema. Check protocol_schema.json for syntax errors.";
        return {
          valid: false,
          error: msg
        };
      }

      // validate
      tv4.validate(p, api.protocolSchema.$$state.value, false, true);

      var isValid = tv4.valid;

      var tv4errors = [];

      // convienence function for interpreting nested tv4 errors
      function getErrors(err) {
        var tmpErrors = [];
        if (!err.subErrors) {
          tmpErrors.push({
            dataPath: err.dataPath,
            message: err.message
          });
        } else {
          _.forEach(err.subErrors, function(sub) {
            tmpErrors = tmpErrors.concat(getErrors(sub));
          });
        }
        return tmpErrors;
      }

      // handle missing schemas
      if (tv4.missing.length > 0) {
        tv4errors.push(
          "The following schemas are missing or contain missing schema references: " + angular.toJson(tv4.missing, true)
        );
        isValid = false;
      }

      // collect actual errors
      var parsed;
      if (tv4.error) {
        tv4errors = tv4errors.concat(getErrors(tv4.error));
        parsed = {};

        _.forEach(tv4errors, function(err) {
          if (!_.includes(_.keys(parsed), err.dataPath)) {
            parsed[err.dataPath] = [err.message];
          } else {
            parsed[err.dataPath].push(err.message);
          }
        });
      }

      // handle invalid
      if (!isValid) {
        logger.error(
          "Protocol schema validation failed. Please check the following protocol paths for issues: \n" +
            angular.toJson(parsed, true)
        );
        //return parsed;
        if (angular.isDefined(parsed) && _.keys(parsed).length > 0) {
          notifications.alert(
            gettextCatalog.getString(
              "The protocol failed validation. Please check the following protocol paths for issues"
            ) +
              ": \n" +
              angular.toJson(parsed, true)
          );
        }
      } else {
        logger.info("Protocol successfully validated");
      }

      return {
        valid: isValid,
        error: parsed
      };
    };

    /**
     * Protocol loading method.
     * Will load a protocol based on the meta data stored in disk.protocol
     * @param {object} meta -  protocol metadata object to load
     * @param  {boolean} validate - validate the current protocol
     * @param  {boolean} notify - pop an alert if the protocol successfully loads
     */
    api.load = function(meta, validate, notify, reload) {
      loading = {}; // reset loading container
      loading.meta = meta;
      loading.validate = typeof validate === "boolean" ? validate : disk.validateProtocols; // if validate is a boolean, use it, otherwise default to disk.validateProtocols
      loading.notify = notify || false;
      loading.reload = reload || false;

      if (!loading.meta && disk.protocol.path) {
        loading.meta = disk.protocol;
      } else if (!meta && !disk.protocol.path) {
        logger.debug("No protocol available");
        return $q.reject();
      }

      // fix path
      loading.meta.path = paths.dir(loading.meta.path);

      //tasks.register('updating protocol', 'Loading Protocol...'); // not getting noticed in the view...

      function validateIfCalledFor() {
        var deferred = $q.defer();

        if (loading.validate) {
          var validationResult = api.validate(loading.p);
          if (validationResult.valid) {
            deferred.resolve();
          } else {
            deferred.reject("Validation Errors: " + JSON.stringify(validationResult.error));
          }
        } else {
          deferred.resolve();
        }

        return deferred.promise;
      }

      function addTask(taskName, taskMessage) {
        if (loading.notify) {
          return tasks.register(taskName, taskMessage);
        } else {
          return $q.resolve();
        }
      }

      var errorCopying = false;
      return (
        addTask("updating protocol", "Loading Protocol Files...")
          //check if we need to re-copy directory
          .then(() => {
            console.log("loading.meta", loading.meta);
            if (loading.meta.contentURI && loading.reload) {
              console.log("re-loading protocol - copying directory");
              return file.copyDirectory(loading.meta.contentURI, loading.meta.name);
            }
          })
          //catch error
          .catch(function() {
            errorCopying = true;
          })
          //propogate error down promise chain
          .then(function() {
            var deferred = $q.defer();
            if (errorCopying) {
              deferred.reject({
                code: 606,
                msg: "Error reloading protocol"
              });
            } else {
              deferred.resolve();
            }
            return deferred.promise;
          })
          .then(loadFiles) // uses loading.meta, sets loading.p, loading.c
          .then(function() {
            return addTask("updating protocol", "Validating Protocol... This process could take several minutes");
          })
          .then(validateIfCalledFor) // uses loading.p, loading.validate
          .then(function() {
            return addTask("updating protocol", "Initializing Protocol...");
          })
          .then(initializeProtocol) // sets loading.p to pm.root
          .then(function() {
            return addTask("updating protocol", "Checking Protocol Files...");
          })
          .then(loadCustomJs)
          .then(validateCustomJsIfCalledFor)
          .then(function() {
            return addTask("updating protocol", "Copying files to tablet...");
          })
          .then(copyFilesOnLoad)
          .then(handleLoadErrors) // uses pm.root, loading.notify
          .catch(function(e) {
            tasks.deregister("updating protocol");
            logger.error("Could not load protocol.  " + JSON.stringify(e));
            if (e.code == 606 && meta !== undefined) {
              notifications.alert("Error reloading protocol. Please delete and re-add.");
            }
          })
          .finally(function() {
            tasks.deregister("updating protocol");
            if (errorCopying) {
              throw "Error loading protocol";
            }
          })
      );
    };

    /**
     * Perform the actual file loading.  json.load is NOT async, but $q is
     * used here for the ability to reject and break the parent promise chain
     *
     * @param {object}  meta  protocol meta-data object
     * @return {promise}
     */
    function loadFiles() {
      var deferred = $q.defer();
      // Load (or re-load) and return a protocol by id.

      // reset callbackQueue
      callbackQueue.clear();

      // load in protocol (p) and calibration (c)
      // loading.p = json.load(loading.meta.path + "protocol.json");
      // loading.c = json.load(loading.meta.path + "calibration.json");
      loading.p = undefined;
      loading.c = undefined;
      return file
        .readFile(loading.meta.path + "protocol.json")
        .then(function(res) {
          var deferred = $q.defer();
          loading.p = res;
          deferred.resolve(loading.p);
          return deferred.promise;
        })
        .then(function() {
          return file.readFile(loading.meta.path + "calibration.json");
        })
        .then(function(res) {
          var deferred = $q.defer();
          loading.c = res;
          deferred.resolve(loading.c);
          return deferred.promise;
        })
        .then(function() {
          var deferred = $q.defer();
          // reject if p didn't load
          if (!loading.p) {
            logger.error("Protocol did not load properly");
            notifications.alert(
              gettextCatalog.getString(
                "Protocol did not load properly. Please validate your protocol before trying to load again."
              )
            );
            deferred.reject("Failed to load protocol file");
          } else {
            deferred.resolve();
          }
          return deferred.promise;
        })
        .catch(function(err) {
          logger.error("Error while loading file: " + err);
        });
    }

    /**
     * Initialize errors containers, process protocol for errors, etc.
     * Saves the initialized protocol to the pm.root
     *
     */
    function initializeProtocol() {
      tasks.register("updating protocol", "Processing Protocol...");
      // initialize error container
      loading.p.errors = [];
      var cCommon, msg;

      // check for encryption requirements
      if (disk.requireEncryptedResults && !loading.p.publicKey) {
        loading.p.errors.push({
          type: gettextCatalog.getString("Public Key"),
          error: gettextCatalog.getString(
            'No public encryption key is defined in the protocol. Results will not be recorded from this protocol while the "Require Encryption" setting is enabled.'
          )
        });
      }

      //tabsint version
      loading.p.protocolTabsintOutdated = false; // always reset to false
      if (loading.p.minTabsintVersion) {
        var mtv = _.map(loading.p.minTabsintVersion.split("."), function(s) {
          return parseInt(s);
        }); //
        var ctv = _.map(version.dm.tabsint.split("-")[0].split("."), function(s) {
          return parseInt(s);
        });

        if (
          mtv[0] < ctv[0] ||
          (mtv[0] === ctv[0] && mtv[1] < ctv[1]) ||
          (mtv[0] === ctv[0] && mtv[1] === ctv[1] && mtv[2] <= ctv[2])
        ) {
          logger.debug(
            "Tabsint version " +
              version.dm.tabsint +
              ", Protocol requires tabsint version " +
              loading.p.minTabsintVersion
          );
        } else {
          msg =
            gettextCatalog.getString("Protocol requires tabsint version ") +
            loading.p.minTabsintVersion +
            gettextCatalog.getString(", but current Tabsint version is ") +
            version.dm.tabsint;
          logger.error(msg);
          loading.p.errors.push({
            type: gettextCatalog.getString("TabSINT Version"),
            error: msg
          });
          loading.p.protocolTabsintOutdated = true;
        }
      }

      // confirm EPHD1 is connected when headset is EPHD1
      loading.p.protocolUsbCMissing = false; // default/reset to false.
      if (loading.p.headset === "EPHD1") {
        loading.p.protocolUsbCMissing = !tabsintNative.isUsbConnected;
        console.log("About to run registerUsbDeviceListener()");
        tabsintNative.registerUsbDeviceListener(api.usbEventCallback);
      } else {
        tabsintNative.unregisterUsbDeviceListener(api.usbEventCallback);
      }

      // pass in new calibration version fields -- if c has any properties, it should have the version info now.
      var reqCalProperties = [
        "headset",
        "tablet",
        "audioProfileVersion",
        "calibrationPySVNRevision",
        "calibrationPyManualReleaseDate"
      ];
      if (_.difference(_.keys(loading.c), reqCalProperties).length > 0) {
        // if calibration contains wav files
        if (_.intersection(_.keys(loading.c), reqCalProperties).length === reqCalProperties.length) {
          // if calibration contains all the required properties
          loading.p.headset = loading.c.headset;
          loading.p._audioProfileVersion = loading.c.audioProfileVersion;
          loading.p._calibrationPySVNRevision = loading.c.calibrationPySVNRevision;
          loading.p._calibrationPyManualReleaseDate = loading.c.calibrationPyManualReleaseDate;
        } else {
          loading.p._audioProfileVersion = "none";
          loading.p._calibrationPySVNRevision = "none";
          loading.p._calibrationPyManualReleaseDate = "none";
          msg = "The loaded protocol calibration file is missing version fields.";
          logger.warn(msg);
          loading.p.errors.push({
            type: "Calibration",
            error: msg
          });
        }
      }
      loading.p.currentCalibration = loading.p.headset || "None"; // sets current headset for site checking, or none if none is specified

      // Does protocol use a common media repo?  If so, does it exist?
      if (loading.p.commonMediaRepository) {
        var midx = _.findIndex(disk.mediaRepos, {
          name: loading.p.commonMediaRepository
        });
        if (midx !== -1) {
          loading.p.commonRepo = disk.mediaRepos[midx];
          cCommon = json.load(loading.p.commonRepo.path + "calibration.json");
        } else {
          msg =
            "The media repository referenced by this protocol is not available (" +
            loading.p.commonMediaRepository +
            "). " +
            "Please try updating this protocol to automatically download the media repository";
          logger.warn("media repository referenced by protocol is not available: " + loading.p.commonMediaRepository);
          loading.p.errors.push({
            type: "Media",
            error: msg
          });
        }
      }

      // Recurse through the whole structure to process it...with defaults:
      loading.p._exportCSV = false;
      loading.p._protocolIdDict = {};
      loading.p._preProcessFunctionList = [];
      loading.p._missingPreProcessFunctionList = [];
      loading.p._missingControllerList = [];
      loading.p._customHtmlList = [];
      loading.p._missingHtmlList = [];
      loading.p._missingWavCalList = [];
      loading.p._missingCommonWavCalList = [];
      loading.p._requiresCha = false;

      processProtocol(loading.p, loading.p._protocolIdDict, loading.p, loading.c, cCommon, loading.meta.path);
      // put the processed protocol on the protocol model, root object
      pm.root = loading.p;

      //HG;10/22/20 FOR WEBBASED ENCRYPTION
      if ("key" in disk.protocol) {
        if (disk.protocol.key !== undefined) {
          pm.root.publicKey = decodeURI(disk.protocol.key);
        }
      }
      disk.protocol = loading.meta;
      disk.headset = pm.root.headset;

      // try connecting the cha
      if (loading.p._requiresCha) {
        logger.info("This exam requires the CHA, attempting to connect...");
        setTimeout(cha.connect, 1000);
      }

      // call each function from the callbackQueue
      callbackQueue.run();
    }

    /**
     * This is where we try to load the custom html files and make sure they
     * reference controllers appropriately.  Performs everything on the protocol at pm.root
     *
     *
     */
    function loadCustomJs() {
      if (pm.root.js) {
        // load any javascript files defined in top-level of protocol
        advancedProtocol.reset();
        return advancedProtocol.loadJs(loading.meta.path, pm.root.js);
      } else {
        return $q.resolve();
      }
    }

    function validateCustomJsIfCalledFor() {
      if (loading.validate && pm.root.js) {
        return checkCustomFiles();
      } else {
        return $q.resolve();
      }
    }

    function checkCustomFiles() {
      var promises = [];

      // check for registered preprocess functions
      pm.root._preProcessFunctionList.forEach(function(preProcessFunction) {
        if (advancedProtocol.registry[preProcessFunction]) {
          // function is registered
        } else {
          pm.root._missingPreProcessFunctionList.push(preProcessFunction);
        }
      });

      // check customHtml files
      if (pm.root._customHtmlList.length > 0) {
        pm.root._customHtmlList.forEach(function(html) {
          promises.push(checkHtml(html));
        });
      }

      return $q.all(promises);
    }

    function checkHtml(html) {
      var htmlDeferred = $q.defer();
      try {
        $.ajax({
          async: false,
          cache: false,
          global: false,
          type: "GET",
          url: html.path,
          success: function(file_html) {
            // success
            var match = 'ng-controller="';
            var ind = file_html.indexOf(match);
            if (ind >= 0) {
              file_html = file_html.slice(ind + match.length);
              var controller = file_html.slice(0, file_html.indexOf('"'));
              if (!window.tabsint.hasController(controller)) {
                logger.warn("Protocol custom html page references an unknown controller: " + controller);
                pm.root._missingControllerList.push(controller);
              }
            } else {
            }
            htmlDeferred.resolve();
          },
          error: function(e, type) {
            logger.error(
              "Failed to load custom html file: " +
                html.path +
                " because of error " +
                JSON.stringify(e) +
                ", of type" +
                JSON.stringify(type)
            );
            pm.root._missingHtmlList.push(html.path);
            htmlDeferred.resolve();
          }
        });
      } catch (e) {
        logger.error(
          "Failed to load custom html file: " +
            html.path +
            ".  Error catch during protocol.load html processing: " +
            JSON.stringify(e)
        );
        pm.root._missingHtmlList.push(html.name);
        htmlDeferred.resolve();
      }
      return htmlDeferred.promise;
    }

    /**
     * This is where we try to copy files from the filesToCopy folder onto the tablet
     * in the Internal Storage/tabsint-files folder
     *
     */
    function copyFilesOnLoad() {
      var deferred = $q.defer();
      if (pm.root.copyFilesOnLoad) {
        var promises = [];
        // load any wav or json files in the filesToCopy folder
        var dir = pm.root.copyFilesOnLoad;
        var fileTransfer = new FileTransfer();
        if (app.tablet) {
          file.createDirRecursively(paths.public(""), dir).then(
            file.getEntries(loading.meta.path + "/filesToCopy").then(function(files) {
              files.forEach(function(f) {
                promises.push(
                  fileTransfer.download(
                    f.nativeURL,
                    paths.public("") + dir + "/" + f.name,
                    function success(theFile) {
                      // do nothing
                    },
                    function fail(reason) {
                      logger.error("File " + f.name + " was not copied over to the " + dir + " directory");
                    }
                  )
                );
              });
              $q.all(promises).then(deferred.resolve);
            })
          );
        }
      } else {
        deferred.resolve();
      }
      return deferred.promise;
    }

    /**
     * Parse error messages and pop notifications to user if errors exist
     *
     */
    function handleLoadErrors() {
      var msg;

      // check calibration of files
      if (pm.root._missingWavCalList.length > 0 || pm.root._missingCommonWavCalList.length > 0) {
        if (pm.root._missingWavCalList.length + pm.root._missingCommonWavCalList.length < 10) {
          if (pm.root._missingWavCalList.length > 0 && pm.root._missingCommonWavCalList.length > 0) {
            msg =
              "Missing calibration(s) for wav files(s): " +
              pm.root._missingWavCalList +
              ", and common media wav file(s): " +
              pm.root._missingCommonWavCalList +
              ".";
          } else if (pm.root._missingWavCalList.length > 0 && pm.root._missingCommonWavCalList.length <= 0) {
            msg = "Missing calibration(s) for wav files(s): " + pm.root._missingWavCalList + ".";
          } else if (pm.root._missingWavCalList.length <= 0 && pm.root._missingCommonWavCalList.length > 0) {
            msg = "Missing common media wav file(s): " + pm.root._missingCommonWavCalList + ".";
          }
        } else {
          if (pm.root._missingWavCalList.length > 0 && pm.root._missingCommonWavCalList.length > 0) {
            msg =
              "Missing calibrations for " +
              pm.root._missingWavCalList.length +
              " wav files(s), and " +
              pm.root._missingCommonWavCalList.length +
              " common media wav files(s)";
          } else if (pm.root._missingWavCalList.length > 0 && pm.root._missingCommonWavCalList.length <= 0) {
            msg = "Missing calibrations for " + pm.root._missingWavCalList.length + " wav files(s). ";
          } else {
            msg = "Missing calibrations for " + pm.root._missingCommonWavCalList.length + " common media wav files(s)";
          }
        }
        logger.warn(msg);
        pm.root.errors.push({
          type: "Calibration",
          error: msg
        });
      } else {
        logger.info("All calibration files found.");
      }

      // check preProcessFunctions and Controllers
      if (
        (pm.root._missingPreProcessFunctionList.length > 0 || pm.root._missingControllerList.length > 0) &&
        !pm.root.js
      ) {
        msg =
          'The protocol uses custom functions that should be found in a customJs.js file, but the protocol does not have a "js" field pointing to this file.  Please make sure the file exists and is referenced properly.';
        pm.root.errors.push({
          type: "Protocol",
          error: msg
        });
      }

      if (pm.root._missingPreProcessFunctionList.length > 0) {
        msg =
          "The protocol references the following undefined pre-process functions: " +
          pm.root._missingPreProcessFunctionList +
          ".  Please make sure each function is defined properly in the customJs.js file.";
        pm.root.errors.push({
          type: "Protocol",
          error: msg
        });
      }

      if (pm.root._missingControllerList.length > 0) {
        msg =
          "The protocol contains custom html pages that reference the following undefined controllers: " +
          pm.root._missingControllerList +
          ".  Please make sure each controller is defined properly in the customJs.js file.";
        pm.root.errors.push({
          type: "Protocol",
          error: msg
        });
      }

      if (pm.root._missingHtmlList.length > 0) {
        msg =
          "The protocol references the following html pages that could not be loaded:  " +
          pm.root._missingHtmlList +
          ".  Please make sure each html page exists and is referenced properly.";
        pm.root.errors.push({
          type: "Protocol",
          error: msg
        });
      }

      // log/display errors
      if (angular.isDefined(pm.root.errors) && pm.root.errors.length > 0) {
        msg =
          gettextCatalog.getString("The protocol contains the following errors and may not function properly.") +
          " \n\n";
        for (var i = 0; i < pm.root.errors.length; i++) {
          var err = pm.root.errors[i];
          msg += err.type + ":\n";
          msg += " - " + err.error + "\n";
        }
        logger.error(" Protocol contains the following errors: " + angular.toJson(pm.root.errors));
        notifications.alert(msg);
      } else if (loading.notify) {
        notifications.alert(
          gettextCatalog.getString("Successfully loaded protocol: ") +
            loading.meta.name +
            gettextCatalog.getString("\nThis protocol requires headset: " + pm.root.headset)
        );
      }

      tasks.deregister("updating protocol");
    }

    /**
     * Recursive method to process protocols and subprotocols.
     * This method mutates the original protocol object loaded from file.
     * @param  {[type]} protocol [description]
     * @param  {[type]} dict     [description]
     * @param  {[type]} p        [description]
     * @param  {[type]} c        [calibration.json obj loaded as part of protocol]
     * @param  {[type]} c2       [calibration.json obj loaded as part of common media repo referenced by protocol]
     * @param  {[type]} prefix   [description]
     * @return {[type]}          [description]
     */
    function processProtocol(protocol, dict, p, c, c2, prefix) {
      _.forEach(protocol.pages, function(page) {
        processPage(page, dict, p, c, c2, prefix);
      });

      // make a dict of all of the  IDs.
      if (_.has(protocol, "protocolId")) {
        dict[protocol.protocolId] = protocol; //todo: we are storing the whole procotocol here...
      }

      if (_.has(protocol, "subProtocols")) {
        _.forEach(protocol.subProtocols, function(obj) {
          processProtocol(obj, dict, p, c, c2, prefix);
        });
      }
    }

    /**
     * Recursive method to process pages.
     * This method mutates the original protocol object loaded from file.
     * @param  {[type]} page   [description]
     * @param  {[type]} dict   [description]
     * @param  {[type]} p      [description]
     * @param  {[type]} c      [calibration.json obj loaded as part of protocol]
     * @param  {[type]} c2     [calibration.json obj loaded as part of common media repo referenced by protocol]
     * @param  {[type]} prefix [description]
     * @return {[type]}        [description]
     */
    function processPage(page, dict, p, c, c2, prefix) {
      // check for missing preProcessFunctions
      if (angular.isDefined(page.preProcessFunction)) {
        p._preProcessFunctionList.push(page.preProcessFunction);
      }

      if (angular.isDefined(page.wavfiles)) {
        _.forEach(page.wavfiles, function(wavfile) {
          // wav files using common media repos
          if (wavfile.useCommonRepo) {
            if (p.commonRepo && p.commonRepo.path) {
              if (c2) {
                if (c2[wavfile.path]) {
                  wavfile.cal = c2[wavfile.path];
                } else {
                  p._missingCommonWavCalList.push(wavfile.path);
                }
              } else {
                p._missingCommonMediaRepo = true;
                p._missingCommonWavCalList.push(wavfile.path);
              }
              wavfile.path = p.commonRepo.path + wavfile.path;
            }
          }

          // wav files using protocol contained media
          else {
            if (c && c[wavfile.path]) {
              wavfile.cal = c[wavfile.path];
              wavfile.cal.tablet = c.tablet;
            } else {
              p._missingWavCalList.push(wavfile.path);
            }

            wavfile.path = prefix + wavfile.path;
          }
        });
      }

      // Fix paths to the image and video files.
      if (angular.isDefined(page.image)) {
        page.image.path = prefix + page.image.path;
      }

      if (angular.isDefined(page.video)) {
        page.video.path = prefix + page.video.path;
      }

      // Access page.responseAreas
      if (angular.isDefined(page.responseArea)) {
        // Fix paths to the image files in image map response Areas
        if (angular.isDefined(page.responseArea.image)) {
          page.responseArea.image.path = prefix + page.responseArea.image.path;
        }

        // Custom Response Area handling
        if (angular.isDefined(page.responseArea.html)) {
          var originalHtmlFile = page.responseArea.html;
          page.responseArea.html = prefix + page.responseArea.html; // fix path
          p._customHtmlList.push({
            name: originalHtmlFile,
            path: page.responseArea.html,
            id: page.id
          }); // add to list for checking later (async)
        }

        // Flag subject history
        if (page.responseArea.type === "subjectIdResponseArea") {
          p._hasSubjectIdResponseArea = true;
        }

        // flag cha reseponse areas
        if (page.responseArea.type.startsWith("cha")) {
          p._requiresCha = true;
        }

        // flag whether protocol requires export to CSV
        if (_.has(page.responseArea, "exportToCSV")) {
          if (page.responseArea.exportToCSV === true) {
            p._exportCSV = true;
          }
        }
        if (_.has(page, "exportToCSV")) {
          if (page.exportToCSV === true) {
            p._exportCSV = true;
          }
        }
        if (page.responseArea.type === "multipleInputResponseArea") {
          _.forEach(page.responseArea.inputList, function(input) {
            if (_.has(input, "exportToCSV")) {
              if (input.exportToCSV === true) {
                p._exportCSV = true;
                return;
              }
            }
          });
        }

        // Alert user if three digit test protocol uses maxSNR or maxLevel, as those parameters have been deprecated since Walrus firmware
        if (page.responseArea.type === "chaThreeDigit") {
          if (angular.isDefined(page.responseArea.examProperties.maxSNR)) {
            notifications.alert(
              "The maxSNR parameter has been deprecated and is no longer used in the exam algorithm."
            );
          }
          if (angular.isDefined(page.responseArea.examProperties.maxLevel)) {
            notifications.alert(
              "The maxLevel parameter has been deprecated and is no longer used in the exam algorithm."
            );
          }
        }

        // Alert user audiometry list response area is deprecated
        if (page.responseArea.type === "chaAudiometryList") {
          notifications.alert("The audiometry list response area was removed in TabSINT v4.4.0.");
        }

        // deprecate chaTAT
        if (page.responseArea.type === "chaTAT") {
          notifications.alert("The Tones At Threshold response area was removed in TabSINT v4.4.0.");
        }

        // deprecate chaCRM
        if (page.responseArea.type === "chaCRM") {
          notifications.alert("The CRM response area was removed in TabSINT v4.4.0.");
        }

        // deprecate chaSoundRecognition
        if (page.responseArea.type === "chaSoundRecognition") {
          notifications.alert("The sound recognition response area was removed in TabSINT v4.4.0.");
        }

        // deprecate freeformResponseArea
        if (page.responseArea.type === "freeformResponseArea") {
          notifications.alert("The freeform response area was removed in TabSINT v4.4.0.");
        }

        // load callbacks if response area has one -- will be performed at the end of the load cycle
        var respAreas = responseAreas.all();
        if (_.has(respAreas[page.responseArea.type], "loadCallback")) {
          var name = respAreas[page.responseArea.type].loadCallback.name;
          if (name === "") {
            name = page.responseArea.type;
          }
          callbackQueue.add(name, respAreas[page.responseArea.type].loadCallback);
        }
      }

      // if page has a subprotocol follow-on (i.e., it has a 'pages' variable) then recurse into that using parent fcn.
      // also if any follow-on is  a page, recurse on that using this fcn.
      if (_.has(page, "followOns")) {
        _.forEach(page.followOns, function(followOn) {
          if (_.has(followOn.target, "id") && !_.has(followOn.target, "reference")) {
            processPage(followOn.target, dict, p, c, c2, prefix); // it's a page.
          } else if (_.has(followOn.target, "pages")) {
            processProtocol(followOn.target, dict, p, c, c2, prefix); // it's a subprotocol
          }
        });
      }

      // is it an inline subprotocol...
      if (_.has(page, "pages")) {
        processProtocol(page, dict, p, c, c2, prefix);
      }
    }

    /**
     * Change protocol source. Called from the admin page
     * Updated version of server.toggleServerMode
     * @param  {string} newSource - new protocol source
     */
    api.changeSource = function(newSource) {
      disk.server = newSource;
    };

    // Reset will be called each time the exam is reset in examLogic
    // Logic here should be run every exam, including:
    // - responseArea-based preload functions
    // - protocol checks and alerts (missing wav cal, missing files, etc.)
    api.reset = function() {
      logger.debug("Resetting protocol");
    };

    api.usbEventCallback = function(connected) {
      logger.debug("protocol.usbEventCallback invoked, connected: " + JSON.stringify(connected));
      pm.root.protocolUsbCMissing = !connected;
    };

    /**
     * Method to override protocol - only used in testing for now
     * @param  {[type]} path     [description]
     * @param  {[type]} validate [description]
     * @return {[type]}          [description]
     */
    api.override = function(path, validate, notify) {
      logger.debug("Overriding protocol with path: " + path);
      var p = api.define({
        name: path,
        path: paths.www(path)
      });
      return api.load(p, validate, notify);
    };

    return api;
  });
