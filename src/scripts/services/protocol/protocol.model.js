/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/* global CordovaSphinx, FileError */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.protocol.model", [])

  .factory("pm", function() {
    var pm = {};

    pm.root = undefined;
    pm.outdated = undefined;
    pm.calibration = undefined;

    // Protocols to be available when testing in the browser, all releative to the 'www/' directory
    // These currently get defined in protocol.$init()
    pm.local = [];

    return pm;
  });
