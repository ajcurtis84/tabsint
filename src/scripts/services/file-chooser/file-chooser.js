/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.file-chooser", [])

  .factory("fileChooser", function(
    app,
    notifications,
    $q,
    $uibModal,
    file,
    logger,
    gettextCatalog,
    devices,
    paths,
    $cordovaFile,
    disk,
    protocol
  ) {
    var api = {
      choose: undefined,
      choosePreAPI30: undefined,
      chooseAndroidScopedStorage: undefined
    };

    api.choose = function(fileOrDirectory) {
      // var deferred = $q.defer();

      if (fileOrDirectory == "directory") {
        if (!app.tablet) {
          notifications.alert(gettextCatalog.getString("File chooser disabled while debugging in the browser"));
          // deferred.reject({ msg: "Cannot choose file in debug browser" });
          // return deferred.promise;
        }

        // Android Filechooser (Forced to implement as of API30)
        if (devices.platform.toLowerCase() === "android") {
          console.log("Running Android Filechooser.");
          return api.chooseAndroidScopedStorage();
        }

        if (devices.platform.toLowerCase() === "ios") {
          // OLD CUSTOM FILECHOOSER CODE (API <= 29 ONLY) - currently disabled and not supported (easy to add with version checking)
          logger.debug("Running API <= 29 file chooser.");
          return api.choosePreAPI30(fileOrDirectory);
        }
      }
    };

    api.chooseAndroidScopedStorage = function() {
      //variables shared throughout promise chain
      var chosenDir = {
        name: undefined, //name of directory
        contentURI: undefined, //content URI of firectory provided by Android
        nativeURL: undefined //internal path the cordova file plugin can read
      };
      var errorCode;
      var isValid = false;
      var alreadyLoaded = [];
      var adminProtocols = [];
      for (var i in disk.protocols) {
        adminProtocols.push(disk.protocols[i].name);
      }
      return (
        file
          .selectDirectory(null)
          .then(function(chosenPath) {
            console.log("API 30 file chooser chose: %O", chosenPath);
            //get the name of this directory
            chosenDir.contentURI = chosenPath;
            return file.getNameFromURI(chosenDir.contentURI);
          })
          .then(function(chosenName) {
            chosenDir.name = chosenName;
            console.log("The chosen directory name is " + chosenDir.name);
            //determine if the protocol is valid - contains protocol.json
            return file.listExternalFiles(chosenDir.contentURI);
          })
          .then(function(listOfFiles) {
            listOfFiles.forEach(item => {
              if (item._display_name == "protocol.json") {
                isValid = true;
              }
            });
            //need to list files to check if protocol is already loaded (non-Android function)
            return file.listFiles(cordova.file.dataDirectory + "protocols/local/");
          })
          .then(function(entriesList) {
            var deferred = $q.defer();
            //fill array of locally added protocols
            for (var fileEntry in entriesList) {
              alreadyLoaded.push(entriesList[fileEntry].name);
            }
            //check if protocol is valid
            if (!isValid) {
              errorCode = 601;
              console.log("No protocol.json file found");
              deferred.reject();
            }
            //check if protocol is an admin protocol or already loaded
            else {
              var toDel = undefined;
              //check if name matches an admin protocol
              if (adminProtocols.includes(chosenDir.name) && !alreadyLoaded.includes(chosenDir.name)) {
                errorCode = 602;
                deferred.reject();
              }
              //check if name matches already loaded protocol
              else if (adminProtocols.includes(chosenDir.name) && alreadyLoaded.includes(chosenDir.name)) {
                console.log("protocol already exists on disk");
                notifications.alert(
                  "Protocol with that name is already loaded locally and has been overwritten with the newer version."
                );
                //save protocol as toDel to delete later
                for (var proto in disk.protocols) {
                  if (disk.protocols[proto].name == chosenDir.name) {
                    toDel = disk.protocols[proto];
                  }
                }
              }
              //check if name is a new protocol name
              else if (!adminProtocols.includes(chosenDir.name) && !alreadyLoaded.includes(chosenDir.name)) {
                toDel = undefined;
              }
              deferred.resolve(toDel);
            }
            return deferred.promise;
          })
          .then(function(deleteDir) {
            //delete directory if already loaded so it can be updated
            if (deleteDir != undefined) {
              console.log("deleting protocol before re-copying");
              return protocol.delete(deleteDir);
            }
          })
          .then(function() {
            console.log("copying directory");
            //copy directory
            return file.copyDirectory(chosenDir.contentURI, chosenDir.name);
          })
          //check to see if any error occurred
          .catch(function(err) {
            if (![601, 602].includes(err.code)) {
              errorCode = 600;
            }
            console.log("error with code: " + errorCode);
          })
          //pass directory or error through to local-server promise chain
          .then(function() {
            var deferred2 = $q.defer();
            if (errorCode) {
              console.log("errorCode", errorCode);
            }
            //if no error, resolve chosenDir
            if (![600, 601, 602].includes(errorCode)) {
              chosenDir.nativeURL = cordova.file.dataDirectory + "protocols/local/" + chosenDir.name;
              deferred2.resolve(chosenDir);
            }
            //if error, reject promise with correct msg
            else {
              if ([601, 602].includes(errorCode)) {
                if (errorCode == 601) {
                  deferred2.reject({
                    code: 601,
                    msg: "No protocol.json file found"
                  });
                }
                if (errorCode == 602) {
                  deferred2.reject({
                    code: 602,
                    msg: "Pre-loaded built-in protocol already exists with this name"
                  });
                }
              } else {
                deferred2.reject({
                  code: 600,
                  msg: "There was an unknown error"
                });
              }
            }
            return deferred2.promise;
          })
      );
    };

    api.choosePreAPI30 = function(fileOrDirectory) {
      var deferred = $q.defer();

      var FileChooserCtrl = function($scope, $uibModalInstance) {
        function getContents(path) {
          logger.debug("-- fileChooser.getContents(path) with path: " + angular.toJson(path));

          file.getEntries(path).then(function(result) {
            $scope.files = result;
            if (path !== file.localFS.nativeURL) {
              $scope.files.unshift({ name: "[parent]" });
              file.getParentDirectory(path).then(function(result) {
                result.name = "[parent]";
                $scope.files[0] = result;
                //console.log('-- fileChooser new file list: ' + angular.toJson($scope.files));
              });
            } else {
              logger.debug("-- fileChoser new file list: " + angular.toJson($scope.files));
            }
          });
        }

        var currentDirectory = file.localFS.nativeURL;
        $scope.fileOrDirectory = fileOrDirectory;
        $scope.selectedFile = undefined;
        $scope.files = [];

        getContents(currentDirectory);

        //console.log('starting file list: '+angular.toJson($scope.files));

        $scope.createDirectory = function() {
          var newDirectoryName = window.prompt("New Directory Name");
          console.log("-- creating new directory" + newDirectoryName + ", at currentDirectory: " + currentDirectory);
          if (angular.isDefined(newDirectoryName) && newDirectoryName !== "") {
            file.createDirectory(currentDirectory, newDirectoryName).then(getContents(currentDirectory));
          }
        };

        $scope.clickFile = function(file) {
          $scope.selectedFile = file;
          //console.log('-- fileChooser.clickFile() '+angular.toJson(file));
          if (file.isDirectory) {
            currentDirectory = file.nativeURL;
            getContents(file.nativeURL);
          }
        };

        // Footer button logic (Select, Cancel)
        $scope.select = function(chosenFile) {
          logger.debug("fileChooser.select() " + angular.toJson(chosenFile));
          $uibModalInstance.close(chosenFile);
        };

        $scope.cancel = function() {
          deferred.reject({ code: 502, msg: "ChooseFile modal cancelled" });
          //console.log('-- chose file cancelled');
          $uibModalInstance.dismiss("cancel");
        };
      };

      // Modal controllers and logic
      var modalInstance = $uibModal.open({
        templateUrl: "scripts/services/views/filechooser.html",
        controller: FileChooserCtrl,
        backdrop: "static"
      });

      // function to call upon $uibModalInstance.close()
      modalInstance.result.then(function(chosenFile) {
        console.log("JKP file chooser chose: %O", chosenFile);
        if (fileOrDirectory === "file") {
          if (chosenFile && chosenFile.isFile) {
            logger.debug("fileChooser chose file: " + angular.toJson(chosenFile));
            deferred.resolve(chosenFile);
          } else {
            logger.debug("fileChooser did not chose a proper file: " + angular.toJson(chosenFile));
            deferred.reject({
              code: 503,
              msg: "ChooseFile modal did not choose a proper file"
            });
          }
        } else if (fileOrDirectory === "directory") {
          if (chosenFile && chosenFile.isDirectory) {
            logger.debug("fileChooser chose directory: " + angular.toJson(chosenFile));
            deferred.resolve(chosenFile);
          } else {
            logger.debug("fileChooser did not chose a proper directory: " + angular.toJson(chosenFile));
            deferred.reject({
              code: 504,
              msg: "ChooseFile modal did not choose a proper directory"
            });
          }
        }
      });

      return deferred.promise;
    };

    return api;
  });
