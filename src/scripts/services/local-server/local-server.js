/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.local-server", [])

  .factory("localServer", function(
    $q,
    logger,
    tasks,
    file,
    fileChooser,
    notifications,
    disk,
    protocol,
    gettextCatalog
  ) {
    var api = {};

    /**
     * Add a protocol from the sd card
     * @param {string} pType - 'media' or 'protocol' to signify what type of protocol is getting loaded
     */
    api.addProtocol = function(pType) {
      // api.defer.addProtocol = $q.defer();
      pType = pType || "protocol";
      var date;
      var meta;

      function getMetaData(theFile) {
        let deferred = $q.defer();
        //Currently using date that protocol was loaded into TabSINT
        var today = new Date(new Date().toLocaleString("en-US", { timeZone: "America/New_York" }));
        var datestring =
          today.getFullYear() +
          "-" +
          ("0" + (today.getMonth() + 1)).slice(-2) +
          "-" +
          ("0" + today.getDate()).slice(-2) +
          " " +
          ("0" + today.getHours()).slice(-2) +
          ":" +
          ("0" + today.getMinutes()).slice(-2) +
          ":" +
          ("0" + today.getSeconds()).slice(-2);

        deferred.resolve([theFile, datestring]);
        return deferred.promise;
      }

      function defineProt([theFile, date]) {
        let deferred = $q.defer();
        meta = protocol.define({
          name: theFile.name,
          path: theFile.nativeURL,
          date: date,
          server: "localServer",
          contentURI: theFile.contentURI
        });
        deferred.resolve([theFile, meta]);
        return deferred.promise;
      }

      function storeProt([theFile, meta]) {
        let deferred = $q.defer();
        if (pType === "protocol") {
          protocol.store(meta);
        } else if (pType === "media") {
          protocol.storeMedia(meta);
        }
        // Check that meta.name comes right after "tabsint-protocols" otherwise don't populate disk.servers.localServer.protocolDir
        let l = meta.path.split("/");
        if (l[l.length - 3] === "tabsint-protocols" && l[l.length - 2] === meta.name) {
          disk.servers.localServer.protocolDir = meta.name;
        }
        console.log("meta", meta);
        deferred.resolve(meta);
        return deferred.promise;
      }

      return fileChooser
        .choose("directory")
        .then(getMetaData)
        .then(defineProt)
        .then(storeProt)
        .catch(function(err) {
          var msg, type;
          if (err.msg === "ChooseFile modal cancelled") {
            logger.debug(pType + " load failed with error: " + angular.toJson(err));
            msg = gettextCatalog.getString("User cancelled local ") + pType + gettextCatalog.getString(" file chooser");
          } else if (err.msg === "ChooseFile modal did not choose a proper file") {
            logger.warn(pType + " load failed with error: " + angular.toJson(err));
            msg = gettextCatalog.getString("The item selected was not a proper file");
            notifications.alert(msg);
          } else if (err.msg === "No protocol.json file found") {
            logger.warn(pType + " load failed with error: " + angular.toJson(err));
            notifications.alert("No protocol.json file found. Please check this protocol or try another.");
          } else if (err.msg === "Pre-loaded built-in protocol already exists with this name") {
            logger.warn(pType + " load failed with error: " + angular.toJson(err));
            notifications.alert(
              "Pre-loaded built-in protocol already exists with this name. Please change this protocols name and try again."
            );
          } else if (err.msg === "Protocol with the same name is already loaded, canceling load") {
            logger.warn(pType + " load failed with error: " + angular.toJson(err));
            notifications.alert(
              "Protocol with the same name was already loaded. Loading of current protocol has been canceled."
            );
          } else {
            logger.error(pType + " load failed with error: " + angular.toJson(err));
            msg = gettextCatalog.getString(
              "Load failed for an unknown reason. Please verify the protocol syntax and location. For further troubleshooting help, please see the documentation at https://tabsint.org"
            );
            notifications.alert(msg);
          }

          return $q.reject(msg);
        });
    };

    api.changeResultsDirectory = function() {
      fileChooser.choose("directory").then(function(directory) {
        var tmp = directory.fullPath;
        if (tmp[0].indexOf("/") >= 0) {
          tmp = tmp.slice(1);
        }

        if (tmp[tmp.length - 1].indexOf("/") >= 0) {
          tmp = tmp.slice(0, -1);
        }
        disk.servers.localServer.resultsDir = tmp;
      });
    };

    if (window.tabsint) {
      window.tabsint.localServer = api;
    }

    return api;
  });
