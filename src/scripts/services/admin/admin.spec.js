/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import "../../app";

beforeEach(angular.mock.module("tabsint"));

describe("AdminLogic", function() {
  var adminLogic,
    disk,
    $rootScope,
    // $window,
    pm,
    $httpBackend,
    examLogic,
    config,
    notifications,
    json,
    logger,
    timeout,
    sqLite,
    network,
    file,
    router,
    results;

  beforeEach(
    angular.mock.module(function($provide) {
      $provide.value("authorize", {
        modalAuthorize: function(targetFn) {
          targetFn();
        } // assume authorized.
      });
    })
  );
  beforeEach(
    angular.mock.module(function($provide) {
      $provide.value("chooseCha", {
        discover: function() {}
      });
    })
  );

  beforeEach(
    angular.mock.inject(function(
      $injector,
      _adminLogic_,
      _disk_,
      _$rootScope_,
      _$httpBackend_,
      // _$window_,
      _examLogic_,
      _pm_,
      _config_,
      _notifications_,
      _json_,
      _logger_,
      _$timeout_,
      _results_,
      _sqLite_,
      _network_,
      _file_,
      _router_
    ) {
      adminLogic = _adminLogic_;
      disk = _disk_;
      $rootScope = _$rootScope_;
      $httpBackend = _$httpBackend_;
      // $window = _$window_;
      examLogic = _examLogic_;
      config = _config_;
      notifications = _notifications_;
      json = _json_;
      logger = _logger_;
      timeout = _$timeout_;
      sqLite = _sqLite_;
      network = _network_;
      file = _file_;
      router = _router_;
      results = _results_;
      pm = _pm_;
    })
  );

  // $window.resolveLocalFileSystemURL = function() {
  //   return {};
  // };

  // spyOn($window, "resolveLocalFileSystemURL").and.returnValue($window.resolveLocalFileSystemURL);
});
