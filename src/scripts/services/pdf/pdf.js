/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import pdfMake from "pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

angular
  .module("tabsint.services.pdf", [])
  .factory("pdfService", function($cordovaFile, $q, app, file, gettextCatalog, logger, paths, results) {
    var api = {
      createPdf: undefined
    };

    api.createPdf = function(docDefinition) {
      return $q(function(resolve, reject) {
        const pdfDocGenerator = pdfMake.createPdf(docDefinition);
        const dir = "Documents/tabsint-pdfs/";
        if (!app.tablet) {
          // Open PDF in browser if TabSINT running in browser
          pdfDocGenerator.open();
        } else {
          pdfDocGenerator.getBlob(blob => {
            let timeStamp = results.current.testDateTime.split(".")[0];
            // same convention as results for naming. (tablet ID, timestamp, etc.)
            return file
              .createDirRecursively(cordova.file.externalRootDirectory, dir)
              .then(function() {
                return $cordovaFile.writeFile(
                  cordova.file.externalRootDirectory + dir,
                  results.filename(results.current, ".pdf"),
                  blob
                );
              })
              .then(function() {
                console.log("Pdf created");
              })
              .catch(function(e) {
                logger.error("Failed to save pdf to file with error: " + angular.toJson(e));
                notifications.alert(
                  gettextCatalog.getString("Failed to save pdf to file. Please file an issue at") +
                    " https://gitlab.com/creare-com/tabsint"
                );
                return $q.reject("Failed to create pdf with error: " + e);
              });
            // file.createFile("tabsint-pdfs", results.filename(results.current, ".pdf"), blob);
          });
        }
      });
    };
    return api;
  });
