/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.paths", [])

  .factory("paths", function(logger, app, devices) {
    var paths = {};

    /**
     * Method to handle https paths
     * @param  {string} path - path to clean up
     * @return {string}      cleaned up path
     */
    paths.https = function(path) {
      if (angular.isUndefined(path)) {
        logger.error("No path specified in paths.https");
        return undefined;
      }

      if (path.slice(0, 8).toLowerCase() !== "https://") {
        path = "https://" + path;
      }
      if (path.slice(-1) !== "/") {
        path = path + "/";
      }

      return path;
    };

    /**
     * Method to handle directory paths - puts them in the form "path/"
     * @param  {string} path - path to clean up
     * @return {string}      cleaned up path
     */
    paths.dir = function(path) {
      if (angular.isUndefined(path)) {
        return "";
      }

      // remove the preceding /
      if (path[0] === "/") {
        path = path.slice(1);
      }

      // add a trailing /
      if (path.slice(-1) !== "/") {
        path = path + "/";
      }

      return path;
    };

    /**
     * Method to handle file paths - puts them in the form "path/file.ext"
     * @param  {string} path - path to clean up
     * @return {string}      cleaned up path
     */
    paths.file = function(path) {
      if (angular.isUndefined(path)) {
        return "";
      }

      // remove the preceding /
      if (path[0] === "/") {
        path = path.slice(1);
      }

      return path;
    };

    /**
     * Method to handle relative file paths for gitlab protocols.
     * This is not a native url
     * @param  {string} repo - repository object from gitlab
     * @return {string} cleaned up path
     */
    paths.gitlab = function(repo) {
      var base = paths.dir("gitlab/"); // base directory for all repositories
      var namespace = paths.dir(repo.namespace.path);
      var repository = paths.dir(repo.dir);

      return paths.dir(base + namespace + repository);
    };

    /**
     *
     * Method to handle paths relative to the /www directory
     * @param {string} path='' - raw path string
     * @returns {string} cleaned up path
     */
    paths.www = function(path) {
      // clean up path name first
      path = paths.file(path);

      // cordova.file will not load right away
      if (app.tablet && cordova.file && !path.startsWith(cordova.file.applicationDirectory)) {
        path = cordova.file.applicationDirectory + "www/" + path;
      } else if (app.test && !path.startsWith("base/www/")) {
        path = "base/www/" + path;
      }

      return path;
    };

    /**
     *
     * Method to handle paths relative to data directory
     * @param {string} path='' - raw path string
     * @returns {string} cleaned up path
     */
    paths.data = function(path, file) {
      // clean up path name first
      path = paths.file(path);

      // cordova.file will not load right away
      if (
        app.tablet &&
        cordova.file &&
        devices.platform === "android" &&
        !path.startsWith(cordova.file.dataDirectory)
      ) {
        path = cordova.file.dataDirectory + "files/" + path;
      } else if (
        app.tablet &&
        cordova.file &&
        devices.platform === "ios" &&
        !path.startsWith(cordova.file.dataDirectory)
      ) {
        path = cordova.file.documentsDirectory + path;
      }

      return path;
    };

    /**
     *
     * Method to handle paths relative to the public directory
     * @param {string} path='' - raw path string
     * @returns {string} cleaned up path
     */
    paths.public = function(path, file) {
      // clean up path name first
      path = paths.file(path);

      if (app.tablet && cordova.file) {
        if (devices.platform === "android" && !path.startsWith(cordova.file.externalRootDirectory)) {
          path = cordova.file.externalRootDirectory + "Documents/" + path;
        } else if (devices.platform === "ios" && !path.startsWith(cordova.file.documentsDirectory)) {
          path = cordova.file.documentsDirectory + path;
        }
      }

      return path;
    };

    return paths;
  });
