/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.services.plugins", []) // must load plugin modules before this module

  .factory("plugins", function(logger, responseAreas, $q) {
    var api = {
      add: {
        elem: undefined,
        responseArea: undefined,
        onEvent: undefined
      },
      runEvent: undefined,
      events: undefined,
      elems: undefined
    };

    // Elements
    api.elems = {
      adminPanels: [],
      navbar: [],
      localProtocols: [],
      headsets: []
    };

    // function to add elements around the app (i.e. admin page, navbar, etc)
    api.add.elem = function(to, data) {
      if (_.includes(_.keys(api.elems), to)) {
        api.elems[to].push(data);
      } else {
        logger.error("Trying to add data to " + to + " but this field is not supported in plugins.elems");
      }
    };

    // Response Areas
    api.add.responseArea = function(type, spec) {
      if (typeof type === "object") {
        _.forEach(type, function(spec, type) {
          addResponseArea(type, spec);
        });
      } else {
        addResponseArea(type, spec);
      }

      function addResponseArea(type, spec) {
        if (!_.isUndefined(responseAreas.native[type])) {
          logger.error("Trying to add response area " + type + " but it is already defined in native tabsint");
          return;
        }
        if (_.isUndefined(responseAreas.plugins[type])) {
          responseAreas.plugins[type] = spec;
        } else {
          logger.error("Trying to add response area " + type + " but it is already defined");
        }
      }
    };

    // function to run functions at certain moments in the app (i.e. exam reset, switching pages)
    api.events = {
      appLoad: [],
      switchToAdminView: [],
      switchToExamView: [],
      resetStart: [],
      resetEnd: [],
      pageStart: [],
      pageEnd: []
    };

    api.add.onEvent = function(what, runFunc) {
      if (typeof what === "object") {
        _.forEach(what, function(w) {
          addOnEvent(w);
        });
      } else {
        addOnEvent(what);
      }
      function addOnEvent(event) {
        if (_.includes(_.keys(api.events), event)) {
          api.events[event].push(runFunc);
        } else {
          logger.error("Trying to add function to " + event + " but this field is not supported in plugins.actions");
        }
      }
    };

    api.runEvent = function(event, params) {
      var p = [];
      _.forEach(api.events[event], function(runFunc) {
        try {
          p.push(runFunc(params));
        } catch (e) {
          logger.debug("plugins.runEvent failed for event " + event + " with error: " + angular.toJson(e));
        }
      });

      return $q.all(p);
    };

    return api;
  });
