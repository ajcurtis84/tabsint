// jshint ignore: start

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import "angular-mocks";
import "../../../app";

beforeEach(angular.mock.module("tabsint"));

describe("Media Service", function() {
  var gain, disk, devices, media;

  beforeEach(
    angular.mock.inject(function($injector, _media_, _gain_, _disk_, _devices_) {
      gain = _gain_;
      disk = _disk_;
      devices = _devices_;
      media = _media_;
    })
  );
  //NOTE: These tests are only for regression. Volume levels determined
  // using TabSINT 4.1.0 and may not be correct. Testing to see if TabSINT
  // volume calculations stay the same between versions for these scenarios.
  it("Returned volume should be 0.08604629807912162 (arbitrary, TabE calibration, TabE playback)", function() {
    let wavfile = {
      path: "file:///android_asset/www/res/protocol/WAHTS-test/S1_Talker3_PALE_99_Noise_70_Mono.wav",
      playbackMethod: "arbitrary",
      targetSPL: 70,
      cal: {
        refType: "as-recorded",
        RMSC: 0.0042680985522230595,
        realWorldRMSA: 0.04919963839889143,
        RMSA: 0.00437500508228025,
        wavRMSC: 0.09565749165442168,
        wavRMSA: 0.0980535025200617,
        calibrationFilter: "full",
        wavRMSZ: 0.0993730943062474,
        normFactor: 22.41220311199188,
        RMSZ: 0.004433883354067802,
        realWorldRMSZ: 0.049861760985498674,
        realWorldRMSC: 0.04799740833918311,
        scaleFactor: 0.13519823071552697,
        tablet: "TabE"
      }
    };
    devices.model = "SamsungTabE";
    expect(media.calculateVolume(wavfile)).toBeCloseTo(0.086, 3);
  });
  it("Returned volume should be 0.231 (arbitrary, TabE calibration, Nexus 7 playback)", function() {
    let wavfile = {
      path: "file:///android_asset/www/res/protocol/WAHTS-test/S1_Talker3_PALE_99_Noise_70_Mono.wav",
      playbackMethod: "arbitrary",
      targetSPL: 70,
      cal: {
        refType: "as-recorded",
        RMSC: 0.0042680985522230595,
        realWorldRMSA: 0.04919963839889143,
        RMSA: 0.00437500508228025,
        wavRMSC: 0.09565749165442168,
        wavRMSA: 0.0980535025200617,
        calibrationFilter: "full",
        wavRMSZ: 0.0993730943062474,
        normFactor: 22.41220311199188,
        RMSZ: 0.004433883354067802,
        realWorldRMSZ: 0.049861760985498674,
        realWorldRMSC: 0.04799740833918311,
        scaleFactor: 0.13519823071552697,
        tablet: "TabE"
      }
    };
    devices.model = "Nexus 7";
    expect(media.calculateVolume(wavfile)).toBeCloseTo(0.231, 3);
  });
  it("Returned volume should be 0.145 (arbitrary, TabE calibration, Tab A playback)", function() {
    let wavfile = {
      path: "file:///android_asset/www/res/protocol/WAHTS-test/S1_Talker3_PALE_99_Noise_70_Mono.wav",
      playbackMethod: "arbitrary",
      targetSPL: 70,
      cal: {
        refType: "as-recorded",
        RMSC: 0.0042680985522230595,
        realWorldRMSA: 0.04919963839889143,
        RMSA: 0.00437500508228025,
        wavRMSC: 0.09565749165442168,
        wavRMSA: 0.0980535025200617,
        calibrationFilter: "full",
        wavRMSZ: 0.0993730943062474,
        normFactor: 22.41220311199188,
        RMSZ: 0.004433883354067802,
        realWorldRMSZ: 0.049861760985498674,
        realWorldRMSC: 0.04799740833918311,
        scaleFactor: 0.13519823071552697,
        tablet: "TabE"
      }
    };
    devices.model = "SamsungTabA";
    expect(media.calculateVolume(wavfile)).toBeCloseTo(0.145, 3);
  });
  it("Returned volume should be 0.068 (as-recorded, TabE calibration, TabE playback)", function() {
    let wavfile = {
      path: "file:///android_asset/www/res/protocol/WAHTS-test/S1_Talker3_PALE_99_Noise_70_Mono.wav",
      playbackMethod: "as-recorded",
      targetSPL: 70,
      cal: {
        refType: "as-recorded",
        RMSC: 0.0042680985522230595,
        realWorldRMSA: 0.04919963839889143,
        RMSA: 0.00437500508228025,
        wavRMSC: 0.09565749165442168,
        wavRMSA: 0.0980535025200617,
        calibrationFilter: "full",
        wavRMSZ: 0.0993730943062474,
        normFactor: 22.41220311199188,
        RMSZ: 0.004433883354067802,
        realWorldRMSZ: 0.049861760985498674,
        realWorldRMSC: 0.04799740833918311,
        scaleFactor: 0.13519823071552697,
        tablet: "TabE"
      }
    };
    devices.model = "SamsungTabE";
    expect(media.calculateVolume(wavfile)).toBeCloseTo(0.068, 3);
  });
  it("Returned volume should be 0.182 (as-recorded, TabE calibration, Nexus 7 playback)", function() {
    let wavfile = {
      path: "file:///android_asset/www/res/protocol/WAHTS-test/S1_Talker3_PALE_99_Noise_70_Mono.wav",
      playbackMethod: "as-recorded",
      targetSPL: 70,
      cal: {
        refType: "as-recorded",
        RMSC: 0.0042680985522230595,
        realWorldRMSA: 0.04919963839889143,
        RMSA: 0.00437500508228025,
        wavRMSC: 0.09565749165442168,
        wavRMSA: 0.0980535025200617,
        calibrationFilter: "full",
        wavRMSZ: 0.0993730943062474,
        normFactor: 22.41220311199188,
        RMSZ: 0.004433883354067802,
        realWorldRMSZ: 0.049861760985498674,
        realWorldRMSC: 0.04799740833918311,
        scaleFactor: 0.13519823071552697,
        tablet: "TabE"
      }
    };
    devices.model = "Nexus 7";
    expect(media.calculateVolume(wavfile)).toBeCloseTo(0.182, 3);
  });
  it("Returned volume should be 0.114 (as-recorded, TabE calibration, Tab A playback)", function() {
    let wavfile = {
      path: "file:///android_asset/www/res/protocol/WAHTS-test/S1_Talker3_PALE_99_Noise_70_Mono.wav",
      playbackMethod: "as-recorded",
      targetSPL: 70,
      cal: {
        refType: "as-recorded",
        RMSC: 0.0042680985522230595,
        realWorldRMSA: 0.04919963839889143,
        RMSA: 0.00437500508228025,
        wavRMSC: 0.09565749165442168,
        wavRMSA: 0.0980535025200617,
        calibrationFilter: "full",
        wavRMSZ: 0.0993730943062474,
        normFactor: 22.41220311199188,
        RMSZ: 0.004433883354067802,
        realWorldRMSZ: 0.049861760985498674,
        realWorldRMSC: 0.04799740833918311,
        scaleFactor: 0.13519823071552697,
        tablet: "TabE"
      }
    };
    devices.model = "SamsungTabA";
    expect(media.calculateVolume(wavfile)).toBeCloseTo(0.114, 3);
  });
  it("Returned volume should be 0.032 (arbitrary, Nexus 7 calibration, TabE playback)", function() {
    let wavfile = {
      path: "file:///android_asset/www/res/protocol/WAHTS-test/S1_Talker3_PALE_99_Noise_70_Mono.wav",
      playbackMethod: "arbitrary",
      targetSPL: 70,
      cal: {
        refType: "as-recorded",
        RMSC: 0.0042680985522230595,
        realWorldRMSA: 0.04919963839889143,
        RMSA: 0.00437500508228025,
        wavRMSC: 0.09565749165442168,
        wavRMSA: 0.0980535025200617,
        calibrationFilter: "full",
        wavRMSZ: 0.0993730943062474,
        normFactor: 22.41220311199188,
        RMSZ: 0.004433883354067802,
        realWorldRMSZ: 0.049861760985498674,
        realWorldRMSC: 0.04799740833918311,
        scaleFactor: 0.13519823071552697,
        tablet: "Nexus 7"
      }
    };
    devices.model = "SamsungTabE";
    expect(media.calculateVolume(wavfile)).toBeCloseTo(0.032, 3);
  });
  it("Returned volume should be 0.086 (arbitrary, Nexus 7 calibration, Nexus 7 playback)", function() {
    let wavfile = {
      path: "file:///android_asset/www/res/protocol/WAHTS-test/S1_Talker3_PALE_99_Noise_70_Mono.wav",
      playbackMethod: "arbitrary",
      targetSPL: 70,
      cal: {
        refType: "as-recorded",
        RMSC: 0.0042680985522230595,
        realWorldRMSA: 0.04919963839889143,
        RMSA: 0.00437500508228025,
        wavRMSC: 0.09565749165442168,
        wavRMSA: 0.0980535025200617,
        calibrationFilter: "full",
        wavRMSZ: 0.0993730943062474,
        normFactor: 22.41220311199188,
        RMSZ: 0.004433883354067802,
        realWorldRMSZ: 0.049861760985498674,
        realWorldRMSC: 0.04799740833918311,
        scaleFactor: 0.13519823071552697,
        tablet: "Nexus 7"
      }
    };
    devices.model = "Nexus 7";
    expect(media.calculateVolume(wavfile)).toBeCloseTo(0.086, 3);
  });
  it("Returned volume should be 0.054 (arbitrary, Nexus 7 calibration, Tab A playback)", function() {
    let wavfile = {
      path: "file:///android_asset/www/res/protocol/WAHTS-test/S1_Talker3_PALE_99_Noise_70_Mono.wav",
      playbackMethod: "arbitrary",
      targetSPL: 70,
      cal: {
        refType: "as-recorded",
        RMSC: 0.0042680985522230595,
        realWorldRMSA: 0.04919963839889143,
        RMSA: 0.00437500508228025,
        wavRMSC: 0.09565749165442168,
        wavRMSA: 0.0980535025200617,
        calibrationFilter: "full",
        wavRMSZ: 0.0993730943062474,
        normFactor: 22.41220311199188,
        RMSZ: 0.004433883354067802,
        realWorldRMSZ: 0.049861760985498674,
        realWorldRMSC: 0.04799740833918311,
        scaleFactor: 0.13519823071552697,
        tablet: "Nexus 7"
      }
    };
    devices.model = "SamsungTabA";
    expect(media.calculateVolume(wavfile)).toBeCloseTo(0.054, 3);
  });
  it("Returned volume should be 0.025 (as-recorded, Nexus 7 calibration, TabE playback)", function() {
    let wavfile = {
      path: "file:///android_asset/www/res/protocol/WAHTS-test/S1_Talker3_PALE_99_Noise_70_Mono.wav",
      playbackMethod: "as-recorded",
      targetSPL: 70,
      cal: {
        refType: "as-recorded",
        RMSC: 0.0042680985522230595,
        realWorldRMSA: 0.04919963839889143,
        RMSA: 0.00437500508228025,
        wavRMSC: 0.09565749165442168,
        wavRMSA: 0.0980535025200617,
        calibrationFilter: "full",
        wavRMSZ: 0.0993730943062474,
        normFactor: 22.41220311199188,
        RMSZ: 0.004433883354067802,
        realWorldRMSZ: 0.049861760985498674,
        realWorldRMSC: 0.04799740833918311,
        scaleFactor: 0.13519823071552697,
        tablet: "Nexus 7"
      }
    };
    devices.model = "SamsungTabE";
    expect(media.calculateVolume(wavfile)).toBeCloseTo(0.025, 3);
  });
  it("Returned volume should be 0.068 (as-recorded, Nexus 7 calibration, Nexus 7 playback)", function() {
    let wavfile = {
      path: "file:///android_asset/www/res/protocol/WAHTS-test/S1_Talker3_PALE_99_Noise_70_Mono.wav",
      playbackMethod: "as-recorded",
      targetSPL: 70,
      cal: {
        refType: "as-recorded",
        RMSC: 0.0042680985522230595,
        realWorldRMSA: 0.04919963839889143,
        RMSA: 0.00437500508228025,
        wavRMSC: 0.09565749165442168,
        wavRMSA: 0.0980535025200617,
        calibrationFilter: "full",
        wavRMSZ: 0.0993730943062474,
        normFactor: 22.41220311199188,
        RMSZ: 0.004433883354067802,
        realWorldRMSZ: 0.049861760985498674,
        realWorldRMSC: 0.04799740833918311,
        scaleFactor: 0.13519823071552697,
        tablet: "Nexus 7"
      }
    };
    devices.model = "Nexus 7";
    expect(media.calculateVolume(wavfile)).toBeCloseTo(0.068, 3);
  });
  it("Returned volume should be 0.043 (as-recorded, Nexus 7 calibration, Tab A playback)", function() {
    let wavfile = {
      path: "file:///android_asset/www/res/protocol/WAHTS-test/S1_Talker3_PALE_99_Noise_70_Mono.wav",
      playbackMethod: "as-recorded",
      targetSPL: 70,
      cal: {
        refType: "as-recorded",
        RMSC: 0.0042680985522230595,
        realWorldRMSA: 0.04919963839889143,
        RMSA: 0.00437500508228025,
        wavRMSC: 0.09565749165442168,
        wavRMSA: 0.0980535025200617,
        calibrationFilter: "full",
        wavRMSZ: 0.0993730943062474,
        normFactor: 22.41220311199188,
        RMSZ: 0.004433883354067802,
        realWorldRMSZ: 0.049861760985498674,
        realWorldRMSC: 0.04799740833918311,
        scaleFactor: 0.13519823071552697,
        tablet: "Nexus 7"
      }
    };
    devices.model = "SamsungTabA";
    expect(media.calculateVolume(wavfile)).toBeCloseTo(0.043, 3);
  });
});
