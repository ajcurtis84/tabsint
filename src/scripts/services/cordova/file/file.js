/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/*global LocalFileSystem, FileTransfer */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
//import * as $ from "jquery";
//import { isDimensionToken } from "html2canvas/dist/types/css/syntax/parser";

angular
  .module("tabsint.services.cordova.file", [])

  .factory("file", function(
    $window,
    app,
    cordova,
    $q,
    notifications,
    logger,
    networkModel,
    disk,
    devices,
    paths,
    $cordovaFile,
    gettextCatalog,
    tabsintNative,
    tabsintFS
  ) {
    var file = {
      ready: undefined,
      loadFileSystem: undefined,
      createFile: undefined, //we should deprecate
      download: undefined,
      copyAsset: undefined,
      readFile: undefined,
      getFile: undefined, //we should deprecate
      getDirectory: undefined, //we should deprecate
      removeDirRecursively: undefined,
      removeFile: undefined,
      removeFilesStar: undefined,
      list: undefined,
      listRecurisvely: undefined,
      getMetadata: undefined,
      abort: undefined,
      localFS: undefined,
      dataFS: undefined,
      downloadInProgress: false,
      getParentDirectory: undefined,
      getEntriesAtRoot: undefined,
      getEntries: undefined,
      createDirRecursively: undefined,
      selectDirectory: undefined,
      copyDirectory: undefined,
      getNameFromURI: undefined,
      listExternalFiles: undefined,
      deleteCopiedInternalDir: undefined,
      listFiles: undefined,
      initExtStorage: undefined,
      checkUriPermission: undefined
    };

    var onError = function(error) {
      logger.error("in file factory.  code: " + error.code + "\n" + "message: " + error.message + "\n");
      return $q.reject();
    };

    var deferredReady = $q.defer();

    /**
     * File system ready promise.
     * This gets deferred at the end of file.loadFileSystem
     */
    file.ready = function() {
      return deferredReady.promise;
    };

    /**
     * File system loading method.
     * This will run in the app.run block to make sure the file system is loaded before the app starts
     */
    file.loadFileSystem = function() {
      var deferred = $q.defer();

      // Load Local filesystem
      if (app.tablet) {
        cordova
          .ready()
          .then(function() {
            // Get local filesystem
            var deferredFileSystem = $q.defer();

            var PERSISTENT;
            if (typeof LocalFileSystem === "undefined") {
              PERSISTENT = $window.PERSISTENT;
            } else {
              PERSISTENT = LocalFileSystem.PERSISTENT; // android compatibility stuff.
            }

            $window.requestFileSystem(
              PERSISTENT,
              0,
              function(fs) {
                //TODO, try just window, timing?, synchronous?  fix alert. quota
                file.fs = fs; // where protocols, etc. go
                file.directoryReader = fs.root.createReader();
                deferredFileSystem.resolve();
              },
              onError
            );

            return deferredFileSystem.promise;
          })
          .then(function() {
            var deferredFileSystemURL = $q.defer();
            if (app.tablet && devices.platform.toLowerCase() === "android") {
              // place files on the sdcard so they can be accessed in rooted and non-rooted tablets by
              // pluggin into a computer and browsing
              $window.resolveLocalFileSystemURL(window.cordova.file.externalRootDirectory, function(fs) {
                file.localFS = fs; // where saveToFile files go, for easier access on android
                deferredFileSystemURL.resolve();
              });
            } else if (app.tablet && devices.platform.toLowerCase() === "ios") {
              // grab the dataDirectory for ios (ios doesn't have any external directories via cordova)
              $window.resolveLocalFileSystemURL(window.cordova.file.documentsDirectory, function(fs) {
                file.localFS = fs; // where saveToFile files go, for easier access on android
                deferredFileSystemURL.resolve();
              });
            } else {
              logger.debug(" Browser has no local file system");
              deferredFileSystemURL.resolve();
            }

            return deferredFileSystemURL.promise;
          })
          .then(function() {
            if (angular.isUndefined(file.fs)) {
              var msg = gettextCatalog.getString(
                "The file system did not loaded properly.  Re-open TabSINT manually after it closes."
              );
              logger.error(msg);
              notifications.alert(gettextCatalog.getString("ERROR: ") + msg);
              navigator.app.exitApp();
            } else {
              logger.debug("Checked file system.  fs.root = " + file.fs.root.fullPath);
            }
            logger.debug("File system ready");
            deferredReady.resolve();
          });
      } else {
        logger.log("Browser filesystem ready");
        deferredReady.resolve();
      }
    };

    /**
     *
     */
    file.createFile = function(path, fileName, data) {
      var deferred = $q.defer();
      path = path || "TabSINT_Externals";
      fileName = fileName || "TabSINT_test_file";
      data = data || "test text to write to test file";
      // put the file in the downloads folder
      //path = 'download/' + path;

      function gotFileEntry(fE) {
        fE.createWriter(gotFileWriter, onError);
      }

      function gotFileWriter(writer) {
        writer.onwriteend = function() {
          logger.info("Created file " + fileName + " at path " + path);
          deferred.resolve();
        };
        writer.write(data);
      }

      function gotDirectory() {
        var newPath = path + "/" + fileName;
        file.localFS.getFile(newPath, { create: true, exclusive: false }, gotFileEntry, onError);
      }

      // start by creating the directory if it doesn't exist yet
      if (app.tablet) {
        $window.resolveLocalFileSystemURL(window.cordova.file.externalRootDirectory, function(fs) {
          file.localFS = fs; // where saveToFile files go, for easier access on android
          deferred.resolve();
        });
        file.localFS.getDirectory(path, { create: true, exclusive: false }, gotDirectory, onError);
      } else {
        deferred.reject("File service unavailable in the browser");
      }

      return deferred.promise;
    };

    /**
     * File downloading method.
     * Downloads file relative to file.fs.root.nativeURL
     */
    file.download = function(url, target, options, dir) {
      // indicate the process
      file.downloadInProgress = true;

      var q = $q.defer();
      var promise = q.promise;
      var fileTransfer = new FileTransfer();
      var trustAllHosts = false;
      options = options || {};

      // define abort function
      file.abort = function() {
        return fileTransfer.abort();
      };

      // define progress event
      fileTransfer.onprogress = function(progressEvent) {
        if (progressEvent.lengthComputable) {
          var pct = (progressEvent.loaded / progressEvent.total) * 100;
          q.notify("Download in progress: " + pct.toFixed(2) + "% done.");
        } else {
          q.notify("Download in progress. Percent done is unknown.");
        }

        if (!networkModel.status) {
          logger.error("networkModel.status turned to false while downloading file");
        }
      };

      // handle directory and filename
      var directory = file.fs.root.nativeURL; // defaults to app files directory
      if (dir === "sdcard") {
        if (app.tablet && devices.platform.toLowerCase() === "android") {
          directory = window.cordova.file.externalRootDirectory;
        } else {
          logger.warn("Attempting to download to sdcard on non-android device");
        }
      } else if (dir) {
        // path is a relative filename
        directory = directory + paths.dir(dir);
      }

      // download the file
      if (networkModel.status) {
        fileTransfer.download(
          url,
          directory + target,
          function(theFile) {
            logger.debug("Download complete");
            q.resolve(theFile);
          },
          function(error) {
            if (error.code !== 4) {
              logger.error("Could not download file:" + angular.toJson(error));
            }
            error.msg = "Could not download file.";
            q.reject(error);
          },
          trustAllHosts,
          options
        );
      } else {
        logger.error("networkModel.status is false while trying to download file");
        q.reject("offline");
      }

      // clean up after process is over (on success and error)
      promise = promise.finally(function() {
        file.downloadInProgress = false;
        file.abort = undefined;
      });

      return promise;
    };

    /**
     * Cheat to gain access to files packed within the zipped apk/ipa.
     * files in the android_asset folder are not accessible to the java layer via normal File methods
     */
    file.copyAsset = function(src, target, dir) {
      var deferred = $q.defer();

      if (app.tablet) {
        var fileTransfer = new FileTransfer();

        var directory = file.fs.root.nativeURL; // defaults to app files directory
        if (dir) {
          // path is a relative filename
          directory = directory + paths.dir(dir);
        }
        fileTransfer.download(src, directory + target, success, fail);
      } else {
        deferred.reject();
      }

      function success(theFile) {
        deferred.resolve(theFile);
      }
      function fail(reason) {
        deferred.reject(reason);
      }

      return deferred.promise;
    };

    /**
     * Function to read file from file system or app storage
     * returns content of file
     */
    file.readFile = function(path) {
      var deferred = $q.defer();
      var res;
      $window.resolveLocalFileSystemURL(
        path,
        function readFile(fileEntry) {
          fileEntry.file(function(file) {
            var reader = new FileReader();
            reader.onloadend = function() {
              logger.debug("Successfully read " + path);
              try {
                res = JSON.parse(reader.result);
              } catch (e) {
                res = reader.result;
              }
              deferred.resolve(res);
            };
            reader.readAsText(file); // can we delete this
          });
        },
        function fileNotFound() {
          console.log(path, "fileNotFound");
          var split_path = path.split("/");
          if (split_path[split_path.length - 1] === "calibration.json") {
            deferred.resolve();
          }
        }
      );
      return deferred.promise;
    };

    /**
     *
     */
    file.getFile = function(path) {
      var deferred = $q.defer();
      file.fs.root.getFile(
        path,
        { create: false },
        function(fileEntry) {
          deferred.resolve(fileEntry);
        },
        function(fileError) {
          logger.debug("No file found at path: " + path);
          deferred.reject(fileError);
        }
      );

      return deferred.promise;
    };

    /**
     *
     */
    file.getDirectory = function(path, create) {
      var deferred = $q.defer();
      file.fs.root.getDirectory(
        path,
        { create: create },
        function(directoryEntry) {
          deferred.resolve(directoryEntry);
        },
        function(fileError) {
          if (fileError && fileError.code === 1) {
            // directory does not exist
            logger.debug("Directory at path " + path + " does not exist");
            deferred.reject();
          } else {
            logger.error("Failed to get directory at path " + path + " with error: " + angular.toJson(fileError));
            deferred.reject();
          }
        }
      );

      return deferred.promise;
    };

    /**
     *
     */
    file.removeDirRecursively = function(path) {
      var deferred = $q.defer();
      file.getDirectory(path).then(
        function(d) {
          d.removeRecursively(
            function() {
              logger.debug("Removed directory at path: " + path);
              deferred.resolve();
            },
            function(e) {
              logger.debug("Unable to remove directory: " + path + " with error: " + angular.toJson(e));
              deferred.reject();
            }
          );
        },
        function() {
          deferred.reject();
        }
      );

      return deferred.promise;
    };

    /**
     * Remove file.
     * @param {string} path - path to remove relative to file.fs.root
     * @returns {promise} Promise to remove file
     */
    file.removeFile = function(path) {
      var deferred = $q.defer();
      file.getFile(paths.file(path)).then(
        function(f) {
          f.remove(
            function() {
              logger.debug("Removed file: " + path);
              deferred.resolve();
            },
            function() {
              logger.debug("Unable to remove file: " + path);
              deferred.reject();
            }
          );
        },
        function(e) {
          deferred.reject(e);
        }
      );
      return deferred.promise;
    };

    /**
     *
     */
    file.removeFilesStar = function(directory, partialFileName) {
      //logger.debug('entered file.removeFilesStar() with path = '+directory+'and partial fileName = ' + partialFileName);
      directory = directory || file.fs.root.createReader();
      var promise;
      directory.readEntries(function(entries) {
        for (var i = 0; i < entries.length; i++) {
          if (entries[i].name.indexOf(partialFileName) > -1 && entries[i].name !== "protocol") {
            if (!promise) {
              logger.debug("In file.removeFilesStar(), removing " + entries[i].name);
              promise = file.removeFile(entries[i].name);
            } else {
              logger.debug("In file.removeFilesStar(), removing " + entries[i].name);
              promise = promise.then(file.removeFile(entries[i].name));
            }
          }
        }
      }, onError);
      return promise;
    };

    file.listFiles = function(dirPath) {
      var deferred = $q.defer();
      $window.resolveLocalFileSystemURL(
        dirPath,
        function(fileSystem) {
          var reader = fileSystem.createReader();
          reader.readEntries(
            function(entries) {
              console.log("entries from: " + dirPath, entries);
              deferred.resolve(entries);
            },
            function(err) {
              console.log(err);
              deferred.reject(err);
            }
          );
        },
        function(err) {
          console.log(err);
          //if dir doesnt exist, return empty list
          if (err.code == 1) {
            deferred.resolve([]);
          } else {
            deferred.reject(err);
          }
        }
      );
      return deferred.promise;
    };

    /**
     * List the file contents of directory
     * @param {string} [root=file.fs.root.nativeURL] - root file path for directory
     * @param {string} [dir=''] - directory name to list contents of.
     * @returns {promise} promise to list directory contents that resolves with array of file names
     */
    file.list = function(root, dir) {
      if (!root) {
        root = file.fs.root.nativeURL;
      } // default to persistant
      if (!dir) {
        dir = "";
      } // list the contents of file.fs.root if no directory specified

      return $cordovaFile.checkDir(root, dir).then(function(dirEntry) {
        var q = $q.defer();
        var reader = dirEntry.createReader();
        reader.readEntries(
          function(entries) {
            console.log(
              'Files in directory  "' +
                root +
                dir +
                '": ' +
                angular.toJson(
                  _.map(entries, function(entry) {
                    return entry.name;
                  })
                )
            );
            q.resolve(entries);
          },
          function(e) {
            console.log("Failed to list file contents with error: " + angular.toJson(e));
            q.reject(e);
          }
        );
      });
    };

    /**
     * List all file objects of a directory, including subdirectories.
     * @param {string} the parent folder
     * @returns an array of strings - full path to each file
     */
    file.listRecursively = function(parentDir) {
      var list = [];

      function addEntry(entry) {
        var promises = [];
        return file
          .getEntries(entry)
          .then(function(entries) {
            entries.forEach(function(e) {
              if (e.isFile) {
                list.push({ path: e.fullPath, nativeURL: e.nativeURL });
              } else if (e.isDirectory) {
                promises.push(addEntry(e.nativeURL));
              }
            });
          })
          .then(function() {
            return $q.all(promises);
          });
      }

      return addEntry(parentDir).then(function() {
        //console.log('list: ' + JSON.stringify(list));
        return list;
      });
    };

    /**
     *
     */
    file.getMetadata = function(path) {
      return file.getFile(path).then(function(fileEntry) {
        var deferred = $q.defer();
        fileEntry.getMetadata(
          function(data) {
            deferred.resolve(data);
          },
          function() {
            return $q.reject("ERROR: Unable to retrieve metadata for file: " + path);
          }
        );
        return deferred.promise;
      });
    };

    // fileChooser functions
    //
    /**
     * File chooser function
     */
    file.getParentDirectory = function(path) {
      var deferred = $q.defer();
      $window.resolveLocalFileSystemURL(
        path,
        function(fileSystem) {
          fileSystem.getParent(
            function(result) {
              console.log("-- file.getParentDirectory() found: " + angular.toJson(result));
              deferred.resolve(result);
            },
            function(error) {
              deferred.reject(error);
            }
          );
        },
        function(error) {
          deferred.reject(error);
        }
      );
      return deferred.promise;
    };

    file.selectDirectory = function(uriString) {
      return tabsintFS.getExtStorageUri(uriString);
    };

    file.getNameFromURI = function(uriString) {
      return tabsintFS.getNameFromURI(uriString);
    };

    file.listExternalFiles = function(uriString) {
      return tabsintFS.listFilesTree(uriString);
    };

    file.copyDirectory = function(uriString, dirName) {
      return tabsintFS.copyExtStorageFiles(uriString, dirName);
    };

    file.deleteCopiedInternalDir = function(uriString, dirName) {
      return tabsintFS.deleteCopiedInternalDir(uriString, dirName);
    };

    file.checkUriPermission = function(uriString) {
      return tabsintFS.checkUriPermission(uriString);
    };

    /**
     * File chooser function
     * This will NOT find files in public directories unless they are media (tested .png)
     * I think with android 30 this needs to be done with intents
     *  --> at least ACTION_GET_CONTENT but ideally ACTION_OPEN_DOCUMENT
     */
    file.getEntries = function(path) {
      var deferred = $q.defer();
      $window.resolveLocalFileSystemURL(
        path,
        function(fileSystem) {
          var directoryReader = fileSystem.createReader();
          directoryReader.readEntries(
            function(entries) {
              deferred.resolve(entries);
            },
            function(error) {
              deferred.reject(error);
            }
          );
        },
        function(error) {
          deferred.reject(error);
        }
      );
      return deferred.promise;
    };

    /**
     * File Chooser function - create directory in localFS
     *
     * @param {type} path
     * @param {type} name
     * @returns {unresolved} promise
     */
    file.createDirectory = function(path, name) {
      var deferred = $q.defer();
      $window.resolveLocalFileSystemURL(
        path,
        function(fileSystem) {
          fileSystem.getDirectory(
            name,
            { create: true },
            function() {
              deferred.resolve();
            },
            function(error) {
              deferred.reject(error);
            }
          );
        },
        function(error) {
          deferred.reject(error);
        }
      );
      return deferred.promise;
    };

    /**
     * Create a directory recursively.
     * If the directory already exists, it will return true.
     * @returns {promise} - promise that resolves with the full of the created directory
     */
    file.createDirRecursively = function(root, path) {
      path = paths.dir(path);
      path = path.slice(0, -1); // need to remove the final /
      var dirs = path.split("/"); // split the path into directories

      return $cordovaFile
        .checkDir(root, dirs[0])
        .then(
          function(success) {
            return success.nativeURL;
          },
          function() {
            return $cordovaFile.createDir(root, dirs[0], false).then(function(success) {
              return success.nativeURL;
            });
          }
        )
        .then(function(newRoot) {
          if (dirs.length > 1) {
            return file.createDirRecursively(newRoot, dirs.slice(1).join("/")); // remove first directory, then join the rest and run again
          } else {
            logger.info("Successfully found/created new directory tree: " + newRoot);
            var deferred = $q.defer();
            deferred.resolve();
            return deferred.promise;
          }
        });
    };

    file.initExtStorage = function() {
      if (disk.storageRootDir == undefined) {
        var rootUri = null;
        var tabsintUri = null;

        let notification = new Promise(function(resolve, reject) {
          notifications.alert(
            `TabSINT 4.4.0 requires permission to access this device's shared storage. Please navigate to and select the Documents folder when prompted.`,
            resolve
          );
        });

        notification
          .then(() => {
            return tabsintFS.getExtStorageUri(null);
          })
          .then(uri => {
            rootUri = uri;
            return tabsintFS.exists(uri, "tabsint");
          })
          .then(tabsintUri => {
            if (tabsintUri) {
              return tabsintUri;
            } else {
              return tabsintFS.createDirectory(rootUri, "tabsint");
            }
          })
          .then(uri => {
            disk.storageRootDir = tabsintUri = uri;
            return tabsintFS.exists(uri, ".uuid.txt");
          })
          .then(uuidUri => {
            if (uuidUri) {
              // open and read it
              return tabsintFS.readUuidFile(uuidUri);
            } else {
              // create and write it
              return tabsintFS.createFile(tabsintUri, ".uuid", "text/plain").then(uri => {
                return tabsintFS.writeUuidFile(uri);
              });
            }
          })
          .then(uuid => {
            devices.updateTabsintUUID(uuid);
          })
          .catch(err => {
            console.log("Something went wrong setting Intall UUID: " + err);
          });
      } else {
        // we've previously stored a root uri
        var rootUri = null;
        var tabsintUri = null;
        tabsintFS
          .checkUriPermission(disk.storageRootDir)
          .then(hasPermission => {
            console.log("hasPermission - " + hasPermission ? "true" : "false");
            if (!hasPermission) {
              // need to reacquire permission, what if it's different?
              let notification = new Promise(function(resolve, reject) {
                notifications.alert(
                  `The file system may have changed. Please navigate to and select the Documents folder when prompted.`,
                  resolve
                );
              });

              return notification
                .then(() => {
                  return tabsintFS.getExtStorageUri(null);
                })
                .then(uri => {
                  rootUri = uri;
                  return tabsintFS.exists(uri, "tabsint");
                })
                .then(tabsintUri => {
                  if (tabsintUri) {
                    return tabsintUri;
                  } else {
                    return tabsintFS.createDirectory(rootUri, "tabsint");
                  }
                })
                .then(tabsintUri => {
                  if (tabsintUri !== disk.storageRootDir) {
                    disk.storageRootDir = tabsintUri;

                    notifications.alert(
                      `TabSINT storage locations have changed. Please remember to relocate any desired files.`
                    );
                  }
                });
            }
          })
          .then(() => {
            return tabsintFS.uriExists(disk.storageRootDir);
          })
          .then(uriExists => {
            if (!uriExists) {
              // have permission, need to create tabsint dir

              console.log("Uri doesn't exist" + uriExists);
              return tabsintFS.createDirectoryFromUri(disk.storageRootDir).then(tabsintUri => {
                if (tabsintUri !== disk.storageRootDir) {
                  disk.storageRootDir = tabsintUri;

                  notifications.alert(
                    `TabSINT storage locations have changed. Please remember to relocate any desired files.`
                  );
                }
              });
            }
          })
          .then(function() {
            return tabsintFS.exists(disk.storageRootDir, ".uuid.txt");
          })
          .then(uuidUri => {
            if (uuidUri) {
              // open and read it
              return tabsintFS.readUuidFile(uuidUri);
            } else {
              // create and write it
              return tabsintFS.createFile(disk.storageRootDir, ".uuid", "text/plain").then(uri => {
                return tabsintFS.writeUuidFile(uri);
              });
            }
          })
          .then(uuid => {
            devices.updateTabsintUUID(uuid);
          })
          .catch(function(err) {
            console.log("err" + err); // maybe don't have permissions or it does not exist? user deleted it?
          });
      }
    };

    return file;
  });
