/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.csv", [])

  .factory("csv", function($q, cha, disk) {
    var api = {};

    api.initialize = function() {
      api.audiometryResult = {};
      api.bekesyMLDResult = {};
      api.mpanlResult = {};
      api.threeDigitResult = {};
      api.wahtsCalibrationCheckResult = {};
      api.multipleChoiceResult = {};
      api.multipleInputResult = {};
      api.likertResult = {};
      api.textboxResult = {};
      api.audiometryTypesToCSV = [
        "chaManualAudiometry",
        "chaManualAudiometryExam",
        "chaHughsonWestlake",
        "chaBHAFT",
        "chaBekesyLike",
        "chaBekesyMLD",
        "chaDPOAE"
      ];
      api.csv = "";
    };

    api.generateFlatCSV = function(dataIn) {
      var deferred = $q.defer();
      var ear, modifiedPresId, targetEar, maskerEar;
      var result = {
        tabletUUID: dataIn.testResults.tabletUUID,
        protocolName: dataIn.protocolName,
        testDateTime: dataIn.testDateTime,
        tabsintVersion: dataIn.testResults.softwareVersion.version,
        calibrationDate: dataIn.testResults.chaCalibrationDate || undefined,
        firmwareTag: angular.isDefined(cha.myCha) ? cha.myCha.id.buildDateTime || undefined : undefined,
        chaSerialNumber: angular.isDefined(cha.myCha) ? cha.myCha.id.serialNumber || undefined : undefined
      };

      api.initialize();
      var csv = api.csv;

      // Check which ear the audiometry result is for and write a string for csv export
      function whichEar(OutputChannel) {
        if (OutputChannel === "HPL0") {
          return "Left";
        } else if (OutputChannel === "HPR0") {
          return "Right";
        } else if (OutputChannel === "LINEL0" || OutputChannel === "LINEL0 NONE") {
          return "Bone (Left)";
        } else if (OutputChannel === "NONE LINEL0") {
          return "Bone (Right)";
        }
      }

      // Check if the presentationId is unique. If not, append an index and return the modified unique presentationId.
      function checkUniquePresId(currentPresId, currentRes) {
        if (!(currentPresId in currentRes)) {
          return currentPresId;
        } else {
          var count = 0;
          modifiedPresId = currentPresId + "_" + count.toString();
          while (modifiedPresId in currentRes) {
            count += 1;
            modifiedPresId = currentPresId + "_" + count.toString();
          }
          return modifiedPresId;
        }
      }

      let mapEar = {
        0: "Left",
        1: "Right",
        2: "Both"
      };
      // Iterate through responses.
      // For results that should be exported to CSV, put in a format that is convenient for CSV export.
      dataIn.testResults.responses.forEach(function(resp, i) {
        if (api.audiometryTypesToCSV.includes(resp.responseArea) && resp.page.responseArea.exportToCSV == true) {
          let freq = angular.isDefined(resp.ThresholdFrequency)
            ? resp.ThresholdFrequency
            : angular.isDefined(resp.examProperties)
            ? resp.examProperties.F
            : undefined;
          let thresh = angular.isDefined(resp.ThresholdLevel) ? resp.ThresholdLevel : resp.response;
          let units = angular.isDefined(resp.Units)
            ? resp.Units
            : angular.isDefined(resp.examProperties)
            ? angular.isDefined(resp.examProperties.LevelUnits)
              ? resp.examProperties.LevelUnits
              : resp.response
            : resp.response;
          if (resp.responseArea === "chaBekesyMLD") {
            [targetEar, maskerEar] = angular.isDefined(resp.examProperties)
              ? [
                  angular.isDefined(resp.examProperties.TargetEar) ? mapEar[resp.examProperties.TargetEar] : "Both",
                  angular.isDefined(resp.examProperties.MaskerEar) ? mapEar[resp.examProperties.MaskerEar] : "Both"
                ]
              : ["Both", "Both"];
            api.bekesyMLDResult[checkUniquePresId(resp.presentationId, api.bekesyMLDResult)] = [
              targetEar,
              maskerEar,
              freq,
              thresh,
              units,
              resp.ResultType
            ];
          } else {
            ear = angular.isDefined(resp.examProperties) ? whichEar(resp.examProperties.OutputChannel) : undefined;
            api.audiometryResult[checkUniquePresId(resp.presentationId, api.audiometryResult)] = [
              ear,
              freq,
              thresh,
              units,
              resp.ResultType
            ];
          }
        } else if (resp.responseArea === "mpanlResponseArea" && resp.page.responseArea.exportToCSV == true) {
          if (resp.response === "skipped") {
            api.mpanlResult[checkUniquePresId(resp.presentationId, api.mpanlResult)] = resp.response;
          } else {
            resp.mpanlsData.forEach(function(mpanlRes, mpanlIdx) {
              api.mpanlResult[checkUniquePresId(resp.presentationId, api.mpanlResult)] = [
                mpanlRes.freq,
                mpanlRes.level,
                mpanlRes.limit,
                mpanlRes.att,
                mpanlRes.levelUnderWAHTS,
                mpanlRes.noiseFloor
              ];
            });
          }
        } else if (resp.responseArea === "chaThreeDigit" && resp.page.responseArea.exportToCSV == true) {
          ear = angular.isDefined(resp.page.responseArea.examProperties.ear)
            ? resp.page.responseArea.examProperties.ear
            : "both";
          let response = typeof resp.response === "string" ? resp.response : resp.response.join("");
          let currentDigits = angular.isDefined(resp.currentDigits) ? resp.currentDigits.join("") : undefined;
          api.threeDigitResult[checkUniquePresId(resp.presentationId, api.threeDigitResult)] = [
            ear,
            resp.correct,
            response,
            currentDigits,
            resp.currentMasker,
            resp.targetType,
            resp.digitScore,
            resp.presentationScore,
            resp.currentSNR,
            resp.maskerLevel,
            resp.targetLevel
          ];
        } else if (resp.responseArea === "multipleChoiceResponseArea" && resp.page.responseArea.exportToCSV == true) {
          api.multipleChoiceResult[checkUniquePresId(resp.presentationId, result)] = resp.response;
        } else if (resp.responseArea === "textboxResponseArea" && resp.page.responseArea.exportToCSV == true) {
          api.textboxResult[checkUniquePresId(resp.presentationId, result)] = resp.response;
        } else if (resp.responseArea === "likertResponseArea" && resp.page.responseArea.exportToCSV == true) {
          api.likertResult[checkUniquePresId(resp.presentationId, result)] = resp.response;
        } else if (resp.responseArea === "multipleInputResponseArea") {
          resp.page.responseArea.inputList.forEach(function(element, idx) {
            if (element.exportToCSV) {
              api.multipleInputResult[checkUniquePresId(element.text, result)] = resp.response[idx];
            }
          });
        } else if (resp.responseArea === "chaCalibrationCheck" && resp.page.responseArea.exportToCSV == true) {
          Object.entries(resp.calibrationData).forEach(function(element, idx) {
            api.wahtsCalibrationCheckResult[checkUniquePresId(element[1].title, result)] = "";
            api.wahtsCalibrationCheckResult[checkUniquePresId(element[0] + " " + element[1].xLabel, result)] =
              element[1].frequencies;
            api.wahtsCalibrationCheckResult[checkUniquePresId(element[0] + " " + element[1].yLabel, result)] =
              element[1].measured;
          });
        }
      });

      // Generate csv string to be writen to a CSV file
      // Start with header and multiple choice/multiple input/textbox responses
      var fields = Object.keys(result);
      fields.forEach(function(item, idx) {
        csv += item + "," + JSON.stringify(result[item]);
        csv += "\r\n";
      });

      if (Object.keys(api.textboxResult).length !== 0) {
        csv += "\r\n Textbox Results \r\n";
        Object.entries(api.textboxResult).forEach(function(item, idx) {
          csv += item;
          csv += "\r\n";
        });
      }
      if (Object.keys(api.multipleChoiceResult).length !== 0) {
        csv += "\r\n Multiple Choice Results \r\n";
        Object.entries(api.multipleChoiceResult).forEach(function(item, idx) {
          csv += item;
          csv += "\r\n";
        });
      }
      if (Object.keys(api.multipleInputResult).length !== 0) {
        csv += "\r\n Multiple Input Results \r\n";
        Object.entries(api.multipleInputResult).forEach(function(item, idx) {
          csv += item;
          csv += "\r\n";
        });
      }
      if (Object.keys(api.likertResult).length !== 0) {
        csv += "\r\n Likert Results \r\n";
        Object.entries(api.likertResult).forEach(function(item, idx) {
          csv += item;
          csv += "\r\n";
        });
      }

      // Write the WAHTS Calibration Check results . Only do this if we have results to export to CSV.
      if (Object.keys(api.wahtsCalibrationCheckResult).length !== 0) {
        csv += "\r\n WAHTS Calibration Check Test Results \r\n";
        Object.entries(api.wahtsCalibrationCheckResult).forEach(function(item, idx) {
          csv += item;
          csv += "\r\n";
        });
      }

      // Write the audiometry results with column names. Only do this if we have audiometry results to export to CSV.
      if (Object.keys(api.audiometryResult).length !== 0) {
        var audiometryFields = Object.keys(api.audiometryResult);
        csv += "\r\n AUDIOMETRY Results\r\n";
        csv += "Presentation Id, Ear, Frequency (Hz), Threshold, Threshold Units, Response Type \r\n";
        audiometryFields.forEach(function(item, idx) {
          csv += item + "," + JSON.stringify(api.audiometryResult[item]);
          csv += "\r\n";
        });
      }

      // Write the BekesyMLD audiometry results with column names. Only do this if we have audiometry results to export to CSV.
      if (Object.keys(api.bekesyMLDResult).length !== 0) {
        var bekesyMLDFields = Object.keys(api.bekesyMLDResult);
        csv += "\r\n Bekesy MLD AUDIOMETRY Results\r\n";
        csv +=
          "Presentation Id, Target Ear, Masker Ear, Frequency (Hz), Threshold, Threshold Units, Response Type \r\n";
        bekesyMLDFields.forEach(function(item, idx) {
          csv += item + "," + JSON.stringify(api.bekesyMLDResult[item]);
          csv += "\r\n";
        });
      }

      // Write the MPANLs results with column names. Only do this if we have MPANL results to export to CSV.
      if (Object.keys(api.mpanlResult).length !== 0) {
        var mpanlFields = Object.keys(api.mpanlResult);
        csv += "\r\n MPANLs Results\r\n";
        csv +=
          "Presentation Id, Frequency (Hz), Level (dB SPL), MPANL Limit, Headset Attenuation (dB SPL), Level Under the WAHTS (dB SPL), Dosimeter Noise Floor (dB SPL) \r\n";
        mpanlFields.forEach(function(item, idx) {
          csv += item + "," + JSON.stringify(api.mpanlResult[item]);
          csv += "\r\n";
        });
      }

      // Write the Three Digit Test results with column names. Only do this if we have Three Digit results to export to CSV.
      if (Object.keys(api.threeDigitResult).length !== 0) {
        var threeDigitFields = Object.keys(api.threeDigitResult);
        csv += "\r\n Three Digit Test Results \r\n";
        csv +=
          "Presentation Id, Ear, Correct, Response, Digits Presented, Masker Presented, Target Type, Digit Score, Presentation Score, SNR, Masker Level, Target Level \r\n";
        threeDigitFields.forEach(function(item, idx) {
          csv += item + "," + JSON.stringify(api.threeDigitResult[item]);
          csv += "\r\n";
        });
      }

      api.csv = csv
        .replace(/"/g, "")
        .replace(/\[/g, "")
        .replace(/\]/g, "");

      deferred.resolve(api.csv);
      return deferred.promise;
    };

    return api;
  });
