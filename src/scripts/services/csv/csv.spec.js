/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";

beforeEach(angular.mock.module("tabsint"));

describe("Csv", function() {
  var csv;

  beforeEach(
    angular.mock.inject(function(_csv_) {
      csv = _csv_;
    })
  );

  describe("csv.initialize", function() {
    beforeEach(function() {
      csv.initialize();
    });

    it("should load the supported audiometry types.", function() {
      expect(csv.audiometryTypesToCSV.length).toEqual(7);
    });
  });

  describe("csv.generateFlatCSV", function() {
    it("should export 7 header fields.", function() {
      let dataIn = {
        testResults: {
          tabletUUID: "xxxxxxxxxxx-1234",
          softwareVersion: {
            version: "testing"
          },
          chaCalibrationDate: Date.now(),
          responses: []
        },
        protocolName: "Unit Test Protocol",
        testDateTime: Date.now(),
        firmwareTag: "Dinausor",
        chaSerialNumber: "Test-01234"
      };
      csv.generateFlatCSV(dataIn);
      expect(csv.csv.match(/,/g).length).toEqual(7);
    });

    it("should throw an error if dataIn does not have header fields information", function() {
      let dataIn = {
        0: "Empty"
      };
      expect(() => {
        csv.generateFlatCSV(dataIn);
      }).toThrowError(TypeError);
    });

    it("should export Bekesy MLD Results", function() {
      let dataIn = {
        protocolName: "Unit Test Protocol",
        testDateTime: Date.now(),
        firmwareTag: "Dinausor",
        chaSerialNumber: "Test-01234",
        testResults: {
          tabletUUID: "xxxxxxxxxxx-1234",
          softwareVersion: {
            version: "testing"
          },
          chaCalibrationDate: Date.now(),
          responses: [
            {
              presentationId: "chaBekesyMLD",
              responseArea: "chaBekesyMLD",
              page: {
                responseArea: {
                  exportToCSV: true
                }
              },
              examProperties: {
                TargetEar: 0,
                MaskerEar: 1
              },
              ThresholdFrequency: 8000,
              ThresholdLevel: 40,
              Units: "dB SPL",
              ResultType: "Unit Test"
            }
          ]
        }
      };
      csv.generateFlatCSV(dataIn);
      expect(csv.bekesyMLDResult["chaBekesyMLD"]).toEqual(["Left", "Right", 8000, 40, "dB SPL", "Unit Test"]);
    });
  });
});
