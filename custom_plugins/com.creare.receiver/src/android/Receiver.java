/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at
         http://www.apache.org/licenses/LICENSE-2.0
       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/

// largely copied from cordova-plugin-intent
// https://github.com/napolitano/cordova-plugin-intent/blob/master/src/android/IntentPlugin.java
package com.creare.receiver;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;
import java.lang.System;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.provider.MediaStore;
import android.database.Cursor;
import android.content.ClipData;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.net.Uri;

import android.content.ContentResolver;
import android.content.Context;
import android.app.Activity;
import android.webkit.MimeTypeMap;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;



/**
 * This class provides access to notifications on the device.
 *
 * Be aware that this implementation gets called on
 * navigator.notification.{alert|confirm|prompt}, and that there is a separate
 * implementation in org.apache.cordova.CordovaChromeClient that gets
 * called on a simple window.{alert|confirm|prompt}.
 */
public class Receiver extends CordovaPlugin {
  public static final String ACTION_SEND_EXTERNAL_DATA = "sendExternalData";
  public static final String ACTION_GET_EXTERNAL_DATA_INTENT = "getExternalDataIntent";
  public static final String ACTION_SET_EXTERNAL_DATA_INTENT_HANDLER = "setExternalDataIntentHandler";

  private CallbackContext onNewIntentCallbackContext = null;
  private static final String TAG = "ReceiverPlugin";

  /**
   * Constructor.
   */
  public Receiver() {
    // Log.d(TAG, "Initializing Class");
  }

  /**
   * Executes the request and returns PluginResult.
   *
   * @param action            The action to execute.
   * @param args              JSONArray of arguments for the plugin.
   * @param callbackContext   The callback context used when calling back into JavaScript.
   * @return                  True when the action was valid, false otherwise.
   */
  public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

    if (action.equals(ACTION_SEND_EXTERNAL_DATA)) {
      return sendExternalData(args.getString(0), args.getString(1), callbackContext);
    } else if (action.equals(ACTION_GET_EXTERNAL_DATA_INTENT)) {
      return getExternalDataIntent(callbackContext);
    } else if (action.equals(ACTION_SET_EXTERNAL_DATA_INTENT_HANDLER)) {
      return setExternalDataIntentHandler(callbackContext);
    } else {
      callbackContext.error("Invalid action");
      return false;
    }

  }


  /**
   * Send data to another app
   * This method requires another installed app with appropriate intent filters.
   *
   * @param appName         string.  ex:  com.creare.receiver
   * @param data            string.  data = JSON.stringify(dataObject)
   * @param callbackContext The callback context used when calling back into JavaScript.
   * @return                Boolean regarding success
   */
  public boolean sendExternalData(final String appName, final String data, final CallbackContext callbackContext){
    if (data != null && (data instanceof String)) {
      if (appName != null && (appName instanceof String)) {
        Context activityContext = this.cordova.getActivity().getApplicationContext();
        Activity activity = this.cordova.getActivity();
        //Log.d(TAG, "activity: " + activityContext);
        //Log.d(TAG, "activity.active: " + activityContext.active);
        //if (activity.active == true) {
        //  activity.finishAffinity();
        //  System.exit(0);
        //}
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setPackage(appName);
        sendIntent.putExtra("TABSINT_DATA_JSON_STRING", data);
        sendIntent.setType("text/plain");
        sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        sendIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activityContext.startActivity(sendIntent);
        try {
          JSONObject r = new JSONObject();
          r.put("success",true);
          r.put("message", "Receiver.sendDataExternal");
          r.put("data", data);
          callbackContext.success(r);
        } catch (JSONException e) {

        }
        return true;
      } else {
        try {
          JSONObject r = new JSONObject();
          r.put("success",false);
          r.put("message", "Could not send data to another app - no appName specified.  Name format is 'com.myorg.myapp'.");
          callbackContext.error(r);
        } catch (JSONException e) {

        }
        return false;
      }
    } else {
      try {
        JSONObject r = new JSONObject();
        r.put("success",false);
        r.put("message", "Could not send data to another app - Data must be a string.  If data is any other type, use JSON.stringify().");
        callbackContext.error(r);
      } catch (JSONException e) {

      }
      return false;
    }
  }


  /**
   * Send a JSON representation of the cordova intent back to the caller
   *
   * @param callbackContext
   */
  public boolean getExternalDataIntent (final CallbackContext callbackContext) {
      Intent intent = cordova.getActivity().getIntent();
      String action = intent.getAction();
      String type = intent.getType();
      if (Intent.ACTION_SEND.equals(action) && type != null) {
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getIntentJson(intent)));
        return true;
      } else {
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.NO_RESULT));
        return true;
      }
  }

  /**
   * Register handler for onNewIntent event
   *
   * @param data
   * @param context
   * @return
   */
  public boolean setExternalDataIntentHandler (final CallbackContext context) {
      this.onNewIntentCallbackContext = context;

      PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
      result.setKeepCallback(true);
      context.sendPluginResult(result);

      return true;
  }

  /**
   * Triggered on new intent
   *
   * @param intent
   */
  @Override
  public void onNewIntent(Intent intent) {
      PluginResult pluginResult;
      String action = intent.getAction();
      String type = intent.getType();
      if (this.onNewIntentCallbackContext != null) {
          if (Intent.ACTION_SEND.equals(action) && type != null) {
            pluginResult = new PluginResult(PluginResult.Status.OK, getIntentJson(intent));
            pluginResult.setKeepCallback(true);
            this.onNewIntentCallbackContext.sendPluginResult(pluginResult);
          } else {
            pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            this.onNewIntentCallbackContext.sendPluginResult(pluginResult);
          }
      }
  }


  /**
   * Return JSON representation of intent attributes
   *
   * @param intent
   * @return
   */
  private JSONObject getIntentJson(Intent intent) {
      JSONObject intentJSON = null;

      try {
          intentJSON = new JSONObject();

          intentJSON.put("type", intent.getType());
          intentJSON.put("data", intent.getStringExtra("TABSINT_DATA_JSON_STRING"));

          return intentJSON;
      } catch (JSONException e) {
          Log.d(TAG, TAG + " Error thrown during intent > JSON conversion");
          Log.d(TAG, e.getMessage());
          Log.d(TAG, Arrays.toString(e.getStackTrace()));

          return null;
      }
  }

}
