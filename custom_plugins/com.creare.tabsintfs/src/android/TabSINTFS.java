package com.creare.tabsintfs;

import android.app.Activity;
import android.database.Cursor;
import android.provider.DocumentsContract;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.content.ContentResolver;
import android.webkit.MimeTypeMap;
import android.provider.OpenableColumns;

import androidx.documentfile.provider.DocumentFile;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;

import java.util.UUID;
import java.util.ArrayList;
import java.util.List;

public class TabSINTFS extends CordovaPlugin {

    private static final String TAG = "TabSINTFSPlugin";

    /* Available actions for the plugin to execute */
    private static final class Action {
        public static final String INITIALIZE = "initialize";
        public static final String GET_EXT_STORAGE_URI = "getExtStorageUri";
        public static final String CHECK_URI_PERMISSION = "checkUriPermission";
        public static final String URI_EXISTS = "uriExists";
        public static final String EXISTS = "exists";
        public static final String CREATE_DIRECTORY = "createDirectory";
        public static final String CREATE_DIRECTORY_FROM_URI = "createDirectoryFromUri";
        public static final String CREATE_FILE = "createFile";
        public static final String WRITE_UUID_FILE = "writeUuidFile";
        public static final String READ_UUID_FILE = "readUuidFile";
        public static final String LIST_DOCUMENT_FILES = "listDocumentFiles";
        public static final String LIST_FILES = "listFiles";
        public static final String LIST_FILES_TREE = "listFilesTree";
        public static final String GET_NAME_FROM_URI = "getNameFromURI";
        public static final String COPY_EXTERNAL_STORAGE_FILES = "copyExtStorageFiles";
        public static final String DELETE_COPIED_INTERNAL_DIR = "deleteCopiedInternalDir";
        public static final String GET_DOCUMENT_FILE_NAME = "getDocumentFileName";
    };

    private static final int REQUEST_CODE_OPEN_DOCUMENT_TREE = 95; /* as far as I know, this value is arbitrary */
    private static final int ACTION_ERROR = -99;

    private CallbackContext callbackContext;

    public TabSINTFS() {
    }

    @Override
    public void pluginInitialize() {
        Log.d(TAG, "pluginInitialize");
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action          The action to execute.
     * @param args            JSONArry of arguments for the plugin.
     * @param callbackContext The callback id used when calling back into
     *                        JavaScript.
     * @return True if the action was valid, false if not.
     */
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        Log.d(TAG, "execute: " + action + " == " + args.toString());

        PluginResult result = null;
        String uriString = null;

        switch (action) {
            case Action.INITIALIZE:
                Log.d(TAG, "Executing action: " + Action.INITIALIZE + " with args: " + args.toString());
                this.pluginInitialize();
                callbackContext.success();
                break;
            case Action.GET_EXT_STORAGE_URI:
                Log.d(TAG, "Executing action: " + Action.GET_EXT_STORAGE_URI + " with args: " + args.toString());
                this.callbackContext = callbackContext;

                // this is async, so we set NO_RESULT and hold onto the callback
                result = new PluginResult(PluginResult.Status.NO_RESULT);
                result.setKeepCallback(true);
                callbackContext.sendPluginResult(result);

                this.getExtStorageUri(args.getString(0));
                break;
            case Action.CHECK_URI_PERMISSION:
                Log.d(TAG, "Executing action: " + Action.CHECK_URI_PERMISSION + " with args: " + args.toString());

                int permission = this.checkUriPermission(args.getString(0));

                if (permission == ACTION_ERROR) {
                    result = new PluginResult(PluginResult.Status.ERROR);
                    callbackContext.sendPluginResult(result);
                    return false;
                } else {
                    result = new PluginResult(PluginResult.Status.OK,
                            permission == PackageManager.PERMISSION_GRANTED ? true : false);
                    callbackContext.sendPluginResult(result);
                }
                break;
            case Action.URI_EXISTS:
                Log.d(TAG, "Executing action: " + Action.URI_EXISTS + " with args: " + args.toString());

                int exists = this.uriExists(args.getString(0));

                if (exists == ACTION_ERROR) {
                    result = new PluginResult(PluginResult.Status.ERROR);
                    callbackContext.sendPluginResult(result);
                    return false;
                } else {
                    result = new PluginResult(PluginResult.Status.OK,
                            exists == 1 ? true : false);
                    callbackContext.sendPluginResult(result);
                }
                break;
            case Action.EXISTS:
                Log.d(TAG, "Executing action: " + Action.EXISTS + " with args: " + args.toString());

                result = new PluginResult(PluginResult.Status.OK, this.exists(args.getString(0), args.getString(1)));
                callbackContext.sendPluginResult(result);
                break;
            case Action.CREATE_DIRECTORY:
                Log.d(TAG, "Executing action: " + Action.CREATE_DIRECTORY + " with args: " + args.toString());
                uriString = this.createDirectory(args.getString(0), args.getString(1));

                if (uriString == null) {
                    result = new PluginResult(PluginResult.Status.ERROR);
                    callbackContext.sendPluginResult(result);
                    return false;
                } else {
                    result = new PluginResult(PluginResult.Status.OK, uriString);
                    callbackContext.sendPluginResult(result);
                }
                break;
            case Action.CREATE_DIRECTORY_FROM_URI:
                Log.d(TAG, "Executing action: " + Action.CREATE_DIRECTORY_FROM_URI + " with args: " + args.toString());
                uriString = this.createDirectoryFromUri(args.getString(0));

                if (uriString == null) {
                    result = new PluginResult(PluginResult.Status.ERROR);
                    callbackContext.sendPluginResult(result);
                    return false;
                } else {
                    result = new PluginResult(PluginResult.Status.OK, uriString);
                    callbackContext.sendPluginResult(result);
                }
                break;
            case Action.CREATE_FILE:
                Log.d(TAG, "Executing action: " + Action.CREATE_FILE + " with args: " + args.toString());
                uriString = this.createFile(args.getString(0), args.getString(1), args.getString(2));

                if (uriString == null) {
                    result = new PluginResult(PluginResult.Status.ERROR);
                    callbackContext.sendPluginResult(result);
                    return false;
                } else {
                    result = new PluginResult(PluginResult.Status.OK, uriString);
                    callbackContext.sendPluginResult(result);
                }
                break;
            case Action.WRITE_UUID_FILE:
                Log.d(TAG, "Executing action: " + Action.WRITE_UUID_FILE + " with args: " + args.toString());

                result = new PluginResult(PluginResult.Status.OK, this.writeUuidFile(args.getString(0)));
                callbackContext.sendPluginResult(result);
                break;
            case Action.READ_UUID_FILE:
                Log.d(TAG, "Executing action: " + Action.READ_UUID_FILE + " with args: " + args.toString());

                result = new PluginResult(PluginResult.Status.OK, this.readUuidFile(args.getString(0)));
                callbackContext.sendPluginResult(result);
                break;
            case Action.LIST_DOCUMENT_FILES:
                Log.d(TAG, "Executing action: " + Action.LIST_DOCUMENT_FILES + " with args: " + args.toString());

                ArrayList<String> documentFilelist = this.listDocumentFiles(args.getString(0));

                if (documentFilelist == null) {
                    result = new PluginResult(PluginResult.Status.ERROR);
                    callbackContext.sendPluginResult(result);
                    return false;
                } else {

                    result = new PluginResult(PluginResult.Status.OK, new JSONArray(documentFilelist));
                    callbackContext.sendPluginResult(result);
                }

                break;
            case Action.LIST_FILES:
                Log.d(TAG, "Executing action: " + Action.LIST_FILES + " with args: " + args.toString());

                ArrayList<JSONObject> fileList = this.listFiles(args.getString(0));
                if (fileList == null) {
                    result = new PluginResult(PluginResult.Status.ERROR);
                    callbackContext.sendPluginResult(result);
                    return false;
                } else {
                    result = new PluginResult(PluginResult.Status.OK, new JSONArray(fileList));
                    callbackContext.sendPluginResult(result);
                }

                break;
            case Action.LIST_FILES_TREE:
                Log.d(TAG, "Executing action: " + Action.LIST_FILES_TREE + " with args: " + args.toString());

                ArrayList<JSONObject> fileListTree = this.listFilesTree(args.getString(0));
                if (fileListTree == null) {
                    result = new PluginResult(PluginResult.Status.ERROR);
                    callbackContext.sendPluginResult(result);
                    return false;
                } else {
                    result = new PluginResult(PluginResult.Status.OK, new JSONArray(fileListTree));
                    callbackContext.sendPluginResult(result);
                }

                break;
            case Action.GET_DOCUMENT_FILE_NAME:
                Log.d(TAG, "Executing action: " + Action.GET_DOCUMENT_FILE_NAME + " with args: " + args.toString());

                String documentFileName = this.getDocumentFileName(args.getString(0));
                if (documentFileName == null) {
                    result = new PluginResult(PluginResult.Status.ERROR);
                    callbackContext.sendPluginResult(result);
                    return false;
                } else {
                    result = new PluginResult(PluginResult.Status.OK, documentFileName);
                    callbackContext.sendPluginResult(result);
                }

                break;
            case Action.GET_NAME_FROM_URI:
                Log.d(TAG, "Executing action: " + Action.GET_NAME_FROM_URI + " with args: " + args.toString());
                result = new PluginResult(PluginResult.Status.OK, this.getNameFromURI(args.getString(0)));
                callbackContext.sendPluginResult(result);
                break;
            case Action.COPY_EXTERNAL_STORAGE_FILES:
                Log.d(TAG, "Executing action: " + Action.COPY_EXTERNAL_STORAGE_FILES + " with args: " + args.toString());
                result = new PluginResult(PluginResult.Status.OK, this.copyExtStorageFiles(args.getString(0),args.getString(1)));
                callbackContext.sendPluginResult(result);
                break;
            case Action.DELETE_COPIED_INTERNAL_DIR:
                Log.d(TAG, "Executing action: " + Action.DELETE_COPIED_INTERNAL_DIR + " with args: " + args.toString());
                result = new PluginResult(PluginResult.Status.OK, this.deleteCopiedInternalDir(args.getString(0),args.getString(1)));
                callbackContext.sendPluginResult(result);
                break;
            default:
                Log.d(TAG, "Invalid action with args: " + args.toString());
                result = new PluginResult(PluginResult.Status.INVALID_ACTION);
                callbackContext.sendPluginResult(result);
                return false;
        }

        return true;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        Log.d(TAG, "onActivityResult: " + Integer.toString(requestCode) + " ("
                + Integer.toString(resultCode) + ") " + resultData.toString());
        PluginResult result = null;
        if (resultCode == Activity.RESULT_OK && resultData != null) {
            Uri uri = resultData.getData();
            switch (requestCode) {
                case REQUEST_CODE_OPEN_DOCUMENT_TREE:
                    this.persistRWPermissions(uri, resultData.getFlags());
                    result = new PluginResult(PluginResult.Status.OK, uri.toString());
                    result.setKeepCallback(false);
                    this.callbackContext.sendPluginResult(result);
                    break;
                default:
                    Log.e(TAG, "onActivityResult: Unknown request code.");
                    result = new PluginResult(PluginResult.Status.INVALID_ACTION);
                    result.setKeepCallback(false);
                    this.callbackContext.sendPluginResult(result);
            }
        } else {
            Log.e(TAG, "onActivityResult: Activity cancelled or no URI returned.");
            result = new PluginResult(PluginResult.Status.ERROR);
            result.setKeepCallback(false);
            this.callbackContext.sendPluginResult(result);
        }

        this.callbackContext = null; // TODO: need to clean this up and not leave dirty context around
    }

    public void getExtStorageUri(String uriString) {
        Log.d(TAG, "getExtStorageUri: " + uriString);
        Uri uri = null;

        if (uriString != null) {
            uri = Uri.parse(uriString);
        }

        this.showFileChooser(uri);
    }

    public int checkUriPermission(String uriString) {
        Log.d(TAG, "checkUriPermission: " + uriString);

        int permission = -1;

        try {
            permission = cordova.getActivity().getApplicationContext().checkCallingOrSelfUriPermission(
                    Uri.parse(uriString),
                    Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        } catch (Exception e) {
            permission = ACTION_ERROR;
        }

        return permission;
    }

    public int uriExists(String uriString) {
        Log.d(TAG, "uriExists: " + uriString);
        int exists = -1;
        try {
            DocumentFile doc = DocumentFile.fromTreeUri(cordova.getActivity().getApplicationContext(),
                    Uri.parse(uriString));
            exists = doc.exists() ? 1 : 0;
        } catch (Exception e) {
            exists = ACTION_ERROR;
        }

        Log.d(TAG, "Uri exists: " + (exists == 1 ? "true" : "false"));
        return exists;
    }

    public String exists(String uriString, String name) {
        Log.d(TAG, "exists: " + uriString + " = " + name);

        DocumentFile root = DocumentFile.fromTreeUri(cordova.getActivity().getApplicationContext(),
                Uri.parse(uriString));

        DocumentFile target = root.findFile(name);

        if (target != null && target.exists()) {
            Log.d(TAG, "target exists");
            return target.getUri().toString();
        } else {
            Log.d(TAG, "target does not exist");
            return null;
        }
    }

    public String createDirectory(String uriString, String name) {
        Log.d(TAG, "createDirectory: " + uriString + " = " + name);

        DocumentFile root = DocumentFile.fromTreeUri(cordova.getActivity().getApplicationContext(),
                Uri.parse(uriString));

        DocumentFile newDir = root.createDirectory(name);

        if (newDir != null) {
            Log.d(TAG, "new dir created: " + newDir.getUri().toString());
            return newDir.getUri().toString();
        } else {
            Log.d(TAG, "no dir created :(");
            return null;
        }
    }

    public String createDirectoryFromUri(String uriString) {
        Log.d(TAG, "createDirectoryFromUri: " + uriString);

        DocumentFile target = DocumentFile.fromTreeUri(cordova.getActivity().getApplicationContext(),
                Uri.parse(uriString));

        Log.d(TAG, "target: " + target.getUri());
        Log.d(TAG, "target name: " + target.getName());

        DocumentFile parent = target.getParentFile();

        DocumentFile newDir = parent.createDirectory(target.getName());

        if (newDir != null) {
            Log.d(TAG, "new dir created: " + newDir.getUri().toString());
            return newDir.getUri().toString();
        } else {
            Log.d(TAG, "no dir created :(");
            return null;
        }
    }

    public String createFile(String uriString, String name, String mimeType) {
        Log.d(TAG, "createFile: " + uriString + " = " + name + "(" + mimeType + ")");

        DocumentFile root = DocumentFile.fromTreeUri(cordova.getActivity().getApplicationContext(),
                Uri.parse(uriString));

        DocumentFile newFile = root.createFile(mimeType, name);

        if (newFile != null) {
            Log.d(TAG, "new file created: " + newFile.getUri().toString());
            return newFile.getUri().toString();
        } else {
            Log.d(TAG, "no file created :(");
            return null;
        }
    }

    public String writeUuidFile(String uriString) {
        Log.d(TAG, "writeUuidFile: " + uriString);

        String uuid = UUID.randomUUID().toString();
        DocumentFile uuidFile = DocumentFile.fromTreeUri(cordova.getActivity().getApplicationContext(),
                Uri.parse(uriString));

        try {
            OutputStream outFile = cordova.getActivity().getContentResolver()
                    .openOutputStream(uuidFile.getUri());

            Writer w = new OutputStreamWriter(outFile, "UTF-8");
            w.write(uuid);
            w.close();
        } catch (Exception e) {
            Log.e(TAG, "Error writing uuid file: " + e);
            uuid = null;
        }

        return uuid;
    }

    public String readUuidFile(String uriString) {
        Log.d(TAG, "readUuidFile: " + uriString);

        String uuid = null;
        DocumentFile uuidFile = DocumentFile.fromTreeUri(cordova.getActivity().getApplicationContext(),
                Uri.parse(uriString));

        try {
            InputStream inFile = cordova.getActivity().getContentResolver()
                    .openInputStream(uuidFile.getUri());

            BufferedReader r = new BufferedReader(new InputStreamReader(inFile, "UTF-8"));
            uuid = r.readLine();
            r.close();
        } catch (Exception e) {
            Log.e(TAG, "Error reading uuid file: " + e);
        }

        return uuid;
    }

    public ArrayList<String> listDocumentFiles(String uriString) {
        ArrayList<String> fileList = new ArrayList<String>();

        try {
            DocumentFile dir = DocumentFile.fromTreeUri(cordova.getActivity().getApplicationContext(),
                    Uri.parse(uriString));

            for (DocumentFile file : dir.listFiles()) {
                fileList.add(file.getUri().toString());
            }
        } catch (Exception e) {
            fileList = null;
        }

        return fileList;
    }

    public ArrayList<JSONObject> listFiles(String uriString) {
        ArrayList<JSONObject> fileList = new ArrayList<JSONObject>();
        try {
            Uri dirUri = Uri.parse(uriString);
            Uri childrenUri = null;
            childrenUri = DocumentsContract.buildChildDocumentsUriUsingTree(dirUri,DocumentsContract.getDocumentId(dirUri));
            
            Cursor dirCursor = cordova.getActivity().getContentResolver().query(childrenUri,
                    new String[] { DocumentsContract.Document.COLUMN_DOCUMENT_ID,
                            DocumentsContract.Document.COLUMN_DISPLAY_NAME,
                            DocumentsContract.Document.COLUMN_MIME_TYPE },
                    null, null, null);

            if (dirCursor == null) {
                throw new Exception("Query cursor returned null.");
            }

            while (dirCursor.moveToNext()) {
                JSONObject childEntry = new JSONObject();
                childEntry.put(DocumentsContract.Document.COLUMN_DOCUMENT_ID,
                        dirCursor.getString(dirCursor.getColumnIndex(
                                DocumentsContract.Document.COLUMN_DOCUMENT_ID)));
                childEntry.put(DocumentsContract.Document.COLUMN_DISPLAY_NAME,
                        dirCursor.getString(dirCursor.getColumnIndex(
                                DocumentsContract.Document.COLUMN_DISPLAY_NAME)));
                childEntry.put(DocumentsContract.Document.COLUMN_MIME_TYPE,
                        dirCursor.getString(dirCursor.getColumnIndex(
                                DocumentsContract.Document.COLUMN_MIME_TYPE)));

                fileList.add(childEntry);
            }

        } catch (Exception e) {
            fileList = null;
        }
        return fileList;
    }

    public ArrayList<JSONObject> listFilesTree(String uriString) {
        ArrayList<JSONObject> fileList = new ArrayList<JSONObject>();
        try {
            Uri dirUri = Uri.parse(uriString);
            Uri childrenUri = null;
            childrenUri = DocumentsContract.buildChildDocumentsUriUsingTree(dirUri,DocumentsContract.getTreeDocumentId(dirUri));
            
            Cursor dirCursor = cordova.getActivity().getContentResolver().query(childrenUri,
                    new String[] { DocumentsContract.Document.COLUMN_DOCUMENT_ID,
                            DocumentsContract.Document.COLUMN_DISPLAY_NAME,
                            DocumentsContract.Document.COLUMN_MIME_TYPE },
                    null, null, null);

            if (dirCursor == null) {
                throw new Exception("Query cursor returned null.");
            }

            while (dirCursor.moveToNext()) {
                JSONObject childEntry = new JSONObject();
                childEntry.put(DocumentsContract.Document.COLUMN_DOCUMENT_ID,
                        dirCursor.getString(dirCursor.getColumnIndex(
                                DocumentsContract.Document.COLUMN_DOCUMENT_ID)));
                childEntry.put(DocumentsContract.Document.COLUMN_DISPLAY_NAME,
                        dirCursor.getString(dirCursor.getColumnIndex(
                                DocumentsContract.Document.COLUMN_DISPLAY_NAME)));
                childEntry.put(DocumentsContract.Document.COLUMN_MIME_TYPE,
                        dirCursor.getString(dirCursor.getColumnIndex(
                                DocumentsContract.Document.COLUMN_MIME_TYPE)));

                fileList.add(childEntry);
            }

        } catch (Exception e) {
            fileList = null;
        }
        return fileList;
    }

    public String getDocumentFileName(String uriString) {
        Log.d(TAG, "getDocumentFileName: " + uriString);
        String documentName = null;
        try {
            DocumentFile doc = DocumentFile.fromTreeUri(cordova.getActivity().getApplicationContext(),
                    Uri.parse(uriString));
            documentName = doc.getName();
        } catch (Exception e) {
            documentName = null;
        }
        return documentName;
    }

    private void showFileChooser(Uri initialUri) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);

        if (initialUri != null) {
            intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, initialUri);
        }

        cordova.setActivityResultCallback(this);
        cordova.getActivity().startActivityForResult(intent, REQUEST_CODE_OPEN_DOCUMENT_TREE);
    }

    private void persistRWPermissions(Uri uri, int flags) {
        cordova.getActivity().getContentResolver().takePersistableUriPermission(uri,
                flags & (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION));
    }

    private String getNameFromURI(String uriString) {
        Log.d(TAG, "getNameFromURI: " + uriString);
        Uri uri = Uri.parse(uriString);
        File file = new File(uri.getPath());
        String fileName = file.getName();
        Log.d(TAG, "fileName: " + fileName);
        return fileName;
    }

    public Boolean copyExtStorageFiles(String uriString, String dirName) {
        Log.d(TAG, "copyExtStorageFiles for URI: " + uriString);
        Uri uri = Uri.parse(uriString);
        final ContentResolver resolver = cordova.getActivity().getContentResolver();
        // create directory to copy files into
        File dir = new File(cordova.getActivity().getApplicationContext().getFilesDir().toString() + "/protocols/local/" + dirName);
        Log.d(TAG, "dir: " + dir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        // delete contents of the directory
        this.clearDir(dir);
        // create childrenUri to loop through all files and copy each one
        Uri childrenUri = DocumentsContract.buildChildDocumentsUriUsingTree(uri,DocumentsContract.getTreeDocumentId(uri));
        Log.d(TAG, "childrenUri: " + childrenUri);

        Cursor c = null;
        String[] documentFieldsRequested = new String[] {
                DocumentsContract.Document.COLUMN_DOCUMENT_ID,
                DocumentsContract.Document.COLUMN_DISPLAY_NAME,
                DocumentsContract.Document.COLUMN_MIME_TYPE
        };
        c = resolver.query(childrenUri, documentFieldsRequested, null, null, null);
        // save directories and add them to the stack
        List<String> srcList = new ArrayList<String>();
        List<String> dstList = new ArrayList<String>();
        while (c.moveToNext()) {
            final String documentId = c.getString(0);
            final String filename = c.getString(1);
            final String columnMimeType = c.getString(2);
            final boolean isDir = columnMimeType.equals(DocumentsContract.Document.MIME_TYPE_DIR);
            final Uri documentUri = DocumentsContract.buildDocumentUriUsingTree(uri, documentId);
            Log.d(TAG, "copyExtStorageFiles for documentURI: " + documentUri);

            try {

                if (!isDir) {
                    File copy_loc_file = new File(
                            cordova.getActivity().getApplicationContext().getFilesDir().toString()
                                    + "/protocols/local/" + dirName + "/" + filename);
                    if (!copy_loc_file.exists()) {
                        copy_loc_file.createNewFile();
                    }
                    Log.d(TAG, "copy_loc_file:" + copy_loc_file);
                    this.copyFileUsingStream(documentUri, copy_loc_file);
                } else if (isDir) {
                    File copy_loc_dir = new File(cordova.getActivity().getApplicationContext().getFilesDir().toString()
                            + "/protocols/local/" + dirName + "/" + filename);
                    Log.d(TAG, "copy_loc_dir:" + copy_loc_dir);
                    if (!copy_loc_dir.exists()) {
                        copy_loc_dir.mkdirs();
                    }
                    srcList.add(documentUri.toString());
                    dstList.add("/protocols/local/" + dirName + "/" + filename);
                }
                Log.d(TAG, "srcList: " + srcList);
                Log.d(TAG, "dstList: " + dstList);

            } catch (Exception e) {
                Log.e(TAG, "ERROR in copyExtStorageFiles: " + e.toString());
            } finally {
                Log.e(TAG, "finally inside loop in copyExtStorageFiles");
            }

        }
        c.close();
        // after cursor is closed we want to finish off directories in the stack
        for (int i = 0; i < srcList.size(); i++) {
            Log.d(TAG, "srcList.get(i): " + srcList.get(i));
            Log.d(TAG, "dstList.get(i): " + dstList.get(i));
            try {
                this.copyStackUsingStream(srcList.get(i), dstList.get(i));
            } catch (IOException e) {
                Log.e(TAG, "ERROR in copyStackUsingStream try/catch: " + e.toString());
            }
        }
        return true;
    }

    public Boolean deleteCopiedInternalDir(String uriString, String dirName) {
        Log.d(TAG, "Deleting Directory @ URI: " + uriString);
        Uri uri = Uri.parse(uriString);
        final ContentResolver resolver = cordova.getActivity().getContentResolver();
        // delete contents of the directory
        File dir = new File(cordova.getActivity().getApplicationContext().getFilesDir().toString() + "/protocols/local/" + dirName);
        this.clearDir(dir);
        // now, delete the directory
        dir.delete();
        Log.d(TAG, "Directory Deleted: " + uriString);
        return true;
    }

    private void copyFileUsingStream(Uri source, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(cordova.getActivity().getApplicationContext().getContentResolver()
                    .openFileDescriptor(source, "r").getFileDescriptor());
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }

    private void copyStackUsingStream(String dirURIString, String dstString) throws IOException {
        final ContentResolver resolver = cordova.getActivity().getContentResolver();

        Uri dirUri = Uri.parse(dirURIString);
        Log.d(TAG, "(copyStackUsingStream) dirUri: " + dirUri);

        String[] dstStringSplit = dstString.split("/");
        String prevDirName = dstString;
        String dirName = dstStringSplit[dstStringSplit.length - 1];
        Log.d(TAG, "(copyStackUsingStream) prevDirName: " + prevDirName);
        Log.d(TAG, "(copyStackUsingStream) dirName: " + dirName);

        // supposedly should be .getDocumentId instead of .getTreeDocumentId
        Uri childrenUri = DocumentsContract.buildChildDocumentsUriUsingTree(dirUri,
                DocumentsContract.getDocumentId(dirUri));
        Log.d(TAG, "(copyStackUsingStream) childrenUri: " + childrenUri);

        Cursor c = null;
        String[] documentFieldsRequested = new String[] {
                DocumentsContract.Document.COLUMN_DOCUMENT_ID,
                DocumentsContract.Document.COLUMN_DISPLAY_NAME,
                DocumentsContract.Document.COLUMN_MIME_TYPE
        };

        c = resolver.query(childrenUri, documentFieldsRequested, null, null, null);
        List<String> srcListStack = new ArrayList<String>();
        List<String> dstListStack = new ArrayList<String>();
        while (c.moveToNext()) {
            final String documentId = c.getString(0);
            final String filename = c.getString(1);
            final String columnMimeType = c.getString(2);
            final boolean isDir = columnMimeType.equals(DocumentsContract.Document.MIME_TYPE_DIR);
            final Uri documentUri = DocumentsContract.buildDocumentUriUsingTree(dirUri, documentId);
            Log.d(TAG, "(copyStackUsingStream) documentUri: " + documentUri);

            try {
                if (!isDir) {
                    File copy_loc_file = new File(
                            cordova.getActivity().getApplicationContext().getFilesDir().toString() + prevDirName + "/"
                                    + filename);
                    if (!copy_loc_file.exists()) {
                        copy_loc_file.createNewFile();
                    }
                    this.copyFileUsingStream(documentUri, copy_loc_file);
                } else if (isDir) {
                    File copy_loc_dir = new File(cordova.getActivity().getApplicationContext().getFilesDir().toString()
                            + prevDirName + "/" + filename);
                    if (!copy_loc_dir.exists()) {
                        copy_loc_dir.mkdirs();
                    }
                    srcListStack.add(documentUri.toString());
                    dstListStack.add(prevDirName + "/" + filename);
                }
            } catch (Exception e) {
                Log.e(TAG, "(copyStackUsingStream) ERROR in copyExtStorageFiles: " + e.toString());
            } finally {
                Log.e(TAG, "(copyStackUsingStream) finally inside loop in copyExtStorageFiles");
            }

        }
        c.close();
        for (int i = 0; i < srcListStack.size(); i++) {
            Log.d(TAG, "srcListStack.get(i): " + srcListStack.get(i));
            Log.d(TAG, "dstListStack.get(i): " + dstListStack.get(i));
            try {
                this.copyStackUsingStream(srcListStack.get(i), dstListStack.get(i));
            } catch (IOException e) {
                Log.e(TAG, "ERROR in copyStackUsingStream Stack try/catch: " + e.toString());
            }
        }
    }

    public void clearDir(File clr) {
        File[] files = clr.listFiles();
        if (files == null) {
            return;
        }
        for (File f : files) {
            if (f.isFile()) {
                f.delete();
                Log.e(TAG, "Deleted File: " + f.toString());
            }
            if (f.isDirectory()) {
                this.clearDir(f);
            }
        }
    }

}