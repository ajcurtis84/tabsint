# TabSINT File System Plugin


This plugin implements OS-dependent file system operations that may not be available via Cordova's javascript interface.

Android:
* Interfacing with Scoped Storage and the Storage Access Framework, for example,
  access to files outside of App local storage (i.e., Documents/)

## Adding Functionality
Adding functionality requires several changes:

* Create an "Action" tag in TabSINTFS.java
* Implement functionality as a public or private class function
* Add a `case` to the `switch` statement in `execute()`
* Remember to send a PluginResult:
```
result = new PluginResult(PluginResult.Status.OK, <data to return>);
callbackContext.sendPluginResult(result);
```

* In the case of an error condition, send an error and return false from `execute()`:
```
result = new PluginResult(PluginResult.Status.ERROR);
callbackContext.sendPluginResult(result);
return false;
```

* In the case of an asynchronous call, set a `NO_RESULT` and save the callback:
```
this.callbackContext = callbackContext;
result = new PluginResult(PluginResult.Status.NO_RESULT);
result.setKeepCallback(true);
callbackContext.sendPluginResult(result);
```

* Add the call signature to the javascript api file, `tabsintfs.js`