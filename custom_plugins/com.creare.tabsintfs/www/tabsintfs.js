var TabSINTFS = {
  initialize: function() {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs initialize.");
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "initialize");
    });
  },
  getExtStorageUri: function(uriString) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs getExtStorageUri: " + uriString);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "getExtStorageUri", [uriString]);
    });
  },
  checkUriPermission: function(uriString) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs checkUriPermission: " + uriString);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "checkUriPermission", [uriString]);
    });
  },
  uriExists: function(uriString) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs uriExists: " + uriString);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "uriExists", [uriString]);
    });
  },
  exists: function(uriString, name) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs exists: " + uriString + " / " + name);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "exists", [uriString, name]);
    });
  },
  createDirectory: function(uriString, name) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs createDirectory: " + uriString + " / " + name);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "createDirectory", [uriString, name]);
    });
  },
  createDirectoryFromUri: function(uriString) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs createDirectoryFromUri: " + uriString);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "createDirectoryFromUri", [uriString]);
    });
  },
  createFile: function(uriString, name, mimeType) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs createFile: " + uriString + " / " + name);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "createFile", [uriString, name, mimeType]);
    });
  },
  writeUuidFile: function(uriString) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs writeUuidFile: " + uriString);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "writeUuidFile", [uriString]);
    });
  },
  readUuidFile: function(uriString) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs readUuidFile: " + uriString);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "readUuidFile", [uriString]);
    });
  },
  listDocumentFiles: function(uriString) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs listDocumentFiles: " + uriString);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "listDocumentFiles", [uriString]);
    });
  },
  listFiles: function(uriString) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs listFiles: " + uriString);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "listFiles", [uriString]);
    });
  },
  listFilesTree: function(uriString) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs listFilesTree: " + uriString);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "listFilesTree", [uriString]);
    });
  },
  copyExtStorageFiles: function(uriString, dirName) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs copyExtStorageFiles with starting directory: " + uriString);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "copyExtStorageFiles", [uriString, dirName]);
    });
  },
  getNameFromURI: function(uriString) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs getNameFromURI with starting directory: " + uriString);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "getNameFromURI", [uriString]);
    });
  },
  deleteCopiedInternalDir: function(uriString, dirName) {
    console.log(
      "[tabsintfs.js] Cordova plugin tabsintfs deleteCopiedInternalDir with starting directory: " + uriString
    );
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "deleteCopiedInternalDir", [uriString, dirName]);
    });
  },
  getDocumentFileName: function(uriString) {
    console.log("[tabsintfs.js] Cordova plugin tabsintfs getDocumentFileName: " + uriString);
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, "TabSINTFS", "getDocumentFileName", [uriString]);
    });
  }
};
module.exports = TabSINTFS;
