/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */
var platform = require("cordova/platform");
var TabSINTNative = {
  initialize: function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "initialize", []);
  },
  resetAudio: function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "resetAudio", []);
  },
  setAudio: function(successCallback, errorCallback, volume) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "setAudio", [volume]);
  },
  getAudioVolume: function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "getAudioVolume", []);
  },
  sendExternalData: function(successCallback, errorCallback, externalAppName, data) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "sendExternalData", [externalAppName, data]);
  },
  getExternalDataIntent: function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "getExternalDataIntent");
  },
  setExternalDataIntentHandler: function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "setExternalDataIntentHandler");
  },
  registerUsbDeviceListener: function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "registerUsbDeviceListener");
  },
  unregisterUsbDeviceListener: function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "unregisterUsbDeviceListener");
  },
  scanFlicButton: function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "scanFlicButton");
  },
  disconnectFlicButton: function(successCallback, errorCallback, button) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "disconnectFlicButton", [button]);
  },
  getConnectedFlicButtons: function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "getConnectedFlicButtons");
  }
};
module.exports = TabSINTNative;
